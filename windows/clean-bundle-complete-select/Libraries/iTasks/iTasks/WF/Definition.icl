implementation module iTasks.WF.Definition

import StdEnv

from Data.Map import :: Map(..)
from Data.Set import :: Set
import qualified Data.Set as Set
import Data.Functor
from System.Time import :: Timestamp, :: Timespec
import Text, Text.GenJSON

import iTasks
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskIO

:: Task a =: Task` (Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld))

Task :: !(Event TaskEvalOpts *IWorld -> *(TaskResult a, *IWorld)) -> Task a
Task f = Task` f

apTask :: !(Task a) !Event TaskEvalOpts !*IWorld -> *(TaskResult a, *IWorld)
apTask (Task` f) event opts=:{TaskEvalOpts|taskId} iworld
	# (res,iworld) = f event opts iworld
	| event=:DestroyEvent || event=:ServerInterruptedEvent
		# iworld = clearEventsFor taskId iworld
		# iworld = clearTaskSDSRegistrations ('Set'.singleton taskId) iworld
		= (res, iworld)
	| otherwise
		= (res, iworld)

exception :: !e -> TaskException | TC, toString e
exception e = (dynamic e, toString e)

:: ExceptionList =: ExceptionList [TaskException]
instance toString ExceptionList
where
	toString (ExceptionList l) = join "\n" (map snd l)

derive JSONDecode Event, Set
derive JSONEncode Event, Set

instance Functor TaskValue
where
	fmap f (NoValue)		= NoValue
	fmap f (Value v s)		= Value (f v) s

//Task id

instance toString TaskId
where
	toString (TaskId topNo taskNo)		= join "-" [toString topNo,toString taskNo]

instance fromString TaskId
where
	fromString s = case split "-" s of
		[topNo,taskNo]	= TaskId (toInt topNo) (toInt taskNo)
		_				= TaskId 0 0

instance == TaskId
where
	(==) (TaskId a0 b0) (TaskId a1 b1) = a0 == a1 && b0 == b1

instance < TaskId
where
	(<) (TaskId a0 b0) (TaskId a1 b1) = if (a0 == a1) (b0 < b1) (a0 < a1)

class toInstanceNo t :: t -> InstanceNo
instance toInstanceNo InstanceNo where toInstanceNo no = no
instance toInstanceNo TaskId where toInstanceNo (TaskId no _) = no

derive gDefault TaskListFilter, TaskId

fullTaskListFilter :: TaskListFilter
fullTaskListFilter =
	{ TaskListFilter
	| onlyIndex                   = ?None
	, onlyTaskId                  = ?None
	, notTaskId                   = ?None
	, onlyAttribute               = ?None
	, onlySelf                    = False
	, includeValue                = False
	, includeTaskAttributes       = False
	, includeManagementAttributes = False
	, includeProgress             = False
	}

