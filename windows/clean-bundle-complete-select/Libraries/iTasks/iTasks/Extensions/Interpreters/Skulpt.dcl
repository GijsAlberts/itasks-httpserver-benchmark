definition module iTasks.Extensions.Interpreters.Skulpt
/**
* This module provides the skulpt in-browser python interpreter as an editor component.
*/

import iTasks

:: SkulptInput m
	= SkulptSetScript !String
	| SkulptSetState !m
	| SkulptRunScript

:: SkulptOutput m = 
	{ output :: ![String]
	, error  :: !?SkulptError
	, state  :: !SkulptInterpreterState
	, model  :: !m
	}

:: SkulptInterpreterState
	= Stopped
	| Running
	| Paused

:: SkulptError =
	{ line :: !Int
	, message :: !String
	}

//Very minimal FFI representation
:: SkulptValue
	= SkulptNone
	| SkulptBool !Bool
	| SkulptInt !Int
	| SkulptFloat !Real
	| SkulptString !String
	| SkulptList ![SkulptValue]
	| SkulptDict !(Map String SkulptValue)

/**
 * You can define a set of functions to provide an interface
 * between a value in the editor and the python script
 *
 * The function can either return a value (and an updated model) directly,
 * or return a function that will get the result from a future model.
 * In the latter case, the interpreter is paused. When the interpreter resumes
 * execution the function is used to produce the return value that the script continues with.
 */
:: SkulptFunction m :== [SkulptValue] m -> (Either SkulptValue (m -> SkulptValue),m)

derive class iTask SkulptInput, SkulptOutput, SkulptInterpreterState, SkulptError

skulptInterpreter :: (Map String (SkulptFunction m)) m -> Editor [SkulptInput m] (SkulptOutput m) | TC m & JSONEncode{|*|}, JSONDecode{|*|} m
