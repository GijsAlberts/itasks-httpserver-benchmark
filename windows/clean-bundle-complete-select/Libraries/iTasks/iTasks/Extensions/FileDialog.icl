implementation module iTasks.Extensions.FileDialog

import iTasks
import Data.Maybe, Data.Either, Data.Tuple, System.Directory, System.FilePath, System.File, Text, System.OS
import iTasks.SDS.Definition
import iTasks.Internal.SDS
import iTasks.UI.Layout
from iTasks.Internal.IWorld import :: IWorld{world}
from Data.Map import unions

derive class iTask FileInfo, Tm

editFilePath :: !String Action !(?FilePath) -> Task (?FilePath)
editFilePath title action initialPath
	=   (determineInitialDir initialPath
	>>- \(initDir,initFile) ->
		withShared (initDir,initFile)
		\sSelection ->
			((navigateUp sSelection <<@ ApplyLayout navigateUpLayout)
			 ||-
			 (chooseFromCurrentDirectory sSelection <<@ ApplyLayout fileListLayout)
			 ||-
			 editFilename sSelection
			) >>* [OnAction ActionCancel (always (return ?None))
				  ,OnAction action (ifValue (isJust o snd) (\(dir,?Just file) -> return (?Just (dir </> file))))
				  ]

		) <<@ InWindow <<@ Title title
where
	determineInitialDir :: (?FilePath) -> Task (FilePath, ?String)
	determineInitialDir ?None = accWorldError getCurrentDirectory snd @ (\d -> (d,?None))
 	determineInitialDir (?Just path)
		= accWorldError (canonicalizePath path) snd
		>>- \fullPath ->
			 accWorldError (getFileInfo fullPath) snd 
		@ \{FileInfo|directory} -> if directory (fullPath,?None) (takeDirectory fullPath,?Just (dropDirectory fullPath))

	navigateUp :: (Shared sds (FilePath,?String)) -> Task (FilePath, ?String) | RWShared sds
	navigateUp sSelection
   		= editSharedChoiceWithShared [ChooseFromDropdown fst] (ancestorDirectories sSelection) (selection sSelection)

	chooseFromCurrentDirectory :: (Shared sds (FilePath,?String))-> Task (FilePath, ?String) | RWShared sds
	chooseFromCurrentDirectory sSelection
		= editSharedChoiceWithShared [ChooseFromList view] (filesInCurDir sSelection) (selection sSelection)
	where
		view (path,?None) = "[DIR] " +++ dropDirectory path
		view (path,?Just filename) = "[FILE] " +++ filename

	selection sds = mapReadWrite (?Just, const) ?None sds
 
	editFilename :: (Shared sds (FilePath, ?String))  -> Task (FilePath,?String) | RWShared sds
	editFilename sSelection = updateSharedInformation [UpdateSharedAs snd (\(d,_) f -> (d,f))] sSelection

	fileListLayout = setUIAttributes (unions [sizeAttr FlexSize (ExactSize 200)/*, minWidthAttr (ExactBound 400)*/])
	navigateUpLayout = layoutSubUIs SelectChildren (setUIAttributes (widthAttr FlexSize))

ancestorDirectories :: (Shared sds (FilePath,?String))
	-> SDSLens () [(FilePath,?String)] (FilePath,?String) | RWShared sds
ancestorDirectories	sds = mapRead (ancestors o fst) sds
where
	ancestors "" = [("/",?None)]
	ancestors path = [(path,?None):ancestors (takeDirectory path)]

filesInCurDir :: (Shared sds (FilePath,?String)) -> SDSSequence () [(FilePath,?String)] () | RWShared sds
filesInCurDir selection 
	= sdsSequence "filesIn" id (\() (p,_) -> p) (\() _ = Right snd)
		(SDSWriteConst (\_ _ -> Ok ?None)) (SDSWriteConst (\_ _ -> Ok ?None)) selection directoryListingWithDir

//Files are listed as (<current dir>,?Just <file name>)
//Directories are listed as (<child dir>, ?None)

directoryListingWithDir :: SDSLens FilePath [(FilePath,?String)] ()
directoryListingWithDir = mapRead (map combine) directoryListingWithInfo
where
	combine (parentDir,filename,{FileInfo|directory})
		| directory = (parentDir </> filename, ?None)
		| otherwise = (parentDir, ?Just filename)

//UTIL
//Entries are: (Dir, Filename, Fileinfo)
directoryListingWithInfo :: SDSSource FilePath [(FilePath,String,FileInfo)] ()
directoryListingWithInfo = createReadOnlySDSError read
where
	read path iworld = case readDirectory path iworld of
		(Error (_,e),iworld) = (Error (exception (e +++ " : " +++ path)),iworld)
		(Ok files,iworld) = readInfo [(path,f) \\ f <- files | f <> "." && f <> ".."] iworld

	readInfo [] iworld = (Ok [],iworld)
	readInfo [(d,f):fs] iworld=:{IWorld|world} = case getFileInfo (d</>f) world of
		(Error (_,e),world) = (Error (exception (e +++ " : " +++ f)),{IWorld|iworld & world=world})
		(Ok i,world) = case readInfo fs {IWorld|iworld & world=world} of
			(Error e, iworld) = (Error e,iworld)
			(Ok fis, iworld) = (Ok [(d,f,i):fis],iworld)

isAbsolute :: FilePath -> Bool
isAbsolute path = IF_WINDOWS (indexOf ":\\" path == 1) (startsWith "/" path)

canonicalizePath :: FilePath *World -> *(!MaybeOSError FilePath,!*World) 
canonicalizePath path world 
	| isAbsolute path = (Ok path,world)
	| otherwise = case getCurrentDirectory world of
		(Ok curDir,world) = (Ok (curDir </> path),world)
		(Error e,world) = (Error e, world)

