implementation module iTasks.Internal.Test.Stubs

import iTasks.Internal.IWorld
import ABC.Interpreter
import System.Time
import StdEnv
import iTasks
from Data.Map import newMap

//TEST STUBS
toStubIWorld :: !*World -> *IWorld
toStubIWorld world
	# (opts, world) = defaultEngineOptions world
	= 
		{ IWorld
		| options = opts
		, clock = zero
		, clockDependencies = [|]
		, current =
			{ taskTime = 0
			, taskInstance = 0
			, sessionInstance = ?None
			, attachmentChain = []
			, nextTaskNo = 0
			}
		, random = []
		, symbols = {}
		, sdsNotifyRequests = newMap
		, sdsNotifyReqsByTask = newMap
		, memoryShares = newMap
		, readCache = newMap
		, writeCache = newMap
		, abcInterpreterEnv =
			{ pie_code_start     = -1
			, pie_symbols        = {}
			, pie_symbols_64     = {}
			, pie_sorted_symbols = {}
			, pie_host_symbols   = {}
			, pie_symbol_offset  = 0
			}
		, ioTasks = {done=[], todo=[]}
		, ioStates = newMap
		, signalHandlers = []
		, world = world
		, resources = []
		, onClient = False
		, shutdown = ?None
		}
	
fromStubIWorld :: !*IWorld -> *World
fromStubIWorld iworld=:{IWorld|world} = world
