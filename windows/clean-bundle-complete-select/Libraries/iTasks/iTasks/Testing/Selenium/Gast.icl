implementation module iTasks.Testing.Selenium.Gast

import StdEnv, StdOverloadedList

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import Control.Monad
from Text import class Text(concat), instance Text String

import Gast.Gen

import iTasks.Internal.Generic.Visualization
import iTasks.Testing.Selenium.Interface

withGast ::
	!TestId !Int !GenState !(a -> Bool)
	!(a -> JS TestState JSVal)
	-> JS TestState JSVal
	| ggen{|*|}, gText{|*|} a
withGast test n genState p testf =
	Foldl
		(\p (i,val) ->
			p `then` \_ ->
			testIsActive test >>= \is_active ->
			if is_active (testf val) (resolvePromise ()))
		(resolvePromise ())
		[!(i,val) \\ i <|- [!1..n] & val <|- Filter p values] `then` \_ ->
	testIsActive test >>= \is_active ->
	if is_active (ok test) (resolvePromise ())
where
	values = ggen{|*|} genState
