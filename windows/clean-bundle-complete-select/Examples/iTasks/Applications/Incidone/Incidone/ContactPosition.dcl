definition module Incidone.ContactPosition
//This module provides a type for representing geographic positions of contacts
import iTasks
import iTasks.Extensions.GIS.Leaflet

from Incidone.OP.Concepts import :: Contact, :: ContactGeo

:: ContactPosition
    = PositionDescription String (?(Real,Real))
    | PositionLatLng (Real,Real)

//Abstract maps
:: ContactMap =
    { perspective   :: ContactMapPerspective
    , layers        :: [ContactMapLayer]
    }

:: ContactMapPerspective =
    { center        :: (!Real,!Real)
    , zoom          :: !Int
    , cursor        :: !?(!Real,!Real)
    , bounds        :: !?ContactBounds
    }
:: ContactBounds    :== ((!Real,!Real),(!Real,!Real))

:: ContactMapLayer =
    { title         :: String
    , def           :: ContactMapLayerDefinition
    }

:: ContactMapLayerDefinition
    = CMTileLayer    !String //Url in the form "/path-to-tiles/{z}/{x}/{y}.png"
    | CMRegionsLayer ![ContactMapRegion]
    | CMMarkersLayer ![ContactMapMarker]

:: ContactMapRegion =
    { regionId      :: !String
    , title         :: !String
    , color         :: !String
    , points        :: ![ContactPosition]
    }

:: ContactMapMarker =
    { markerId      :: !String
    , title         :: !?String
    , position      :: !ContactPosition
    , type          :: !?ContactMapMarkerType
    , heading       :: !?Int
    , track         :: !?ContactTrack
    , selected      :: !Bool
    }

:: ContactMapMarkerType
    = CMAIS         //AIS contact
    | CMUnit        //Coastguard controlled unit
    | CMNeedsHelp   //Contact needing help
    | CMOther       //Other types of contacts

:: ContactTrack     = ContactTrack [(DateTime,Real,Real)]

withinBounds                :: ContactBounds ContactPosition -> Bool

latLng                      :: ContactPosition -> ?(Real,Real)

contactToMapMarker          :: Bool Bool Contact -> ContactMapMarker
contactGeoToMapMarker       :: Bool Bool ContactGeo -> ContactMapMarker

toLeafletMap                :: ContactMap -> LeafletMap
toLeafletPerspective        :: ContactMapPerspective -> LeafletPerspective

fromLeafletMap              :: ContactMap LeafletMap -> ContactMap
perspectiveFromLeafletMap   :: LeafletMap -> ContactMapPerspective

selectionFromLeafletMap     :: LeafletMap -> [LeafletObjectID]

//Standard layers available to use in all map views
standardPerspective         :: SimpleSDSLens ContactMapPerspective
standardMapLayers           :: SimpleSDSLens [ContactMapLayer]

derive JSONEncode ContactPosition, ContactMapPerspective
derive JSONDecode ContactPosition, ContactMapPerspective
derive gEditor ContactPosition, ContactMapPerspective
derive gText ContactPosition, ContactMapPerspective

derive gDefault ContactPosition, ContactMapPerspective
derive gEq ContactPosition, ContactMapPerspective

derive class iTask ContactMap, ContactMapLayer, ContactMapRegion, ContactMapMarker, ContactMapMarkerType, ContactTrack

