implementation module AsyncIO

import code from "cAsyncIO.", "hashtable.", library "mswsock", library "kernel32", library "winsock2", library "ucrtbase"

import StdDebug
import StdEnv
import Data.Maybe
import qualified Data.List as DL
import qualified Data.Map as DM
from Data.Func import $
from Data.Queue import :: Queue, dequeue, newQueue
from Data.Map import :: Map
from Data.Error import :: MaybeError (..), :: MaybeErrorString, isError, fromOk
from Data.GenHash import generic gHash
from iTasks.Internal.IWorld import
	:: IWorld{clients, listeners, pipes, sendMap, asyncIoFd, world, ioStates, processes, instances, lastOnDataFd}
	, :: IOStates, :: IOState (..), :: ConnectionId
from iTasks.Internal.SDS import read, write, directResult, :: AsyncRead (..), :: AsyncWrite (..), instance Readable SDSLens, instance Identifiable SDSLens, instance Writeable SDSLens, instance Modifiable SDSLens, instance Registrable SDSLens
from iTasks.Internal.Task import class Writeable, class Readable, class Identifiable,
	:: ConnectionTask (..), :: ConnectionHandlersIWorld (..),
    :: ConnectionTask` (..), :: ConnectionHandlersIWorld` (..)
from iTasks.SDS.Definition import :: SDSLens(..), :: TaskContext (..), :: SDSIdentity, :: SimpleSDSLens, :: SDSLensOptions
	, class RWShared, class Registrable, class Modifiable
from iTasks.SDS.Combinators.Common import sdsFocus
from iTasks.WF.Definition import :: TaskId (..), :: InstanceNo, :: TaskException, :: InstanceKey, :: TaskListFilter {onlyTaskId}
	, :: TaskNo, fullTaskListFilter,
	instance < TaskId, instance == TaskId
from iTasks.WF.Derives import generic gHash
from iTasks.UI.Definition import encodeUIChange
from iTasks.Internal.Util import >-=
from iTasks.Internal.TaskIO import queueRefresh, taskOutput, :: TaskOutput, :: TaskOutputMessage (..)
from iTasks.Internal.WebService import wsockTextMsg
from iTasks.WF.Tasks.IO import :: ExternalProcessHandlersIWorld(..), :: ExitCode (..)
from iTasks.UI.Definition import :: UIChange (..), :: UI (..), :: UIAttributeChange, :: UIChildChange, :: UIAttributes, :: UIType
from iTasks.Internal.TaskState import fullExtendedTaskListFilter, :: ExtendedTaskListFilter
	, :: TaskMeta {taskId, instanceKey}, taskListMetaData
from iTasks.Internal.TaskServer import sdsExceptionSocket
from System.OSError import :: MaybeOSError, :: OSError, :: OSErrorMessage, :: OSErrorCode
from System.OS import IF_WINDOWS
from System._Process import :: ProcessHandle (..), toPid
from System.Process import terminateProcess, checkProcess
from Text.GenJSON import :: JSONNode (..), instance toString JSONNode

listenerEvents = [InConnectionEvent]
pipeEvents = [ReadEventPipe, SendEventPipe, ReadAndSendEventPipe, DisconnectEventPipe]

instance == PipeTask where
	(==) p1 p2 = p1.fdStdIn == p2.fdStdIn

instance == IOEvent where
	(==) InConnectionEvent InConnectionEvent = True
	(==) OutConnectionEvent OutConnectionEvent = True
	(==) ReadEventSock ReadEventSock = True
	(==) ReadEventPipe ReadEventPipe = True
	(==) SendEventSock SendEventSock = True
	(==) SendEventPipe SendEventPipe = True
	(==) ReadAndSendEventSock ReadAndSendEventSock = True
	(==) ReadAndSendEventPipe ReadAndSendEventPipe = True
	(==) DisconnectEventSock DisconnectEventPipe = True
	(==) DisconnectEventPipe DisconnectEventPipe = True
	(==) _ _ = False

// TODO should be in System.Process but System.Process is likely going to be changed/located within asyncio. 
instance == ProcessHandle where
	(==) p1 p2 = (toPid p1) == (toPid p2)

instance < ProcessHandle where
	(<) p1 p2 = (toPid p1) < (toPid p2)

instance == FDType where 
  (==) Socket Socket = True 
  (==) Pipe Pipe = True 
  (==) _ _ = False

ioInitC :: !*env -> (!AsyncIOFD, !*env)
ioInitC _ = code {
	ccall ioInitC ":I:A"
}

ioGetEventsC :: !AsyncIOFD !TimeoutMS !{#Int} !{#Int} !*env -> (!Int, !*env)
ioGetEventsC _ _ fdList evKinds world = code {
	ccall ioGetEventsC "IIAA:I:A"
}

acceptC_ :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !FD), !*env)
acceptC_ mainFD listenFd world = code {
	ccall acceptC_ "II:VII:A"
}

windowsAcceptC :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !FD), !*env)
windowsAcceptC mainFD listenFd world = code {
	ccall windowsAcceptC "II:VII:A"
}

tcplistenC :: !AsyncIOFD !Port_ !*env -> (!(!ErrorCode, !FD),!*env)
tcplistenC _ _ world = code {
	ccall tcplistenC "II:VII:A"
}

connectC :: !AsyncIOFD !IPAddr !Port_ !*env -> (!(!ErrorCode, !FD), !*env)
connectC _ ip port world = code {
	ccall connectC "III:VII:A"
}

queueIOWriteSock :: !FD !String !*env -> (!ErrorCode, !*env)
queueIOWriteSock fd data world = queueWriteSockC fd (data) (size data) world
where
	queueWriteSockC :: !FD !String !Int !*env -> (!ErrorCode, !*env)
	queueWriteSockC fd data size world = code {
		ccall queueWriteSockC "IsI:I:A"
	}

queueIOWritePipe :: !FD !String !*env -> (!ErrorCode, !*env)
queueIOWritePipe fd data world = queueWritePipeC fd (data) (size data) world
where
	queueWritePipeC :: !FD !String !Int !*env -> (!ErrorCode, !*env)
	queueWritePipeC fd data size world = code {
		ccall queueWritePipeC "IsI:I:A"
	}

signalSendSockC :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)
signalSendSockC asyncIoFd socket world = code {
	ccall signalSendSockC "II:I:A"
}

getpeernameC :: !FD !FD !*env -> (!(!ErrorCode, !IPAddr), !*env)
getpeernameC clientFd listenFd world = code {
	ccall getpeernameC "II:II:A"
}

retrieveDataC :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !String), !*env)
retrieveDataC asyncIoFd fd world = code {
	ccall retrieveDataC "II:VIS:A"
}

windowsReadSockC :: !FD !*env -> (!ErrorCode, !*env)
windowsReadSockC socket world = code {
	ccall windowsReadSockC "I:VI:A"
}

windowsReadPipeC :: !FD !*env -> (!ErrorCode, !*env)
windowsReadPipeC pipe world = code {
	ccall windowsReadPipeC "I:VI:A"
}

ioMonitorPipeC :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)
ioMonitorPipeC asyncIoFd pipe world = code {
	ccall ioMonitorPipeC "II:I:A"
}

signalSendPipeC :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)
signalSendPipeC asyncIoFd socket world = code {
	ccall signalSendPipeC "II:I:A"
}

cleanupFdC :: !AsyncIOFD !FD !Bool !*env -> (!ErrorCode, !*env)
cleanupFdC asyncIoFd fd isASocket world = code {
	ccall cleanupFdC "III:I:A"
}

windowsCreatePipe :: !*env -> (!ErrorCode, !MaybeOSError (!ReadHandleFD, !WriteHandleFD), !*env)
windowsCreatePipe world
	# (err, pipeHandles, world) = windowsCreatePipeC world
	| err == -1 = abort "windowsCreatePipe: windowsCreatePipeC failed."
	= (err, Ok pipeHandles, world)
where
	windowsCreatePipeC :: !*env -> (!ErrorCode, !(!ReadHandleFD, !WriteHandleFD), !*env)
	windowsCreatePipeC world = code {
		ccall windowsCreatePipeC ":III:A"
	}

windowsSetPacketsToSendC :: !FD !Int !*env -> (!ErrorCode, !*env)
windowsSetPacketsToSendC fd numPackets world = code {
		ccall windowsSetPacketsToSendC "II:I:A"
	}

processIdle :: !Bool *IWorld -> *IWorld
processIdle useOldHttpServer iworld=:{asyncIoFd, pipes, clients}
	# iworld = if useOldHttpServer iworld (processTaskOutput iworld)
	= processSendMap iworld
where
	processSendMap iworld=:{sendMap} = 'DM'.foldrWithKey` (processSendQueue) iworld sendMap

	processSendQueue fd output iworld=:{clients, pipes}
		# mbClientTask = 'DM'.get fd clients
		| isNone mbClientTask
			# mbPipeTask = 'DM'.get fd pipes
			| isNone mbPipeTask = iworld
			# pipeTask = fromJust mbPipeTask
			= processPipe fd pipeTask iworld
		# clientTask = fromJust mbClientTask
		= processClient fd clientTask iworld

	// Function which verifies that the viewportKeys of client instances match.
	// Sends TaskOutput for instances to the clients.
	processTaskOutput iworld=:{instances, sendMap}
		# (mbr,iworld) = read taskOutput EmptyContext iworld
		| isError mbr = abort "sdsexception"
		# output = directResult (fromOk mbr)
		# (activeInstances, removedInstances, revokedInstances, iworld) = verifyInstances instances iworld
		# (messages, output) = dequeueOutput (map (\(no,fd,_) -> (no,fd)) activeInstances) output
		# messages
			=  [(i,fd,TORemoved) \\ (i,fd) <- removedInstances]
			++ [(i,fd,TORevoked) \\ (i,fd) <- revokedInstances]
			++ messages
		# sendMap = foldr (\(i,fd,out) sendMap -> 'DM'.alter (addToSendMap (wsockTextMsg o toString o jsonMessage $ (i,out))) fd sendMap) 
			sendMap messages
		# (mbw, iworld) = write output taskOutput EmptyContext {iworld & sendMap = sendMap}
		| isError mbw = abort "sdsexception"
	    = {iworld & instances = activeInstances}

	jsonMessage (instanceNo, output)
		= JSONArray [JSONInt 0,JSONString (type output),JSONObject [("instanceNo",JSONInt instanceNo):fields output]]
	where
		type (TOUIChange _) = "ui-change"
		type (TOSetCookie _ _ _) = "set-cookie"
		type (TORemoved) = "removed"
		type (TORevoked) = "revoked"
		type (TOException _) = "exception"

		fields (TOUIChange change) = [("change",encodeUIChange change)]
		fields (TOSetCookie name value maxttl) = [("name",JSONString name),("value",JSONString value),("max-age",maybe JSONNull JSONInt maxttl)]
		fields (TOException description) = [("description",JSONString description)]
		fields _ = []

	verifyInstances :: [(InstanceNo,FD,String)] *IWorld -> (![(InstanceNo,FD,String)],![(InstanceNo,FD)],![(InstanceNo,FD)],!*IWorld)
	verifyInstances instances iworld
		# tfilter = {TaskListFilter|fullTaskListFilter & onlyTaskId = ?Just [TaskId no 0 \\ (no,_,_) <- instances]}
		# focus = (TaskId 0 0, TaskId 0 0,tfilter,fullExtendedTaskListFilter)
		= case read (sdsFocus focus taskListMetaData) EmptyContext iworld of
			(Ok (ReadingDone (_,metas)), iworld)
				# metas = 'DM'.fromList [(no,m) \\ m=:{TaskMeta|taskId=TaskId no _} <- metas]
				# (active,removed,revoked) = foldr (verify metas) ([],[],[]) instances
				= (active,removed,revoked,iworld)
			(_,iworld) = ([], map (\(no,fd,_) -> (no,fd)) instances,[],iworld)
	where
		verify metas (instanceNo, fd, viewportKey) (active,removed,revoked) = case 'DM'.get instanceNo metas of
			(?Just {TaskMeta|instanceKey= ?Just key})
				= if (viewportKey == key) ([(instanceNo, fd, key):active],removed,revoked)
					(active,removed,[(instanceNo,fd):revoked])
			_
				= (active,[(instanceNo,fd):removed],revoked)

	dequeueOutput :: ![(InstanceNo,FD)] !(Map InstanceNo TaskOutput) -> (![(InstanceNo, FD, TaskOutputMessage)],!Map InstanceNo TaskOutput)
	dequeueOutput [] states = ([],states)
	dequeueOutput [(i,fd):is] states
		# (output,states) = dequeueOutput is states
		= case 'DM'.get i states of
			?Just out = ([(i,fd,c) \\ c <- toList out] ++ output,'DM'.put i newQueue states)
			?None     = (output,states)
	where
		toList q = case dequeue q of
			(?None,  q) = []
			(?Just x,q) = [x:toList q]

	processClient :: !FD !ClientTask *IWorld -> *IWorld
	processClient clientFd
		ct=:{cTaskId, cCt=(ConnectionTask` {ConnectionHandlersIWorld`|onTick, onDestroy} sds)}
			iworld=:{asyncIoFd, ioStates, clients, sendMap}
		# iworld=:{sendMap} = case 'DM'.get cTaskId ioStates of
			?Just (IOActive conStates)
			// If FD is being closed only send the data do not handle the onTick.
			| isFDBeingClosed clientFd sendMap
				= case 'DM'.get clientFd sendMap of
					?None = abort "processIdle: sendMap was none."
					(?Just (out, closed))
						# iworld=:{world} = sendData clientFd Socket out iworld
						# (err, world) = signalSendSockC (fromJust asyncIoFd) clientFd world
						| err == -1 = abort "processIdle: error in signalSendSockC."
						# clients =  'DM'.put clientFd {ClientTask|ct & closed = True} clients
						= {iworld & world = world, clients = clients}
			// Otherwise: handle the onTick.
			= case 'DM'.get clientFd conStates of
					?Just (conState, closed)
						# (mbr, iworld) = read sds EmptyContext iworld
						| isError mbr = sdsExceptionSocket mbr cTaskId ioStates clientFd iworld
						# r = directResult (fromOk mbr)
						// conState remains the same so it is not processed.
						# (mbConState, close, iworld) = onTick conState iworld
						| close
							= onCloseSocket clientFd [] iworld
						= iworld
					?None = iworld // abort ("processIdle: no client conState for connected client." +++ toString clientFd)
			?Just (IODestroyed conStates)
				= case 'DM'.get clientFd conStates of
					?Just (conState, closed)
						# (mbl, out, iworld) = onDestroy conState iworld
						= onCloseSocket clientFd out iworld
					_ = abort "processIdle"
			_ = abort "processIdle: No IOState for connected client."
			// And send the data.
		= case 'DM'.get clientFd sendMap of
			?None = iworld
			(?Just (out, closed))
				# iworld=:{world} = sendData clientFd Socket out {iworld & sendMap = sendMap}
				# (err, world) = signalSendSockC (fromJust asyncIoFd) clientFd world
				| err == -1 = abort "processIdleCBs: error in signalSendSockC."
				| closed
					# clients = 'DM'.put clientFd {ClientTask|ct & closed = True} clients
					= {iworld & world = world, clients = clients}
				= {iworld & world = world}


	processPipe :: FD PipeTask *IWorld -> *IWorld
	processPipe fd pt=:{fdStdIn,closed} iworld=:{asyncIoFd, pipes, sendMap}
		| fd <> fdStdIn = iworld
		| isFDBeingClosed fdStdIn sendMap
			# pipes = 'DM'.put fdStdIn {PipeTask|pt & closed = True} pipes
			# iworld = flushSendMap iworld
			= {iworld & pipes = pipes}
		= flushSendMap iworld
	where 
		flushSendMap iworld=:{sendMap}
			# mbSendMap = 'DM'.get fdStdIn sendMap
			| isNone mbSendMap = iworld
			# (out,_) = fromJust mbSendMap
			# iworld=:{world} = sendData fdStdIn Pipe out iworld
			# (err, world) = signalSendPipeC (fromJust asyncIoFd) fdStdIn world
			| err <> 0 = abort "processPipe: flushSendMap failed."
			= {iworld & world = world}

// This function processes the events that are returned by the I/O multiplexer.
processIO :: ![FDEvent] *IWorld -> *IWorld
processIO [] iworld = iworld
processIO [fdEv=:{fd,ev}:fdEvs] iworld=:{IWorld|ioStates, listeners, clients, sendMap}
= if ('DL'.elem ev pipeEvents) processPipeIO processSockIO
where
	processSockIO
		| not ('DM'.member fd clients) && not ('DL'.elem ev listenerEvents) = iworld
		# taskId = if ('DL'.elem ev listenerEvents) ('DM'.find fd listeners).lTaskId ('DM'.find fd clients).cTaskId
		= case 'DM'.get taskId ioStates of
			?Just (IODestroyed conStates)
				= abort "processIO: IODestroyed."
			?Just (IOException e) 
				= abort "processIO: IOException."
			_ // IOActive conStates/?None.
				# iworld = processEvent fdEv iworld
				= processIO fdEvs iworld
	processPipeIO
		# iworld = processEvent fdEv iworld
		= processIO fdEvs iworld

// This function is used to process individual I/O multiplexing events.
processEvent :: !FDEvent *IWorld -> *IWorld
processEvent {fd, ev=InConnectionEvent} iworld=:{IWorld|world, asyncIoFd, clients, listeners, ioStates, sendMap}
	# listenerTask=:{lTaskId, lCt=ConnectionTask` {ConnectionHandlersIWorld`|onConnect} sds, mbLastClient, removeOnClose}
		= 'DM'.find fd listeners
	// Accept connection.
	# ((err,clientFd),world) = acceptC_ (fromJust asyncIoFd) fd world
	| err == -1 = abort "processEvent: acceptC error."
	| err == -2 = {iworld & world = world} // Connection request was aborted, nothing is done.
	# client = case err of
			0 = clientFd
			1 = fromJust mbLastClient
	# ((err,ip),world) = getpeernameC client fd world
	| err == -1 = abort "processEvent: getpeernameC error."
	# iworld = {iworld & world = world}
	// handle onConnect.
	# (mbr, iworld) = read sds EmptyContext iworld
    | isError mbr = sdsExceptionSocket mbr lTaskId ioStates fd iworld
	# (mbl, mbw, out, close, iworld) = onConnect fd (toString o toDottedDecimal $ ip) (directResult (fromOk mbr)) iworld
	| isError mbl = abort "processEvent: error in conState of onConnect"
	# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
    | isError mbSdsErr = sdsExceptionSocket mbSdsErr lTaskId ioStates fd iworld
	# clientTask = {cTaskId = lTaskId, mbIpAddr = ?None, cCt = listenerTask.lCt, connected = True
                    , closed = False, removeOnClose = removeOnClose}
	# clients  = 'DM'.put client clientTask clients
	# listeners = 'DM'.put fd {listenerTask & mbLastClient = ?Just clientFd} listeners
	# ioState = 'DM'.find lTaskId ioStates
	# iworld=:{world} = case ioState of
		(IOActive conStates)
			# conStates = 'DM'.put client (fromOk mbl, False) conStates
			# ioStates = 'DM'.put lTaskId (IOActive conStates) ioStates
			= {iworld & clients = clients, listeners = listeners, ioStates = ioStates}
		_
			= abort "processEvent, no ioState for incoming connection request"
	| close
		= onCloseSocket clientFd out {iworld & clients = clients}
	# sendMap = 'DM'.alter (addToSendMap out) client sendMap
	// Initialize read on Windows
	# (err, world) = windowsReadSockC client world
	| err == -1 = abort "windowsReadSockC failed."
	= {iworld & world = world, sendMap = sendMap}
processEvent {fd, ev=OutConnectionEvent} iworld=:{IWorld|ioStates, asyncIoFd, clients, sendMap}
	# ct=:{cTaskId, mbIpAddr, cCt=(ConnectionTask` {ConnectionHandlersIWorld`|onConnect} sds)} = 'DM'.find fd clients
	# (mbr, iworld) = read sds EmptyContext iworld
    | isError mbr = sdsExceptionSocket mbr cTaskId ioStates fd iworld
	# (mbl, mbw, out, close, iworld) =
		onConnect fd (fromJust mbIpAddr) (directResult (fromOk mbr)) iworld
	| isError mbl = abort "processEvent: error in conState of onConnect."
	// ioState needs to be added even on close since the onDisconnect has to be called using the connection state.
	# ioStates = 'DM'.put cTaskId (IOActive ('DM'.put fd (fromOk mbl, False) 'DM'.newMap)) ioStates
	# clients = 'DM'.put fd {ct & connected=True} clients
	# iworld = {iworld & ioStates = ioStates, clients = clients}
	# (mbSdsErr, iworld=:{world, asyncIoFd}) = writeShareIfNeeded sds mbw iworld
    | isError mbSdsErr = sdsExceptionSocket mbSdsErr cTaskId ioStates fd iworld
	| close
		= onCloseSocket fd out iworld
	# sendMap = 'DM'.alter (addToSendMap out) fd sendMap
	# (err, world) = windowsReadSockC fd world
	| err == -1 = abort "processEvent: error in windowsReadSockC."
	= {iworld & ioStates = ioStates, world = world, sendMap = sendMap}
processEvent {fd, ev=ReadEventSock} iworld=:{world, asyncIoFd, ioStates, clients, sendMap}
	= readSockRcvBuf iworld
where
	/* On Linux/macOS this function reads until EWOULDBLOCK/EAGAIN is returned.
	   On Windows, looping is not required and an asynchronous read operation is initiated.
	*/
	readSockRcvBuf iworld=:{asyncIoFd, world, sendMap}
		| isFDBeingClosed fd sendMap = iworld
		# ((err, data), world) = IF_WINDOWS (windowsRead world) (retrieveDataC (fromJust asyncIoFd) fd world)
		# iworld = {iworld & world = world}
		| err == -1 = iworld
		| err == 1
			# iworld = processData data iworld
			// Looping should not be done on Windows.
			= (IF_WINDOWS iworld (readSockRcvBuf iworld))
		| err == 0
			# iworld = IF_WINDOWS (processData data iworld) iworld
			= cleanupSocket fd iworld
		= abort ("readSockRcvBuf failed with unrecognized error code " +++ toString err)
	windowsRead world
		# ((err, data), world) = retrieveDataC (fromJust asyncIoFd) fd world
		| err == -1 = abort "processEvent: retrieveDataC failed."
		# (err, world) = windowsReadSockC fd world
		= ((err, data), world)
	processData data iworld
		# ct=:{cTaskId, cCt=(ConnectionTask` {ConnectionHandlersIWorld`|onData} sds), closed} = 'DM'.find fd clients
		= case 'DM'.get cTaskId ioStates of
			?Just (IOActive conStates)
				= case 'DM'.get fd conStates of
					?Just (conState, closed)
						# (mbr, iworld) = read sds EmptyContext iworld
						| isError mbr = sdsExceptionSocket mbr cTaskId ioStates fd iworld
						# iworld = {iworld & lastOnDataFd = ?Just fd}
						# (mbl, mbw, out, close, iworld) = 
							onData data conState (directResult (fromOk mbr)) iworld
						# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
						| isError mbSdsErr = sdsExceptionSocket mbSdsErr cTaskId ioStates fd iworld
						| close
							=  onCloseSocket fd out iworld
						# sendMap = 'DM'.alter (addToSendMap out) fd sendMap
						# ioStates = 'DM'.put cTaskId (IOActive ('DM'.put fd (fromOk mbl, False) conStates)) ioStates
						= {iworld & sendMap = sendMap, ioStates = ioStates}
					_ = abort "Processevent: no connection state found."
			_ = abort "readSockRcvBuf: no active IOState."
processEvent {fd, ev=SendEventSock} iworld=:{clients}
	# clientTask = 'DM'.get fd clients
	| isNone clientTask = iworld
	# {ClientTask|closed} = fromJust clientTask
	| closed = trace_n "Callback returned close and data remaining to be sent has been sent." cleanupSocket fd iworld
	= iworld
processEvent {fd, ev=ReadAndSendEventSock} iworld
	# iworld = processEvent {FDEvent|fd = fd, ev = ReadEventSock} iworld
	= processEvent {fd=fd, ev=SendEventSock} iworld
processEvent {fd, ev=DisconnectEventSock} iworld
	# iworld = IF_WINDOWS iworld (processEvent {FDEvent|fd = fd, ev = ReadEventSock} iworld)
	= cleanupSocket fd iworld
processEvent {fd, ev=ReadAndSendEventPipe} iworld=:{world, asyncIoFd}
	# iworld = processEvent {FDEvent|fd = fd, ev = ReadEventPipe} iworld
	= processEvent {FDEvent|fd = fd, ev = SendEventPipe} iworld
processEvent {fd, ev=SendEventPipe} iworld=:{pipes}
	# pipeTask = 'DM'.get fd pipes
	| isNone pipeTask = iworld
	# {fdStdIn, fdStdOut, fdStdErr, closed, processHandle} = fromJust pipeTask
	| closed
		# iworld = cleanupPipe fdStdIn iworld
		# iworld = cleanupPipe fdStdOut iworld
		# iworld=:{world} = cleanupPipe fdStdErr iworld
		# (mbErr, world) =  terminateProcess processHandle world
		= {iworld & world = world}
	= iworld
processEvent {fd, ev=SendEventNop} iworld = iworld
processEvent {fd, ev=ReadEventPipe} iworld=:{asyncIoFd}
	= readPipeRcvBuf iworld
where 
	/*  On Linux/macOS this function reads until EWOULDBLOCK/EAGAIN is returned, which indicates the receive buffer of the fd is empty.
	 *  On Windows, looping is not required and an asynchronous read operation is initiated.
	 */
	readPipeRcvBuf iworld=:{world, sendMap, pipes}
		# mbPt = 'DM'.get fd pipes
		| isNone mbPt = iworld
		# pt=:{fdStdIn} = fromJust mbPt
		| isFDBeingClosed fdStdIn sendMap = iworld
		# ((err,data), world) = IF_WINDOWS (windowsRead world) (retrieveDataC (fromJust asyncIoFd) fd world)
		# iworld = {iworld & world = world}
		// Looping should not be done on Windows.
		| err > 0 
			# iworld = processData data iworld
			= IF_WINDOWS (iworld) (readPipeRcvBuf iworld)
		| err == 0 
			# iworld = IF_WINDOWS (processData data iworld) iworld
			= cleanupPipe fd iworld
		= iworld
	windowsRead world
		# ((err, data), world) = retrieveDataC (fromJust asyncIoFd) fd world
		| err == -1 = abort "processEvent: retrieveDataC failed."
		# (err, world) = windowsReadPipeC fd world
		= ((err, data), world)
	processData data iworld=:{pipes}
		# pt=:{sdsout, isLegacy, handlers, sdshandlers, fdStdIn, fdStdOut, fdStdErr}
			= trace ("Received data:\n" +++ data) 'DM'.find fd pipes
		| isLegacy // externalProcess or externalProcess` is used.
			| isNone sdsout = abort "processEvent: sdsout may not be none if legacy implementation is used."
			# sdsout = fromJust sdsout
			# (mbr, iworld) = read sdsout EmptyContext iworld
			| isError mbr = cleanupPipe fd iworld
			# (currSDSOut, currSDSErr)  = directResult (fromOk mbr)
			# (mbwErr, iworld) = case isErrData fdStdOut fdStdErr of
				True = write ((currSDSOut, currSDSErr ++ [data])) sdsout EmptyContext iworld
				False = write ((currSDSOut ++ [data], currSDSErr)) sdsout EmptyContext iworld
			| isError mbwErr = cleanupPipe fd iworld
			= iworld
		// externalProcessHandlers is used.
		| isNone sdshandlers = trace_n "processEvent: sdshandlers may not be none if handlers implementation is used." iworld
		# sdshandlers = fromJust sdshandlers
		# (mbr, iworld) = read sdshandlers EmptyContext iworld
		| isError mbr = trace_n "errmbr" onClosePipe pt [] iworld
		# (mbw, out, close, iworld) = case isErrData fdStdOut fdStdErr of
			True = (fromJust handlers).onErrData data (directResult (fromOk mbr)) iworld
			False = (fromJust handlers).onOutData data (directResult (fromOk mbr)) iworld
		# (mbSdsErr, iworld=:{sendMap}) = writeShareIfNeeded sdshandlers mbw iworld
		| isError mbSdsErr = onClosePipe pt out iworld
		| close = onClosePipe pt out iworld
		# sendMap = 'DM'.alter (addToSendMap out) fdStdIn sendMap
		= trace_n "DS" {iworld & sendMap = sendMap}
	isErrData fdStdOut fdStdErr = fd == fdStdErr && isPty fdStdOut fdStdErr
	isPty fdStdOut fdStdErr = fdStdOut <> fdStdErr
	
processEvent {fd, ev=DisconnectEventPipe} iworld
	// Read receive buffer until error on Linux to retrieve remaining data.
	# iworld =  IF_WINDOWS iworld (processEvent {FDEvent|fd = fd, ev = ReadEventPipe} iworld)
    = cleanupPipe fd iworld

sendData :: !FD !FDType ![String] *IWorld -> *IWorld
sendData fd Socket packets iworld=:{world}
	# (err, world) = windowsSetPacketsToSendC fd (length packets) world
	= processSend fd packets Socket queueIOWriteSock {iworld & world = world}
sendData fd Pipe packets iworld=:{world}
	# (err, world) = windowsSetPacketsToSendC fd (length packets) world
	= trace_n "PS" processSend fd packets Pipe queueIOWritePipe {iworld & world = world}

processSend :: !FD ![String] FDType (Int String *World -> (ErrorCode, *World)) *IWorld -> *IWorld
processSend fd [] _ _ iworld = iworld
// Empty packets are not sent.
processSend fd ["":packets] fdType sendFunc iworld = processSend fd packets fdType sendFunc iworld
processSend fd [packet:packets] fdType sendFunc iworld=:{world}
	# (err, world) = sendFunc fd packet world 
  // TODO pipe disconnectev
	| err == -1 
		# ev = if (fdType == Socket) DisconnectEventSock DisconnectEventPipe
		= processEvent {fd=fd, ev=ev} {iworld & world = world}
	= processSend fd packets fdType sendFunc {iworld & world = world}

writeShareIfNeeded :: !(sds () r w) !(?w) !*IWorld -> (!MaybeError TaskException (), !*IWorld) | TC r & TC w & Writeable sds
writeShareIfNeeded sds ?None iworld  = (Ok (), iworld)
writeShareIfNeeded sds (?Just w) iworld = case write w sds EmptyContext iworld of
	(Error e, iworld) = (Error e, iworld)
	(Ok WritingDone, iworld) = (Ok (), iworld)

addToSendMap :: ![String] !(?([String], !Bool)) -> (?([String], Bool))
addToSendMap [] x = x
addToSendMap toAdd ?None = ?Just (toAdd, False)
addToSendMap toAdd (?Just (existing, closed)) = ?Just (existing ++ toAdd, closed)

toEvents :: ![Int] -> [IOEvent]
toEvents evs = 'DL'.map toEvent evs
where 
	toEvent 0  = InConnectionEvent
	toEvent 1  = OutConnectionEvent
	toEvent 2  = ReadEventSock
	toEvent 3  = ReadEventPipe
	toEvent 4  = SendEventSock
	toEvent 5  = SendEventPipe
	toEvent 6  = ReadAndSendEventSock
	toEvent 7  = ReadAndSendEventPipe
	toEvent 8  = DisconnectEventSock
	toEvent 9  = DisconnectEventPipe
	toEvent 10 = SendEventNop

cleanupSocket :: !FD *IWorld -> *IWorld
cleanupSocket fd iworld=:{asyncIoFd, clients, ioStates}
	// It is possible that the socket may be attempted to be cleaned up multiple times on macos/linux.
	# alreadyCleanedUp = not ('DM'.member fd clients)
	| alreadyCleanedUp = trace_n "Already cleaned up" iworld
	# ct=:{cTaskId, cCt} = 'DM'.find fd clients
	# iworld = case trace_n ("Cleaned up disconnected socket " +++ toString fd +++ ".") 'DM'.get cTaskId ioStates of
		?Just (IOActive conStates)
			= case 'DM'.get fd conStates of
				?Just (conState, closed)
					# (conState, iworld) = handleOnDisconnect cCt conState iworld
					# conStates = 'DM'.put fd (conState, True) conStates
					# ioStates = 'DM'.put cTaskId (IOActive conStates) ioStates
					= {iworld & ioStates = ioStates}

				?None = abort "cleanupSocket: no conState found."
		?Just (IODestroyed conStates)
			= case 'DM'.get fd conStates of
				?Just (conState, closed)
					# (conState, iworld) = handleOnDisconnect cCt conState iworld
					# conStates = 'DM'.put fd (conState, True) conStates
					# ioStates = 'DM'.put cTaskId (IODestroyed conStates) ioStates
					= {iworld & ioStates = ioStates}
				?None = abort "cleanupSocket: no conState found."
		?Just (IOException e)
			= abort "cleanupSocket: IOException"
		?None 
			= abort "cleanupSocket: no ioState found."
	#! iworld = deleteConnectionState ct iworld
	#! iworld=:{world} = deleteClient iworld
	#! (err, world) = cleanupFdC (fromJust asyncIoFd) fd True world
	| err <> 0 = abort "cleanupSocketC failed."
	= {iworld & world = world}
where 
	deleteConnectionState :: ClientTask *IWorld -> *IWorld
	deleteConnectionState ct=:{cTaskId, removeOnClose} iworld=:{world, clients, ioStates}
		# ioState = 'DM'.find cTaskId ioStates
		// Delete connection state
		= case ioState of
			(IOActive conStates)
				# conStates = if removeOnClose ('DM'.del fd conStates) (conStates)
				# ioStates = 'DM'.put cTaskId (IOActive conStates) ioStates
				= {iworld & ioStates = ioStates}
			(IODestroyed conStates)
				# conStates = if removeOnClose ('DM'.del fd conStates) (conStates)
				# ioStates = 'DM'.put cTaskId (IODestroyed conStates) ioStates
				= {iworld & ioStates = ioStates}
			(IOException e)
				= abort "IOException in ioState for client."
	deleteClient :: *IWorld -> *IWorld
	deleteClient iworld=:{clients}
		# clients = 'DM'.del fd clients
		= {iworld & clients = clients}
	handleOnDisconnect :: ConnectionTask` Dynamic *IWorld -> (Dynamic, *IWorld)
	handleOnDisconnect cCt=:(ConnectionTask` {ConnectionHandlersIWorld`|onDisconnect} sds) conState iworld
		# (mbr, iworld) = read sds EmptyContext iworld
		| isError mbr = abort "SDSException"
		# (mbl, mbw, iworld) =  onDisconnect conState (directResult (fromOk mbr)) iworld
		# (mbSdsErr, iworld) = writeShareIfNeeded sds mbw iworld
		| isError mbSdsErr = abort "SDSException"
		= (fromOk mbl, iworld)

cleanupPipe :: FD *IWorld -> *IWorld
cleanupPipe fd iworld=:{asyncIoFd, pipes, world}
	# alreadyCleanedUp = not ('DM'.member fd pipes)
	| alreadyCleanedUp = trace_n "Already cleaned up" iworld
	# (err, world) = cleanupFdC (fromJust asyncIoFd) fd False world
	| err == -1 = abort "cleanupPipeC failed."
	# pipes = 'DM'.del fd pipes
	= trace_n ("Cleaned up disconnected pipe " +++ toString fd +++ ".") {iworld & world = world, pipes = pipes}

checkProcesses :: *IWorld -> *IWorld
checkProcesses iworld=:{processes, world}
	# (err, iworld) =
		'DM'.foldrWithKey`
		(\ph (taskId, mbPt, _) (mbErr, iworld=:{processes}) ->
				 (
				 	checkProcessIWorld ph mbPt taskId >-=
					\mbExit iworld -> case mbExit of
						// Still running.
						?None = (mbErr, iworld)
						// Process exited, call onExit and remove process from IWorld.
						?Just exit
							# processes = 'DM'.alter (addExitCode exit) ph processes
							# iworld = {iworld & processes = processes}
							# iworld = queueRefresh taskId iworld
							= (mbErr, iworld)
				) iworld
		)
		(Ok (), iworld) processes
	= iworld

addExitCode :: Int (?(TaskId, ?PipeTask, ?Int)) -> ?(TaskId, ?PipeTask, ?Int)
addExitCode exit ?None = ?None
addExitCode exit (?Just (taskId, mbPt, mbExit)) = ?Just (taskId, mbPt, ?Just exit)

checkProcessIWorld :: !ProcessHandle (?PipeTask) TaskId *IWorld -> (!MaybeOSError (?Int), *IWorld)
checkProcessIWorld ph mbPt taskId iworld=:{world, processes}
	# (mbErr, world) = checkProcess ph world
	# iworld = {iworld & world = world}
	| isError mbErr 
		# processes = 'DM'.alter (addExitCode 0) ph processes
		# iworld = queueRefresh taskId iworld 
		= (mbErr, {iworld & processes = processes})
	= (mbErr, iworld)

onCloseSocket :: !FD ![String] *IWorld -> *IWorld
onCloseSocket fd out iworld=:{asyncIoFd, sendMap}
	# outSendMap = 'DM'.get fd sendMap
	# sendMap = 'DM'.put fd ([], True) sendMap
	| out == [] && isNone outSendMap
			= trace_n "Callback returned close = True, no data to send"
				cleanupSocket fd {iworld & sendMap = sendMap}
	# out = trace_n "Callback returned close = True, there is data to send" append outSendMap out
	# iworld=:{world} = sendData fd Socket out iworld
	# (err, world) = signalSendSockC (fromJust asyncIoFd) fd world
	| err <> 0 = abort "signalSendSockC failed."
	= {iworld & sendMap = sendMap, world = world}

append :: (?([String], Bool)) [String] -> [String]
append (?Just (prevOut, closed)) out = prevOut ++ out
append ?None out = out

onClosePipe :: !PipeTask ![String] *IWorld -> *IWorld
onClosePipe pt=:{fdStdIn, fdStdOut, fdStdErr} out iworld=:{sendMap}
	# outSendMap = 'DM'.get fdStdIn sendMap
	# sendMap = 'DM'.put fdStdIn ([], True) sendMap
	| out == [] && isNone outSendMap
		# iworld = cleanupPipe fdStdIn {iworld & sendMap = sendMap}
		# iworld = cleanupPipe fdStdOut iworld
		= cleanupPipe fdStdErr iworld
	# out = append outSendMap out
	# iworld=:{world, asyncIoFd} = sendData fdStdIn Pipe out iworld
	# (err, world) = signalSendPipeC (fromJust asyncIoFd) fdStdIn world
	| err <> 0 = abort "signalSendSockC failed."
	= {iworld & sendMap = sendMap, world = world}

isFDBeingClosed :: !FD !(Map FD ([String], Bool)) -> Bool
isFDBeingClosed fd sendMap = case 'DM'.get fd sendMap of
	?None = False
	?Just(_, closed) = closed

// Brought this to AsyncIO since it is part of the TCP library.
toDottedDecimal :: !Int -> String
toDottedDecimal ip
	=	(toString ((ip>>24) bitand 255))	+++"."+++
		(toString ((ip>>16) bitand 255))	+++"."+++
		(toString ((ip>> 8) bitand 255))	+++"."+++
		(toString ( ip      bitand 255))
