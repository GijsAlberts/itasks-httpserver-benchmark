#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#include <mswsock.h>
#include <stdlib.h> 
#include <errno.h>
#include <string.h>
#include <windows.h>
#include <handleapi.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <processthreadsapi.h>
#include <errhandlingapi.h>
#include <fileapi.h>
#include <winbase.h>
#include "Clean.h"
#include "cAsyncIO.h"
#include "hashtable.h"
#include "Basetsd.h"
#pragma comment(lib, "ws2_32.lib")

// Maximum number of file descriptors that will be monitored by the I/O multiplexing mechanism.
#define MAX_SUPPORTED_FDS 2500

// Events returned to Clean. These use the Clean type naming convention to emphasize that these events are returned to Clean.
#define InConnectionEvent 0
#define OutConnectionEvent 1
#define ReadEventSock 2
#define ReadEventPipe 3
#define SendEventSock 4 
#define SendEventPipe 5
#define ReadAndSendEventSock 6
#define ReadAndSendEventPipe 7
#define DisconnectEventSock 8
#define DisconnectEventPipe 9
// Used to indicate that sending an individual packet completed on Windows, however the Linux/macOS way of processing is emulated by only returning a proper SendEvent once all the packets were succesfully sent.
// This is done to emulate the old close behavior for callbacks while performing the send non-blocking/without a local select call.
#define SendEventNop 10 

int rcv_buf_size;
hashtable_t* ht;
DWORD bytes_received;
// Used to generate unique names for the named pipe. Alongside the process id, this should guarantee the pipes to be uniquely named.
// IOCP is only supported on named pipes, not anonymous pipes.
unsigned int uniqid_pipe;

typedef struct io_data_s {
    SOCKET fd; 
    int op;
    // Used for storing addr. TODO probably can be 4 bytes.
    char buffer[32];
    // Overlapped is a field to allow io_data_t to be retrieved in ioGetEventsC through the overlapped struct, using CONTAINING_RECORD. 
    OVERLAPPED overlapped;
} io_data_t;

int ioInitC() {
	// Request winsock2.2.
	WSADATA wsaData;
	int err = WSAStartup(MAKEWORD(2,2), &wsaData);

	if (err) {
		printf("WSAStartup failed: %d\n", err);
		return -1;
	}

	// Init rcv_buf_size.
	int s; 
	s = socket (PF_INET, SOCK_STREAM, 0);
	int size = sizeof(rcv_buf_size);
	getsockopt (s, SOL_SOCKET, SO_RCVBUF,  (char*) &rcv_buf_size, &size);
	closesocket(s);

	// Init hashtable for storing send/receivebuffers.
	ht = initHT(2500);

	HANDLE iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE,NULL,0,0);
	if (iocp == NULL) {
		return -1;
	}
	return PtrToInt(iocp); 
}

// -1 = not ok, 0 = ok.
int ioMonitorFd (int main_fd, int fd, int op) {
	HANDLE iocp = CreateIoCompletionPort((HANDLE) IntToPtr(fd), (HANDLE) IntToPtr(main_fd), fd, 1);
	if(iocp == NULL) {
		printf("Err in ioMonitorFd %d\n", WSAGetLastError());
		return -1;
	}
	return 0;
} 

int ioGetEventsC (int main_fd, int timeout, CleanIntArray pFdList, CleanIntArray pEvKinds) {
	unsigned int bytes;
	ULONG numEvents; 
	int size = CleanIntArraySize(pFdList);
	OVERLAPPED_ENTRY ovls [10240];
	if(GetQueuedCompletionStatusEx((HANDLE) IntToPtr(main_fd), ovls, 10240, &numEvents, timeout, FALSE)) {
		for (int i = 0; i < numEvents; i++) {
			// Retrieve io_data_t struct from OVERLAPPED struct passed to async operation (acceptEx, ...).
			io_data_t* io_data = (io_data_t*) CONTAINING_RECORD(ovls[i].lpOverlapped, io_data_t, overlapped);
			pFdList[i] = io_data->fd;
			pEvKinds[i] = io_data->op;
			printf("Received completion packet for op %d.\n", io_data->op);
			printf("Status: %lld\n", ovls[i].lpOverlapped->Internal);

			int disconnect_ev = io_data->op == ReadEventPipe || io_data->op == SendEventPipe ? DisconnectEventPipe : DisconnectEventSock;
			
			if (io_data->op == SendEventSock || io_data->op == SendEventPipe)  {
				char key[5];
				int err = fdToFdStr(io_data->fd, key);
				if (err) {
					return -1;	
				}
				table_entry_t* entry = getHT(ht, key);
				if (entry == NULL) {
					return -1;
				}
				if (entry->packets_to_send <= 0) {
					printf("Must be a packet to send when performing WSASend so this should never occur.\n");
					return -1;
				}
				
				entry->packets_to_send--;
				
				if (entry->packets_to_send != 0) {
					pEvKinds[i] = SendEventNop;
				}
			}

			// If WSARecv()/ReadFile completed, the length of the CleanString receive buffer passed back to Clean 
			// upon the next call to retrieveDataC
			// equals the amount of bytes transferred. 0 bytes having been transferred indicates a disconnect.
			if (io_data->op == ReadEventSock || io_data->op == ReadEventPipe) {
				char key[5];
				int err = fdToFdStr(io_data->fd, key);
				if (err) {
					return -1;	
				}
				table_entry_t* entry = getHT(ht, key);
				if (entry == NULL) {
					return -1;
				}
				printf("bt: %d socket: %d\n", ovls[i].dwNumberOfBytesTransferred, (int)io_data->fd);
				// Graceful disconnect.
				if(ovls[i].dwNumberOfBytesTransferred == 0) {
					pEvKinds[i] = disconnect_ev;    
				}
				CleanStringLength(entry->rcv_buf_clean) = ovls[i].dwNumberOfBytesTransferred;
			}
			free(io_data);
		}
		return numEvents;
	} 

	if (GetLastError() != WAIT_TIMEOUT) {
		return GetLastError();
	}
	return 0;
}

io_data_t* initIOData(int fd, int op) {
	io_data_t *io_data = (io_data_t*) malloc(sizeof(io_data_t));
	if (io_data == NULL) {
		return NULL;
	}
	io_data->fd = fd;
	io_data->op = op;
	memset(&io_data->overlapped, 0, sizeof(OVERLAPPED));    
	return io_data;
}

void acceptC_ (int main_fd, int listen_fd, int *p_err_code, int *p_client_fd) {
	//Function pointer for AcceptEx() function.
	LPFN_ACCEPTEX acceptEx = NULL;
	GUID guid_acceptex = WSAID_ACCEPTEX;

	SOCKET client_fd = INVALID_SOCKET;
	DWORD idc; 

	// Load AcceptEx function into memory (this is the recommended way to use the AcceptEx function). 
	int err = WSAIoctl(
			(SOCKET) listen_fd, 
			SIO_GET_EXTENSION_FUNCTION_POINTER,
			&guid_acceptex,
			sizeof(guid_acceptex),
			&acceptEx,
			sizeof(acceptEx),
			&idc,
			NULL, 
			NULL
			);

	if (err == SOCKET_ERROR) {
		
		*p_err_code = -1;
		return;
	}

	// Create socket.
	client_fd = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (client_fd == INVALID_SOCKET) {
		printf("wsasock %d\n", WSAGetLastError());
		*p_err_code = -1;
		return;
	}
	*p_client_fd = (int) client_fd;

	// Initialize buffers and add the buffers to a hashtable which holds the buffers of the client socket.
	printf("Allocating buffers for client fd %d\n", (int) client_fd);
	err = initFdBuffers((int) client_fd);
	if (err) {
		*p_err_code = -1; 
		return;
	}

	// Information passed to ioGetEventsC which retrieves/uses io_data_t once the connection is accepted.
	io_data_t *io_data = initIOData(listen_fd, InConnectionEvent);
	if (io_data == NULL)  {
		*p_err_code = -1;
		return;
	}

	BOOL ok = acceptEx(
			(SOCKET) listen_fd,
			client_fd,
			&(io_data->buffer),
			0, 
			sizeof(SOCKADDR_IN) + 16, 
			sizeof(SOCKADDR_IN) + 16,
			&idc, 
			&(io_data->overlapped)
			);
	if (WSAGetLastError() != WSA_IO_PENDING && !ok) {
	    printf("acceptex %d\n", WSAGetLastError());
		*p_err_code = -1;
		return; 
	}
	printf("Accepting connection on client fd %d\n", (int) client_fd);
	err = ioMonitorFd(main_fd, client_fd, 0);
	if (err) {
		*p_err_code = -1;
		return;
	}
	*p_err_code = 1;
}

void windowsAcceptC (int main_fd, int listen_fd, int *p_err_code, int *p_client_fd) {
	acceptC_(main_fd, listen_fd, p_err_code, p_client_fd);
}

void tcplistenC(int main_fd, int port, int *p_err_code, int *p_listen_fd) {
	SOCKET      listen_fd;
	SOCKADDR_IN srv_adr;

	listen_fd = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (listen_fd==INVALID_SOCKET)
		return;

	srv_adr.sin_family = AF_INET;            
	srv_adr.sin_addr.s_addr = INADDR_ANY;    
	srv_adr.sin_port = htons((short int)port);

	*p_err_code = bind(listen_fd, (LPSOCKADDR) &srv_adr, sizeof(srv_adr));
	if (*p_err_code) {
		closesocket(listen_fd);
		return;
	};

	*p_err_code = listen (listen_fd,128);
	if (*p_err_code) {
		closesocket(listen_fd);
		return;
	};

	*p_err_code = ioMonitorFd(main_fd, listen_fd, 0);
	if (*p_err_code) {
		closesocket(listen_fd); 
		return;
	}

	*p_listen_fd = (int) listen_fd;
}

void connectC (int main_fd, int ip_addr, int port, int *p_err_code, int *p_client_fd) {
	SOCKET client_fd;
	SOCKADDR_IN srv_adr,client_adr;

	*p_err_code = -1;

	client_fd = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (client_fd==INVALID_SOCKET)
		return;

	client_adr.sin_family = AF_INET;         
	client_adr.sin_addr.s_addr = INADDR_ANY; 
	client_adr.sin_port = 0;         

	int err = bind(client_fd, (LPSOCKADDR) &client_adr, sizeof(client_adr));
	if (err){
		closesocket(client_fd);
		return;
	}

	srv_adr.sin_family = AF_INET;            
	srv_adr.sin_addr.s_addr = htonl(ip_addr);
	srv_adr.sin_port = htons((short int)port);   

	LPFN_CONNECTEX connectEx = NULL;
	GUID guid_connectex = WSAID_CONNECTEX;

	DWORD idc; 

	// Load ConnectEx function into memory (this is the recommended way to use the ConnectEx function). 
	err = WSAIoctl(
			(SOCKET) client_fd, 
			SIO_GET_EXTENSION_FUNCTION_POINTER,
			&guid_connectex,
			sizeof(guid_connectex),
			&connectEx,
			sizeof(connectEx),
			&idc,
			NULL, 
			NULL
			);

	if (err == SOCKET_ERROR) {
		*p_err_code = -1;
		return;
	}

	// Information passed to ioGetEventsC which retrieves the completion packet once the connection is accepted.
	io_data_t *io_data = initIOData(client_fd, OutConnectionEvent);
	if(io_data == NULL) {
		*p_err_code = -1;
		return;
	}

	err = ioMonitorFd(main_fd, client_fd, 0);
	if (err) {
		*p_err_code = -1;
		return;
	}

	BOOL ok = connectEx(client_fd, (SOCKADDR*) &srv_adr, sizeof(srv_adr), NULL, 0, NULL, &(io_data->overlapped));

	if (!ok && WSAGetLastError() != WSA_IO_PENDING) {
		*p_err_code = -1;
		return; 
	}
	err = initFdBuffers((int) client_fd);
	if (err) {
		*p_err_code = -1; 
		return;
	}
	*p_client_fd = (int) client_fd;
	*p_err_code = 0;
}


// Initiates asynchronous send operation on Windows, sending the data.
int queueWriteSockC(int fd, char* p_send_data, int size) {
	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		return -1;
	}

	io_data_t *io_data = initIOData(fd, SendEventSock);
	if (io_data == NULL) {
		return -1;
	}

	// WSABUF may be stack allocated, it is copied by WSASend.
	WSABUF send_buf;
	send_buf.buf = p_send_data;
	send_buf.len = size;

	if (SOCKET_ERROR == WSASend((SOCKET) fd, &send_buf, 1, NULL, 0, &io_data->overlapped, NULL)) {
		if(WSAGetLastError() != WSA_IO_PENDING) {
			printf("WSAGetLastError(): %d\n", WSAGetLastError());
			return -1;          
		}
	}
	return 0;
}

// Initiates asynchronous write operation, sending the data.
int queueWritePipeC(int fd, char* p_send_data, int size) {
	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		return -1;
	}

	io_data_t *io_data = initIOData(fd, SendEventPipe);
	if(io_data == NULL) {
		return -1;
	}

	BOOL success = WriteFile(IntToPtr(fd), p_send_data, size, NULL, &io_data->overlapped);
	if (!success) {
		// ERROR_NO_DATA indicates that the pipe is being closed.
		if(GetLastError() != ERROR_IO_PENDING && GetLastError() != ERROR_NO_DATA) {
			printf("cAsyncio.c, queuePipeWriteC failed, GetLastError(): %d.\n", GetLastError());
			return -1;          
		}
	}
 printf("%d\n", GetLastError());
	return 0;
}

// Should do nothing on Windows.
int signalSendSockC(int main_fd, int fd) {
	return 0;
}

// Should do nothing on Windows.
int signalSendPipeC(int main_fd, int fd) {
	return 0;
}

int getpeernameC(int client_fd, int listen_fd, int* p_host) {
	SOCKADDR_IN addr = {0};
	int addr_len = sizeof(addr);
	SOCKET listen_sock = (SOCKET) listen_fd;
	int err = setsockopt((SOCKET) client_fd, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, (char*) &listen_sock, sizeof(listen_sock));
	if(err == SOCKET_ERROR) {
		printf("setsockopt err: %d\n", WSAGetLastError());
		return -1;
	}

	if (SOCKET_ERROR == getpeername((SOCKET) client_fd, (SOCKADDR*) &addr, &addr_len)) {
		printf("getpeername err: %d\n", WSAGetLastError());
		return -1; 
	}
	*p_host = ntohl(addr.sin_addr.s_addr);
	return err;
}

void retrieveDataC(int main_fd, int fd, int *p_err_code, CleanString *p_received) {
	*p_err_code = -1;
	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		return;
	}

	table_entry_t* entry = getHT(ht, key);
	if (entry == NULL) {
		return;
	}

	// Return buffer from previous windowsReadSockC call.
	// Clean makes a copy of this String on the heap once this function returns.
	// The length of the buffer gets set to the correct number of bytes through ioGetEventsC using the amount of bytes transferred when the read operation completes.
	*p_received = entry->rcv_buf_clean;
	*p_err_code = 0;

}

// Initiates asynchronous read operation.
void windowsReadSockC(int fd, int *p_err_code) {
	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		*p_err_code = -1;
		return;
	}

	table_entry_t* entry = getHT(ht, key);
	if (entry == NULL) {
		*p_err_code = -1;
		return;
	}

	// This buffer will be overwritten with the data that is received through the next WSARecv call.
	// WSABUF rcv_buf may be stack allocated, the buffer contained within (entry->rcv_buf_clean) must live until completion.
	WSABUF rcv_buf; 
	rcv_buf.buf = CleanStringCharacters(entry->rcv_buf_clean);
	rcv_buf.len = rcv_buf_size;

	io_data_t *io_data = initIOData(fd, ReadEventSock);
	if(io_data == NULL) {
		*p_err_code = -1;
		return;
	}

	DWORD flags = 0;
	// The lpNumberOfBytesRecvd parameter may return erronous results, so it is set to NULL (see MSDN for WSARecv).
	// The overlapped struct will contain the actual number of bytes transferred after completion.
	// The length of the CleanString rcv_buf_clean is set to the proper length in ioGetEventsC.
	// When the received CleanString is then returned upon the next call to retrieveDataC it will have the proper length.
	if (SOCKET_ERROR == WSARecv((SOCKET) fd, &rcv_buf, 1, NULL, &flags, &io_data->overlapped, NULL)) {
		int err = WSAGetLastError();
		// Connection reset by peer (non-graceful disconnect)
		if (err == WSAECONNRESET || err == WSAETIMEDOUT || err == WSAECONNABORTED) {
			*p_err_code = 0;
			return;
		}
		if (WSAGetLastError() != WSA_IO_PENDING) {
			printf("readSockC failed, WSAGetLastError(): %d\n", WSAGetLastError());
			*p_err_code = -1;
			return;
		}
	}
	*p_err_code = 1;
}

void windowsReadPipeC (int fd, int* p_err_code) {
	*p_err_code = -1;

	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		return;
	}

	table_entry_t* entry = getHT(ht, key);
	if (entry == NULL) {
		return;
	}

	io_data_t *io_data = initIOData(fd, ReadEventPipe);
	if (io_data == NULL) {
		return;
	}

	printf("Performing readfile on fd %d.\n", fd);
	BOOL syncSuccess = ReadFile(IntToPtr(fd), CleanStringCharacters(entry->rcv_buf_clean), rcv_buf_size, NULL, &io_data->overlapped);
	if (!syncSuccess) {
		// Pipe has been closed.
		if(GetLastError() == ERROR_BROKEN_PIPE) {
			*p_err_code = 0;      
			return;
		}
		if(GetLastError() != ERROR_IO_PENDING)  {
			printf("Error during readfile, errno: %d\n", GetLastError());  
			return;
		}
	}
	*p_err_code = 1;
}

int cleanupFdC(int main_fd, int fd, int isASocket){
	char key[5];   
	int err = fdToFdStr(fd, key);
	if (err) {
		return -1;	
	}

	// Frees buffers/tableentry for fd. 
	err = removeHT(ht, key);
	if (err) {
		return -1; 
	}
	
	if (isASocket) {
		err = closesocket((SOCKET) fd);
		if (err) {
			printf("cAsyncIO.c, closesocket failed.\n");
			return -1;      
		}
	} else {
		int success = CloseHandle(IntToPtr(fd));
		if (!success) {
			printf("cAsyncIO.c, CloseHandle failed.\n");
			return -1;      
		}
	}
	return 0;
}

int fdStrToFd(char* p_fd_str) {
	int fd; 
	sscanf(p_fd_str, "%d", &fd);
	return fd;
}

int fdToFdStr(int fd, char* p_fd_str) {
	int err = sprintf(p_fd_str, "%d", fd);
	if (err < 0) {
		printf("cAsyncIO.c, sprintf failed.\n");
		return -1; 
	}
	return 0;
}

int initFdBuffers(int client_fd) {
	char key[5];   
	int err = fdToFdStr(client_fd, key);
	if (err) {
		return -1;	
	}

	table_entry_t *entry = (table_entry_t*) malloc(sizeof(table_entry_t)+129);
	if (entry == NULL) {
		return -1; 
	}

	entry->rcv_buf_clean = (CleanString) malloc(rcv_buf_size + sizeof(long));
	if (entry->rcv_buf_clean == NULL) {
		return -1; 
	}
	CleanStringLength(entry->rcv_buf_clean) = rcv_buf_size;

	entry->packets_to_send = 0;

	return putHT(ht, key, entry);
}

int ioMonitorPipeC(int main_fd, int pipe_fd) {
	int err = initFdBuffers(pipe_fd);
	if (err) {
		return -1; 
	}
	return ioMonitorFd(main_fd, pipe_fd, 0);
}

// Monitoring pipes with IOCP is only possible for named pipes, not for anonymous pipes according to MSDN (https://docs.microsoft.com/en-us/windows/win32/ipc/anonymous-pipe-operations)
// Therefore, a named pipe is created and nothing is done with the name. On the same page, MSDN states that anonymous pipes are implemented as named pipes with a unique name.
// This function also creates a named pipe with a unique name while at the same time it allows to use IOCP operations (e.g ReadFile w overlapped), unlike the anonymous pipes created through CreatePipe.
int windowsCreatePipeC(int* p_read_handle, int* p_write_handle) {
	HANDLE write_handle; 
	HANDLE read_handle; 
	CHAR name_buffer[100];

	// Write unique name for pipe into name_buffer.
	sprintf(name_buffer, "\\\\.\\Pipe\\CleanPipe.%08x.%08x", GetCurrentProcessId(), uniqid_pipe);
	uniqid_pipe++;

	// Default security attributes, inheritance by child process is allowed.
	SECURITY_ATTRIBUTES* sec_attr = (SECURITY_ATTRIBUTES*) malloc(sizeof(SECURITY_ATTRIBUTES));
	sec_attr->nLength = sizeof(SECURITY_ATTRIBUTES);
	sec_attr->lpSecurityDescriptor = NULL;
	sec_attr->bInheritHandle = TRUE;

	read_handle = CreateNamedPipeA
			(name_buffer, 
			 PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED | FILE_FLAG_FIRST_PIPE_INSTANCE,
			 PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS,
			 1,
			 0,
			 0,
			 0,
			 sec_attr
			);

	if (read_handle == INVALID_HANDLE_VALUE) {
		return -1;  
	}

	write_handle = CreateFileA(name_buffer, GENERIC_READ | GENERIC_WRITE, 0, sec_attr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL);

	if (write_handle == INVALID_HANDLE_VALUE) {
		CloseHandle(read_handle);
		return -1;  
	}

	*p_read_handle = PtrToInt(read_handle);
	*p_write_handle = PtrToInt(write_handle);
	return 0;
}

int windowsSetPacketsToSendC(int fd, int num_packets) {
	char key[5];
	int err = fdToFdStr(fd, key);
	table_entry_t* entry = getHT(ht, key);
	if (entry == NULL) {
		return -1;
	}

	entry->packets_to_send = entry->packets_to_send += num_packets;
	return putHT(ht, key, entry);
}

