#ifndef __CHT__
#define __CHT__

#include <winsock2.h>
#include <windows.h>
#include "Clean.h"

typedef struct table_entry_s table_entry_t;

typedef struct table_entry_s {
    // The internal buffer of the receivebuffer provided to WSARecv is the buffer (CleanStringCharacters) of the CleanString below. 
    // The received data thus gets loaded into rcv_buf_clean, this is done to return a CleanString to Clean.
    CleanString rcv_buf_clean; 
    int packets_to_send;
    table_entry_t* next_entry;
    char key[]; 
} table_entry_t;

typedef struct hashtable_s {
    int size;
    table_entry_t** table;
} hashtable_t;

hashtable_t* initHT(int size);

table_entry_t* getHT(hashtable_t* ht, char* key);

int removeHT(hashtable_t* ht, char* key);

int putHT(hashtable_t* ht, char* key, table_entry_t* entry);

int hash(char* key);
#endif
