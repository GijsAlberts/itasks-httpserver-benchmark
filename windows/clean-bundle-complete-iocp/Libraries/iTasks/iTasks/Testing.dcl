definition module iTasks.Testing

from Testing.TestEvents import :: EndEventType

:: TestReport :== [(String,EndEventType)]

//* Test if all tests have passed.
allPassed :: TestReport -> Bool

//* Check if no tests have failed (skipped and passed).
noneFailed :: TestReport -> Bool
