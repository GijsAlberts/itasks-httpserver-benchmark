definition module iTasks.Extensions.GIS.Leaflet

import iTasks
from Text.HTML import :: SVGElt
from Data.Set import :: Set

/**
* @param create editor for viewing
*/
leafletEditor :: !Bool -> Editor LeafletMap LeafletMap

/*
 * Customization of editors
 *
 * @param map options
 * @param handlers
 * @param initial value
 * @result editor
 */
customLeafletEditor :: !MapOptions !(LeafletEventHandlers s) s -> Editor (LeafletMap, s) (LeafletMap, s) | iTask s

:: MapOptions =
	{ attributionControl :: !Bool
	, zoomControl        :: !Bool
	, editable           :: !Bool
	, viewOnly           :: !Bool
	}

:: LeafletMap =
	{ perspective :: !LeafletPerspective //* How the map should decide what should be in view
	, bounds      :: !?LeafletBounds     //* The actual bounds of the map (updated by the client; writes are ignored)
	, center      :: !?LeafletLatLng     //* The actual center of the map (updated by the client; writes are ignored)
	, zoom        :: !?Int               //* The actual zoom level of the map (updated by the client; writes are ignored)
	, tilesUrls   :: ![TileLayer]
	, objects     :: ![LeafletObject]    //* Markers, lines and polygon
	, icons       :: ![LeafletIcon]      //* Custom icons used by markers. They are referenced using their 'iconId' string.
	}

/**
 * This type describes how the application prefers the map to be drawn.
 *
 * When the perspective is `FitToBounds`, a view is automatically computed such
 * that a given region or set of objects is visible.
 *
 * `CenterAndZoom` simply sets the center coordinate and zoom level.
 *
 * When the user drags or zooms the map, the perspective of a `LeafletMap` is
 * reset to `CenterAndZoom`. This prevents applications from automatically
 * moving the view after the user has changed it. It is of course possible to
 * programmatically change it back to `FitToBounds`.
 */
:: LeafletPerspective
	= CenterAndZoom !LeafletLatLng !Int
	| FitToBounds !FitToBoundsOptions !FitToBoundsRegion

:: FitToBoundsOptions =
	{ padding :: !(!Int, !Int) //* The horizontal and vertical padding in pixels
	, maxZoom :: !Int          //* The maximum zoom level
	}

:: FitToBoundsRegion
	= SpecificRegion !LeafletBounds          //* Fits the view to the specified bounds.
	| SelectedObjects !(Set LeafletObjectID) //* Fits the view s.t. the given object IDs are visible.
	| AllObjects                             //* Fits the view s.t. all `objects` are visible.

:: TileLayer = {url :: !String, attribution :: !?HtmlTag}

:: LeafletIconID =: LeafletIconID String
:: LeafletIcon =
    { iconId        :: !LeafletIconID
    , iconUrl       :: !String
    , iconSize      :: !(!Int,!Int)
    }

:: LeafletLatLng =
    { lat :: !Real
    , lng :: !Real
    }

:: LeafletBounds =
    { southWest :: !LeafletLatLng
    , northEast :: !LeafletLatLng
    }

:: LeafletObject
    = Marker    !LeafletMarker
    | Polyline  !LeafletPolyline
    | Polygon   !LeafletPolygon
    | Circle    !LeafletCircle
    | Rectangle !LeafletRectangle
    | Window    !LeafletWindow

leafletObjectIdOf :: !LeafletObject -> LeafletObjectID
leafletPointsOf :: !LeafletObject -> [LeafletLatLng]
leafletBoundingRectangleOf :: ![LeafletObject] -> LeafletBounds

:: LeafletObjectID =: LeafletObjectID String
:: LeafletMarker =
    { markerId      :: !LeafletObjectID
    , position      :: !LeafletLatLng
    , title         :: !?String
    , icon          :: !?LeafletIconID //* Reference to an icon defined for this map.
    , popup         :: !?HtmlTag
    }

:: LeafletPolyline =
    { polylineId    :: !LeafletObjectID
    , points        :: ![LeafletLatLng]
    , style         :: ![LeafletStyleDef LeafletLineStyle]
    , editable      :: !Bool
    }

:: LeafletPolygon =
    { polygonId     :: !LeafletObjectID
    , points        :: ![LeafletLatLng]
    , style         :: ![LeafletStyleDef LeafletAreaStyle]
    , editable      :: !Bool
    }

:: LeafletCircle =
    { circleId :: !LeafletObjectID
    , center   :: !LeafletLatLng
    , radius   :: !Real            //* the radius (in meters)
    , style    :: ![LeafletStyleDef LeafletAreaStyle]
    , editable :: !Bool
    }

:: LeafletRectangle =
    { rectangleId   :: !LeafletObjectID
    , bounds        :: !LeafletBounds
    , style         :: ![LeafletStyleDef LeafletAreaStyle]
    , editable      :: !Bool
    }

:: LeafletWindow =
    { windowId       :: !LeafletObjectID
    , initPosition   :: !LeafletWindowPos
    , title          :: !String
    , content        :: !HtmlTag
    , relatedMarkers :: ![(LeafletObjectID, [LeafletStyleDef LeafletLineStyle])] // connecting lines are drawn between the window and the markers
                                                                                 // to visualise the relation
    }

:: LeafletLineStyle    = LineStrokeColor !String // html/css color definition
                       | LineStrokeWidth !Int
                       | LineOpacity     !Real   // between 0.0 and 1.0
                       | LineDashArray   !String // a list of comma separated lengths of alternating dashes and gaps (e.g. "1,5,2,5")

:: LeafletAreaStyle = AreaLineStrokeColor !String // html/css color definition
                    | AreaLineStrokeWidth !Int
                    | AreaLineOpacity     !Real   // between 0.0 and 1.0
                    | AreaLineDashArray   !String // a list of comma separated lengths of alternating dashes and gaps (e.g. "1,5,2,5")
                    | AreaNoFill                  // inside of polygone is not filled, all other fill options are ignored
                    | AreaFillColor       !String // html/css color definition
                    | AreaFillOpacity     !Real

:: CSSClass =: CSSClass String
:: LeafletStyleDef style = Style style
                         | Class CSSClass


:: LeafletWindowPos = { x :: !Int, y :: !Int }

//Event handlers allow the customization of the map editor behaviour
:: LeafletEventHandlers s :== [LeafletEventHandler s]

:: LeafletEventHandler s
	= OnMapClick    (LeafletLatLng (LeafletMap,s) -> (LeafletMap,s))
	| OnMapDblClick (LeafletLatLng (LeafletMap,s) -> (LeafletMap,s))
	| OnMarkerClick (LeafletObjectID (LeafletMap,s) -> (LeafletMap,s))
	| OnHtmlEvent   (String (LeafletMap,s) -> (LeafletMap,s))

//A minimal state for tracking a set of selected markers
//and the last place that the map was clicked
:: LeafletSimpleState =
	{ cursor    :: ?LeafletLatLng
	, selection :: [LeafletObjectID]
	}

simpleStateEventHandlers :: LeafletEventHandlers LeafletSimpleState

//Inline SVG based icons can be encoded as 'data uri's' which can be used instead of a url to an external icon image
svgIconURL :: !SVGElt !(!Int,!Int) -> String

//Public tileserver of openstreetmaps
openStreetMapTiles :: TileLayer

instance == LeafletObjectID
instance == LeafletIconID

instance < LeafletObjectID

derive JSONEncode LeafletMap, LeafletLatLng
derive JSONDecode LeafletMap, LeafletLatLng
derive gDefault   LeafletMap, LeafletLatLng
derive gEq        LeafletMap, LeafletLatLng
derive gText      LeafletMap, LeafletLatLng
derive gEditor    LeafletMap, LeafletLatLng
derive class iTask
	LeafletIcon, LeafletBounds, LeafletObject, LeafletMarker, LeafletPolyline,
	LeafletPolygon, LeafletWindow, LeafletWindowPos, LeafletLineStyle,
	LeafletStyleDef, LeafletAreaStyle, LeafletObjectID, CSSClass,
	LeafletIconID, LeafletCircle, LeafletRectangle, LeafletSimpleState,
	LeafletPerspective, FitToBoundsOptions, FitToBoundsRegion
