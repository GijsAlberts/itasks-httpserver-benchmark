implementation module iTasks.Extensions.Files

import StdFile, StdArray, StdFunctions

import Control.Applicative
import Control.Monad.Identity
import Control.Monad.State
import Data.Error, Text
import Data.Functor
import Data.Tree
import System.Directory
import System.FilePath
import System.File
import System.Time
import qualified Control.Monad as CM
import qualified System.File as SF
import qualified System.Directory as SD
from Data.List import elem, find

import iTasks

// withShared in the file selection tasks requires these instances:
derive class iTask RTree, FileInfo
derive gHash RTree, FileInfo

deleteFile :: !FilePath -> Task ()
deleteFile path = accWorldError ('SF'.deleteFile path) (addExplanation ["Failed to delete ",path])

moveFile :: !FilePath !FilePath -> Task ()
moveFile srcPath dstPath = accWorldError ('SF'.moveFile srcPath dstPath) (addExplanation ["Failed to move ",srcPath," to ",dstPath])

copyFile :: !FilePath !FilePath -> Task ()
copyFile srcPath dstPath = accWorldError ('SF'.copyFile srcPath dstPath) (\e -> concat ["Failed to copy ",srcPath," to ",dstPath,": ",snd e])

createDirectory :: !FilePath !Bool -> Task ()
createDirectory path False = accWorldError ('SD'.createDirectory path) (addExplanation ["Failed to create directory ",path])
createDirectory path True = accWorldError (createWithParents path) id
where
	createWithParents path world = create [] (split {pathSeparator} path) world

	create _ [] world = (Ok (),world)
	create [] ["":rest] world = create [""] rest world //Special case for absolute paths
	create base [dir:rest] world
		# next = base ++ [dir]
		# path = join {pathSeparator} next
		# (exists,world) = 'SF'.fileExists path world
		| exists = create next rest world //This part exists, continue
		| otherwise = case 'SD'.createDirectory path world of
			(Error e,world) = (Error (addExplanation ["Failed to create directory ",path] e),world)
			(Ok (),world) = create next rest world

deleteDirectory :: !FilePath !Bool -> Task ()
deleteDirectory path False = accWorldError ('SD'.removeDirectory path) (addExplanation ["Failed to delete directory ",path])
deleteDirectory path True = accWorldError (deleteDirectoryRecursive path) id

deleteDirectoryRecursive path world = case 'SD'.readDirectory path world of
	(Error e,world) = (Error (addExplanation expl e), world)
	(Ok content,world) = case deleteContent content world of
		(Error e,world) = (Error e,world)
		(Ok (),world) = case 'SD'.removeDirectory path world of
			(Error e,world) = (Error (addExplanation expl e),world)
			(Ok (),world) = (Ok (),world)
where
	expl = ["Failed to delete directory ",path]

	deleteContent [] world = (Ok (),world)
	deleteContent [".":rest] world = deleteContent rest world
	deleteContent ["..":rest] world = deleteContent rest world
	deleteContent [entry:rest] world = case getFileInfo thispath world of
		(Error e,world) = (Error (addExplanation expl e), world)
		(Ok {FileInfo|directory},world)
		| directory = case deleteDirectoryRecursive thispath world of
			(Error e,world) = (Error e,world)
			(Ok (),world) = deleteContent rest world
		| otherwise = case 'SF'.deleteFile thispath world of
			(Error e,world) = (Error (addExplanation expl e),world)
			(Ok (),world) = deleteContent rest world
	where
		thispath = path </> entry
		expl = ["Failed to delete directory ",path," while checking ",thispath]

copyDirectory :: !FilePath !FilePath -> Task ()
copyDirectory  srcPath dstPath = accWorldError (copyDirectory` srcPath dstPath) id

copyDirectory` srcPath dstPath world = case readDirectory srcPath world of
	(Error e,world) = (Error (addExplanation expl e), world)
	(Ok content,world) = case 'SD'.createDirectory dstPath world of
		(Error e,world) = (Error (addExplanation expl e),world)
		(Ok (),world) = copyContent content world
where
	expl = ["Failed to copy directory ",srcPath," to ",dstPath]

	copyContent [] world  = (Ok (),world)
	copyContent [".":rest] world = copyContent rest world
	copyContent ["..":rest] world = copyContent rest world
	copyContent [entry:rest] world = case getFileInfo (srcPath </> entry) world of
		(Error e,world) = (Error (addExplanation expl e), world)
		(Ok {FileInfo|directory},world)
			| directory = case copyDirectory` (srcPath </> entry) (dstPath </> entry) world of
				(Error e,world) = (Error (concat (expl ++ [e])),world)
				(Ok (),world) = copyContent rest world
			| otherwise = case 'SF'.copyFile (srcPath </> entry) (dstPath </> entry) world of
				(Error e,world) = (Error (concat (expl ++ [snd e])), world)
				(Ok (),world) = copyContent rest world

addExplanation :: ![String] !OSError -> String
addExplanation expl (_,msg) = concat (expl ++ [": ", msg])

selectFileTree :: !Bool !Bool !FilePath [FilePath]-> Task [FilePath]
selectFileTree exp multi root initial
	= accWorld (readDirectoryTree root ?None) @ numberTree
	>>- \tree->editSelection [SelectMultiple multi,selectOption] tree
		[i\\(i, (f, _))<-leafs tree | elem f initial]
where
	selectOption = SelectInTree
		(\tree->[{foldTree (fp2cn exp) tree & label=root}])
		(\tree sel->[f\\(i, (f, _))<-leafs tree | isMember i sel])

selectFileTreeLazy :: !Bool !FilePath -> Task [FilePath]
selectFileTreeLazy multi root = accWorld (readDirectoryTree root (?Just 1)) >>- \tree->
	withShared tree \stree->let numberedtree = mapRead numberTree stree in
	withShared [] \ssel->
	editSharedSelectionWithShared [SelectMultiple multi,selOpt] numberedtree ssel
	-|| whileUnchanged (ssel >*< numberedtree) (\(sel, tree)->case sel of
		[i] = case find ((==)i o fst) (leafs tree) of
			?Just (i, (fp, Ok {directory=True}))
				= accWorld (readDirectoryTree fp (?Just 1))
				@ flip (mergeIn i) tree
				>>- \newtree->set ([], newtree) (ssel >*< stree) @? const NoValue
			_ = unstable ()
		_ = unstable ()
	)
	@ map (fst o snd)
where
	mergeIn j newtree = foldTree \(i, t) cs->if (i == j) newtree (RNode t cs)

	unstable a = return a @? \(Value a _)->Value a False

	selOpt :: SelectOption (RTree (Int, (FilePath, MaybeOSError FileInfo))) (Int, (FilePath, MaybeOSError FileInfo))
	selOpt = SelectInTree
		(\tree->[{foldTree (fp2cn True) tree & label=root}])
		(\tree sel->[t\\t=:(i, _)<-leafs tree | isMember i sel])

fp2cn :: Bool (Int, (FilePath, MaybeOSError FileInfo)) [ChoiceNode] -> ChoiceNode
fp2cn exp (i, (fp, mfi)) cs =
	{ id=i
	, label=dropDirectory fp +++ if (isError mfi) (" (" +++ snd (fromError mfi) +++ ")") ""
	, icon=icon mfi
	, expanded=exp
	, children=cs
	}
where
	icon (Ok {directory=True}) = ?Just "folder"
	icon (Ok _) = ?Just "document"
	icon _ = ?Just "document-error"

selectFileTreeWithShared ::
	!Bool !FilePath
	!(sds (RTree (FilePath,MaybeOSError FileInfo)) (RTree (FilePath,MaybeOSError FileInfo,Bool,Bool)) ())
	-> Task [FilePath]
	| RWShared sds
selectFileTreeWithShared multi root sds =
	accWorld (readDirectoryTree root ?None) >>- \tree ->
	get (sdsFocus tree sds) @ numberTree >>- \tree ->
	editSelection
		[SelectMultiple multi, SelectInTree (\t -> [toChoiceNode t]) fromSelection]
		tree
		[i \\ (i,(_,_,selected,_)) <- leafs tree | selected]
where
	toChoiceNode :: !(RTree (Int, (FilePath,MaybeOSError FileInfo,Bool,Bool))) -> ChoiceNode
	toChoiceNode (RNode (i,(path,info,_,expanded)) children) =
		{ id       = i
		, label    = dropDirectory path +++ if (isError info) (" (" +++ snd (fromError info) +++ ")") ""
		, icon     = icon info
		, expanded = expanded
		, children = map toChoiceNode children
		}
	where
		icon (Ok {directory=True}) = ?Just "folder"
		icon (Ok _) = ?Just "document"
		icon _ = ?Just "document-error"

	fromSelection :: !(RTree (Int, (FilePath,MaybeOSError FileInfo,Bool,Bool))) [Int] -> [FilePath]
	fromSelection tree selected = [f \\ (i,(f,_,_,_)) <- leafs tree | isMember i selected]

numberTree :: ((RTree a) -> RTree (Int, a))
numberTree = flip evalState zero o foldTree \a cs->
	(\lvs i->RNode (i, a) lvs) <$> 'CM'.sequence cs <*> getState <* modify inc
