definition module iTasks.Extensions.Admin.UserAdmin
/**
* This extension provides workflows for managing the users of an iTask system.
*/
import iTasks
import iTasks.Extensions.User
from iTasks.Extensions.Document import :: Document

:: UserAccount			=
	{ credentials	:: !Credentials
	, title			:: !?UserTitle
	, roles			:: ![Role]
	}

/**
 * A user account which can safely be stored, as it does not contains the password in cleartext.
 */
:: StoredUserAccount =
	{ credentials :: !StoredCredentials
	, title       :: !?UserTitle
	, roles       :: ![Role]
	, token       :: !?StoredCredentials
	}

/**
 * Stored user credentials not containing the password in cleartext.
 */
:: StoredCredentials = { username           :: !Username //* The username.
                       , saltedPasswordHash :: !String   //* The salted SHA1 password hash.
                       , salt               :: !String   //* The 32-byte random salt.
                       }

derive class iTask UserAccount, StoredUserAccount, StoredCredentials

// User accounts in the iTasks store

//* All user accounts
userAccounts			::				SDSLens () [StoredUserAccount] [StoredUserAccount]

//* All users
users					:: 				SDSLens () [User] ()
//* Users with a specific role
usersWithRole			:: !Role ->		SDSLens () [User] ()

//* Alternative user account file, in a user defined location
userAccountsFile :: SDSLens FilePath [StoredUserAccount] [StoredUserAccount]

//* Utility lenses for looking up user accounts
userAccountIn :: (sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens UserId (?StoredUserAccount) (?StoredUserAccount) | RWShared sds

userTokenIn :: (sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens String (?StoredUserAccount) () | RWShared sds

/**
* Authenticates a user by username and password
*
* @param Username: The username
* @param Password: The password
* @param Persistent: Set a persistent authentication token in browser cookies
*
* @result A single user who matches the given credentials, or `?None` of none or more than one exists.
*/
authenticateUser	:: !Username !Password !Bool -> Task (?User)
authenticateUserWith :: !Username !Password !Bool (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task (?User) | RWShared sds

/**
* Check the browser cookies for a stored persistent login token.
* - If no token is set, the task blocks indefinitely.
* - If a valid token is set, it returns the user.
* - If an invalid token is set, it returns `?None`.
*/
authenticateUsingToken :: Task (?User)
authenticateUsingTokenWith :: (sds String (?StoredUserAccount) ()) -> Task (?User) | RWShared sds

/**
* Clear any persistent authentication cookies
*/
clearAuthenticationToken :: Task ()

/**
* Wraps a task with an authentication task
*
* @param	the task to wrap
*/
doAuthenticated :: (Task a) -> Task a | iTask a
doAuthenticatedWith :: !(Credentials -> Task (?User)) (Task a) -> Task a | iTask a

/**
* Add a new user
*
* @param User details: The user-information which needs to be stored
*
* @return The stored user
*/
createUser			:: !UserAccount -> Task StoredUserAccount
/**
* Delete an existing user
*
* @param User: The user who needs to be deleted
*
* @return The deleted user
*/
deleteUser			:: !UserId -> Task ()
/**
* Browse and manage the existing users
*/
manageUsers			:: Task ()

createUserFlow :: Task ()
updateUserFlow :: UserId -> Task StoredUserAccount
changePasswordFlow :: !UserId -> Task StoredUserAccount
deleteUserFlow :: UserId -> Task StoredUserAccount
importUserFileFlow :: Task ()
exportUserFileFlow :: Task Document

changeOwnPasswordFlow :: !UserId -> Task ()
changeOwnPasswordFlowWith :: !UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task () | RWShared sds

/**
* Do a task in a basic multi-user setting, users need to log in before they can do a task
*/
minimalMultiUserTask :: ((Task (?User)) -> Task (?User))
	(User -> Task ()) (User -> Task ())
	(User -> [(String,Task ())])
	(sds UserId (?StoredUserAccount) (?StoredUserAccount))
	(sds String (?StoredUserAccount) ()) -> Task () | RWShared sds

/**
* Create set of user names handy for giving demo's: alice, bob, carol, ...
*/
importDemoUsersFlow :: Task [UserAccount]

