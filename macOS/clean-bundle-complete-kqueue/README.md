# Using Clean

Compiling Clean programs on a mac can be done using one of the following tools:

- clm: A build tool for simple projects. All compilation options and paths to libraries are specified as command line arguments
- cpm: A build tool that allows you to specify build configurations of programs in Clean project files (.prj).
       You can use cpm create or edit project files and build those projects.

Both tools rely on the environment variable `CLEAN_HOME` to point to where you installed Clean to find libraries and executables.
So for example if you have unpacked this package to `/Users/myusername/clean` you should the following exports to your .bashrc/.profile/etc
```
export CLEAN_HOME=/Users/myusername/clean
export PATH=$PATH:$CLEAN_HOME/bin
```

# Dependencies 

Clean uses the linker and assembler from Apple. These are not
installed by default. Therefore first Apple's XCode must be installed.
For more information on XCode see:
https://developer.apple.com/xcode/

# More Info

Additional information about Clean is can be found on the website:

  http://clean.cs.ru.nl

Bug reports, questions and suggestions are welcome. Please send them to:

  mailto:clean@cs.ru.nl
