implementation module iTasks.WF.Tasks.IO

import iTasks.Internal.SDS
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.SDS.Definition
import iTasks.WF.Definition
import iTasks.WF.Derives
import iTasks.UI.Definition
import iTasks.UI.Editor

import iTasks.Internal.IWorld
import iTasks.Internal.Task
import iTasks.Internal.TaskState
import iTasks.Internal.TaskEval
import iTasks.Internal.TaskServer
import iTasks.Internal.Generic.Visualization
import iTasks.Internal.Generic.Defaults
import iTasks.WF.Tasks.Core

import qualified System._Process
import Text, Text.GenJSON, StdString, StdInt, StdBool, StdList, StdTuple, Data.Tuple, Data.Func
import StdFunc => qualified return
import qualified Data.Map as DM
import Data.Maybe
import StdDebug
import qualified Data.Set as DS
import qualified Data.List as DL
from System._Process import 
	runProcessIO, runProcessIO`, runProcessPty, runProcessPty`, closeProcessIO, terminateProcessCode, checkProcess, readPipeNonBlocking, writePipe
    ,:: ProcessHandle, :: ProcessIO (..), :: ReadPipe, :: WritePipe, :: ProcessPtyOptions (..)
from AsyncIO import 
    ioMonitorPipeC, windowsReadPipeC, writeShareIfNeeded, onClosePipe, addToSendMap, onCloseSocket, isFDBeingClosed
	, :: AsyncIOFD, :: ErrorCode, :: PipeTask (..), :: ClientTask (..), instance < ProcessHandle
from iTasks.Internal.WebService import httpServer, httpServer`, :: WebService
// from iTasks.Engine import class Startable (..), :: StartableTask (..), :: WebTask, defaultEngineOptions
import iTasks.Engine

from Data.Functor import class Functor(..)
from iTasks.Internal.DynamicUtil import toDyn
from iTasks.WF.Combinators.Common import >>~, ||-, -&&-, -||-

derive JSONEncode ProcessHandle, ProcessIO
derive JSONDecode ProcessHandle, ProcessIO

liftOSErr f iw = case (liftIWorld f) iw of
	(Error (_, e), iw) = (Error (exception e), iw)
	(Ok a, iw) = (Ok a, iw)

wrapPipeTask :: (sdsin () [String] [String]) (sdshandlers () r w) (ExternalProcessHandlers l r w) 
	!FD !FD !FD !ProcessHandle -> PipeTask
	| TC l & TC r & TC w & RWShared sdsin & RWShared sdshandlers
wrapPipeTask sdsin sdshandlers {ExternalProcessHandlers|onStartup, onOutData, onErrData, onShareChange, onExit}
	fdStdOut fdStdErr fdStdIn processHandle =
	{PipeTask|sdsin = ?Just (sdsFocus () sdsin), sdsout = ?None, handlers = ?Just handlersIWorld
	,sdshandlers = ?Just (toDynamic sdshandlers), isLegacy = False, fdStdIn = fdStdIn, fdStdOut = fdStdOut, fdStdErr = fdStdErr
	, closed=False, processHandle = processHandle}
	where 
	handlersIWorld = {ExternalProcessHandlersIWorld|onStartup = onStartup`, onOutData = onOutData`, onErrData = onErrData`,
					  onShareChange = onShareChange`, onExit = onExit`}
	onStartup` (r :: r^) iworld
		# (mbw, out, close) = onStartup r
		= (toDyn <$> mbw, out, close, iworld)
	onStartup` _ _ = abort "wrapPipeTask, onStartup overloaded type variable pattern match failed."
	onOutData` data (r :: r^) iworld
		# (mbw, out, close) = onOutData data r
		= (toDyn <$> mbw, out, close, iworld)
	onOutData` _ _ _ = abort "wrapPipeTask, onOutData overloaded type variable pattern match failed."
	onErrData` data (r :: r^) iworld
		# (mbw, out, close) = onErrData data r
		= (toDyn <$> mbw, out, close, iworld)
	onErrData` _ _ _ = abort "wrapPipeTask, onErrData overloaded type variable pattern match failed."
	onShareChange` (r :: r^) iworld
		# (mbw, out, close) = onShareChange r
		= (toDyn <$> mbw, out, close, iworld)
	onShareChange` _ _ = abort "wrapPipeTask, onShareChange overloaded type variable pattern match failed."
	onExit` exit (r :: r^) iworld
		# (mbw) = onExit exit r
		= (toDyn <$> mbw, iworld)
	onExit` _ _ _ = abort "onExit, onExit overloaded type variable pattern match failed."

monitorSDSIn :: !FD (Shared sdsin [String]) -> Task () | RWShared sdsin
monitorSDSIn fdStdIn sdsin = Task eval
	where 
	eval event {taskId, lastEval} iworld=:{sendMap} 
		| isDestroyOrInterrupt event = trace_n "DestroyEvent for monitorSDSIn." (DestroyedResult, iworld)
		| isRefreshForTask event taskId 
			# (mbr, iworld) = readRegister taskId sdsin iworld
			| isError mbr = abort "SDSException"
			# out = directResult (fromOk mbr) 
			| out == [] = (ValueResult (Value () False) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
			# sendMap = 'DM'.alter (addToSendMap out) fdStdIn sendMap
			# iworld = {iworld & sendMap = sendMap} 
			# (mbw, iworld) = write [] sdsin EmptyContext iworld
			| isError mbw = abort "processIdleCBs: mbw err."
			= (ValueResult (Value () False) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
		= (ValueResult (Value () False) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)

monitorSDSHandlersPipe :: !FD (sds () r w) (ExternalProcessHandlers l r w) -> Task () | RWShared sds & iTask l & iTask r & iTask w
monitorSDSHandlersPipe fdStdIn sdshandlersx handlers = Task evalinit
	where 
	evalinit event {taskId, lastEval} iworld 
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		| isRefreshForTask event taskId 
			# (mbr, iworld) = readRegister taskId sdshandlersx iworld
			| isError mbr = abort "sdsexception"
			= (ValueResult NoValue (mkTaskEvalInfo lastEval) NoChange (Task evalinit2), iworld)
	evalinit2 event {taskId, lastEval} iworld=:{pipes, sendMap} 
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
		| isRefreshForTask event taskId 
			# (mbr, iworld) = readRegister taskId sdshandlersx iworld
			| isError mbr = abort "sdsexception"
			= (ValueResult NoValue (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
	eval event {taskId, lastEval} iworld=:{pipes, sendMap} 
		| isDestroyOrInterrupt event = trace_n "DestroyEvent for monitorSDSHandlersPipe" (DestroyedResult, iworld)
		| isRefreshForTask event taskId 
			# (mbr, iworld) = readRegister taskId sdshandlersx iworld
			| isError mbr = abort "SDSException"
			# mbPt = 'DM'.get fdStdIn pipes
			| isFDBeingClosed fdStdIn sendMap = trace_n "process was closed so onShareChange is not evaluated." 
				(ValueResult (Value () True) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
			| isNone mbPt = trace_n "process was closed so onShareChange is not evaluated." 
				(ValueResult (Value () True) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
			# pt=:{PipeTask|handlers, sdshandlers} = fromJust mbPt
			# sdshandlers = fromJust sdshandlers
			# (mbr, iworld) = read sdshandlers EmptyContext iworld
			# r = directResult (fromOk mbr)
			# {ExternalProcessHandlersIWorld|onShareChange} = fromJust handlers
			# (mbw, out, close, iworld) = onShareChange r iworld
			# (mbSdsErr, iworld) = writeShareIfNeeded sdshandlers mbw iworld
			| isError mbSdsErr = abort "sdsexception"
			| close
				# iworld = onClosePipe pt out iworld 
				= (ValueResult (Value () True) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
			# sendMap = 'DM'.alter (addToSendMap out) fdStdIn sendMap
			# iworld = {iworld & sendMap = sendMap}
			= trace_n "Refresh for handlers" (ValueResult (Value () False) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)
		= (ValueResult (NoValue) (mkTaskEvalInfo lastEval) NoChange (Task eval), iworld)

externalProcessHandlers :: !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions)
	!(Shared sdsin [String]) !(sdshandlers () r w) !(ExternalProcessHandlers l r w) -> Task Int
	| RWShared sdsin & RWShared sdshandlers & iTask l & iTask r & iTask w
externalProcessHandlers cmd args dir exitCode mopts sdsin sdshandlers handlers 
	= Task startProcess 
	  >>~ \(phpio, pipeStdIn) -> (monitorSDSHandlersPipe pipeStdIn sdshandlers handlers -||- monitorSDSIn pipeStdIn sdsin) ||- 
	  	Task (evalinit phpio)
where
	startProcess event eo=:{TaskEvalOpts|lastEval} iworld=:{asyncIoFd}
		| isDestroyOrInterrupt event = trace_n "destroy startProcess" (DestroyedResult, iworld)
		# asyncIoFd = fromJust asyncIoFd
		= case liftOSErr (maybe (runProcessIO` asyncIoFd cmd args dir) (runProcessPty` asyncIoFd cmd args dir) mopts) iworld of
			(Error e, iworld)  = trace_n "Exception while executing external process" (ExceptionResult e, iworld)
			(Ok phpio=:(ph, pio), iworld)
				# pipeStdIn = 'System._Process'._fromWritePipe pio.stdIn 
				= (ValueResult (Value (phpio, pipeStdIn) True) (mkTaskEvalInfo lastEval) 
					(mkUIIfReset event rep) (Task startProcess), iworld)
	evalinit phpio=:(ph, pio) event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld=:{pipes, processes, world, asyncIoFd}
		| isDestroyOrInterrupt event = trace_n "errdestroyinterrupt" (DestroyedResult, iworld)
		// pipeStdOut, pipeStdErr and pipeStdIn are the same if a pseudoterminal is used.
		# pipeStdOut = 'System._Process'._fromReadPipe pio.stdOut
		# pipeStdErr = 'System._Process'._fromReadPipe pio.stdErr
		# pipeStdIn = 'System._Process'._fromWritePipe pio.stdIn
		# pipeTask = wrapPipeTask sdsin sdshandlers handlers pipeStdOut pipeStdErr pipeStdIn ph
		# pipes = 'DM'.put pipeStdOut pipeTask pipes
		# pipes = 'DM'.put pipeStdErr pipeTask pipes
		# pipes = 'DM'.put pipeStdIn pipeTask pipes
		# processes = 'DM'.put ph (taskId, ?Just pipeTask, ?None) processes
		# iworld=:{world} = {iworld & world = world, pipes = pipes, processes = processes}
		# iworld = case pipeStdIn == pipeStdOut of
				// pty (no windows).
				True = iworld 
				// Pipes.
				False 
					// Perform async read operation on Windows for stdout/stderr pipes.
					# (err, world) = windowsReadPipeC pipeStdOut world
					| err == -1 = abort "externalProcessHandlers: windowsReadPipeC failed."
					# (err, world) = windowsReadPipeC pipeStdErr world
					| err == -1 = abort "externalProcessHandlers: windowsReadPipeC failed."
					// stdin gets read on Windows to be able to detect disconnect so that the pipe is closed when it is no longer needed.
					# (err, world) = windowsReadPipeC pipeStdIn world
					| err == -1 = abort "externalProcessHandlers: windowsReadPipeC failed."
					= {iworld & world = world}
		// Handle onstartup.
		# (mbr, iworld) = read (fromJust pipeTask.sdshandlers) EmptyContext iworld
		| isError mbr = abort "SDSException"
		# {ExternalProcessHandlersIWorld|onStartup} = fromJust (pipeTask.handlers)
		# (mbw, out, close, iworld) = onStartup (directResult (fromOk mbr)) iworld
		# (mbSdsErr, iworld=:{sendMap}) = writeShareIfNeeded (fromJust pipeTask.sdshandlers) mbw iworld
		| isError mbSdsErr = abort "SDSException"
		| close
			# iworld = onClosePipe pipeTask out iworld
			= (ValueResult (NoValue) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (evalinit2 phpio)), iworld)
		# sendMap = 'DM'.alter (addToSendMap out) pipeTask.fdStdIn sendMap
		# iworld = {iworld & sendMap = sendMap}
		= (ValueResult (NoValue) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (evalinit2 phpio)), iworld)
	evalinit2 phpio=:(ph,pio) event {taskId,lastEval} iworld
		= (ValueResult (Value 0 False) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination phpio)), iworld)
	monitorForTermination phpio=:(ph,pio) event {taskId,lastEval} iworld=:{processes, asyncIoFd}
		| isRefreshForTask event taskId  
			// TODO Set of processes.
			# (taskId, mbPt, mbExit) = 'DM'.find ph processes
			| isNone mbPt = abort "monitorForTermination: no pipeTask for process."
			# pt = fromJust mbPt
			| isNone mbExit = abort "monitorForTermination: no exitcode."
			# exit = fromJust mbExit
			# sdshandlers = fromJust pt.sdshandlers
			# {ExternalProcessHandlersIWorld|onExit} = fromJust pt.handlers
			# (mbr, iworld) = read sdshandlers EmptyContext iworld
			| isError mbr = abort "SDSException"
			# (mbw, iworld) = onExit (ExitCode exit) (directResult (fromOk mbr)) iworld
			# (mbSdsErr, iworld) = writeShareIfNeeded sdshandlers mbw iworld	
			# processes = 'DM'.del ph processes
			# iworld = {iworld & processes = processes}
			= trace_n ("Process terminated so stable value is returned, exitcode: " +++ toString exit) 
			  (ValueResult (Value exit True) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination phpio)), iworld)
		= (ValueResult (Value 0 False) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination phpio)), iworld)

	rep = stringDisplay ("External process: " <+++ cmd)

externalProcess` :: !Timespec !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions) 
                    !(Shared sds1 [String]) !(Shared sds2 ([String], [String])) -> Task Int 
					| RWShared sds1 & RWShared sds2
externalProcess` unused cmd args dir exitCode mopts sdsin sdsout 
	= Task startProcess >>~ \(phpio, pipeStdIn) ->  monitorSDSIn pipeStdIn sdsin ||- Task (evalinit phpio)
where
	startProcess event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld=:{asyncIoFd}
		# asyncIoFd = fromJust asyncIoFd
		= case liftOSErr (maybe (runProcessIO` asyncIoFd cmd args dir) (runProcessPty` asyncIoFd cmd args dir) mopts) iworld of
			(Error e, iworld)  = trace_n "Exception while executing external process" (ExceptionResult e, iworld)
			(Ok phpio=:(ph,pio), iworld=:{world, asyncIoFd, pipes}) 
				# pipeStdIn = 'System._Process'._fromWritePipe pio.stdIn
				= (ValueResult (Value (phpio, pipeStdIn) True) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) 
					(Task startProcess), iworld)
	evalinit _ event _ iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	evalinit phpio=:(ph,pio) event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld=:{asyncIoFd, pipes, world, processes}
		// For Linux pipeStdOut, pipeStdErr and pipeStdIn are the same if a pseudoterminal is used.
		# pipeStdOut = 'System._Process'._fromReadPipe pio.stdOut
		# pipeStdErr = 'System._Process'._fromReadPipe pio.stdErr
		# pipeStdIn = 'System._Process'._fromWritePipe pio.stdIn
		# pt = { sdsin = ?Just (sdsFocus () sdsin), sdsout = ?Just (sdsFocus () sdsout)
			   , handlers = ?None, sdshandlers = ?None, isLegacy = True
			   , fdStdIn = pipeStdIn, fdStdOut = pipeStdOut, fdStdErr = pipeStdErr
			   , closed = False, processHandle = ph
			   }
		# processes = 'DM'.put ph (taskId, ?Just pt, ?None) processes
		# iworld=:{world} = {iworld & world = world, processes = processes}
		// pty.
		| pipeStdOut == pipeStdErr && pipeStdErr == pipeStdIn
			// windowsReadPipeC not done here since Windows does not support pty.
			# pipes 
				= 'DM'.put pipeStdIn
					pt pipes
			# iworld = {iworld & pipes = pipes}
			= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (evalinit2 (ph, pio))), iworld)
		// Pipes.
		# pipes 
			= 'DM'.put pipeStdOut 
				{ sdsin = ?None, sdsout = ?Just (sdsFocus () sdsout)
				, handlers = ?None, sdshandlers = ?None, isLegacy = True
				, fdStdOut = pipeStdOut, fdStdErr = pipeStdErr, fdStdIn = pipeStdIn
				, closed=False, processHandle = ph
				} pipes
		// Perform async read operation on Windows for stdout/stderr pipes.
		# (err, world) = windowsReadPipeC pipeStdOut world
		| err == -1 = abort "externalProcess`: windowsReadPipeC failed."
		# pipes 
			= 'DM'.put pipeStdErr 
				{ sdsin = ?None, sdsout = ?Just (sdsFocus () sdsout)
				, handlers = ?None, sdshandlers = ?None, isLegacy = True, fdStdOut = pipeStdOut
				, fdStdErr = pipeStdErr, fdStdIn = pipeStdIn
				, closed=False, processHandle = ph} pipes
		# (err, world) = windowsReadPipeC pipeStdErr world
		| err == -1 = abort "externalProcess`: windowsReadPipeC failed."
		# pipes 
			= 'DM'.put pipeStdIn 
				{ sdsin = ?Just (sdsFocus () sdsin), sdsout = ?None
				, handlers = ?None, sdshandlers = ?None, isLegacy = True
				, fdStdOut = pipeStdOut, fdStdErr = pipeStdErr, fdStdIn = pipeStdIn
				, closed=False, processHandle = ph} pipes
		// stdin gets read on Windows to be able to detect disconnect to close pipe when it is no longer needed.
		# (err, world) = windowsReadPipeC pipeStdIn world
		| err == -1 = abort "windowsReadPipeC failed."
		# iworld = {iworld & world = world, pipes = pipes}
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (evalinit2 (ph, pio))), iworld)

	evalinit2 (ph, pio) event {TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event
			# iworld = clearTaskSDSRegistrations ('DS'.singleton taskId) iworld
			= apIWTransformer iworld
			$       liftOSErr (terminateProcessCode ph exitCode)
			>-= \_->liftOSErr (closeProcessIO pio)
			>-= \_->tuple (Ok DestroyedResult)
	evalinit2 (ph, pio) event {taskId,lastEval} iworld
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination (ph, pio))), iworld)

	monitorForTermination (ph, pio) event {TaskEvalOpts|taskId} iworld
		| isDestroyOrInterrupt event
			# iworld = trace_n "monitorForTermination task Destroyed." clearTaskSDSRegistrations ('DS'.singleton taskId) iworld
			= apIWTransformer iworld
			$       liftOSErr (terminateProcessCode ph exitCode)
			>-= \_->liftOSErr (closeProcessIO pio)
			>-= \_->tuple (Ok DestroyedResult)
	monitorForTermination phpio=:(ph,pio) event {taskId,lastEval} iworld=:{processes, asyncIoFd}
		| isRefreshForTask event taskId  
			// TODO Set of processes.
			# (taskId, _, mbExit) ='DM'.find ph processes
			| isNone mbExit = abort "monitorForTermination: no exitcode."
			# exit = fromJust mbExit
			# processes = 'DM'.del ph processes
			# iworld = {iworld & processes = processes}
			= trace_n ("Process terminated so stable value is returned, exitcode: " +++ toString exit) 
			  (ValueResult (Value exit True) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination phpio)), iworld)
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (monitorForTermination (ph, pio))), iworld)
		
	rep = stringDisplay ("External process: " <+++ cmd)

externalProcess :: !Timespec !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions) 
				   !(Shared sds1 [String]) !(Shared sds2 ([String], [String])) -> Task Int 
				   | RWShared sds1 & RWShared sds2
externalProcess poll cmd args dir exitCode mopts sdsin sdsout = Task evalinit
where
    evalinit event _ iworld
        | isDestroyOrInterrupt event = (DestroyedResult, iworld)
    evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
        = case liftOSErr (maybe (runProcessIO cmd args dir) (runProcessPty cmd args dir) mopts) iworld of
            (Error e, iworld)  = (ExceptionResult e, iworld)
            (Ok phpio, iworld) = eval phpio event evalOpts iworld

    eval (ph, pio) event {TaskEvalOpts|taskId} iworld
        | isDestroyOrInterrupt event
            # iworld = clearTaskSDSRegistrations ('DS'.singleton taskId) iworld
            = apIWTransformer iworld
            $       liftOSErr (terminateProcessCode ph exitCode)
            >-= \_->liftOSErr (closeProcessIO pio)
            >-= \_->tuple (Ok DestroyedResult)
    //TODO: Support async sdss
    eval (ph, pio) event {taskId,lastEval} iworld
        | not (isRefreshForTask event taskId)
            = (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task (eval (ph, pio))), iworld)
        = apIWTransformer iworld $
            read sdsout EmptyContext                    >-= \(ReadingDone (stdoutq, stderrq))->
            liftOSErr (readPipeNonBlocking pio.stdOut)  >-= \stdoutData->
            liftOSErr (readPipeNonBlocking pio.stdErr)  >-= \stderrData->
            (if (stdoutData == "" && stderrData == "")
                (tuple (Ok WritingDone))
                (write (stdoutq ++ filter ((<>)"") [stdoutData]
                       ,stderrq ++ filter ((<>)"") [stderrData]
                       ) sdsout EmptyContext))          >-= \WritingDone->
            liftOSErr (checkProcess ph)                 >-= \mexitcode->case mexitcode of
                ?Just i = tuple (Ok (ValueResult (Value i True) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (return i)))
                ?None =
                    readRegister taskId clock >-= \_->
                    readRegister taskId sdsin >-= \(ReadingDone stdinq)->
                    if (stdinq =: [])
                        (tuple $ Ok WritingDone)
                        (   // the process might have terminated since `checkProcess`,
                            // for this case we ignore the EPIPE error and handle termination at next task evaluation
                            liftOSErr (writePipeNoErrorOnBrokenPipe (concat stdinq) pio.stdIn) >-= \_ ->
                            write [] sdsin EmptyContext
                        )
                    >-= \WritingDone ->
                    tuple (Ok (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep)
                        (Task (eval (ph, pio)))))

    writePipeNoErrorOnBrokenPipe :: !String !WritePipe !*World -> (!MaybeOSError (), !*World)
    writePipeNoErrorOnBrokenPipe str pipe world
        # (res, world)= writePipe str pipe world
        = case res of
            Error (32, _) = (Ok (), world)
            res           = (res,   world)

    rep = stringDisplay ("External process: " <+++ cmd)
    clock = sdsFocus {start=zero,interval=poll} iworldTimespec

// The task will always get two refreshevents, hence there is an evalinit and evalinit2. 
// After that the only refreshevents are queued for the task are refreshevents caused by 
// modifying the sds for the ConnectionHandlers. This is used to evaluate the onShareChange callback.
tcplisten` :: !Int !Bool !(sds () r w) (ConnectionHandlers l r w) -> Task [l] | iTask l & iTask r & iTask w & RWShared sds
tcplisten` port removeClosed sds handlers = Task evalinit
where
	evalinit event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld
		| isDestroyOrInterrupt event = trace_n "evalinit destroy." (DestroyedResult, iworld)
		= case addListener` taskId port removeClosed (wrapConnectionTask` handlers sds) iworld of
			(Error e, iworld) = (ExceptionResult e, iworld)
			(Ok _, iworld)
				# (mbr, iworld) = readRegister taskId sds iworld
				| isError mbr = abort "SDSException"
				= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task evalinit2), iworld)

	evalinit2 event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld
		| isDestroyOrInterrupt event = trace_n "evalinit2 destroy." (DestroyedResult, iworld)
		# (mbr, iworld) = readRegister taskId sds iworld
		| isError mbr = abort "SDSException"
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)
	eval event {TaskEvalOpts|taskId} iworld=:{ioStates}
		| isDestroyOrInterrupt event
			# ioStates = case 'DM'.get taskId ioStates of
				?Just (IOActive values) = trace_n "Destroyed ioState tcplisten" 'DM'.put taskId (IODestroyed values) ioStates
				_                       = ioStates
			= (DestroyedResult,{iworld & ioStates = ioStates})
	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates, clients, sendMap}
		= case 'DM'.get taskId ioStates of
			?Just (IOException e) = (ExceptionResult (exception e), iworld)
			?Just (IOActive values)
				# iworld = 
					'DM'.foldrWithKey (\fd (conState,close) iworld -> handleOnShareChangeClient taskId fd conState close iworld)
						iworld values
				# value = Value [l \\ (fd,(l :: l^,close)) <- 'DM'.toList values] False
				= (ValueResult value (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)
			?None = (ValueResult (Value [] False) (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)

httpServerTask :: a !Int -> Task () | Startable a
httpServerTask startable port = Task evalinit
where
	webTasks startable = [t \\ WebTask t <- toStartable startable]
	evalinit event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld=:{world}
		| isDestroyOrInterrupt event = trace_n "evalinit destroy." (DestroyedResult, iworld)
		# tasks = webTasks startable 
		| isEmpty tasks = abort "empty"
		# (options=:{keepaliveTime, serverDirectory}, world) = defaultEngineOptions world
		# service = engineWebService` serverDirectory tasks
		# server = httpServer` port serverDirectory keepaliveTime service taskOutput
		= case addListener` taskId port True server {iworld & world = world} of
			(Error e, iworld) = (ExceptionResult e, iworld)
			(Ok _, iworld) = (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task evalinit2), iworld)

	evalinit2 event evalOpts=:{TaskEvalOpts|taskId, lastEval} iworld
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)
	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates, clients, sendMap}
		= case 'DM'.get taskId ioStates of
			?Just (IOException e) = (ExceptionResult (exception e), iworld)
			?Just (IOActive values)
				= (ValueResult (Value () False) (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)
			?None = (ValueResult (Value () False) (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)

handleOnShareChangeClient :: !TaskId !FD Dynamic !Bool *IWorld -> *IWorld
handleOnShareChangeClient taskId fd conState close iworld=:{clients, sendMap, ioStates}
	# mbCt = 'DM'.get fd clients 
	| isNone mbCt = trace_n "No clienttask found in handleOnShareChangeClient" iworld 
	# ct=:{ClientTask|cCt=(ConnectionTask` handlers sdshandlers), cTaskId} = fromJust mbCt
	# (mbr, iworld) = readRegister taskId sdshandlers iworld
	| isError mbr = abort "SDSException"
	// If a callback returns close, the connection is kept alive until the remaining data to be sent is sent.
	// However, the onShareChange should not be evaluated after a callback returns close is true.
	| isBeingClosed fd ('DM'.get fd sendMap) 
		= trace_n "Locally disconnected." iworld
	# r = directResult (fromOk mbr)
	# {ConnectionHandlersIWorld`|onShareChange} = handlers
	# (mbl, mbw, out, close, iworld) = onShareChange conState r iworld
	| isError mbl = abort "handleOnShareChangeClient: error in connection state "
	# (mbSdsErr, iworld) = writeShareIfNeeded sdshandlers mbw iworld
	| isError mbSdsErr = abort "sdsexception"
	# ioStates = case 'DM'.get cTaskId ioStates of 
		?Just (IOActive conStates)
			# conStates = 'DM'.put fd (fromOk mbl, False) conStates
			= 'DM'.put cTaskId (IOActive conStates) ioStates
		?Just (IODestroyed conStates)
			# conStates = 'DM'.put fd (fromOk mbl, False) conStates
			= 'DM'.put cTaskId (IOActive conStates) ioStates
		_ = abort "IOException in handleOnShareChangeClient."
	| close
		= onCloseSocket fd out iworld 
	# sendMap = 'DM'.alter (addToSendMap out) fd sendMap
	= {iworld & sendMap = sendMap, ioStates = ioStates}

rep port = stringDisplay ("Listening for connections on port "<+++ port)

isBeingClosed fd ?None = False  
isBeingClosed fd (?Just (_, c)) = c

tcplisten :: !Int !Bool !(sds () r w) (ConnectionHandlers l r w) -> Task [l] | iTask l & iTask r & iTask w & RWShared sds
tcplisten port removeClosed sds handlers = Task evalinit
where
	evalinit event _ iworld
		| isDestroyOrInterrupt event = (DestroyedResult, iworld)
	evalinit event evalOpts=:{TaskEvalOpts|taskId} iworld
		= case addListener taskId port removeClosed (wrapConnectionTask handlers sds) iworld of
			(Error e, iworld) = (ExceptionResult e, iworld)
			(Ok _, iworld) = eval event evalOpts iworld

	eval event {TaskEvalOpts|taskId} iworld=:{ioStates}
		| isDestroyOrInterrupt event
			# ioStates = case 'DM'.get taskId ioStates of
				?Just (IOActive values) = 'DM'.put taskId (IODestroyed values) ioStates
				_                       = ioStates
			= (DestroyedResult,{iworld & ioStates = ioStates})
	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates}
		= case 'DM'.get taskId ioStates of
			?Just (IOException e) = (ExceptionResult (exception e), iworld)
			?Just (IOActive values)
				# value = Value [l \\ (_,(l :: l^,_)) <- 'DM'.toList values] False
				= (ValueResult value (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)
			?None = (ValueResult (Value [] False) (mkTaskEvalInfo lastEval) (mkUIIfReset event (rep port)) (Task eval), iworld)

	rep port = stringDisplay ("Listening for connections on port "<+++ port)

tcpconnect` :: !String !Int !(sds () r w) (ConnectionHandlers l r w) -> Task l | iTask l & iTask r & iTask w & RWShared sds
tcpconnect` host port sds handlers = Task evalinit
where
	evalinit event _ iworld
		| isDestroyOrInterrupt event
			= (DestroyedResult, iworld)
	evalinit event eo=:{TaskEvalOpts|taskId, lastEval} iworld
		= case addConnection` taskId host port (wrapConnectionTask` handlers sds) iworld of
			(Error e,iworld) = (ExceptionResult e, iworld)
			(Ok _,iworld) 
				# (mbr, iworld) = readRegister taskId sds iworld
				| isError mbr = abort "SDSException"
				= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task evalinit2), iworld)
	evalinit2 event eo=:{TaskEvalOpts|taskId, lastEval} iworld
		| isDestroyOrInterrupt event = trace_n "evalinit2 destroy." (DestroyedResult, iworld)
		# (mbr, iworld) = readRegister taskId sds iworld
		| isError mbr = abort "SDSException"
		= (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
	eval event evalOpts=:{TaskEvalOpts|taskId} iworld=:{ioStates}
		| isDestroyOrInterrupt event
			# ioStates = case 'DM'.get taskId ioStates of
				?Just (IOActive values) = trace_n "destroyed client iostate." 'DM'.put taskId (IODestroyed values) ioStates
				_                       = ioStates
			= (DestroyedResult, {iworld & ioStates = ioStates})
	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates}
		= case 'DM'.get taskId ioStates of
			?None = (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
			?Just (IOActive values)
				| 'DL'.isEmpty ('DM'.keys values) = trace_n "Connection was closed." 
					(ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
				# fd = 'DL'.head ('DM'.keys values)
				= case 'DM'.get fd values of
					?Just (l :: l^, close)
						# iworld = handleOnShareChangeClient taskId fd (toDyn l) close iworld
						= (ValueResult (Value l close) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
					_ = (ExceptionResult (exception "Corrupt IO task result"),iworld)
			?Just (IOException e)
				= (ExceptionResult (exception e),iworld)

	rep = stringDisplay ("TCP client " <+++ host <+++ ":" <+++ port)

tcpconnect :: !String !Int !(?Timeout) !(sds () r w) (ConnectionHandlers l r w) -> Task l | iTask l & iTask r & iTask w & RWShared sds
tcpconnect host port timeout sds handlers = Task evalinit
where
	//We cannot make ioStates local since the engine uses it
	evalinit event _ iworld
		| isDestroyOrInterrupt event
			= (DestroyedResult, iworld)
	evalinit event eo=:{TaskEvalOpts|taskId} iworld
		= case addConnection taskId host port timeout (wrapConnectionTask handlers sds) iworld of
			(Error e,iworld) = (ExceptionResult e, iworld)
			(Ok _,iworld) = eval event eo iworld

	eval event evalOpts=:{TaskEvalOpts|taskId} iworld=:{ioStates}
		| isDestroyOrInterrupt event
			# ioStates = case 'DM'.get taskId ioStates of
				?Just (IOActive values) = 'DM'.put taskId (IODestroyed values) ioStates
				_                       = ioStates
			= (DestroyedResult, {iworld & ioStates = ioStates})

	eval event evalOpts=:{TaskEvalOpts|taskId,lastEval} iworld=:{ioStates}
		= case 'DM'.get taskId ioStates of
			?None = (ValueResult NoValue (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
			?Just (IOActive values)
				= case 'DM'.get 0 values of
					?Just (l :: l^, s)
						= (ValueResult (Value l s) (mkTaskEvalInfo lastEval) (mkUIIfReset event rep) (Task eval), iworld)
					_
						= (ExceptionResult (exception "Corrupt IO task result"),iworld)
			?Just (IOException e)
				= (ExceptionResult (exception e),iworld)

	rep = stringDisplay ("TCP client " <+++ host <+++ ":" <+++ port)
