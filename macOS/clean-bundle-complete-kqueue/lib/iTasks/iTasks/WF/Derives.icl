implementation module iTasks.WF.Derives

import iTasks.Internal.TaskState
import iTasks.WF.Definition
import iTasks.WF.Combinators.Core
import iTasks.UI.Editor.Common
import iTasks.SDS.Sources.System
import iTasks.Internal.IWorld

import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.GenHash
import qualified Data.Map as Map
import Data.Map.GenJSON
import Data.Maybe
import qualified Data.Set as Set
from Data.Set import instance Foldable Set
import System.Time
import Text.HTML
import Text.GenJSON
import StdArray 
import StdDebug

import graph_copy

gEq{|(->)|} _ _ fa fb		= False // HACK: Compare string representations of graphs functions are never equal
gEq{|Dynamic|} _ _			= False	// dynamics are never equal

derive gEditor    SVGElt, SVGAttr, SVGAlign, SVGColor, SVGDefer, SVGFillOpacity, SVGFuncIRI, SVGLengthAdjust, SVGLengthUnit, SVGLineCap, SVGFillRule, SVGLineJoin, SVGMeetOrSlice, SVGStrokeMiterLimit, SVGPaint, SVGStrokeDashArray, SVGStrokeDashOffset, SVGStrokeWidth, SVGTransform, SVGZoomAndPan, SVGLength, SVGICCColor
derive gText      SVGElt, SVGAttr, SVGAlign, SVGColor, SVGDefer, SVGFillOpacity, SVGFuncIRI, SVGLengthAdjust, SVGLengthUnit, SVGLineCap, SVGFillRule, SVGLineJoin, SVGMeetOrSlice, SVGStrokeMiterLimit, SVGPaint, SVGStrokeDashArray, SVGStrokeDashOffset, SVGStrokeWidth, SVGTransform, SVGZoomAndPan, SVGLength, SVGICCColor
derive gEditor    HtmlAttr
derive gText      HtmlAttr

gHash{|Dynamic|} x = trace_n "gHash{|Dynamic|} should not be used" gHash{|*|} (copy_to_string (hyperstrict x))
gHash{|Map|} fk fv map = murmurHash_combine [murmurHash_combine2 (fk k) (fv v) \\ (k,v) <- 'Map'.toList map]
gHash{|Set|} fx xs = murmurHash_combine [fx x \\ x <- 'Set'.toList xs]
derive gHash      JSONNode

gText{|Set|} m tf ms = gText{|*->*|} m tf ('Set'.toList <$> ms)

derive JSONEncode		TaskValue, TaskInstance, TaskListItem, ValueStatus, Action, Timespec, ClockParameter
derive JSONDecode		TaskValue, TaskInstance, TaskListItem, ValueStatus, Action, Timespec, ClockParameter
derive gEq				TaskValue, TaskInstance, TaskListItem, ValueStatus, Action, Timespec, ClockParameter
derive gText	        TaskValue, TaskInstance, TaskListItem, ValueStatus, Action
derive gEditor			TaskValue, TaskInstance, TaskListItem, ValueStatus, Action, Timespec, ClockParameter, Set
derive gHash			TaskValue, TaskListItem, TaskInstance, ValueStatus, Action, Timespec, ClockParameter

derive class iTask TaskId, TaskListFilter, AttachmentStatus
derive gHash       TaskId, TaskListFilter, AttachmentStatus

derive gHash Timestamp, TaskChange, ExtendedTaskListFilter
