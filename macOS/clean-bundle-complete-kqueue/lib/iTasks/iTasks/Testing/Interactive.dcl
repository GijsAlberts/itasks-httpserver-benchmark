definition module iTasks.Testing.Interactive

from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import :: JSONNode, generic JSONEncode, generic JSONDecode

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.UI.Editor import :: Editor
from iTasks.UI.Editor.Generic import generic gEditor, :: EditorPurpose
from iTasks.WF.Definition import :: Task, class iTask

/**
 * Test a specific editor.
 *
 * @param The editor to test.
 * @param Value to test with 
 * @param Read only mode
 */
testEditor :: !(Editor a (?a)) !(?a) !Bool -> Task a | iTask a

/**
 * A generic test rig for testing the different editor variants for a type
 *
 * @param The name of the type to test (e.g. `Int` or `MyADT`).
 */
testCommonInteractions :: !String -> Task a | iTask, gDefault{|*|} a
