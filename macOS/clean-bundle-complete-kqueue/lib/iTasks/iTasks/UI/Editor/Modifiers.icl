implementation module iTasks.UI.Editor.Modifiers

from StdFunc import o, const, flip, id
import StdBool, StdString, StdList
import iTasks.UI.Editor, iTasks.UI.Definition, iTasks.UI.Tune
import Data.Error, Text.GenJSON, Data.Tuple, Data.Functor
import Data.GenEq, Data.Func, Data.List
import qualified Data.Map as DM
import Data.Maybe

withDynamicHintAttributes :: !String !(Editor r (?w)) -> Editor r (?w)
withDynamicHintAttributes typeDesc editor = withDynamicHintAttributesWithError` False typeDesc
	(mapEditorWrite
		(\mbVal -> case mbVal of
			?Just val -> Ok val
			?None     -> Error ("This value is not in the required format of a " +++ typeDesc))
		editor)

withDynamicHintAttributesWithError :: !String !(Editor r (MaybeError String w)) -> Editor r (?w)
withDynamicHintAttributesWithError typeDesc editor = withDynamicHintAttributesWithError` True typeDesc editor

withDynamicHintAttributesWithError` :: !Bool !String !(Editor r (MaybeError String w)) -> Editor r (?w)
withDynamicHintAttributesWithError` alwaysUseCustomError typeDesc
		editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue} =
	{ Editor
	| onReset    = onReset
	, onEdit     = onEdit
	, onRefresh  = onRefresh
	, writeValue = \st -> error2mb <$> writeValue st
	}
where
	onReset attr mbval vst=:{VSt|taskId,optional} = case editorOnReset attr mbval vst of
		(Ok (UI type attr items,mask,mbw),vst)
			//Add tooltip attributes
			# attr = 'DM'.union (stdAttributes alwaysUseCustomError typeDesc (valueError mask) optional mask) attr
			= (Ok (UI type attr items,mask,error2mb <$> mbw),vst)
		(e,vst) = (liftError e,vst)

	onEdit e omask vst=:{VSt|optional} = case editorOnEdit e omask vst of
		(Ok ?None,vst) = (Ok ?None,vst)
		(Ok (?Just (change,nmask,mbw)),vst)
			= (Ok (?Just (addTooltipAttrChanges omask nmask optional change,nmask,error2mb <$> mbw)),vst)
		(Error e,vst) = (Error e,vst)

	onRefresh e omask vst=:{VSt|optional} = case editorOnRefresh e omask vst of
		(Ok (change,nmask,mbw),vst)
			= (Ok (addTooltipAttrChanges omask nmask optional change,nmask,error2mb <$> mbw),vst)
		(Error e,vst) = (Error e,vst)
	
	addTooltipAttrChanges omask nmask optional change
		# attrChange = case stdAttributeChanges alwaysUseCustomError typeDesc optional (valueError omask) omask (valueError nmask) nmask of
			[] = NoChange
			cs = ChangeUI cs []
		= mergeUIChanges change attrChange

	valueError mask = case writeValue mask of
		Error e      -> ?Just e
		Ok (Error e) -> ?Just e
		Ok (Ok _)    -> ?None

/**
* Set basic tooltip and error information based on the verification
*/
stdAttributes :: !Bool !String !(?String) !Bool !EditState -> UIAttributes
stdAttributes alwaysUseCustomError typename mbError optional mask =
	'DM'.fromList (stdAttributesList alwaysUseCustomError typename mbError optional mask)

stdAttributesList :: !Bool !String !(?String) !Bool !EditState -> [(String, JSONNode)]
stdAttributesList _ typename ?None optional mask =
	[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_VALID)
	, (TOOLTIP_ATTRIBUTE, JSONString ("You have correctly entered a " +++ typename))
	]
stdAttributesList alwaysUseCustomError typename (?Just error) optional mask
	| not $ isTouched mask =
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_INFO)
		, ( TOOLTIP_ATTRIBUTE
		  , JSONString ("Please enter a " +++ typename +++ if optional "" " (this value is required)")
		  )
		]
	| isCompound mask && not alwaysUseCustomError =
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_INVALID)
		, (TOOLTIP_ATTRIBUTE, JSONString ("You need to enter a "+++ typename +++ " (this value is required)"))
		]
	| otherwise =
		[ (TOOLTIP_TYPE_ATTRIBUTE, JSONString TOOLTIP_TYPE_INVALID)
		, (TOOLTIP_ATTRIBUTE, JSONString error)
		]

stdAttributeChanges :: !Bool !String !Bool !(?String) !EditState !(?String) !EditState -> [UIAttributeChange]
stdAttributeChanges alwaysUseCustomError typename optional oldMbError om newMbError nm
	| needsChanges
		= [SetAttribute k v \\ (k,v) <- stdAttributesList alwaysUseCustomError typename newMbError optional nm]
		= []
where
	needsChanges
		| isNone oldMbError && isNone newMbError = False
		| isNone oldMbError || isNone newMbError = True
		// both are invalid
		# otouched = isTouched om
		# ntouched = isTouched nm
		| not otouched && not ntouched = False
		| otouched || ntouched = True
		// both are touched
		| otherwise = isCompound om <> isCompound nm

viewConstantValue :: !r !(Editor r w) -> Editor () w
viewConstantValue val e =
	mapEditorInitialValue (?Just o fromMaybe ()) $
	mapEditorRead (const val) e

ignoreEditorReads :: !(Editor rb wa) -> Editor ra wa
ignoreEditorReads editor=:{Editor|onReset=editorOnReset, onRefresh=editorOnRefresh}
	= {Editor|editor & onReset=onReset, onRefresh=onRefresh}
where
	onReset attr _ vst = editorOnReset attr ?None vst
	onRefresh new st vst = (Ok (NoChange,st,?None),vst)

mapEditorInitialValue :: !((?r) -> ?r) !(Editor r w) -> Editor r w
mapEditorInitialValue toNewMode editor=:{Editor|onReset=editorOnReset} = {Editor| editor & onReset = onReset}
where
	onReset attr mbval vst = editorOnReset attr (toNewMode mbval) vst

mapEditorRead :: !(r -> rb) !(Editor rb w) -> Editor r w
mapEditorRead tof editor=:{Editor|onReset=editorOnReset,onRefresh=editorOnRefresh}
	= {Editor| editor & onReset=onReset,onRefresh=onRefresh}
where
	onReset attr mbval vst = editorOnReset attr (tof <$> mbval) vst
	onRefresh new st vst = editorOnRefresh (tof new) st vst

/**
* Map editor reads to a different domain.
* When refreshing the current value of the modified editor is used to compute the new value
*/
mapEditorReadWithValue :: !(r -> rb) !(r w -> rb) !(Editor rb w) -> Editor r w
mapEditorReadWithValue tof tofr editor=:{Editor|onReset=editorOnReset,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset,onRefresh=onRefresh}
where
	onReset attr mbval vst = editorOnReset attr (tof <$> mbval) vst
	onRefresh new st vst = case editorWriteValue st of
		(Ok w) = editorOnRefresh (tofr new w) st vst
		(Error e) = (Error e,vst)

mapEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r wb) -> Editor r (?w)
mapEditorWriteError fromf editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= editorModifierWithStateToEditor {EditorModifierWithState|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mode vst = case editorOnReset attr mode vst of
		(Ok (ui,est,mbw),vst)
			# (mbtype,mbtooltip) = errorInfoUI ui
			# (ReplaceUI ui,st,est,mbw) = mapFromF (ReplaceUI ui,(mbtype,mbtooltip,False),est,mbw)
			= (Ok (ui,st,est,mbw),vst)
		(Error e,vst) = (Error e,vst)
	onEdit e st est vst = case editorOnEdit e est vst of
		(Ok (?Just (change, est, mbw)),vst) = (Ok $ ?Just $ mapFromF (change, st, est, mbw), vst)
		(Ok ?None,vst) = (Ok ?None,vst)
		(Error e,vst) = (Error e,vst)
	onRefresh new st est vst = case editorOnRefresh new est vst of
		(Ok (change, est, mbw),vst) = (Ok $ mapFromF (change, st, est, mbw), vst)
		(Error e,vst) = (Error e,vst)

	writeValue st est = case editorWriteValue est of
		Ok w = case fromf w of
			Ok w = Ok (?Just w)
			Error e = Ok ?None
		Error e = Error e

	mapFromF (change, st, est, ?None) = (change, st, est, ?None)
	mapFromF (change, st, est, ?Just w) = case fromf w of
		(Ok w)
			# (change,st) = restoreErrorInfo change st
			= (change, st, est, ?Just (?Just w))
		(Error e)
			# (change,st) = maskErrorInfo e change st
			= (change, st, est, ?Just ?None)

	//Track the tooltip and type attributes of the underlying editor
	errorInfoUI (UI _ attrs _) = ('DM'.get TOOLTIP_TYPE_ATTRIBUTE attrs, 'DM'.get TOOLTIP_ATTRIBUTE attrs)

	errorInfoChange NoChange = (?None,?None)
	errorInfoChange (ReplaceUI ui)
		# (typeUpd,tooltipUpd) = errorInfoUI ui
		= (?Just typeUpd,?Just tooltipUpd)
	errorInfoChange (ChangeUI attrChanges _)
		# typeUpd = foldl (check TOOLTIP_TYPE_ATTRIBUTE) ?None attrChanges
		# tooltipUpd = foldl (check TOOLTIP_ATTRIBUTE) ?None attrChanges
		= (typeUpd,tooltipUpd)
	where
		check attr cur (SetAttribute k v) = if (k == attr) (?Just (?Just v)) cur
		check attr cur (DelAttribute k) = if (k == attr) (?Just ?None) cur

	//Restore masked value if necessary and track underlying /tooltip
	restoreErrorInfo change st=:(mbtype,mbtooltip,masked)
		# (newtype,newtooltip) = errorInfoChange change
		= (mergeUIChanges (restore st) change, (fromMaybe mbtype newtype,fromMaybe mbtooltip newtooltip,False))
	where
		restore (mbtype,mbtooltip,True)
			# typeChange = maybe [DelAttribute TOOLTIP_TYPE_ATTRIBUTE] (\e -> [SetAttribute TOOLTIP_TYPE_ATTRIBUTE e]) mbtype
			# tooltipChange = maybe [DelAttribute TOOLTIP_ATTRIBUTE] (\e -> [SetAttribute TOOLTIP_ATTRIBUTE e]) mbtooltip
			= ChangeUI (typeChange ++ tooltipChange) []
		restore _
			= NoChange

	maskErrorInfo msg change (mbtype,mbtooltip,masked)
		# (newtype,newtooltip) = errorInfoChange change
		= (mergeUIChanges change (mask msg), (fromMaybe mbtype newtype, fromMaybe mbtooltip newtooltip, True))
	where
		mask msg = ChangeUI
			[ SetAttribute TOOLTIP_TYPE_ATTRIBUTE (JSONString TOOLTIP_TYPE_INVALID)
			, SetAttribute TOOLTIP_ATTRIBUTE (JSONString msg)
			]
			[]

mapEditorWrite :: !(wb -> w) !(Editor r wb) -> Editor r w
mapEditorWrite fromf editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset, onEdit=onEdit,onRefresh=onRefresh,writeValue = writeValue}
where
	onReset attr mbval vst = case editorOnReset attr mbval vst of
		(Ok (ui,st,mbw),vst) = (Ok (ui,st,fmap fromf mbw),vst)
		(Error e,vst) = (Error e,vst)

	onEdit e st vst = case editorOnEdit e st vst of
		(Ok (?Just (ui,st,mbw)),vst) = (Ok (?Just (ui,st,fmap fromf mbw)),vst)
		(Ok ?None,vst) = (Ok ?None, vst)
		(Error e,vst) = (Error e,vst)

	onRefresh new st vst = case editorOnRefresh new st vst of
		(Ok (ui,st,mbw),vst) = (Ok (ui,st,fmap fromf mbw),vst)
		(Error e,vst) = (Error e,vst)

	writeValue st = case editorWriteValue st of
		Ok w = Ok (fromf w)
		Error e = Error e

loopbackEditorWrite :: !(w -> ?r) !(Editor r w) -> Editor r w
loopbackEditorWrite loopback editor=:{Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= {Editor| editor & onReset=onReset, onEdit=onEdit,onRefresh=onRefresh,writeValue = writeValue}
where
	onReset attr mbval vst = case editorOnReset attr mbval vst of
		(Ok (ui,st,?None),vst) = (Ok (ui,st,?None),vst)
		(Ok (ui,st,?Just w),vst) = case loopback w of
			?Just r = case onRefresh r st vst of
				(Ok (change,st,mbw),vst) = (Ok (applyUIChange change ui,st,mbw),vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (ui,st,?Just w),vst)
		(Error e,vst) = (Error e,vst)

	onEdit e st vst = case editorOnEdit e st vst of
		(Ok ?None,vst) = (Ok ?None,vst)
		(Ok (?Just (ui,st,?None)),vst) = (Ok (?Just (ui,st,?None)),vst)
		(Ok (?Just (change1,st,?Just w)),vst) = case loopback w of
			?Just r = case onRefresh r st vst of
				(Ok (change2,st,mbw),vst) = (Ok (?Just (mergeUIChanges change1 change2,st,mbw)),vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (?Just (change1,st,?Just w)),vst)
		(Error e,vst) = (Error e,vst)

	onRefresh new st vst = case editorOnRefresh new st vst of
		(Ok (ui,st,?None),vst) = (Ok (ui,st,?None),vst)
		(Ok (change1,st,?Just w),vst) = case loopback w of
			?Just r = case onRefresh r st vst of
				(Ok (change2,st,mbw),vst) = (Ok (mergeUIChanges change1 change2,st,mbw),vst)
				(Error e,vst) = (Error e,vst)
			?None = (Ok (change1,st,?Just w),vst)
		(Error e,vst) = (Error e,vst)

	writeValue st = case editorWriteValue st of
		Ok w = Ok w
		Error e = Error e

mapEditorWithState :: !s !(r s -> (?rb,s)) !(wb s -> (w,s)) !(Editor rb wb) -> Editor r w | TC s
mapEditorWithState init onread onwrite {Editor|onReset=editorOnReset,onEdit=editorOnEdit,onRefresh=editorOnRefresh,writeValue=editorWriteValue}
	= editorModifierWithStateToEditor
		{EditorModifierWithState|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst
		# (mbval,s) = case mbval of
			?None = (?None, init)
			?Just r = onread r init
		= appFst (fmap (\(ui, st, mbw) ->
			let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
				(ui, s`, st, mbw`)))
		$ editorOnReset attr mbval vst

	onEdit event s st vst
		= appFst (fmap $ fmap (\(ui, st, mbw) ->
			let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
				(ui, s`, st, mbw`)))
		$ editorOnEdit event st vst

	onRefresh r s st vst
		= case onread r s of
			(?None,s) = (Ok (NoChange, s, st,?None),vst)
			(?Just r,s)
				= appFst (fmap (\(ui, st, mbw) ->
					let (mbw`,s`) = maybe (?None,s) (\w -> appFst ?Just $ onwrite w s) mbw in
						(ui, s`, st, mbw`)))
				$ editorOnRefresh r st vst
	writeValue s st = case editorWriteValue st of
		Ok w = let (w`,s`) = onwrite w s in Ok w` //FIXME: We can't store s`
		Error e = Error e
