implementation module iTasks.Extensions.Document

import iTasks.WF.Definition
import iTasks.UI.Editor.Controls, iTasks.UI.Editor.Modifiers

import iTasks.Internal.Task, iTasks.Internal.IWorld, iTasks.Internal.TaskState
import StdEnv

import Text.GenJSON, Text.Encodings.MIME, Text.HTML, System.FilePath, System.File, System.OSError
import Data.Error, Data.Func, Data.Functor
import Data.Maybe
from StdFunc import const

CHUNK_SIZE :== 1048576 // 1M

//* Documents
gText{|Document|} _ (?Just val)
	| val.Document.size == 0 = ["No Document"]
	| otherwise              = [val.Document.name]
gText{|Document|} _ ?None    = [""]


gEditor{|Document|} ViewValue = mapEditorWrite (const ?None) $ mapEditorRead toView htmlView
where
	toView {Document|contentUrl,name} = ATag [HrefAttr contentUrl, TargetAttr "_blank"] [Text name]

gEditor{|Document|} EditValue = mapEditorWrite (fmap fromView) $ mapEditorRead toView documentField
where
	toView {Document|documentId,contentUrl,name,mime,size} = (documentId,contentUrl,name,mime,size)
	fromView (documentId,contentUrl,name,mime,size) = {Document|documentId=documentId,contentUrl=contentUrl,name=name,mime=mime,size=size}

derive JSONEncode		Document
derive JSONDecode		Document
derive gEq				Document

instance toString Document
where
	toString doc = ""
	
instance == Document
where
	(==) doc0 doc1 = doc0.documentId == doc1.documentId

instance toString FileException
where
	toString (FileException path error) = case error of
		CannotOpen	= "Cannot open file '" +++ path +++ "'"
		CannotClose	= "Cannot close file '" +++ path +++ "'"
		IOError		= "Error reading/writing file '" +++ path +++ "'"

derive class iTask	FileException, FileError

importDocument :: !Bool !FilePath -> Task Document
importDocument delete filename = mkInstantTask eval
where
	eval taskId iworld =  readDocument taskId filename iworld
	
	readDocument taskId filename iworld=:{IWorld|current={taskTime},world}
	# (ok,file,world)	= fopen filename FReadData world
	| not ok			= (openException filename,{IWorld|iworld & world = world})
	# (content,file)	= readAll file
	# (ok,world)		= fclose file world
	| not ok				= (closeException filename,{IWorld|iworld & world = world})
	# name					= dropDirectory filename
	# mime					= extensionToMimeType (takeExtension name)
	# (mbDocument,iworld)	= createDocument name mime content {IWorld|iworld & world = world}
	| isError mbDocument = let e = fromError mbDocument in (Error (dynamic e, toString e), iworld)
	# (mbErr,world) = if delete (deleteFile filename iworld.world) (Ok (), iworld.world)
	# iworld & world = world
	| isError mbErr
		= let e = fromError mbErr in (Error (dynamic e, snd e), iworld)
		= (Ok (fromOk mbDocument), iworld)

	readAll file
	# (chunk,file) = freads file CHUNK_SIZE
	| size chunk < CHUNK_SIZE
		= (chunk,file)
	| otherwise
		# (rest,file) = readAll file
		= (chunk +++ rest,file)


exportDocument :: !FilePath !Document -> Task Document
exportDocument filename document = mkInstantTask eval
where
	eval taskId iworld = writeDocument taskId filename document iworld

writeDocument taskId filename document iworld
	# (mbContent,iworld=:{IWorld|current={taskTime},world})
							= loadDocumentContent document.Document.documentId iworld
	| isNone mbContent		= (ioException filename, {IWorld|iworld & world = world})
	# (ok,file,world)		= fopen filename FWriteData world
	| not ok				= (openException filename,{IWorld|iworld & world = world})
	# file					= fwrites (fromJust mbContent) file
	# (ok,world)			= fclose file world
	| not ok				= (closeException filename,{IWorld|iworld & world = world})	
	= (Ok document, {IWorld|iworld & world = world})

ioException s
	# e = FileException s IOError
	= Error (dynamic e, toString e)
openException s	
	# e = FileException s CannotOpen
	= Error (dynamic e, toString e)
closeException s
	# e = FileException s CannotClose
	= Error (dynamic e, toString e)

deleteDocument :: !Document -> Task ()
deleteDocument document = mkInstantTask eval
where
	eval taskId iworld
		# (_,iworld) = deleteDocumentShares document.documentId iworld
		= (Ok (), iworld)
