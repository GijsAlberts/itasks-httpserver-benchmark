implementation module iTasks.Extensions.EmailAddress

import StdEnv

import qualified Control.Monad
import Data.Func
import Data.Functor
import Text.HTML

import iTasks

instance toString EmailAddress
where
	toString (EmailAddress email) = email

instance html EmailAddress
where
	html (EmailAddress email) = ATag [HrefAttr ("mailto:" +++ email)] [Text email]

derive class iTask \ gText, gEditor EmailAddress

gText{|EmailAddress|} _ x = gTextWithToString x

gEditor{|EmailAddress|} purpose = mapEditorRead toString (editor purpose)
where
	editor ViewValue = mapEditorWrite (const ?None) textView
	editor EditValue =
		mapEditorWrite ('Control.Monad'.join) $
		withDynamicHintAttributesWithError "email address" $
		mapEditorWrite checkEmailAddress textField

	// Very crude checker
	checkEmailAddress :: !(?String) -> MaybeError String (?EmailAddress)
	checkEmailAddress ?None = Ok ?None
	checkEmailAddress (?Just address) = checkUserName 0
	where
		checkUserName i
			| i >= size address
				= Error "email addresses must contain the '@' symbol"
			| address.[i] == '@'
				| i == 0
					= Error "this email address does not contain a user name"
					= checkDomainName True (i+1)
			| isValid address.[i]
				= checkUserName (i+1)
				= Error ("the character '"+++{#address.[i]}+++"' is not allowed")
		checkDomainName first_call i
			| i >= size address
				= Error "the domain name must contain a period ('.')"
			| address.[i] == '.'
				| first_call
					= Error "the domain name may not start with a period"
					= checkTLD True (i+1)
			| isValid address.[i]
				= checkDomainName False (i+1)
				= Error ("the character '"+++{#address.[i]}+++"' is not allowed")
		checkTLD first_call i
			| i >= size address
				| first_call
					= Error "the domain name may not end with a period"
					= Ok (?Just (EmailAddress address))
			| address.[i] == '.'
				| first_call
					= Error "the domain name may not contain two consecutive periods"
					= checkTLD True (i+1)
			| isValid address.[i]
				= checkTLD False (i+1)
				= Error ("the character '"+++{#address.[i]}+++"' is not allowed")
		isValid c = ' ' < c && c <= '~'
