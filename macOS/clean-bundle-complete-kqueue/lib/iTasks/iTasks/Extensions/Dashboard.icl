implementation module iTasks.Extensions.Dashboard
import iTasks
import iTasks.UI.Editor, iTasks.UI.Definition
import qualified Data.Map as DM, Data.Error
import Text.HTML, StdMisc, StdArray, Data.Func
import ABC.Interpreter.JavaScript

derive JSONEncode ControlLight
derive JSONDecode ControlLight
derive gEq ControlLight
derive gDefault ControlLight
derive gText ControlLight

gEditor{|ControlLight|} _ = controlLightEditlet

//SVG Based fake control light
controlLightEditlet :: Editor ControlLight (?ControlLight)
controlLightEditlet = mapEditorWrite (const ?None) $ withClientSideInit initUI $ leafEditorToEditor
	{ LeafEditor
	| onReset    = onReset
	, onEdit     = \() m vst -> (Error "unexpected edit event",vst)
	, onRefresh  = \val st vst -> (Ok (if (writeValue st === Ok val) NoChange (ChangeUI [SetAttribute "value" (JSONString (color val))] []),st,?None),vst)
	, writeValue = writeValue
	}
where
	onReset attr mbval world
		# val = fromMaybe LightOff mbval
		# attr = 'DM'.unions [sizeAttr (ExactSize 20) (ExactSize 20),valueAttr (JSONString (toString (svgLight (color val)))), attr]
		= (Ok (uia UIHtmlView attr,val,?None), world)

    initUI _ me world 
		# (jsOnAttributeChange,world) = jsWrapFun (onAttributeChange me) me world
		# world = (me .# "onAttributeChange" .= jsOnAttributeChange) world
		= world

	writeValue :: ControlLight -> *MaybeErrorString ControlLight
	writeValue s = Ok s

	onAttributeChange me {[0]=name,[1]=value} world = case jsValToString name of
		?Just "value"
			# color = fromJS "" value
			= (me .# "domEl.children" .# 0 .# "children" .# 1 .# "setAttribute" .$! ("fill",color)) world
		_
			= jsTrace "Unknown attribute change" world

    color LightOnGreen  = "green"
    color LightOnRed    = "red"
    color LightOnOrange = "orange"
    color _             = "#333"

    svgLight val = SvgTag [StyleAttr "flex: 1; align-self: stretch;"] [ViewBoxAttr "0" "0" "100" "100"]
                          [defs,light val,glass,flare]

    defs  = DefsElt [] [] [glassgr,flaregr]
    where
    	glassgr = RadialGradientElt [IdAttr "glass-gradient"] []
				   [StopElt [] [OffsetAttr "0%",StopColorAttr "white"],StopElt [] [OffsetAttr "100%",StopColorAttr "white",StopOpacityAttr "0"]]
    	flaregr = LinearGradientElt [IdAttr "flare-gradient"] [X1Attr (SVGLength "0" PX),X2Attr (SVGLength "0" PX),Y1Attr (SVGLength "0" PX),Y2Attr (SVGLength "1" PX)] 
                   [StopElt [] [OffsetAttr "0%",StopColorAttr "white"],StopElt [] [OffsetAttr "90%",StopColorAttr "white",StopOpacityAttr "0"]]

    light val = CircleElt [] [CxAttr (SVGLength "50" PX),CyAttr (SVGLength "50" PX),RAttr (SVGLength "45" PX),FillAttr (PaintColor (SVGColorText val) ?None)]
    glass = CircleElt [StyleAttr "stroke: #000;stroke-width: 8px"] [FillAttr (PaintFuncIRI (IRI ("#glass-gradient")) ?None),CxAttr (SVGLength "50" PX),CyAttr (SVGLength "50" PX),RAttr (SVGLength "45" PX)]
    flare = EllipseElt [] [FillAttr (PaintFuncIRI (IRI ("#flare-gradient")) ?None),CxAttr (SVGLength "50" PX),CyAttr (SVGLength "45" PX),RxAttr (SVGLength "35" PX),RyAttr (SVGLength "30" PX)]
