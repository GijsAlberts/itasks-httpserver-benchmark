module example

import iTasks

Start w = doTasks addTwoNumbers w

:: IntPair = {firstInteger :: Int, secondInteger :: Int}
derive class iTask IntPair

addTwoNumbers :: Task Int
addTwoNumbers = 
    ApplyLayout frameCompact @>> 
    Title "Enter two integers" @>> 
    enterInformation [] >>* 
        [   OnAction 
            (Action "View the sum") 
            (
                hasValue \{IntPair|firstInteger, secondInteger} -> 
                viewInformation [] (firstInteger + secondInteger)
                <<@ Title "The sum of the numbers is:"
            ) 
        ]
