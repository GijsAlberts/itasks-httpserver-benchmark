definition module iTasks.Testing.Selenium.Gast

from ABC.Interpreter.JavaScript import :: JSVal
from ABC.Interpreter.JavaScript.Monad import :: JS

from Gast.Gen import :: GenState, generic ggen

from iTasks.Internal.Generic.Visualization import :: TextFormat, generic gText
from iTasks.Testing.Selenium.Interface import :: ClientTestOptions, :: TestState

/**
 * Run a test multiple times, using Gast to generate values.
 *
 * These are folded into a Promise structure, so the test function may return
 * either a value or a promise.
 *
 * @param The number of times the test will be run.
 * @param To generate values. Use Gast's {{`genState`}} if unsure what to do.
 * @param A filter to optionally exclude values; use `const True` to disable.
 * @param The function to perform for each generated value.
 */
withGast
	:: !Int !GenState !(a -> Bool) !(a -> JS TestState JSVal)
	-> JS TestState JSVal
	| ggen{|*|}, gText{|*|} a
