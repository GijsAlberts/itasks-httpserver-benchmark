implementation module iTasks.Internal.IWorld

import StdEnv
from StdFunc import seqList, :: St

from ABC.Interpreter import prepare_prelinked_interpretation, :: PrelinkedInterpretationEnvironment
from TCPIP import :: TCP_Listener, :: TCP_Listener_, :: TCP_RChannel_, :: TCP_SChannel_, :: TCP_DuplexChannel, :: DuplexChannel, :: IPAddress, :: ByteSeq

import Data.Func
import Data.Integer
from Data.Map import :: Map
import qualified Data.Map as DM
import Data.Maybe
import Math.Random
import StdOverloadedList
import System.CommandLine
import System.Directory
import System.File
import System.FilePath
import System.OS
import System.Signal
import symbols_in_program

import iTasks.Engine
import iTasks.Extensions.DateTime
import iTasks.Internal.Task
import iTasks.Internal.TaskEval
import iTasks.Internal.Util
import iTasks.SDS.Combinators.Common
import iTasks.SDS.Combinators.Core
import iTasks.WF.Definition
import iTasks.WF.Derives
import iTasks.Internal.TaskServer

createIWorld :: !EngineOptions !*World -> Either (!String, !*World) *IWorld
createIWorld options world
	# (ts=:{tv_nsec=seed}, world) = nsTime world
	# (mbAbcEnv,           world) = prepare_prelinked_interpretation options.appPath options.byteCodePath world
	# (symbols,            world) = if (isNone options.distributed) ({}, world) (initSymbols options world)
	= case mbAbcEnv of
		?Just abcEnv = Right
			{ IWorld
			| options = options
			, clock   = ts
			, clockDependencies = [|]
			, current =
				{ TaskEvalState
				| taskTime        = 0
				, taskInstance    = 0
				, sessionInstance = ?None
				, attachmentChain = []
				, nextTaskNo      = 0
				}
			, sdsNotifyRequests   = 'DM'.newMap
			, sdsNotifyReqsByTask = 'DM'.newMap
			, memoryShares        = 'DM'.newMap
			, readCache           = 'DM'.newMap
			, writeCache          = 'DM'.newMap
			, abcInterpreterEnv   = abcEnv
			, shutdown            = ?None
			, ioTasks             = {done = [], todo = []}
			, ioStates            = 'DM'.newMap
			, world               = world
			, signalHandlers      = []
			, resources           = []
			, random              = genRandInt seed
			, symbols             = symbols
			, onClient            = False
			}
		?None =
			Left ("Failed to parse bytecode, is ByteCode set in the project file?", world)
where
	initSymbols {appPath} world
		# (symbols, world) = accFiles (read_symbols appPath) world
		# world = if (size symbols == 0) (showErr ["No symbols found, did you compile with GenerateSymbolTable: True?. Async tasks and shares will probably not work."] world) world
		= (symbols, world)

// Determines the server executables path
determineAppPath :: !*World -> (!FilePath, !*World)
determineAppPath world
	# ([arg:_],world) = getCommandLine world 
	| dropDirectory arg <> "ConsoleClient.exe"	= toCanonicalPath arg world
	//Using dynamic linker:	
	# (res, world)				= getCurrentDirectory world	
	| isError res				= abort "Cannot get current directory."	
	# currentDirectory			= fromOk res
	# (res, world)				= readDirectory currentDirectory world	
	| isError res				= abort "Cannot read current directory."	
	# batchfiles				= [f \\ f <- fromOk res | takeExtension f == "bat" ]
	| isEmpty batchfiles		= abort "No dynamic linker batch file found."	
	# (infos, world)			= seqList (map getFileInfo batchfiles) world	
	| any isError infos	 		= abort "Cannot get file information."	
	= (currentDirectory </> (fst o hd o sortBy cmpFileTime) (zip2 batchfiles infos), world)	
	where		
		cmpFileTime (_,Ok {FileInfo | lastModifiedTime = x})
					(_,Ok {FileInfo | lastModifiedTime = y}) = x > y

destroyIWorld :: !*IWorld -> *World
destroyIWorld iworld=:{IWorld|world} = world

computeNextFire :: !Timespec !(ClockParameter Timespec) -> Timespec
computeNextFire regTime p=:{start,interval}
	// If interval is zero, just wait for start
	| interval == zero
		= start
	// Start hasn't passed, just wait for start
	| start > regTime
		= start
	// Number of complete intervals that have passed since the registration time
	#! intervalsPassed = divTs (regTime - start) interval
	// Add intervalsPassed+1 intervals to start, that'll be the next fire time
	= start + scaleTs (intervalsPassed+1) interval
where
	//* Floor division of two timespecs
	divTs x y = max zero $ (IF_INT_64_OR_32 intDivTs integerDivTs) x y
	where
		integerDivTs :: !Timespec !Timespec -> Int
		integerDivTs l r = toInt (toNS l / toNS r)
		where
			toNS x = toInteger x.tv_sec*nanofactorInteger+toInteger x.tv_nsec

		//* Floor division, number of r that fit completely in l (only on 64-bit)
		intDivTs :: !Timespec !Timespec -> Int
		intDivTs l r = toNS l / toNS r
		where
			toNS x = x.tv_sec*nanofactorInt+x.tv_nsec

	//* Scale a timespec with an int
	scaleTs = IF_INT_64_OR_32 intScaleTs integerScaleTs
	where
		integerScaleTs :: !Int !Timespec -> Timespec
		integerScaleTs s x
			# nsec = toInteger x.tv_nsec*toInteger s
			= { tv_sec  = toInt $ toInteger x.tv_sec*toInteger s+nsec/nanofactorInteger
			  , tv_nsec = toInt $ nsec rem nanofactorInteger
			  }

		intScaleTs :: !Int !Timespec -> Timespec
		intScaleTs s x
			# nsec = x.tv_nsec*s
			= { tv_sec  = x.tv_sec*s+nsec/nanofactorInt
			  , tv_nsec = nsec rem nanofactorInt
			  }

nanofactorInt :: Int
nanofactorInt = 1000000000

nanofactorInteger :: Integer
nanofactorInteger =: toInteger 1000000000

:: SDSWithRegisterHook p r w =
	{ hook   :: !p TaskId *IWorld -> *(MaybeError TaskException (), *IWorld)
	, source :: !SDSSource p r w
	}

instance Identifiable SDSWithRegisterHook
where
	sdsIdentity {source} = sdsIdentity source

instance Readable SDSWithRegisterHook
where
	readSDS {source} p c iworld = readSDS source p c iworld

instance Writeable SDSWithRegisterHook
where
	writeSDS {source} p c w iworld = writeSDS source p c w iworld

instance Modifiable SDSWithRegisterHook
where
	modifySDS f {source} p c iworld = modifySDS f source p c iworld

instance Registrable SDSWithRegisterHook
where
	readRegisterSDS {hook,source} p c t r iworld
		= case hook p t iworld of
			(Ok _, iworld) = readRegisterSDS source p c t r iworld
			(Error e, iworld) = (ReadException e, iworld)

iworldTimespec :: SDSBox (ClockParameter Timespec) Timespec Timespec
iworldTimespec =: SDSBox
	{ hook   = register
	, source = createReadWriteSDS "IWorld" "timespec" read write
	}
where
	read :: !(ClockParameter Timespec) !*IWorld -> (!MaybeError TaskException Timespec, !*IWorld)
	read _ iworld=:{IWorld|clock} = (Ok clock, iworld)

	write :: !(ClockParameter Timespec) !Timespec !*IWorld -> (!MaybeError TaskException (SDSNotifyPred (ClockParameter Timespec)), !*IWorld)
	write _ ts iworld = (Ok (pred ts), {iworld & clock=ts})

	/**
	 * @param ts Timestamp that is written
	 * @param reg Timestamp of registration
	 * @param p Clock parameter
	 */
	pred :: !Timespec !Timespec !(ClockParameter Timespec) -> Bool
	pred ts reg p = ts >= computeNextFire reg p

	register :: !(ClockParameter Timespec) !TaskId !*IWorld -> (!MaybeError TaskException (), !*IWorld)
	register p tid iworld
		# iworld & clockDependencies = Insert
			(\new old->new.nextFire < old.nextFire)
			{nextFire=computeNextFire iworld.clock p, taskId=tid}
			iworld.clockDependencies
		= (Ok (), iworld)

iworldTimestamp :: SDSLens (ClockParameter Timestamp) Timestamp ()
iworldTimestamp =: mapReadWrite (timespecToStamp, \w r -> ?None) ?None
	$ sdsTranslate "iworldTimestamp translation" (\{start,interval}->{start=timestampToSpec start,interval=timestampToSpec interval}) iworldTimespec

iworldLocalDateTime :: SDSParallel () DateTime ()
iworldLocalDateTime =: sdsParallel "iworldLocalDateTime"
    // ignore value, but use notifications for 'iworldTimestamp'
	(\p -> (p,p)) fst
	(SDSWriteConst \_ _ -> Ok ?None) (SDSWriteConst \_ _ -> Ok ?None)
	(createReadOnlySDS \_ -> iworldLocalDateTime`)
	(sdsFocus {start=Timestamp 0,interval=Timestamp 1} iworldTimestamp)

iworldLocalDateTime` :: !*IWorld -> (!DateTime, !*IWorld)
iworldLocalDateTime` iworld=:{clock={tv_sec}, world}
    # (tm, world) = toLocalTime (Timestamp tv_sec) world
    = (tmToDateTime tm, {iworld & world = world})

iworldResource :: (*Resource -> (Bool, *Resource)) *IWorld -> (*[*Resource], *IWorld)
iworldResource f iworld=:{IWorld|resources}
# (matches, resources) = splitWithUnique f resources
= (matches, {iworld & resources=resources})
where
	splitWithUnique f [] = ([], [])
	splitWithUnique f [r:rs]
	# (ok, r) = f r
	| ok = let (ms, xs) = splitWithUnique f rs in ([r:ms], xs)
	= let (ms, xs) = splitWithUnique f rs in (ms, [r:xs])

//Wrapper instance for file access
instance FileSystem IWorld
where
	fopen filename mode iworld=:{IWorld|world}
		# (ok,file,world) = fopen filename mode world
		= (ok,file,{IWorld|iworld & world = world})
	fclose file iworld=:{IWorld|world}
		# (ok,world) = fclose file world
		= (ok,{IWorld|iworld & world = world})
	stdio iworld=:{IWorld|world}
		# (io,world) = stdio world
		= (io,{IWorld|iworld & world = world})
	sfopen filename mode iworld=:{IWorld|world}
		# (ok,file,world) = sfopen filename mode world
		= (ok,file,{IWorld|iworld & world = world})

instance FileEnv IWorld
where
	accFiles accfun iworld=:{IWorld|world}
		# (x, world) = accFiles accfun world
		= (x, {IWorld | iworld & world=world})
	appFiles appfun iworld=:{IWorld|world}
		# world = appFiles appfun world
		= {IWorld | iworld & world=world}
