definition module iTasks.UI.Editor.Generic
/**
* This module provides a generic function to create editors for arbitrary types
*/
import iTasks.UI.Editor
import StdGeneric
from Text.HTML import :: HtmlTag
from Data.Error import :: MaybeError
from Data.GenEq import generic gEq
from System.Time import :: Timestamp
from Data.Integer import :: Integer
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat

/**
* Main generic editor function
*/
:: EditorPurpose = EditValue | ViewValue

generic gEditor a | gText a, gEq a *! :: !EditorPurpose -> Editor a (?a)

derive gEditor
	UNIT,
	EITHER with ex _ _ ey _ _,
	PAIR with ex _ _ ey _ _,
	OBJECT of {gtd_num_conses,gtd_conses} with ex _ _,
	CONS of {gcd_index,gcd_arity} with ex _ _,
	RECORD of {grd_arity} with ex _ _,
	FIELD of {gfd_name} with ex _ _

derive gEditor Int, Real, Char, Bool, String, [], (), (,), (,,), (,,,), (,,,,), (,,,,,), (->), Dynamic, {}, {!}
derive gEditor ?, Either, MaybeError, Map, JSONNode, HtmlTag, Timestamp, Integer
