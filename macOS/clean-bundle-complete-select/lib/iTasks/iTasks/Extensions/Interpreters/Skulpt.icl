implementation module iTasks.Extensions.Interpreters.Skulpt

import iTasks
import StdArray, Data.Func, Data.Either, Data.Tuple
import ABC.Interpreter.JavaScript
import qualified Data.Map as DM

derive class iTask SkulptInput, SkulptOutput, SkulptInterpreterState, SkulptError

SKULPT_JS :== "skulpt-1.2.0/skulpt.js"
SKULPT_STDLIB_JS :== "skulpt-1.2.0/skulpt-stdlib.js"

skulptInterpreter :: (Map String (SkulptFunction m)) m -> Editor [SkulptInput m] (SkulptOutput m) | TC m & JSONEncode{|*|}, JSONDecode{|*|} m
skulptInterpreter interfaceFunctions initialModel
	= leafEditorToEditor $ withClientSideInitOnLeafEditor initUI 
		{LeafEditor
		|onReset = onReset
		,onEdit = onEdit
		,onRefresh = onRefresh
		,writeValue = writeValue
		}
where
	onReset attr mbval vst=:{VSt|taskId,abcInterpreterEnv}
		# st = {output = [], error = ?None, state = Stopped, model = initialModel}
		# (st,changes) = applyInputs abcInterpreterEnv (fromMaybe [] mbval) st []
		# attr = foldl (\a c -> applyUIAttributeChange c a) attr changes
		= (Ok (UI UIContainer attr [], st, ?None),vst)

	initUI {FrontendEngineOptions|serverDirectory} me world
		# (jsInitDOM,world) = jsWrapFun (initDOM me) me world
		//Check if the skulpt library is loaded and either load it,
		//and delay dom initialization or set the initDOM method to continue
		//as soon as the component's DOM element is available
		# (sk, world) = jsTypeOf (jsGlobal "Sk") .?? ("undefined", world)
		| sk == "undefined"
			# world = addJSFromUrls [serverDirectory+++SKULPT_JS, serverDirectory+++SKULPT_STDLIB_JS] (?Just jsInitDOM) me world
			= world
		| otherwise
			# world = (me .# "initDOMEl" .= jsInitDOM) world
			= world

	initDOM me args world
		# (modelref,world) = jsMakeCleanReference initialModel me world
		# world = (me .# "attributes.model" .= modelref) world
		# world = (me .# "state" .= "stopped") world
		# world = (me .# "attributes.eventTimeout" .= 0) world
		# (cb,world) = jsWrapFun (\a w -> onAttributeChange me a w) me world
		# world = (me .# "onAttributeChange" .= cb) world
		# (outputcb,world) = jsWrapFun (onSkulptOut me) me world
		# (readcb,world) = jsWrapFunWithResult (onSkulptRead me) me world
		# world = (jsGlobal"Sk.configure" .$! (jsRecord ["output" :> outputcb,"read" :> readcb])) world
		# world = registerInterfaceFunctions ('DM'.toList interfaceFunctions) me world
		# (run,world) = (me .# "attributes.run") .?? (False,world)
		| run = startRun me world
		| otherwise = world

	registerInterfaceFunctions [] me world = world
	registerInterfaceFunctions [(name,function):funs] me world
		# (cb,world) = jsWrapFunWithResult (onInterfaceCall me name function) me world
		# (cbw,world) = jsNew "Sk.builtin.func" cb world
		# world = ((jsGlobal "Sk.builtins" .# name) .= cbw) world
		= registerInterfaceFunctions funs me world

	onInterfaceCall	me name fun args world
		# (model,world) = jsGetCleanReference (me .# "attributes.model") world
		| model =: ?None = abort "Failed to read model"
		# (fargs,world) = collectArguments args world
		= case fun fargs (fromJust model) of
			//Direct result
			(Left result,model)
				//Store and sync model
				# (modelref,world) = jsMakeCleanReference model me world
				# world = (me .# "attributes.model" .= modelref) world
				# world = syncModel me world
				= encodeSkulptValue result world
			//Pause 
			(Right resultfun,model)
				//Store and sync model
				# (modelref,world) = jsMakeCleanReference model me world
				# world = (me .# "attributes.model" .= modelref) world
				# world = syncModel me world
				//Create a promise which stores its resolve function on the object
				# (promiseInit,world) = jsWrapFun (storeSuspension resultfun me) me world
				# (promise,world) = jsNew "Promise" promiseInit world
				//Set the component state to 'paused'
				//Return the promise as a Skulpt suspension
				# (suspension,world) = jsNew "Sk.misceval.promiseToSuspension" promise world
				= (suspension,world)
	where
		collectArguments args world = collect 0 args world
		collect i args world
			| i >= size args = ([],world)
			# (x,world) = decodeSkulptValue args.[i] world
			# (xs,world) = collect (i + 1) args world
			= ([x:xs],world)

	decodeSkulptValue value world //More or less a port of https://skulpt.org/docs/ffi.js.html
		# (isSkulptBool,world) = (value jsInstanceOf "Sk.builtin.bool") .?? (False,world)
		| isSkulptBool
			# (v,world) = (value .# "v") .? world
			= (maybe SkulptNone SkulptBool $ jsValToBool v,world)
		# (isSkulptInt,world) = (value jsInstanceOf "Sk.builtin.int_") .?? (False,world)
		| isSkulptInt
			# (v,world) = (value .# "v") .? world
			= (maybe SkulptNone SkulptInt $ jsValToInt v,world)
		# (isSkulptFloat,world) = (value jsInstanceOf "Sk.builtin.float_") .?? (False,world)
		| isSkulptFloat
			# (v,world) = (value .# "v") .? world
			= (maybe SkulptNone SkulptFloat $ jsValToReal v,world)
		# (isSkulptString,world) = (value jsInstanceOf "Sk.builtin.str") .?? (False,world)
		| isSkulptString
			# (v,world) = (value .# "v") .? world
			= (maybe SkulptNone SkulptString $ jsValToString v,world)
		# (isSkulptList,world) = (value jsInstanceOf "Sk.builtin.list") .?? (False,world)
		| isSkulptList
			# (len,world) = (value .# "v.length") .?? (0,world)
			= appFst SkulptList $ decodeSkulptList (value .# "v") 0 len world
		# (isSkulptDict,world) = (value jsInstanceOf "Sk.builtin.dict") .?? (False,world)
		| isSkulptDict
			= appFst SkulptDict $ decodeSkulptDict value world
		= (SkulptNone,world)
	where
		decodeSkulptList list i len world
			| i >= len = ([],world)
			# (x,world) = decodeSkulptValue (list .# i) world
			# (xs,world) = decodeSkulptList list (i + 1) len world
			= ([x:xs],world)

		decodeSkulptDict dict world
			# (iterator,world) = (dict .# "tp$iter" .$ ()) world
			= decodeSkulptDict` dict iterator 'DM'.newMap world
		decodeSkulptDict` dict iterator acc world
			# (nextkey,world) = (iterator .# "tp$iternext" .$ ()) world
			| jsIsUndefined nextkey
				= (acc,world)
			| otherwise
				# (nextvalue,world) = (dict .# "mp$subscript" .$ nextkey) world
				# (key,world) = (nextkey .# "v") .? world
				# (value,world) = decodeSkulptValue nextvalue world
				# acc = maybe acc (\k -> 'DM'.put k value acc) (jsValToString key)
				= decodeSkulptDict` dict iterator acc world

	encodeSkulptValue :: SkulptValue *JSWorld -> (!JSVal,!*JSWorld)
	encodeSkulptValue SkulptNone world = (jsGlobal "Sk.builtin.none.none$", world)
	encodeSkulptValue (SkulptBool x) world = jsNew "Sk.builtin.bool" x world
	encodeSkulptValue (SkulptInt x) world = jsNew "Sk.builtin.int_" x world
	encodeSkulptValue (SkulptFloat x) world = jsNew "Sk.builtin.float_" x world
	encodeSkulptValue (SkulptString x) world = jsNew "Sk.builtin.str" x world
	encodeSkulptValue (SkulptList x) world
		# (encs,world) = mapSt encodeSkulptValue x world
		= jsNew "Sk.builtin.list" encs world
	encodeSkulptValue (SkulptDict x) world
		# (items,world) = mapSt encodeDictItem ('DM'.toList x) world
		= jsNew "Sk.builtin.dict" (flatten items) world
	where
		encodeDictItem (key,value) world
			# (value,world) = encodeSkulptValue value world
			# (key,world) = jsNew "Sk.builtin.str" key world
			= ([key,value], world)

	storeSuspension resultFun me {[0]=resolve} world
		//Store both functions as attributes on the editor component
		# (resultFunRef,world) = jsMakeCleanReference resultFun me world
		# world = (me .# "resumeResultRef" .= resultFunRef) world
		# world = (me .# "resumeResolveRef" .= resolve) world
		//Let the server know the interpreter is now paused
		# world = (me .# "state" .= "paused") world
		# world = syncState me world
		= world

	onAttributeChange me {[0]=name,[1]=value} world = case jsValToString name of
		(?Just "script") = (me .# "attributes.script" .= value) world
		(?Just "model")
			# (model,world) = jsDeserializeJSVal value world
			# (modelref,world) = jsMakeCleanReference model me world
			= (me .# "attributes.model" .= modelref) world
		(?Just "run")
			# (state,world) = (me .# "state") .? world
			= case jsValToString state of
				?Just "stopped" = startRun me world
				?Just "paused" = resumeRun me world
				_ = world
		_  = world

	startRun me world
		# (sk,world) = jsGlobal "Sk" .? world
		# (runCb,world) = jsWrapFunWithResult (doSkulptRun me) me world
		# (rejectCb,world) = jsWrapFun (onSkulptError me) me world
		# (resolveCb,world) = jsWrapFun (onSkulptComplete me) me world
		# (promise,world) = (sk .# "misceval.asyncToPromise" .$ runCb) world
		# world = (promise .# "then" .$! (resolveCb,rejectCb)) world
		= world

	doSkulptRun me _ world
		# world = ((me .# "state") .= "running") world
		# world = syncState me world
		= (jsGlobal "Sk.importMainWithBody" .$ ("<stdin>", False, me .# "attributes.script", True)) world

	resumeRun me world
		//Get model from component
		# (model,world) = jsGetCleanReference (me .# "attributes.model") world
		| model =: ?None = abort "Failed to read model"
		//Compute result based on current model
		# (resultFun,world) = jsGetCleanReference (me .# "resumeResultRef") world
		| resultFun =: ?None = abort "Failed to read resume function"
		# (result,world) = encodeSkulptValue ((fromJust resultFun) (fromJust model)) world
		//Set state back to running
		# world = (me .# "state" .= "running") world
		# world = syncState me world
		//Resolve promise
		# world = ((me .# "resumeResolveRef") .$! result) world
		= world

	onSkulptComplete me args world
		# world = ((me .# "state") .= "stopped") world
		= syncState me world

	syncState me world
		# edit = jsRecord ["state" :> (me .# "state")]
		= (me .# "doEditEvent" .$! (me .# "attributes.taskId",me .# "attributes.editorId",edit)) world

	syncModel me world
		# (model,world) = jsGetCleanReference (me .# "attributes.model") world
		| model =: ?None = abort "Failed to read model"
		# (modelref,world) = jsSerializeOnClient (fromJust model) world
		# edit = jsRecord ["model" :> modelref]
		= (me .# "doEditEvent" .$! (me .# "attributes.taskId",me .# "attributes.editorId",edit)) world

	onSkulptError me {[0]=error} world
		# (msg,world)       = (error .# "toString" .$ ()) world
		# line              = (error .# "traceback" .# 0 .# "lineno")
		# edit              = jsRecord ["message" :> msg,"line" :> line]
		# world             = (me .# "doEditEvent" .$! (me .# "attributes.taskId", me .# "attributes.editorId", edit)) world
		= world

	onSkulptRead me {[0]=modulename} world = case jsValToString modulename of
		?None = (jsNull,world)
		?Just modulename
			# (files,world) = jsGlobal "Sk.builtinFiles.files" .? world
			# (mod,world) = (files .#! modulename) .? world
			= (mod,world)

	onSkulptOut me {[0]=line} world = case jsValToString line of
		(?Just line)
			# edit  = JSONObject [("output",JSONString line)]
			# world = (me .# "doEditEvent" .$! (me .# "attributes.taskId", me .# "attributes.editorId",edit)) world
			= world
		_ = world

	onEdit (JSONObject [("model",JSONString encmodel)]) st vst=:{VSt|abcInterpreterEnv}
		# (model,_) = jsDeserializeFromClient encmodel abcInterpreterEnv
		# st = {SkulptOutput|st & model = model}
		= (Ok (NoChange,st,?Just st),vst)

	onEdit (JSONObject [("state",JSONString state)]) st vst
		# st = case state of
			"running" = {SkulptOutput|st & state = Running}
			"paused" = {SkulptOutput|st & state = Paused}
			"stopped" = {SkulptOutput|st & state = Stopped}
			_ = st
		= (Ok (NoChange,st,?Just st),vst)
	onEdit (JSONObject [("output",JSONString output)]) st vst
		# st = {SkulptOutput|st & output = st.SkulptOutput.output ++ [output]}
		= (Ok (NoChange,st,?Just st),vst)

	onEdit json st vst = case fromJSON json of
		?Just error
			# st = {SkulptOutput|st & state = Stopped, error = ?Just error}
			= (Ok (NoChange,st,?Just st),vst)
		_
			= (Ok (NoChange,st,?None),vst)

	onRefresh inputs st vst=:{VSt|abcInterpreterEnv}
		# (st,changes) = applyInputs abcInterpreterEnv inputs st []
		| changes =: []
			= (Ok (NoChange, st, ?None), vst)
		| otherwise
			= (Ok (ChangeUI changes [], st, ?None), vst)

	applyInputs abcInterpreterEnv  [SkulptSetState model:ss] st changes
		= applyInputs abcInterpreterEnv ss {SkulptOutput|st & model = model}
			[SetAttribute "model" $ JSONString $ jsSerializeGraph model abcInterpreterEnv:changes]
	applyInputs abcInterpreterEnv [SkulptSetScript script:ss] st changes
		= applyInputs abcInterpreterEnv ss st
			[SetAttribute "script" (JSONString script):changes]
	applyInputs abcInterpreterEnv [SkulptRunScript:ss] st changes
		= applyInputs abcInterpreterEnv ss {SkulptOutput|st & output=[], error = ?None}
			[SetAttribute "run" (JSONBool True):changes]
	applyInputs abcInterpreterEnv [] st changes
		= (st,reverse changes)

	writeValue st = Ok st
