implementation module iTasks.Extensions.GIS.Leaflet

import iTasks
import iTasks.UI.Definition, iTasks.UI.Editor
import StdMisc, Data.Tuple, Data.Error, Data.Func, Text, Data.Functor
from Data.List import concatMap
import qualified Data.Map as DM
import qualified Data.Set as Set
import Data.Set.GenJSON
import Text.HTML
from Text.Encodings.Base64 import base64Encode
from iTasks.UI.Editor.Common import diffChildren, :: ChildUpdate (..)
import ABC.Interpreter.JavaScript
import StdArray

LEAFLET_JS           :== "leaflet-1.3.4/leaflet.js"
LEAFLET_JS_WINDOW    :== "leaflet-window.js"
// https://github.com/Leaflet/Leaflet.Editable
LEAFLET_JS_EDITABLE  :== "Leaflet.Editable.js"
LEAFLET_CSS          :== "leaflet-1.3.4/leaflet.css"
LEAFLET_CSS_WINDOW   :== "leaflet-window.css"

:: IconOptions =
    { iconUrl   :: !String
    , iconSize  :: ![Int]
    }

derive JSONEncode IconOptions

derive gToJS MapOptions, LeafletLatLng

leafletObjectIdOf :: !LeafletObject -> LeafletObjectID
leafletObjectIdOf (Marker m)    = m.markerId
leafletObjectIdOf (Polyline p)  = p.polylineId
leafletObjectIdOf (Polygon p)   = p.polygonId
leafletObjectIdOf (Circle c)    = c.circleId
leafletObjectIdOf (Rectangle r) = r.rectangleId
leafletObjectIdOf (Window w)    = w.windowId

leafletPointsOf :: !LeafletObject -> [LeafletLatLng]
leafletPointsOf (Marker m) = [m.position]
leafletPointsOf (Polyline l) = l.LeafletPolyline.points
leafletPointsOf (Polygon p) = p.LeafletPolygon.points
leafletPointsOf (Circle c) = [c.LeafletCircle.center]
leafletPointsOf (Rectangle {LeafletRectangle | bounds=b}) = [b.southWest, b.northEast]
leafletPointsOf (Window w) = []

leafletBoundingRectangleOf :: ![LeafletObject] -> LeafletBounds
leafletBoundingRectangleOf objects
	| isEmpty points = defaultValue
	| otherwise =
		{ southWest = {lat=minList lats, lng=minList lngs}
		, northEast = {lat=maxList lats, lng=maxList lngs}
		}
where
	points = concatMap leafletPointsOf objects
	lats = [p.lat \\ p <- points]
	lngs = [p.lng \\ p <- points]

:: LeafletEdit
	= LDSetManualPerspective
	//Current state
	| LDSetZoom         !Int
	| LDSetCenter       !LeafletLatLng
	| LDSetBounds       !LeafletBounds
	//Updating windows
	| LDRemoveWindow    !LeafletObjectID
	| LDUpdateObject    !LeafletObjectID !LeafletObjectUpdate
	//Events
	| LDMapClick        !LeafletLatLng
	| LDMapDblClick     !LeafletLatLng
	| LDMarkerClick     !LeafletObjectID
	| LDHtmlEvent       !String

:: LeafletObjectUpdate
	= UpdatePolyline  ![LeafletLatLng]
	| UpdatePolygon   ![LeafletLatLng]
	| UpdateCircle    !LeafletLatLng !Real
	| UpdateRectangle !LeafletBounds

svgIconURL :: !SVGElt !(!Int,!Int) -> String
svgIconURL svgelt (width,height) = "data:image/svg+xml;base64," +++ base64Encode svg
where
    svg = concat ["<svg xmlns=\"http://www.w3.org/2000/svg\" width=\""
		, toString width, "\" height=\"", toString height, "\">", toString svgelt, "</svg>"]

openStreetMapTiles :: TileLayer
openStreetMapTiles =
	{ url         = "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
	, attribution = ?Just $ Html "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
	}

leafletEditor :: !Bool -> Editor LeafletMap LeafletMap
leafletEditor viewOnly =
	leafEditorToEditor (leafletEditor` {attributionControl = True, zoomControl = True, editable = True, viewOnly = viewOnly} (const id))

derive JSONEncode MapOptions
leafletEditor` :: !MapOptions !(JSVal *JSWorld -> *JSWorld) -> LeafEditor [LeafletEdit] LeafletMap LeafletMap LeafletMap
leafletEditor` mapOptions postInitUI = withClientSideInitOnLeafEditor initUI
	{ LeafEditor
    | onReset        = onReset
    , onEdit         = onEdit
    , onRefresh      = onRefresh
    , writeValue     = writeValue
    }
where
	onReset attr mbval vst
		# val=:{LeafletMap|perspective,tilesUrls,objects,icons} = fromMaybe gDefault{|*|} mbval
		# mapAttr = 'DM'.fromList
			[("perspective", encodePerspective perspective)
			,("tilesUrls"
			 , JSONArray $
				(\tile ->
					JSONObject
						[ ("url", toJSON tile.url)
						: maybe [] (\attr -> [("attribution", toJSON $ toString attr)]) tile.attribution
						]
				) <$>
					tilesUrls
			 )
			,("icons", JSONArray [toJSON (iconId,{IconOptions|iconUrl=iconUrl,iconSize=[w,h]}) \\ {iconId,iconUrl,iconSize=(w,h)} <- icons])
			:[("fitbounds", attr) \\ attr <- fitBoundsAttribute val]
			]
		# attr = 'DM'.unions
			[ mapAttr
			, sizeAttr (ExactSize 500) (ExactSize 150)
			, attr
			]
		# children = map encodeUI objects
		= (Ok (uiac UIHtmlView attr children, val, ?None), vst)

	encodePerspective :: !LeafletPerspective -> JSONNode
	encodePerspective (CenterAndZoom center zoom) = JSONArray
		[ JSONString "CenterAndZoom"
		, JSONArray [JSONReal center.lat, JSONReal center.lng]
		, JSONInt zoom
		]
	encodePerspective (FitToBounds options _) = JSONArray
		[ JSONString "FitToBounds"
		, toJSON options
		]

	encodeUI (Marker o) = let (JSONObject attr) = toJSON o
                              dataMap = 'DM'.fromList [("type",JSONString "marker"):attr]
                              // translate HtmlTag of popup to HTML code
                              dataMap` = case o.popup of
                                  ?None = dataMap
                                  ?Just html = 'DM'.put "popup" (JSONString (toString html)) dataMap
                          in uia UIData dataMap`
	encodeUI (Polyline o) = let (JSONObject attr) = toJSON o in uia UIData ('DM'.fromList [("type",JSONString "polyline"):attr])
	encodeUI (Polygon o) = let (JSONObject attr) = toJSON o in uia UIData ('DM'.fromList [("type",JSONString "polygon") : attr])
	encodeUI (Circle o) = let (JSONObject attr) = toJSON o in uia UIData ('DM'.fromList [("type",JSONString "circle"): attr])
	encodeUI (Rectangle o) = let (JSONObject attr) = toJSON o in uia UIData ('DM'.fromList [("type",JSONString "rectangle") : attr])
    encodeUI (Window o) = let (JSONObject attr) = toJSON o
                              dataMap = 'DM'.fromList [("type",JSONString "window"): attr]
                              // translate HtmlTag to HTML code
                              dataMap` = 'DM'.put "content" (JSONString (toString o.content)) dataMap
                          in uia UIData dataMap`

	initUI {FrontendEngineOptions|serverDirectory} me world
		# (jsInitDOM,world) = jsWrapFun (initDOM me) me world
		//Check if the leaflet library is loaded and either load it,
		//and delay dom initialization or set the initDOM method to continue
		//as soon as the component's DOM element is available
        # (l, world) = jsTypeOf (jsGlobal "L") .?? ("undefined", world)
        | l == "undefined"
            # world = addCSSFromUrl (serverDirectory+++LEAFLET_CSS_WINDOW) ?None world
			# (cb,world) = jsWrapFun (loadJS serverDirectory jsInitDOM) me world
            # world = addCSSFromUrl (serverDirectory+++LEAFLET_CSS) (?Just cb) world
			= world
		| otherwise
			# world = (me .# "initDOMEl" .= jsInitDOM) world
			= world

	loadJS serverDirectory jsInitDOM _ world
		# world = addJSFromUrl (serverDirectory+++LEAFLET_JS) ?None world
		# world = addJSFromUrl (serverDirectory+++LEAFLET_JS_EDITABLE) ?None world
		# world = addJSFromUrl (serverDirectory+++LEAFLET_JS_WINDOW) (?Just jsInitDOM) world
		= world

	initDOM me args world
        # (l,world)         = jsGlobal "L" .? world
		# (domEl,world) 	= me .# "domEl" .? world
		//Create the map
		# (mapObj,world)    = (l .# "map" .$ (domEl,mapOptions)) world
		# world 			= (me .# "map" .= mapObj) world
		//Set perspective
		# world             = setMapPerspective me (me .# "attributes.perspective") world
        //Add icons
		# world             = setMapIcons me mapObj (me .# "attributes.icons") world
		//Create tile layer
		# (tilesUrls,world) = me .# "attributes.tilesUrls" .? world
		# world             = forall (addMapTilesLayer me mapObj) tilesUrls world
		//Synchronize lat/lng bounds to server (they depend on the size of the map in the browser)
		# world             = syncCurrentState me world
        //Add initial objects
		# (objects,world)   = me .# "children" .? world
		# world             = createMapObjects mapOptions.MapOptions.viewOnly me mapObj objects world
		//Add event handlers
		# (cb,world)       = jsWrapFun (\a w -> onResize me w) me world
		# world            = (me .# "onResize" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onShow me w) me world
		# world            = (me .# "onShow" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onAttributeChange me a w) me world
		# world            = (me .# "onAttributeChange" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onAfterChildInsert mapOptions.MapOptions.viewOnly me a w) me world
		# world            = (me .# "afterChildInsert" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onBeforeChildRemove me a w) me world
		# world            = (me .# "beforeChildRemove" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onViewportChange me w) me world
		# world            = (me .# "onViewportChange" .= cb) world
		# (vp,world)       = (me .# "getViewport" .$ ()) world
		# world            = (vp .# "addChangeListener" .$! me) world
		# (cb,world)       = jsWrapFun (\a w -> beforeRemove me w) me world
		# world            = (me .# "beforeRemove" .= cb) world
		# (cb,world)       = jsWrapFun (\a w -> onHtmlEvent me a w) me world
		# world            = (me .# "onHtmlEvent" .= cb) world
		# world = case mapOptions.MapOptions.viewOnly of
            True
				= world
			False
				# (cb,world)       = jsWrapFun (onMapDragEnd me) me world
				# world            = (mapObj .# "addEventListener" .$! ("dragend",cb)) world
				# (cb,world)       = jsWrapFun (onMapMoveEnd me) me world
				# world            = (mapObj .# "addEventListener" .$! ("moveend",cb)) world
				# (cb,world)       = jsWrapFun (onMapZoomEnd me) me world
				# world            = (mapObj .# "addEventListener" .$! ("zoomend",cb)) world
				# (cb,world)       = jsWrapFun (onMapClick False me) me world
				# world            = (mapObj .# "addEventListener" .$! ("click",cb)) world
				# (cb,world)       = jsWrapFun (onMapClick True me) me world
				# world            = (mapObj .# "addEventListener" .$! ("dblclick",cb)) world
				= world
		# world = postInitUI mapObj world
		= world

	syncCurrentState me world
		| mapOptions.MapOptions.viewOnly
			= world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		# mapObj            = me .# "map"
		# (bounds,world)    = getMapBounds mapObj world
		# (center,world)    = getMapCenter mapObj world
		# (zoom,world)      = getMapZoom mapObj world
		# edit              = toJSON [LDSetBounds bounds,LDSetCenter center,LDSetZoom zoom]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world

	onResize me world
		# (mapObj,world) 	= me .# "map" .? world
        # world             = (mapObj .# "invalidateSize" .$! ()) world
		= world

	onShow me world
		# (mapObj,world)    = me .# "map" .? world
		# world             = (mapObj .# "invalidateSize" .$! ()) world
		# world             = setMapPerspective me (me .# "attributes.perspective") world
		= world

	onMapDragEnd me args world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		// A drag also fires a zoomend event, so there are also LDSetCenter and
		// LDSetBounds edits from onMapMoveEnd below. However, zoomend is fired
		// after dragend, and we need the center and bounds to be set *before*
		// doing LDSetManualPerspective.
		# (center,world)    = getMapCenter (me .# "map") world
		# (bounds,world)    = getMapBounds (me .# "map") world
		# edit              = toJSON [LDSetCenter center,LDSetBounds bounds,LDSetManualPerspective]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world

	onMapMoveEnd me args world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		# (center,world)    = getMapCenter (me .# "map") world
		# (bounds,world)    = getMapBounds (me .# "map") world
		# edit              = toJSON [LDSetCenter center,LDSetBounds bounds]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		# world             = (me .# "attributes.ignorezoomend" .= False) world
		= world

	onMapZoomEnd me args world
		# (ignore,world)    = me .# "attributes.ignorezoomend" .?? (False, world)
		# world             = (me .# "attributes.ignorezoomend" .= False) world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		# (mapObj,world)    = args.[0] .# "target" .? world
		# (center,world)    = getMapCenter mapObj world
		# (zoom,world)      = getMapZoom mapObj world
		# (bounds,world)    = getMapBounds mapObj world
		# edit              = toJSON
			[ LDSetCenter center
			, LDSetZoom zoom
			, LDSetBounds bounds
			: if ignore [] [LDSetManualPerspective]
			]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world

	onMapClick double me args world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
        # (mapObj,world)    = args.[0] .# "target" .? world
        # (clickPos,world)  = args.[0] .# "latlng" .? world
		# (position,world)  = toLatLng clickPos world
		# edit              = toJSON [if double LDMapDblClick LDMapClick position]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world

	onMarkerClick me markerId args world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		# edit              = toJSON [LDMarkerClick markerId]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world

	onAttributeChange me args world = case fromJS "" args.[0] of
		"perspective" -> setMapPerspective me args.[1] world
		"icons"       -> setMapIcons me (me .# "map") args.[1] world
		"fitbounds"   -> fitBounds me args.[1] (me .# "attributes.perspective" .# 1) world
		_             -> jsTrace "unknown attribute change" world

	onHtmlEvent me args world
		# (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		= case (jsValToString args.[0]) of
			?Just event
				# edit = toJSON [LDHtmlEvent event]
				# world = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
				= world
			_	= world


	onAfterChildInsert viewMode me args world
		# (l, world)      	= jsGlobal "L" .? world
		# (mapObj,world)    = me .# "map" .? world
		= createMapObject viewMode me mapObj l args.[1] world

	onBeforeChildRemove me args world
		# (layer,world)     = args.[1] .# "layer" .? world
		//If there is an attached popup remove it first
		# world = removePopup layer world
		// for windows, based on control class
		# (removeMethod, world) = layer .# "remove" .? world
		| not (jsIsUndefined removeMethod) = (layer .# "remove" .$! ()) world
		// for all other objects
		# (mapObj,world)    = me .# "map" .? world
		# world             = (mapObj.# "removeLayer" .$! layer) world
		= world
	where
		removePopup layer world
			# (popup, world)    = layer .# "myPopup" .? world
			| jsIsUndefined popup = world
			# (mapObj,world)    = me .# "map" .? world
			= (mapObj.# "removeLayer" .$! popup) world

	onViewportChange me world
		# (mapObj,world) 	= me .# "map" .? world
		# world             = (mapObj .# "invalidateSize" .$! ()) world
		= world

	beforeRemove me world
		# (vp,world) = (me .# "getViewport" .$ ()) world
		# world      = (vp .# "removeChangeListener" .$! me) world
		# world      = (me .# "map" .# "remove" .$! ()) world
		= world

    onWindowRemove me windowId _ world
        // remove children from iTasks component
        # (children,world)  = me .# "children" .? world
        # world             = forall removeWindow children world
        // send edit event to server
        # (taskId,world)    = me .# "attributes.taskId" .? world
		# (editorId,world)  = me .# "attributes.editorId" .? world
		# edit              = toJSON [LDRemoveWindow windowId]
		# world             = (me .# "doEditEvent" .$! (taskId,editorId,edit)) world
		= world
    where
        removeWindow idx layer world
            # (layerWindowId, world)  = layer .# "attributes.windowId" .?? ("", world)
            | LeafletObjectID layerWindowId === windowId =
                ((me .# "removeChild" .$! idx) world)
            = world

	//Map object access
	toLatLng obj world
		# (lat,world)     = obj .# "lat" .?? (0.0, world)
		# (lng,world)     = obj .# "lng" .?? (0.0, world)
		= ({LeafletLatLng|lat=lat,lng=lng}, world)

	toBounds bounds env
        # (sw,env)          = (bounds .# "getSouthWest" .$ ()) env
        # (ne,env)          = (bounds .# "getNorthEast" .$ ()) env
		# (swpos,env)       = toLatLng sw env
		# (nepos,env)       = toLatLng ne env
        = ({southWest=swpos,northEast=nepos},env)

    getMapBounds mapObj env
        # (bounds,env)      = (mapObj .# "getBounds" .$ ()) env
		= toBounds bounds env

	getMapZoom mapObj world
		# (zoom,world) = (mapObj .# "getZoom" .$? ()) (1, world)
		= (zoom, world)

	getMapCenter mapObj world
        # (center,world)    = (mapObj .# "getCenter" .$ ()) world
        = toLatLng center world

	setMapPerspective me attr world
		# (type,world) = attr .# 0 .?? ("", world)
		= case type of
			"CenterAndZoom"
				# world = (me .# "map.setView" .$! (attr .# 1, attr .# 2)) world
				-> syncCurrentState me world
			"FitToBounds"
				# world = fitBounds me (me .# "attributes.fitbounds") (attr .# 1) world
				-> syncCurrentState me world
			_
				-> jsTraceVal attr (jsTrace "failed to set perspective" world)

	fitBounds me bounds options world
		# world = (me .# "attributes.ignorezoomend" .= True) world
		= (me .# "map.fitBounds" .$! (bounds, options)) world

	addMapTilesLayer me mapObj _ tiles world
		# (tilesUrl, world)    = tiles .# "url" .? world
		| jsIsNull tilesUrl    = world
		# (attribution, world) = tiles .# "attribution" .? world
		# (options, world)     = jsEmptyObject world
		# world                = (options .# "attribution" .= attribution) world
		# (l, world)           = jsGlobal "L" .? world
        # (layer,world)        = (l .# "tileLayer" .$ (tilesUrl, options)) world
        # world                = (layer .# "addTo" .$! mapObj) world
		= world

	setMapIcons me mapObj icons world
		# (l, world)      	= jsGlobal "L" .? world
		# (index,world) 	= jsEmptyObject world
		# world 			= (me .# "icons" .= index) world
		= forall (createMapIcon me mapObj l index) icons world
	where
		createMapIcon me mapObj l index _ def world
			# (iconId,world)   = def .# 0 .?? ("", world)
			# (iconSpec,world) = def .# 1 .? world
        	# (icon,world)     = (l .# "icon" .$ iconSpec) world
			# world            = (index .# iconId .= icon) world
			= world

	createMapObjects viewMode me mapObj objects world
		# (l, world) = jsGlobal "L" .? world
		= forall (\_ obj -> createMapObject viewMode me mapObj l obj) objects world

	createMapObject viewMode me mapObj l object world
		# (type,world) = object .# "attributes.type" .? world
		= case jsValToString type of
			?Just "marker"    = createMarker            me mapObj l object world
			?Just "polyline"  = createPolyline          me mapObj l object world
			?Just "polygon"   = createPolygon           me mapObj l object world
			?Just "circle"    = createCircle            me mapObj l object world
			?Just "rectangle" = createRectangle         me mapObj l object world
			?Just "window"    = createWindow   viewMode me mapObj l object world
			_                 = world

	createMarker me mapObj l object world
        # (markerId,world)    = object .# "attributes.markerId" .?? ("", world)
        # (options,world)     = jsEmptyObject world
		//Set title
		# (title,world)       = object .# "attributes.title" .? world
		# world               = (options .# "title" .= title) world
		# world               = (options .# "alt" .= title) world
		//Optionally set icon
		# (iconId,world)      = object .# "attributes.icon" .? world
		# (icons,world)       = me .# "icons" .? world
		# world               = addIconOption iconId icons options world
		//Create marker
		# (position,world)    = object .# "attributes.position" .? world
        # (layer,world)       = (l .# "marker" .$ (position,options) ) world
		# world               = (object .# "layer" .= layer) world
        //Optionally add popup
        # (popup,world)       = object .# "attributes.popup" .? world
        # world               = addPopup popup position layer world
        //Store marker ID, needed for related markers of windows
        # world               = (layer .# "markerId" .= markerId) world
		//Set click handler
		# (cb,world)          = jsWrapFun (\a w -> onMarkerClick me (LeafletObjectID markerId) a w) me world
        # world               = (layer .# "addEventListener" .$! ("click",cb)) world
        //Add to map
        # world               = (layer .# "addTo" .$! mapObj) world
		= world
	where
		addIconOption iconId icons options world
			| jsIsUndefined iconId = world
			# (icon,world) = icons .# (fromJS "" iconId) .? world
			| jsIsUndefined icon = world
			# world = (options .# "icon" .= icon) world
			= world

        addPopup content position marker world
            | jsIsUndefined content = world
            # (options,world) = jsEmptyObject world
            # world           = (options .# "maxWidth" .= 1000000) world // large nr to let content determine size
            # world           = (options .# "closeOnClick" .= False) world
            # (popup, world)  = (l .# "popup" .$ options) world
            # (_, world)      = (popup .# "setLatLng" .$ position) world
            # (_, world)      = (popup .# "setContent" .$ content) world
            # (_, world)      = (mapObj .# "addLayer" .$ popup) world
            // keep reference in marker object to remove popup if marker is removed
            # world           = (marker .# "myPopup" .= popup) world
            = world

	createPolyline me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# (style,world)       = object .# "attributes.style" .? world
		# world               = forall (applyLineStyle options) style world
		# (points,world)      = object .# "attributes.points" .? world
		# (layer,world)       = (l .# "polyline" .$ (points ,options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "polylineId" me mapObj layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (points, world) = (layer .# "getLatLngs" .$ ()) world
			# (points, world) = jsValToList` points id world
			# (points, world) = foldl (\(res, world) point = appFst (\latLng -> [latLng: res]) $ toLatLng point world)
			                          ([], world)
			                          points
			= (UpdatePolyline $ reverse points, world)

	createPolygon me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# (style,world)       = object .# "attributes.style" .? world
		# world               = forall (applyAreaStyle options) style world
		# (points,world)      = object .# "attributes.points" .? world
		# (layer,world)       = (l .# "polygon" .$ (points ,options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "polygonId" me mapObj layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (points, world) = (layer .# "getLatLngs" .$ ()) world
			# (points, world) = points .# 0 .? world
			# (points, world) = jsValToList` points id world
			# (points, world) = foldl (\(res, world) point = appFst (\latLng -> [latLng: res]) $ toLatLng point world)
			                          ([], world)
			                          points
			= (UpdatePolygon $ reverse points, world)

	createCircle me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# (style,world)       = object .# "attributes.style" .? world
		# world               = forall (applyAreaStyle options) style world
		# (center,world)      = object .# "attributes.center" .? world
		# (radius,world)      = object .# "attributes.radius" .? world
		# world               = (options .# "radius" .= radius) world
		# (layer,world)       = (l .# "circle" .$ (center, options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "circleId" me mapObj layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (radius,   world) = (layer .# "getRadius" .$? ()) (0.0, world)
			# (center,   world) = (layer .# "getLatLng" .$ ()) world
			# (center,   world) = toLatLng center world
			= (UpdateCircle center radius, world)

	createRectangle me mapObj l object world
		//Set options
		# (options,world)     = jsEmptyObject world
		# (style,world)       = object .# "attributes.style" .? world
		# world               = forall (applyAreaStyle options) style world
		# (sw,world)          = object .# "attributes.bounds.southWest" .? world
		# (ne,world)          = object .# "attributes.bounds.northEast" .? world
		# (layer,world)       = (l .# "rectangle" .$ ([sw, ne], options)) world
		# world               = (layer .# "addTo" .$! mapObj) world
		# world               = enableEdit "rectangleId" me mapObj layer object getUpdate world
		# world               = (object .# "layer" .= layer) world
		= world
	where
		getUpdate layer world
			# (bounds, world) = (layer .# "getBounds" .$ ()) world
			# (bounds, world) = toBounds bounds world
			= (UpdateRectangle bounds, world)

	enableEdit idFieldName me mapObj layer object getUpdate world
		# (isEditable,world)  = object .# "attributes.editable" .?? (False, world)
		| not isEditable = world
		# (_, world)  = (layer .# "enableEdit" .$ ()) world
		# (cb, world) = jsWrapFun (onEditing layer) me world
		# (_, world)  = (layer .# "addEventListener" .$ ("editable:vertex:dragend", cb)) world
		# (_, world)  = (layer .# "addEventListener" .$ ("editable:vertex:new",     cb)) world
		# (_, world)  = (layer .# "addEventListener" .$ ("editable:vertex:deleted", cb)) world
		= world
	where
		onEditing layer _ world
			# (update,   world) = getUpdate layer world
			# (objectId, world) = object .# "attributes." +++ idFieldName .?? ("", world)
			# edit              = toJSON [LDUpdateObject (LeafletObjectID objectId) update]
			# (taskId,   world) = me .# "attributes.taskId" .? world
			# (editorId, world) = me .# "attributes.editorId" .? world
			# (_,        world) = (me .# "doEditEvent" .$ (taskId, editorId, edit)) world
			= world

	applyAreaStyle options _ style world
		# (styleType, world) = style .# 0 .? world
		# styleType = jsValToString styleType
		| styleType == ?Just "Style"
			# (directStyle, world) = style .# 1 .? world
			# (directStyleType, world) = directStyle .# 0 .? world
			# (directStyleVal, world)  = directStyle .# 1 .? world
			# directStyleType = jsValToString directStyleType
			= case directStyleType of
				?Just "AreaLineStrokeColor" = (options .# "color"       .= directStyleVal) world
				?Just "AreaLineStrokeWidth" = (options .# "weight"      .= directStyleVal) world
				?Just "AreaLineOpacity"     = (options .# "opacity"     .= directStyleVal) world
				?Just "AreaLineDashArray"   = (options .# "dashArray"   .= directStyleVal) world
				?Just "AreaNoFill"          = (options .# "fill"        .= False)          world
				?Just "AreaFillColor"       = (options .# "fillColor"   .= directStyleVal) world
				?Just "AreaFillOpacity"     = (options .# "fillOpacity" .= directStyleVal) world
				_                           = abort "unknown style"
		| styleType == ?Just "Class"
			# (cls, world) = style .# 1 .? world
			= (options .# "className" .= cls) world
		= abort "unknown style"

    createWindow viewMode me mapObj l object world
        # (layer,world)      = l .# "window" .$ () $ world
		# world              = (object .# "layer" .= layer) world
        # (position,world)   = object .# "attributes.initPosition" .? world
        # (_, world)         = (layer .# "setInitPos" .$ position) world
        # (title,world)      = object .# "attributes.title" .? world
        # (_, world)         = (layer .# "setTitle" .$ title) world
        # (content,world)    = object .# "attributes.content" .? world
        # (_, world)         = (layer .# "setContent" .$ content) world
        # (relMarkers,world) = object .# "attributes.relatedMarkers" .? world
        # world              = forall (\_ relMarker world -> layer .# "addRelatedMarker" .$! relMarker $ world)
                                      relMarkers
                                      world
        // inject function to send event on window remove
		# world = case viewMode of
			True
				= world
			False
                # (windowId,world)   = object .# "attributes.windowId" .?? ("", world)
                # (onWRemove, world) = jsWrapFun (onWindowRemove me (LeafletObjectID windowId)) me world
                = (layer .# "_onWindowClose" .= onWRemove) world
        // inject function to handle window update
        # (cb,world)         = jsWrapFun (onUIChange layer) me world
        # world              = ((object .# "onUIChange") .= cb) world
        // add to map
        # world              = (layer .# "addTo" .$! mapObj) world
        = world
    where
        onUIChange layer changes world
            # world = foldl doChange world [c \\ c <-: changes]
            = world
        where
            doChange world change
                # (attrUpdates, world) = change .# "attributes" .? world
                # world = forall updateAttr attrUpdates world
                = world

            updateAttr _ attrChange world
                # (name,  world) = attrChange .# "name" .?? ("", world)
                # (value, world) = attrChange .# "value" .? world
                = case name of
                    "content"        = layer .# "setContent"        .$! value $ world
                    "title"          = layer .# "setTitle"          .$! value $ world
                    "relatedMarkers" = layer .# "setRelatedMarkers" .$! value $ world
                    _                = abort $ concat ["unknown attribute of leaflet window: \"", name, "\"\n"]

    applyLineStyle options _ style world
        # (styleType, world) = style .# 0 .? world
        # styleType = jsValToString styleType
        | styleType == ?Just "Style"
            # (directStyle, world) = style .# 1 .? world
            # (directStyleType, world) = directStyle .# 0 .? world
            # (directStyleVal, world)  = directStyle .# 1 .? world
            # directStyleType = jsValToString directStyleType
            = case directStyleType of
                ?Just "LineStrokeColor" = (options .# "color"     .= directStyleVal) world
                ?Just "LineStrokeWidth" = (options .# "weight"    .= directStyleVal) world
                ?Just "LineOpacity"     = (options .# "opacity"   .= directStyleVal) world
                ?Just "LineDashArray"   = (options .# "dashArray" .= directStyleVal) world
                _                       = abort "unknown style\n"
        | styleType == ?Just "Class"
            # (cls, world) = style .# 1 .? world
            = (options .# "className" .= cls) world
        = abort "unknown style"

	//Loop through a javascript array
    forall :: !(Int JSVal *JSWorld -> *JSWorld) !JSVal !*JSWorld -> *JSWorld
	forall f array world
		# (len,world) = array .# "length" .?? (0, world)
		= forall` 0 len world
	where
        forall` :: !Int !Int !*JSWorld -> *JSWorld
		forall` i len world
			| i >= len = world
			# (el,world) = array .# i .? world
			= forall` (i + 1) len (f i el world)

	//Process the edits received from the client
	onEdit diffs m vst
		# m = foldl app m diffs
		= (Ok (NoChange, m, ?Just m), vst)
	where
		app m LDSetManualPerspective      = case (m.LeafletMap.center, m.zoom) of
			(?Just center, ?Just zoom) -> {m & perspective=CenterAndZoom center zoom}
			_                          -> m // should not happen
		app m (LDSetZoom zoom)            = {LeafletMap|m & zoom = ?Just zoom}
		app m (LDSetCenter center)        = {LeafletMap|m & center = ?Just center}
		app m (LDSetBounds bounds)        = {LeafletMap|m & bounds = ?Just bounds}
        app m (LDRemoveWindow idToRemove) = {LeafletMap|m & objects = filter notToRemove m.LeafletMap.objects}
        where
            notToRemove (Window {windowId}) = windowId =!= idToRemove
            notToRemove _                   = True
		app m (LDUpdateObject objectId upd) = {LeafletMap|m & objects = withUpdatedObject <$> m.LeafletMap.objects}
		where
			withUpdatedObject :: !LeafletObject -> LeafletObject
			withUpdatedObject obj | leafletObjectIdOf obj === objectId = case (obj, upd) of
				(Polyline polyline, UpdatePolyline points)
					= Polyline {LeafletPolyline| polyline & points = points}
				(Polygon polygon, UpdatePolygon points)
					= Polygon {LeafletPolygon| polygon & points = points}
				(Circle circle, UpdateCircle center radius)
					= Circle {LeafletCircle| circle & center = center, radius = radius}
				(Rectangle rect, UpdateRectangle bounds)
					= Rectangle {LeafletRectangle| rect & bounds = bounds}
			withUpdatedObject obj = obj
		app m _ = m

	//Check for changed objects and update the client
	onRefresh newMap oldMap vst
		//Determine attribute changes
		# attrChanges = diffAttributes oldMap newMap
		//Determine object changes
		# childChanges = diffChildren oldMap.LeafletMap.objects newMap.LeafletMap.objects updateFromOldToNew encodeUI
		# attrChanges = if (isEmpty childChanges)
			attrChanges
			([SetAttribute "fitbounds" attr \\ attr <- fitBoundsAttribute newMap] ++ attrChanges)
		= (Ok (ChangeUI attrChanges childChanges, newMap, ?None),vst)
	where
		//Only center and zoom are synced to the client, bounds are only synced from client to server
		diffAttributes {LeafletMap|perspective=p1,icons=i1} {LeafletMap|perspective=p2,icons=i2}
			//Perspective
			# perspective = if (p1 === p2) [] [SetAttribute "perspective" (encodePerspective p2)]
			//Icons
			# icons = if (i2 === i1) [] [SetAttribute "icons" (JSONArray [toJSON (iconId,{IconOptions|iconUrl=iconUrl,iconSize=[w,h]}) \\ {iconId,iconUrl,iconSize=(w,h)} <- i2])]
			= perspective ++ icons

		updateFromOldToNew :: !LeafletObject !LeafletObject -> ChildUpdate
		updateFromOldToNew (Window old) (Window new) | old.windowId === new.windowId && not (isEmpty changes) =
			ChildUpdate $ ChangeUI changes []
		where
			changes = catMaybes
				[ if (old.LeafletWindow.title == new.LeafletWindow.title)
					?None
					(?Just $ SetAttribute "title" $ toJSON $ new.LeafletWindow.title)
				, if (old.content === new.content)
					?None
					(?Just $ SetAttribute "content" $ toJSON $ toString new.content)
				, if (old.relatedMarkers === new.relatedMarkers)
					?None
					(?Just $ SetAttribute "relatedMarkers" $ toJSON new.relatedMarkers)
				]
		updateFromOldToNew old new | old === new = NoChildUpdateRequired
		                           | otherwise   = ChildUpdateImpossible

	writeValue m = Ok m

	fitBoundsAttribute :: !LeafletMap -> [JSONNode]
	fitBoundsAttribute {perspective=p=:FitToBounds _ region,objects} = [encodeBounds (bounds region)]
	where
		bounds (SpecificRegion bounds) = bounds
		bounds (SelectedObjects ids) = leafletBoundingRectangleOf [o \\ o <- objects | 'Set'.member (leafletObjectIdOf o) ids]
		bounds AllObjects = leafletBoundingRectangleOf objects

		encodeBounds :: !LeafletBounds -> JSONNode
		encodeBounds {southWest=sw,northEast=ne} = JSONArray
			[ JSONArray [JSONReal sw.lat, JSONReal sw.lng]
			, JSONArray [JSONReal ne.lat, JSONReal ne.lng]
			]
	fitBoundsAttribute _ = []

gEditor{|LeafletMap|} purpose = mapEditorWrite ?Just $ leafletEditor (purpose =: ViewValue)

gDefault{|LeafletMap|} =
	{ LeafletMap
	| perspective = defaultValue
	, bounds      = ?None
	, center      = ?None
	, zoom        = ?None
	, tilesUrls   = [openStreetMapTiles]
	, objects     = [Marker homeMarker]
	, icons       = []
	}
where
	homeMarker = {markerId = LeafletObjectID "home", position= {LeafletLatLng|lat = 51.82, lng = 5.86}, title = ?Just "HOME", icon = ?None, popup = ?None}

gDefault{|LeafletPerspective|} = CenterAndZoom {LeafletLatLng|lat = 51.82, lng = 5.86} 7

gDefault{|LeafletBounds|} =
	{ southWest = {lat=50.82, lng=4.86}
	, northEast = {lat=52.82, lng=6.86}
	}

//Comparing reals may have unexpected results, especially when comparing constants to previously stored ones
gEq{|LeafletLatLng|} x y = (toString x.lat == toString y.lat) && (toString x.lng == toString y.lng)

simpleStateEventHandlers :: LeafletEventHandlers LeafletSimpleState
simpleStateEventHandlers = 
	[ OnMapClick \position (l,s) -> (addCursorMarker position l,{LeafletSimpleState|s & cursor = ?Just position})
	, OnMarkerClick \markerId (l,s) -> (l,{LeafletSimpleState|s & selection = toggle markerId s.LeafletSimpleState.selection})
	]
where
	addCursorMarker position l=:{LeafletMap|objects,icons} = {l & objects = addCursorObject objects, icons=addCursorIcon icons}
	where
		addCursorObject [] = [cursor position]
		addCursorObject [o=:(Marker {LeafletMarker|markerId}):os] 
			| markerId =: (LeafletObjectID "cursor") = [cursor position:os]
			| otherwise = [o:addCursorObject os]
		addCursorIcon [] = [icon]
		addCursorIcon [i=:{iconId}:is]
			| iconId =: (LeafletIconID "cursor") = [i:is]
			| otherwise = [i:addCursorIcon is]

	cursor position = Marker 
		{LeafletMarker|markerId=LeafletObjectID "cursor", position= position
		,icon = ?Just (LeafletIconID "cursor"),title= ?None,popup= ?None}
	icon = {LeafletIcon|iconId=LeafletIconID "cursor", iconUrl= svgIconURL (CircleElt hattrs sattrs) (10,10), iconSize = (10,10)}
	where
        sattrs = [CxAttr (SVGLength "5" PX),CyAttr (SVGLength "5" PX),RAttr (SVGLength "3" PX)]
		hattrs = [StyleAttr "fill:none;stroke:#00f;stroke-width:2"]

	toggle (LeafletObjectID "cursor") xs = xs //The cursor can't be selected
	toggle x xs = if (isMember x xs) (removeMember x xs) ([x:xs])

customLeafletEditor :: !MapOptions !(LeafletEventHandlers s) s -> Editor (LeafletMap, s) (LeafletMap, s) | iTask s
customLeafletEditor mapOptions handlers initial = leafEditorToEditor (customLeafletEditor` mapOptions handlers initial)

customLeafletEditor` ::
	!MapOptions !(LeafletEventHandlers s) s -> LeafEditor [LeafletEdit] (LeafletMap,s) (LeafletMap,s) (LeafletMap,s) | iTask s
customLeafletEditor` mapOptions handlers initial =
	{ LeafEditor
    | onReset        = onReset
    , onEdit         = onEdit
    , onRefresh      = onRefresh
    , writeValue     = writeValue 
    }
where
	baseEditor = leafletEditor` mapOptions $ case [h \\ OnMapDblClick h <- handlers] of
		[_:_] -> \me -> me .# "doubleClickZoom" .# "disable" .$! ()
		[]    -> const id

	onReset attributes mbval vst = case baseEditor.LeafEditor.onReset attributes (fst <$> mbval) vst of
		(Error e, vst) = (Error e, vst)
		(Ok (ui,mapState,mbw),vst) = (Ok (ui,(mapState, initial), mapMaybe (\s -> (s,initial)) mbw),vst)

	onEdit edit (mapState,customState) vst = case baseEditor.LeafEditor.onEdit edit mapState vst of
		(Error e, vst) = (Error e, vst)
		(Ok (mapChange,mapState,mbw1), vst)
			//Apply event handlers
			# (newMapState,customState) = updateCustomState handlers edit (mapState,customState)
			//Determine the change to the map
			= case baseEditor.LeafEditor.onRefresh newMapState mapState vst of
				(Error e, vst) = (Error e, vst)
				(Ok (mapRefreshChange,mapState,mbw2),vst)
					# w = if (isNone mbw1 && isNone mbw2) ?None (?Just (mapState,customState))
					= (Ok (mergeUIChanges mapChange mapRefreshChange, (mapState,customState), w),vst)
		
	onRefresh (newMapState,newCustomState) (curMapState,curCustomState) vst
		 = case baseEditor.LeafEditor.onRefresh newMapState curMapState vst of
			(Error e, vst) = (Error e, vst)
			(Ok (mapChange,mapState,mbw),vst) = (Ok (mapChange,(mapState,newCustomState),mapMaybe (\s -> (s,newCustomState)) mbw),vst)	

	writeValue m_and_s = Ok m_and_s

	updateCustomState handlers edits state
		| otherwise = foldl (\s e -> foldl (update e) s handlers) state edits
	where
		update (LDMapClick position)    state (OnMapClick f)    = f position state
		update (LDMapDblClick position) state (OnMapDblClick f) = f position state
		update (LDMarkerClick markerId) state (OnMarkerClick f) = f markerId state
		update (LDHtmlEvent event)      state (OnHtmlEvent f)   = f event state
		update _                        state _                 = state

instance == LeafletObjectID where (==) (LeafletObjectID x) (LeafletObjectID y) = x == y
instance == LeafletIconID where (==) (LeafletIconID x) (LeafletIconID y) = x == y

instance < LeafletObjectID where (<) (LeafletObjectID x) (LeafletObjectID y) = x < y

gDefault{|FitToBoundsOptions|} =
	{ padding = (100, 100)
	, maxZoom = 10
	}

derive JSONEncode LeafletMap, LeafletLatLng, TileLayer
derive JSONDecode LeafletMap, LeafletLatLng, TileLayer
derive gDefault   LeafletLatLng
derive gEq        LeafletMap, TileLayer
derive gText      LeafletMap, LeafletLatLng, TileLayer
derive gEditor    LeafletLatLng
derive class iTask
	LeafletIcon, LeafletBounds, LeafletObject, LeafletMarker, LeafletPolyline,
	LeafletPolygon, LeafletEdit, LeafletWindow, LeafletWindowPos,
	LeafletLineStyle, LeafletStyleDef, LeafletAreaStyle, LeafletObjectID,
	CSSClass, LeafletIconID, LeafletCircle, LeafletObjectUpdate,
	LeafletRectangle, LeafletSimpleState, LeafletPerspective,
	FitToBoundsOptions, FitToBoundsRegion
