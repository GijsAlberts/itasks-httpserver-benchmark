definition module iTasks.Extensions.Form.Pikaday
import iTasks, iTasks.UI.Editor
/**
* Integration of the Pikaday datefield library
*/
pikadayField :: Editor String String

pikadayDateField :: Editor Date (?Date)
