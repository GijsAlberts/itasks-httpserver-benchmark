definition module iTasks.SDS.Sources.Core
/*
* This module provides the builtin shared sources
*/
import iTasks.SDS.Definition
from System.FilePath import :: FilePath
from Data.Error import :: MaybeError, :: MaybeErrorString
from Data.GenHash import generic gHash
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode 

// constant share from which you always read the same value
constShare :: !a -> SDSSource p a () | gHash{|*|} p

// null share to which you can write anything
nullShare :: SDSSource p () a | gHash{|*|} p

// Useful placeholder when you need a share don't intent to use it
unitShare :: SimpleSDSSource ()

// Random source
randomInt :: SDSSource () Int ()

// Random string (the parameters determines its length)
randomString :: SDSSource Int String ()

// world function share
worldShare ::
	(p *World -> *(MaybeErrorString r,*World))
	(p w *World -> *(MaybeErrorString (),*World))
	(p Timespec p -> Bool)
	-> SDSSource p r w
	| gHash{|*|} p

// memory share (essentially a global variable)
memoryShare :: SDSSource String (?a) (?a) | TC a

/**
 * Share that maps to the plain contents of a file on disk.
 * When the file does not exist on reading it returns `?None`. By writing
 * `?None` you can remove the file.
 */
fileShare :: SDSSource FilePath (?String) (?String)

//* Share that maps to a file encoded as JSON.
jsonFileShare :: SDSSource FilePath (?a) (?a) | JSONEncode{|*|}, JSONDecode{|*|} a

// Share that maps to a file that holds a serialized graph representation of the value
graphFileShare :: SDSSource FilePath (?a) (?a)

// Directory
directoryListing :: SDSSource FilePath [String] ()
