module WasmTest

import StdEnv
import Data.Functor
import iTasks
import iTasks.Extensions.JavaScript
import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

// This is a simple test program to try out things with the WebAssembly ABC interpreter.

Start w = doTasks task w
where
	task = allTasks
		[ Title "Check the browser console" @>>
			appJSWorld (jsTrace "Hello world from WebAssembly!") @! ""
		, Title "This is your userAgent:" @>>
			(accJSWorld userAgent >>~ viewInformation [])
		, Title "This is your doNotTrack setting:" @>>
			(runJSMonad () doNotTrack >>~ viewInformation [])
		]

	userAgent :: *JSWorld -> (String, *JSWorld)
	userAgent world = jsGlobal "navigator" .# "userAgent" .?? ("", world)

	doNotTrack :: JS () String
	doNotTrack =
		fromJS "undefined" <$>
		accJS ((.?) (jsWindow .# "navigator.doNotTrack"))
