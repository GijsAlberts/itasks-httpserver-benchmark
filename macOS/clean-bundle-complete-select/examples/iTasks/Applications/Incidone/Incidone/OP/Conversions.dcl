definition module Incidone.OP.Conversions
/**
* This module provides conversion functions between the various
* that are defined in the conceptual model of the operational picture.
*/
import Incidone.OP.Concepts

//Titles
class contactTitle a :: a -> String
instance contactTitle Contact
instance contactTitle ContactShort

class incidentTitle a :: a -> String
instance incidentTitle Incident
instance incidentTitle IncidentShort

//Subtitles
contactSubTitle :: Contact -> String
incidentSubTitle :: Incident -> String

//Icons
class contactIcon a :: a -> String
instance contactIcon Contact
instance contactIcon ContactShort

contactTypeIcon :: (?ContactType) -> String

contactThumbHtml    :: Contact -> HtmlTag
contactAvatarHtml   :: Contact -> HtmlTag

// Identification types
communicationIdentity :: CommunicationDetails -> CommunicationNo

class contactIdentity a :: a -> ContactNo
instance contactIdentity Contact
instance contactIdentity ContactShort

class incidentIdentity a :: a -> IncidentNo
instance incidentIdentity Incident
instance incidentIdentity IncidentShort

// Summary types
contactSummary		:: Contact -> ContactShort

// Details types (extended summary)
contactDetails :: Contact -> ContactDetails
incidentDetails :: Incident -> IncidentDetails

:: IncidentOverview =
	{title                :: ?IncidentTitle
	,summary              :: ?IncidentSummary
	,type                 :: ?IncidentType
    ,contactsNeedingHelp  :: [ContactNameTypePosition]
    }

//Predicates
matchesMMSI :: MMSI Contact -> Bool

//AIS data conversion
:: AISDetails =
    { name                :: ?String
    , mmsi                :: MMSI
    , position            :: ?ContactPosition
    , callsign            :: ?String
    }

aisToContact    :: AISContact -> Contact
aisToContactGeo :: AISContact -> ContactGeo
aisToDetails    :: AISContact -> AISDetails
aisPosition     :: AIVDMCNB -> ContactPosition
aisHeading      :: AIVDMCNB -> ContactHeading


//Complex updates
updAISContactPosition   :: DateTime ContactPosition ContactHeading AISContact -> AISContact
updContactPosition      :: DateTime (?ContactPosition) (?ContactHeading) Contact -> Contact

//Representation expressing the exclusive nature of the communicationmean subtypes
:: XCommunicationMean
    = XCMTelephone TelephoneDetails
    | XCMVHFRadio VHFRadioDetails
    | XCMEmail EmailAccountDetails
    | XCMP2000 P2000ReceiverDetails

toCommunicationMean         :: CommunicationMeanId XCommunicationMean -> CommunicationMean
fromCommunicationMean       :: CommunicationMean -> (CommunicationMeanId,XCommunicationMean)

toNewCommunicationMean      :: XCommunicationMean -> NewCommunicationMean
fromNewCommunicationMean    :: NewCommunicationMean -> XCommunicationMean

//Users
contactUser     :: Contact -> User
userContactNo   :: User -> ?ContactNo
