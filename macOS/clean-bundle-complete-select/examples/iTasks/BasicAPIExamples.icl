module BasicAPIExamples

import iTasks
import Text.HTML
import qualified iTasks.Extensions.Admin.UserAdmin

import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterDateAndTime
import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterInteger
import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterListOfInt
import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterText
import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.HelloWorld
import qualified BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.Leaflet
import qualified BasicAPIExamples.EditorsOnCustomTypes.EnterFamilyTree
import qualified BasicAPIExamples.EditorsOnCustomTypes.EnterPerson
import qualified BasicAPIExamples.EditorsOnCustomTypes.SelectionComponents
import qualified BasicAPIExamples.Extensions.Select2
import qualified BasicAPIExamples.InteractionUsingShares.BrowseAndViewLeafletMap
import qualified BasicAPIExamples.InteractionUsingShares.CurrentDateAndTime
import qualified BasicAPIExamples.InteractionUsingShares.SharedNoteAsList
import qualified BasicAPIExamples.InteractionUsingShares.SharedNotes
import qualified BasicAPIExamples.InteractionUsingShares.UpdateSharedPersonsAndView
import qualified BasicAPIExamples.InteractionWithTheSystem.RunProcess
import qualified BasicAPIExamples.MultiUserExamples.Chat
import qualified BasicAPIExamples.MultiUserExamples.MeetingDate
import qualified BasicAPIExamples.MultiUserExamples.OptionsChat
import qualified BasicAPIExamples.ParallelExamples.TinyTextEditor
import qualified BasicAPIExamples.SequentialExamples.CalculateSumInShare
import qualified BasicAPIExamples.SequentialExamples.CalculateSumStepwise
import qualified BasicAPIExamples.SequentialExamples.CalculateSumStepwiseAndBack
import qualified BasicAPIExamples.SequentialExamples.Calculator
import qualified BasicAPIExamples.SequentialExamples.CoffeeMachine
import qualified BasicAPIExamples.SequentialExamples.EditPerson1by1
import qualified BasicAPIExamples.SequentialExamples.Palindrome

Start :: *World -> *World
Start world = doTasks {WorkflowCollection|name=name,loginMessage= ?Just loginMessage,welcomeMessage= ?Just welcomeMessage,allowGuests=False,workflows=basicAPIExamples} world
where
	name = "iTasks Example Collection"
	loginMessage = DivTag []
		[Text "iTasks is a framework to create information systems from task specifications.",BrTag []
		,Text "Although useful for support of individual tasks, information systems add even more value when a task "
		,Text "requires multiple people to work together to accomplish it."
		,Text "Therefore this example application is a multi-user demonstration of tasks expressed in iTasks.",BrTag [],BrTag[]
		,Text "You can log in with a demonstration user account:",BrTag []
			,UlTag []
				[LiTag [] [Text "Alice (username: alice, password: alice)"]
				,LiTag [] [Text "Bob (username: bob, password: bob)"]
				,LiTag [] [Text "Carol (username: carol, password: carol)"]
				,LiTag [] [Text "An administrator with full access (username: root, password: root)"]
				]
			]

	welcomeMessage = DivTag []
		[H1Tag [] [Text "Welcome"],PTag []
			[Text "In this generic application you can work on multiple tasks concurrently.", BrTag []
			,Text "In the list above you can see the set of ongoing tasks that you can choose to work on.", BrTag []
			,Text "Additionally you can add tasks to this list with the 'New' button. This will open a window with a collection of predefined tasks.", BrTag []
			,Text "These tasks range from simple TODO items, to complex multi-user workflows.", BrTag []
			]
		]

basicAPIExamples :: [Workflow]
basicAPIExamples =
	[restrictedTransientWorkflow "Users" "User management" ["admin"] 'iTasks.Extensions.Admin.UserAdmin'.manageUsers
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterDateAndTime'.wf " Basic API Examples/Editors On Basic And Predefined Types/Enter Date And Time"
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterInteger'.wf " Basic API Examples/Editors On Basic And Predefined Types/Enter Integer"
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterListOfInt'.wf " Basic API Examples/Editors On Basic And Predefined Types/Enter List Of Int"
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.EnterText'.wf " Basic API Examples/Editors On Basic And Predefined Types/Enter Text"
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.HelloWorld'.wf " Basic API Examples/Editors On Basic And Predefined Types/Hello World"
	,'BasicAPIExamples.EditorsOnBasicAndPredefinedTypes.Leaflet'.wf " Basic API Examples/Editors On Basic And Predefined Types/Leaflet"
	,'BasicAPIExamples.EditorsOnCustomTypes.EnterFamilyTree'.wf " Basic API Examples/Editors On Custom Types/Enter Family Tree"
	,'BasicAPIExamples.EditorsOnCustomTypes.EnterPerson'.wf " Basic API Examples/Editors On Custom Types/Enter Person"
	,'BasicAPIExamples.EditorsOnCustomTypes.SelectionComponents'.wf " Basic API Examples/Editors On Custom Types/Selection Components"
	,'BasicAPIExamples.Extensions.Select2'.wf " Basic API Examples/Extensions/Select2"
	,'BasicAPIExamples.InteractionUsingShares.BrowseAndViewLeafletMap'.wf " Basic API Examples/Interaction Using Shares/Browse And View Leaflet Map"
	,'BasicAPIExamples.InteractionUsingShares.CurrentDateAndTime'.wf " Basic API Examples/Interaction Using Shares/Current Date And Time"
	,'BasicAPIExamples.InteractionUsingShares.SharedNoteAsList'.wf " Basic API Examples/Interaction Using Shares/Shared Note As List"
	,'BasicAPIExamples.InteractionUsingShares.SharedNotes'.wf " Basic API Examples/Interaction Using Shares/Shared Notes"
	,'BasicAPIExamples.InteractionUsingShares.UpdateSharedPersonsAndView'.wf " Basic API Examples/Interaction Using Shares/Update Shared Persons And View"
	,'BasicAPIExamples.InteractionWithTheSystem.RunProcess'.wf " Basic API Examples/Interaction With The System/Run Process"
	,'BasicAPIExamples.MultiUserExamples.Chat'.wf " Basic API Examples/Multi User Examples/Chat"
	,'BasicAPIExamples.MultiUserExamples.MeetingDate'.wf " Basic API Examples/Multi User Examples/Meeting Date"
	,'BasicAPIExamples.MultiUserExamples.OptionsChat'.wf " Basic API Examples/Multi User Examples/Options Chat"
	,'BasicAPIExamples.ParallelExamples.TinyTextEditor'.wf " Basic API Examples/Parallel Examples/Tiny Text Editor"
	,'BasicAPIExamples.SequentialExamples.CalculateSumInShare'.wf " Basic API Examples/Sequential Examples/Calculate Sum In Share"
	,'BasicAPIExamples.SequentialExamples.CalculateSumStepwise'.wf " Basic API Examples/Sequential Examples/Calculate Sum Stepwise"
	,'BasicAPIExamples.SequentialExamples.CalculateSumStepwiseAndBack'.wf " Basic API Examples/Sequential Examples/Calculate Sum Stepwise And Back"
	,'BasicAPIExamples.SequentialExamples.Calculator'.wf " Basic API Examples/Sequential Examples/Calculator"
	,'BasicAPIExamples.SequentialExamples.CoffeeMachine'.wf " Basic API Examples/Sequential Examples/Coffee Machine"
	,'BasicAPIExamples.SequentialExamples.EditPerson1by1'.wf " Basic API Examples/Sequential Examples/Edit Person1by1"
	,'BasicAPIExamples.SequentialExamples.Palindrome'.wf " Basic API Examples/Sequential Examples/Palindrome"
	]
