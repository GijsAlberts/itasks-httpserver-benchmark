module SkulptExample

import iTasks
import iTasks.Extensions.Interpreters.Skulpt
import StdFunctions, Data.Func, Data.Either, Text, Text.HTML
import qualified Data.Map as DM

task = withShared script \sscript -> withShared state \sstate ->
	(Label "Goal" @>> viewInformation [] goal)
	||-
	(Label "Arena" @>> viewSharedInformation [ViewAs textViz] sstate)
	||- runPythonScript interface sscript state sstate success

where
	goal = "Write a script that moves the player to the position indicated by the 'X'."

	//Flag (goal) position
	flag = (3,5)
	//Goal accomplished when player ends up at flag position
	success (position,_) = position === flag

	script =
		["for _ in range(5):"
		,"  forward()"
		,"rotateRight()"
		,"for _ in range(3):"
		,"  forward()"
		,"print(\"Done!\")"
		]

	//An (x,y) position and an (x,y) direction vector
	state = ((0,0),(0,1))

	interface = 'DM'.fromList
		[("forward", pausedvoidfun forward)
		,("rotateLeft",pausedvoidfun rotateLeft)
		,("rotateRight",pausedvoidfun rotateRight)
		]

	voidfun f args s = (Left SkulptNone,f s)
	pausedvoidfun f args s = (Right (const (SkulptDict ('DM'.fromList [("foo",SkulptFloat 3.14),("bar",SkulptInt 42)]))),f s)

	forward (position=:(x,y),direction=:(dx,dy)) = ((x+dx,y+dy),direction)

	//Rotate direction vector clockwise
	rotateRight (position,( 0, 1)) = (position,( 1, 0))
	rotateRight (position,( 1, 0)) = (position,( 0,-1))
	rotateRight (position,( 0,-1)) = (position,(-1, 0))
	rotateRight (position,(-1, 0)) = (position,( 0, 1))

	//Rotate direction vector counterclockwise
	rotateLeft (position,( 0, 1)) = (position,(-1, 0))
	rotateLeft (position,( 1, 0)) = (position,( 0, 1))
	rotateLeft (position,( 0,-1)) = (position,( 1, 0))
	rotateLeft (position,(-1, 0)) = (position,( 0,-1))

	//Render a 2D space simple as ascii-art
	textViz ((x,y),dir) = PreTag [] [Text text]
	where
		width = 10
		height = 10
		hline = "\n" +++ (join "" $ repeatn (width * 2 - 1) "-")  +++ "\n"
		text = join hline [join "|" [tile (i,j) \\ i <- [0 .. width - 1]] \\ j <- reverse [0 .. height - 1 ]]

		tile coord
			| coord === (x,y) = case dir of    
				( 0, 1) = "^"
				( 1, 0) = ">"
				( 0,-1) = "v"
				(-1, 0) = "<"
			| coord == flag = "X"
			| otherwise = " "

runPythonScript :: (Map String (SkulptFunction a)) (sdss () [String] [String]) a (sdsa () a a) (a -> Bool) -> Task () | RWShared sdss & RWShared sdsa & iTask a
runPythonScript interface sCode startValue sValue successPred =
	editScript sCode	
	-&&-
	executeScript sCode sValue
	@! ()
where
	editScript sCode 
		= Label "Script" @>> updateSharedInformation
			[UpdateSharedUsing (join "\n") (\_ s -> split "\n" s) (textArea <<@ heightAttr (ExactSize 200))] sCode

	executeScript sCode sValue =
			withShared False
			\requestRun ->
			withShared {SkulptOutput|output = [], error = ?None, state = Stopped, model = startValue}
			\runState ->
				(viewOutput runState 
					>^*
					[OnAction (Action "Start") (ifValue (\s -> s.SkulptOutput.state =: Stopped) (const (startScript requestRun runState)))
					,OnAction (Action "Resume") (ifValue (\s -> s.SkulptOutput.state =: Paused) (const (set True requestRun @! ())))
					]
				)
			-||
				(interactRW (skulptInterpreter interface startValue)
					(mapRead (\s -> ?Just (tof s)) $ mapWrite fromf ?None (sCode |*< (requestRun >*< (runState >*< sValue))))
				)
	where
		startScript requestRun runState
			//Reset start value
			= set startValue sValue
			//Trigger execution
			>-| set True requestRun 
			//Wait until the script has started and stopped again.
			//Check the success criterion
			>-| afterObservations
				[\{SkulptOutput|state} -> state =: Running
				,\{SkulptOutput|state} -> state =: Stopped
				] runState evaluateRun 

		evaluateRun output=:{SkulptOutput|error = ?None}
			= (get (mapRead successPred sValue)
			>>- \success -> viewInformation [] (if success "Congratulations, goal accomplished!" "Too bad, please try again.")
			>>? \_ -> return ()
			) <<@ InWindow
		evaluateRun _ = return ()

		tof (script,(True,({SkulptOutput|state=Stopped},model))) = [SkulptSetState model,SkulptRunScript]
		tof (script,(True,({SkulptOutput|state=Paused},_))) = [SkulptRunScript]
		tof (script,(_,({SkulptOutput|state=Stopped},model))) = [SkulptSetScript (join "\n" script),SkulptSetState model]
		tof _ = []

		fromf skulpt=:{SkulptOutput|model} _ = ?Just (False,(skulpt,model))

		afterObservations [pred] sds task
			= watch sds >>* [OnValue $ ifValue pred task]
		afterObservations [pred:preds] sds task
			= watch sds >>* [OnValue $ ifValue pred (const $ afterObservations preds sds task)]

	viewOutput runState =
		Label "Output" @>> viewSharedInformation [ViewAs view] runState
	where
		view {SkulptOutput|output,error}
			= DivTag [StyleAttr "font-family: monospace"]
				(flatten [[SpanTag [] [Text line],BrTag []] \\ line <- output]
				++ maybe [] (\{SkulptError|message} -> [SpanTag [StyleAttr "color: red"] [Text message]]) error
				)

Start w = doTasks task w
