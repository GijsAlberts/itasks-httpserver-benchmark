implementation module System._Time

import StdEnv
import System.Time
import System._Pointer
import System._Posix

_timegm :: !{#Int} -> Int
_timegm tm = timegm tm

_nsTime :: !*World -> (!Timespec, !*World)
_nsTime w
# (p, w) = mallocSt (IF_INT_64_OR_32 16 12) w
# (r, w) = clock_gettime 0 p w
//For completeness sake
| r <> 0 = abort "clock_gettime error: everyone should have permission to open CLOCK_REALTIME?\n"
# (tv_sec, p) = IF_INT_64_OR_32 (readIntP p 0) (readP (\p -> readInt4Z p 0) p)
# (tv_nsec, p) = readIntP p (IF_INT_64_OR_32 8 4)
= ({Timespec | tv_sec = tv_sec, tv_nsec = tv_nsec}, freeSt p w)
