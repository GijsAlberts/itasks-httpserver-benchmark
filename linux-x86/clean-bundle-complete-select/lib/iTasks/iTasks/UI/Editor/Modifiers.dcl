definition module iTasks.UI.Editor.Modifiers
/**
* This module provides combinator functions for combining editors
*/
import iTasks.UI.Editor, iTasks.UI.Definition, iTasks.UI.Tune
import Data.Error

//### Modifying atributes of editors ### 

/**
 * Adds a tooltip and tooltip-type attribute based on the editor's state.
 * Layouts (such as the automatic default layout) can use these attributes to
 * create icons and tooltips.
 *
 * This is a special case of `withDynamicHintAttributesWithError`, which sets a
 * default error message in case of a `?None` value.
 */
withDynamicHintAttributes :: !String !(Editor r (?w)) -> Editor r (?w)

/**
 * Adds a tooltip and tooltip-type attribute based on the editor's state.
 *
 * This is like `withDynamicHintAttributes`, but allows for custom messages.
 */
withDynamicHintAttributesWithError :: !String !(Editor r (MaybeError String w)) -> Editor r (?w)

/**
* Uses the given editor to view a constant value.
*
* @param The constant value to view.
* @param The editor used to view the value.
* @result An editor viewing the constant value.
*/
viewConstantValue :: !r !(Editor r w) -> Editor () w

/**
* Ignore all data reads
*/
ignoreEditorReads :: !(Editor rb wa) -> Editor ra wa

// ### Mapping editors to different domains ###

/*
* Change the editor's initial value
*/
mapEditorInitialValue :: !((?r) -> ?r) !(Editor r w) -> Editor r w

/**
* Map editor reads to a different domain
*/
mapEditorRead :: !(r -> rb) !(Editor rb w) -> Editor r w

/**
* Map editor reads to a different domain.
* When refreshing the current value of the modified editor is used to compute the new value
*/
mapEditorReadWithValue :: !(r -> rb) !(r w -> rb) !(Editor rb w) -> Editor r w

/**
* Map editor writes to a different domain
*/
mapEditorWrite :: !(wb -> w) !(Editor r wb) -> Editor r w

/**
 * Map editor writes to a different domain with potential errors.
 * If the mapped editor returns an error, the editor writes `?None` and a
 * tooltip-type error and tooltip attribute are set on the editor.
 */
mapEditorWriteError :: !(wb -> MaybeErrorString w) !(Editor r wb) -> Editor r (?w)

/**
* Add a state to an editor which is available to map reads and writes to the editor.
*/
mapEditorWithState :: !s !(r s -> (?rb,s)) !(wb s -> (w,s)) !(Editor rb wb) -> Editor r w | TC s

/**
* Loop back an editor write to an editor read
*/
loopbackEditorWrite :: !(w -> ?r) !(Editor r w) -> Editor r w

