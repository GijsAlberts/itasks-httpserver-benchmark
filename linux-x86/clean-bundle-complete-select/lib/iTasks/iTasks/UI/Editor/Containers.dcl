definition module iTasks.UI.Editor.Containers
/**
* This module provides a set of editors for builtin controls
* of the client-side UI framework.
*
* To keep everything well-typed there are lots of boiler-plate versions to create the containers
*/
from iTasks.UI.Definition import :: UIType(..), :: UIAttributes
from iTasks.UI.Editor import :: Editor 
from Data.Either import :: Either
from Data.Map import :: Map
from Text.GenJSON import :: JSONNode, generic JSONDecode, generic JSONEncode

:: EditorRef =: EditorRef Int
:: TupleSubEditorItem a = NewTupleSubEditor (?a) | ExistingTupleSubEditor (?a)
:: ListSubEditorItem a = NewListSubEditor (?a) | ExistingListSubEditor EditorRef (?a)

/**
* The `group*` functions make it possible to compose editors.
* There are multiple versions:
*
* - `group` and `group1` create the empty and singleton containers.
* - `groupl` creates a container with a variable number of sub-editors of the same type.
* - `group2`,`group3`,`group4` and `group5` create containers with a fixed number of sub-editors of different types.
*
* All containers have two parameters in common, a `UIType` and a function that defines how the sub-editors are
* updated when (new) data flows into the editor.
* 
* Only the `UIContainer`, `UIPanel`, `UITabSet`, `UIWindow`, `UIMenu`, `UIToolbar`, `UIButtonBar`, `UIList`,
* `UIListItem`, and `UIDebug` constructors of `UIType` should be used, because only these refer to client side
* container components.
*
* The read policy function determines if, and how, the sub-editors of the composition are updated.
* For the common case of refreshing all elements in a composition utility read functions are provided.
*
* `groupc` is a special case for choosing between editors based on the edited value
*/
group :: !UIType -> Editor () ()
groupl :: !UIType 
			!((?rl) [(EditorRef,w)] -> [ListSubEditorItem r])
				!(Editor r w) -> Editor rl [w]
group1 :: !UIType
			!(Editor r w) -> Editor r w
group2 :: !UIType 
			!((?rt) (?(wa,wb)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb))
				!(Editor ra wa) !(Editor rb wb) -> Editor rt (wa,wb)
group3 :: !UIType 
			!((?rt) (?(wa,wb,wc)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc))
          		!(Editor ra wa) !(Editor rb wb) !(Editor rc wc) -> Editor rt (wa,wb,wc)
group4 :: !UIType 
			!((?rt) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd))
          		!(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) -> Editor rt (wa,wb,wc,wd)

group5 :: !UIType
			!((?rt) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd,TupleSubEditorItem re))
              !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) !(Editor re we) -> Editor rt (wa,wb,wc,wd,we)

groupc :: !UIType !(Editor Int (?Int)) ![(w -> a, Editor a w)] -> Editor (Int, a) (Either (?Int) w)

//Utility read functions for common cases
fullUpdate2 :: (?(a,b)) (?(wa,wb)) -> (TupleSubEditorItem a, TupleSubEditorItem b)
fullUpdate3 :: (?(a,b,c)) (?(wa,wb,wc)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c)
fullUpdate4 :: (?(a,b,c,d)) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d)
fullUpdate5 :: (?(a,b,c,d,e)) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d, TupleSubEditorItem e)
fullUpdateList :: (?[r]) [(EditorRef,w)] -> [ListSubEditorItem r]

//# UIContainer
container :== group UIContainer
container1 e :== group1 UIContainer e
container2 e1 e2 :== group2 UIContainer fullUpdate2 e1 e2
container3 e1 e2 e3 :== group3 UIContainer fullUpdate3 e1 e2 e3
container4 e1 e2 e3 e4 :== group4 UIContainer fullUpdate4 e1 e2 e3 e4
container5 e1 e2 e3 e4 e5 :== group5 UIContainer fullUpdate5 e1 e2 e3 e4 e5
containerl e :== groupl UIContainer fullUpdateList e
containerc ec es :== groupc UIContainer ec es

//# UIPanel
panel :== group UIPanel
panel1 e :== group1 UIPanel e
panel2 e1 e2 :== group2 UIPanel fullUpdate2 e1 e2
panel3 e1 e2 e3 :== group3 UIPanel fullUpdate3 e1 e2 e3
panel4 e1 e2 e3 e4 :== group4 UIPanel fullUpdate4 e1 e2 e3 e4
panel5 e1 e2 e3 e4 e5 :== group5 UIPanel fullUpdate5 e1 e2 e3 e4 e5
panell e :== groupl UIPanel fullUpdateList e
panelc ec es :== groupc UIPanel ec es

//# UITabSet
tabset :== group UITabSet
tabset1 e :== group1 UITabSet e
tabset2 e1 e2 :== group2 UITabSet fullUpdate2 e1 e2
tabset3 e1 e2 e3 :== group3 UITabSet fullUpdate3 e1 e2 e3
tabset4 e1 e2 e3 e4 :== group4 UITabSet fullUpdate4 e1 e2 e3 e4
tabset5 e1 e2 e3 e4 e5 :== group5 UITabSet fullUpdate5 e1 e2 e3 e4 e5
tabsetl e :== groupl UITabSet fullUpdateList e
tabsetc ec es :== groupc UITabSet ec es

//# UIWindow
window :== group UIWindow
window1 e :== group1 UIWindow e
window2 e1 e2 :== group2 UIWindow fullUpdate2 e1 e2
window3 e1 e2 e3 :== group3 UIWindow fullUpdate3 e1 e2 e3
window4 e1 e2 e3 e4 :== group4 UIWindow fullUpdate4 e1 e2 e3 e4
window5 e1 e2 e3 e4 e5 :== group5 UIWindow fullUpdate5 e1 e2 e3 e4 e5
windowl e :== groupl UIWindow fullUpdateList e
windowc ec es :== groupc UIWindow ec es

//# UIMenu
menu :== group UIMenu
menu1 e :== group1 UIMenu e
menu2 e1 e2 :== group2 UIMenu fullUpdate2 e1 e2
menu3 e1 e2 e3 :== group3 UIMenu fullUpdate3 e1 e2 e3
menu4 e1 e2 e3 e4 :== group4 UIMenu fullUpdate4 e1 e2 e3 e4
menu5 e1 e2 e3 e4 e5 :== group5 UIMenu fullUpdate5 e1 e2 e3 e4 e5
menul e :== groupl UIMenu fullUpdateList e
menuc ec es :== groupc UIMenu ec es

//# UIToolBar
toolbar :== group UIToolBar
toolbar1 e :== group1 UIToolBar e
toolbar2 e1 e2 :== group2 UIToolBar fullUpdate2 e1 e2
toolbar3 e1 e2 e3 :== group3 UIToolBar fullUpdate3 e1 e2 e3
toolbar4 e1 e2 e3 e4 :== group4 UIToolBar fullUpdate4 e1 e2 e3 e4
toolbar5 e1 e2 e3 e4 e5 :== group5 UIToolBar fullUpdate5 e1 e2 e3 e4 e5
toolbarl e :== groupl UIToolBar fullUpdateList e
toolbarc ec es :== groupc UIToolBar ec es

//# UIButtonBar
buttonbar :== group UIButtonBar
buttonbar1 e :== group1 UIButtonBar e
buttonbar2 e1 e2 :== group2 UIButtonBar fullUpdate2 e1 e2
buttonbar3 e1 e2 e3 :== group3 UIButtonBar fullUpdate3 e1 e2 e3
buttonbar4 e1 e2 e3 e4 :== group4 UIButtonBar fullUpdate4 e1 e2 e3 e4
buttonbar5 e1 e2 e3 e4 e5 :== group5 UIButtonBar fullUpdate5 e1 e2 e3 e4 e5
buttonbarl e :== groupl UIButtonBar fullUpdateList e
buttonbarc ec es :== groupc UIButtonBar ec es

//# UIList
list :== group UIList
list1 e :== group1 UIList e
list2 e1 e2 :== group2 UIList fullUpdate2 e1 e2
list3 e1 e2 e3 :== group3 UIList fullUpdate3 e1 e2 e3
list4 e1 e2 e3 e4 :== group4 UIList fullUpdate4 e1 e2 e3 e4
list5 e1 e2 e3 e4 e5 :== group5 UIList fullUpdate5 e1 e2 e3 e4 e5
listl e :== groupl UIList fullUpdateList e
listc ec es :== groupc UIList ec es

//# UIListItem
listitem :== group UIListItem
listitem1 e :== group1 UIListItem e
listitem2 e1 e2 :== group2 UIListItem fullUpdate2 e1 e2
listitem3 e1 e2 e3 :== group3 UIListItem fullUpdate3 e1 e2 e3
listitem4 e1 e2 e3 e4 :== group4 UIListItem fullUpdate4 e1 e2 e3 e4
listitem5 e1 e2 e3 e4 e5 :== group5 UIListItem fullUpdate5 e1 e2 e3 e4 e5
listiteml e :== groupl UIListItem fullUpdateList e
listitemc ec es :== groupc UIListItem ec es

//# UIDebug
debug :== group UIDebug
debug1 e :== group1 UIDebug e
debug2 e1 e2 :== group2 UIDebug fullUpdate2 e1 e2
debug3 e1 e2 e3 :== group3 UIDebug fullUpdate3 e1 e2 e3
debug4 e1 e2 e3 e4 :== group4 UIDebug fullUpdate4 e1 e2 e3 e4
debug5 e1 e2 e3 e4 e5 :== group5 UIDebug fullUpdate5 e1 e2 e3 e4 e5
debugl e :== groupl UIDebug fullUpdateList e
debugc ec es :== groupc UIDebug ec es

