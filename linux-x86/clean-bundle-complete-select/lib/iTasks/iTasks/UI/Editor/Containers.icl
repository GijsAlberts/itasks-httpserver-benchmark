implementation module iTasks.UI.Editor.Containers
/**
* Editor combinators for the builtin containers
*
* To keep everything well-typed there are lots of boiler-plate versions to create the containers
*/
import iTasks.UI.Definition
import iTasks.UI.Editor
import iTasks.UI.Editor.Modifiers
import Data.Error, Data.Func, Data.Functor, Data.Maybe, Data.Either, Data.List
import Text.GenJSON

from Data.Map import :: Map
import Data.Maybe

import StdBool, StdList, StdTuple, StdFunc, StdOrdList, StdEnum

fullUpdateList :: (?[r]) [(EditorRef,w)] -> [ListSubEditorItem r]
fullUpdateList (?Just list) existing
    =  [ExistingListSubEditor ref (?Just val) \\ (ref,_) <- existing & val <- list]
    ++ [NewListSubEditor (?Just val) \\ val <- drop (length existing) list]
fullUpdateList (?None) existing = []

fullUpdate2 :: (?(a,b)) (?(wa,wb)) -> (TupleSubEditorItem a, TupleSubEditorItem b)
fullUpdate2 ?None _ = (NewTupleSubEditor ?None, NewTupleSubEditor ?None)
fullUpdate2 (?Just (a,b)) ?None = (NewTupleSubEditor (?Just a), NewTupleSubEditor (?Just b))
fullUpdate2 (?Just (a,b)) (?Just _) = (ExistingTupleSubEditor (?Just a), ExistingTupleSubEditor (?Just b))

fullUpdate3 :: (?(a,b,c)) (?(wa,wb,wc)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c)
fullUpdate3 ?None _ = (NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None)
fullUpdate3 (?Just (a,b,c)) ?None = (NewTupleSubEditor (?Just a), NewTupleSubEditor (?Just b), NewTupleSubEditor (?Just c))
fullUpdate3 (?Just (a,b,c)) (?Just _) = (ExistingTupleSubEditor (?Just a), ExistingTupleSubEditor (?Just b), ExistingTupleSubEditor (?Just c))

fullUpdate4 :: (?(a,b,c,d)) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d)
fullUpdate4 ?None _ = (NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None)
fullUpdate4 (?Just (a,b,c,d)) ?None = (NewTupleSubEditor (?Just a), NewTupleSubEditor (?Just b), NewTupleSubEditor (?Just c), NewTupleSubEditor (?Just d))
fullUpdate4 (?Just (a,b,c,d)) (?Just _) = (ExistingTupleSubEditor (?Just a), ExistingTupleSubEditor (?Just b), ExistingTupleSubEditor (?Just c), ExistingTupleSubEditor (?Just d))

fullUpdate5 :: (?(a,b,c,d,e)) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem a, TupleSubEditorItem b, TupleSubEditorItem c, TupleSubEditorItem d, TupleSubEditorItem e)
fullUpdate5 ?None _ = (NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None, NewTupleSubEditor ?None)
fullUpdate5 (?Just (a,b,c,d,e)) ?None = (NewTupleSubEditor (?Just a), NewTupleSubEditor (?Just b), NewTupleSubEditor (?Just c), NewTupleSubEditor (?Just d), NewTupleSubEditor (?Just e))
fullUpdate5 (?Just (a,b,c,d,e)) (?Just _) = (ExistingTupleSubEditor (?Just a), ExistingTupleSubEditor (?Just b), ExistingTupleSubEditor (?Just c), ExistingTupleSubEditor (?Just d), ExistingTupleSubEditor (?Just e))

//Empty container
group :: !UIType -> Editor () ()
group type = {Editor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr _ vst   = (Ok (uia type attr, CompoundState ?None [], ?Just ()),vst)
	onEdit _ st vst    = (Ok ?None,vst)
	onRefresh _ st vst = (Ok (NoChange, st, ?None),vst)
	writeValue _     = Ok ()

groupl :: !UIType !((?rl) [(EditorRef,w)] -> [ListSubEditorItem r]) !(Editor r w) -> Editor rl [w]
groupl type refresh {Editor|onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst = case onResetChildren childValues vst of
        (Error e,vst)            = (Error e,vst)
        (Ok (uis, childSts, ws, noWrites),vst) = (Ok (UI type attr uis, (), childSts, if noWrites ?None (?Just ws)),vst)
	where
		childValues = [mbx \\ (NewListSubEditor mbx) <- refresh mbval []]

		onResetChildren [] vst = (Ok ([],[],[],True),vst)
		onResetChildren [mode:modes] vst = case onReset_a emptyAttr mode vst of
			(Error e, vst) = (Error e,vst)
			(Ok (ui, st, mbw), vst) = case maybe (writeValue_a st) Ok mbw of
				(Error e) = (Error e,vst)
				(Ok w) = case onResetChildren modes vst of
					(Error e,vst) = (Error e,vst)
					(Ok (uis, sts, ws, noWrites),vst) = (Ok ([ui: uis], [st: sts], [w:ws], noWrites && isNone mbw),vst)

	onEdit (eventId,e) st childSts vst
		= case onEditChildren 0 childSts vst of
			(Ok ?None,vst) = (Ok ?None,vst)
			(Ok (?Just (i,change,childSts,mbw)),vst) = case mbw of
				?None = (Ok (?Just (change, st, childSts, ?None)), vst)
				(?Just w) = case writeValue st childSts of
					(Error e) = (Error e,vst)
					(Ok ws) = (Ok (?Just (change, st, childSts, ?Just (updateAt i w ws))), vst)
			(Error e,vst) = (Error e,vst)
	where
		onEditChildren i [] vst = (Ok ?None,vst)
		onEditChildren i [childSt:childSts] vst
			= case onEdit_a (eventId,e) childSt vst of
				//Event not found, keep searching
				(Ok ?None,vst) = case onEditChildren (i + 1) childSts vst of
					(Ok ?None, vst) = (Ok ?None, vst)
					(Ok (?Just (i,change,childSts,mbw)),vst) = (Ok (?Just (i,change,[childSt:childSts],mbw)),vst) 
				//Event processed, we can stop searching
				(Ok (?Just (change,childSt,mbw)),vst)
                	# change = case change of NoChange = NoChange ; _ = ChangeUI [] [(i,ChangeChild change)]
					= (Ok (?Just (i,change,[childSt:childSts],mbw)),vst)

	onRefresh new st childSts vst = case writeValue st childSts of
		(Error e) = (Error e,vst)
		(Ok cws)
			# newChildren = refresh (?Just new) [(EditorRef n,w) \\ w <- cws & n <- [0..]]
			//Determine which existing children need to be removed
			# removeIdx = reverse $ sort $ difference [0 .. length cws - 1] [r \\ ExistingListSubEditor (EditorRef r) _ <- newChildren]
			# removals = [(i,RemoveChild) \\ i <- removeIdx] //Remove from in reverse to prevent indexes to change during removal
			//Determine the new order of the remaining existing children and shift them if necessary.
			# reordering = calculateReordering [n \\ ExistingListSubEditor (EditorRef n) _ <- newChildren]
			//Determine the new children, by updating or inserting items
			= case onRefreshChildren 0 childSts newChildren vst of
				(Error e, vst) = (Error e, vst)
				(Ok (updatesAndInserts, childSts, ws, noWrites), vst)
					# change = ChangeUI [] (removals ++ reordering  ++ updatesAndInserts)
					//If the number of children changes, we also need to write the new values
					# numItemsChanged = length cws <> length newChildren
					# noWrites = noWrites && not numItemsChanged
					= (Ok (change,st,childSts, if noWrites ?None (?Just ws)),vst)
	where
		calculateReordering newOrder = calculateMoves 0 (sort newOrder) newOrder //The old order was sorted ascending
		where
			calculateMoves i curOrder [] = []
			calculateMoves i curOrder [r:rs]
				# (idx,curOrder) = removeIndex r curOrder
				| idx <> 0 = [(i + idx, MoveChild i):calculateMoves (i + 1) curOrder rs]
				| otherwise = calculateMoves (i + 1) curOrder rs

		onRefreshChildren i childStates [] vst = (Ok ([],[],[],True),vst)
		onRefreshChildren i childStates [NewListSubEditor mba:items] vst
			= case onReset_a emptyAttr mba vst of
				(Error e, vst) = (Error e,vst)
				(Ok (ui, st, mbw), vst) = case maybe (writeValue_a st) Ok mbw of
					(Error e) = (Error e, vst)
					(Ok w) = case onRefreshChildren (i + 1) childStates items vst of
						(Error e,vst) = (Error e,vst)
						(Ok (cs, sts, ws, noWrites),vst) = (Ok ([(i,InsertChild ui): cs], [st: sts], [w:ws], noWrites && mbw =: ?None),vst)

		onRefreshChildren i childStates [ExistingListSubEditor (EditorRef ref) ?None:items] vst //We don't need to update the item
			# st = childStates !! ref
			= case writeValue_a st of
				(Error e) = (Error e, vst)
				(Ok w) = case onRefreshChildren (i + 1) childStates items vst of
					(Error e,vst) = (Error e,vst)
					(Ok (cs, sts, ws, noWrites),vst) = (Ok (cs, [st:sts], [w:ws], noWrites),vst)

		onRefreshChildren i childStates [ExistingListSubEditor (EditorRef ref) (?Just new):items] vst //Refresh the existing editor
			# st = childStates !! ref
			= case onRefresh_a new st vst of
				(Error e, vst) = (Error e, vst)
				(Ok (c, st, mbw), vst) = case maybe (writeValue_a st) Ok mbw of
					(Error e) = (Error e, vst)
					(Ok w) = case onRefreshChildren (i + 1) childStates items vst of
						(Error e,vst) = (Error e,vst)
						(Ok (cs, sts, ws, noWrites),vst) = (Ok ([(i,ChangeChild c):cs], [st:sts], [w:ws], noWrites && mbw =: ?None),vst)

	writeValue _ childSts = writeValueChildren childSts []
	where
		writeValueChildren [] acc = Ok $ reverse acc
		writeValueChildren [st: sts] acc = case writeValue_a st of
			Ok val = writeValueChildren sts [val: acc]
			Error e = Error e

group1 :: !UIType !(Editor r w) -> Editor r w
group1 type {Editor |onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst = case onReset_a emptyAttr mbval vst of
		(Error e,vst)        = (Error e,vst)
		(Ok (ui1,mask1,mbw),vst) = (Ok (UI type attr [ui1], (), [mask1], mbw),vst)

	onEdit (eventId,e) _ [m1] vst = case onEdit_a (eventId,e) m1 vst of
		(Ok ?None,vst) = (Ok ?None,vst)
		(Ok (?Just (NoChange,m1,mbw)),vst) = (Ok (?Just (NoChange, (), [m1],mbw)),vst)
		(Ok (?Just (c1,m1,mbw)),vst)       = (Ok (?Just (ChangeUI [] [(0,ChangeChild c1)], (), [m1],mbw)),vst)
		(Error e,vst) = (Error e,vst)
	onEdit _ _ _ vst = (Error "Event route out of range",vst)

	onRefresh new _ [m1] vst = case onRefresh_a new m1 vst of
		(Error e,vst)              = (Error e,vst)
		(Ok (NoChange,m1,mbw),vst) = (Ok (NoChange, (), [m1], mbw),vst)
		(Ok (c1,m1,mbw),vst)       = (Ok (ChangeUI [] [(0,ChangeChild c1)], (), [m1], mbw),vst)

	writeValue _ [m1] = writeValue_a m1 
	writeValue _ _ = Error "corrupt editor state in group1"

group2 :: !UIType !((?rt) (?(wa,wb)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb)) !(Editor ra wa) !(Editor rb wb) -> Editor rt (wa,wb)
group2 type refresh
	{Editor |onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
    {Editor |onReset=onReset_b,onEdit=onEdit_b,onRefresh=onRefresh_b,writeValue=writeValue_b}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst
		# (mbval1,mbval2) = case refresh mbval ?None of
			(NewTupleSubEditor mbval1, NewTupleSubEditor mbval2) = (mbval1,mbval2)
			_ = (?None,?None)
		= case onReset_a emptyAttr mbval1 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (ui1,m1,mbw1),vst) = case onReset_b emptyAttr mbval2 vst of
				(Error e,vst)     = (Error e,vst)
				(Ok (ui2,m2,mbw2),vst)
					# ui = UI type attr [ui1,ui2]
					| isNone mbw1 && isNone mbw2
						= (Ok (ui, (), [m1,m2],?None),vst)
					# mbw1 = maybe (writeValue_a m1) Ok mbw1
					| mbw1 =: Error _ = (liftError mbw1,vst)
					# mbw2 = maybe (writeValue_b m2) Ok mbw2
					| mbw2 =: Error _ = (liftError mbw2,vst)
					= (Ok (ui, (), [m1,m2],?Just (fromOk mbw1,fromOk mbw2)),vst)

	onEdit (eventId,e) _ [m1,m2] vst = case onEdit_a (eventId,e) m1 vst of
		(Error e,vst) = (Error e,vst)
		(Ok (?Just (c1,m1,mbw1)),vst)
			# c = case c1 of NoChange = NoChange; _ = ChangeUI [] [(0,ChangeChild c1)]
			| isNone mbw1
				= (Ok (?Just (c, (), [m1,m2], ?None)),vst)
			# mbw2 = writeValue_b m2
			| mbw2 =: Error _ = (liftError mbw2,vst)
			= (Ok (?Just (c, (), [m1,m2], ?Just (fromJust mbw1,fromOk mbw2))),vst)
		(Ok ?None,vst) = case onEdit_b (eventId,e) m2 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (?Just (c2,m2,mbw2)),vst)
				# c = case c2 of NoChange = NoChange; _ = ChangeUI [] [(1,ChangeChild c2)]
				| isNone mbw2
					= (Ok (?Just (c, (), [m1,m2], ?None)),vst)
				# mbw1 = writeValue_a m1
				| mbw1 =: Error _ = (liftError mbw1,vst)
				= (Ok (?Just (c, (), [m1,m2], ?Just (fromOk mbw1,fromJust mbw2))),vst)
			(Ok ?None,vst) = (Ok ?None,vst)
	onEdit _ _ _ vst = (Error "Corrupt state in group2",vst)

	onRefresh new _ [m1,m2] vst
		# mbw1 = writeValue_a m1
		| mbw1 =: (Error _) = (liftError mbw1,vst)
		# mbw2 = writeValue_b m2
		| mbw2 =: (Error _) = (liftError mbw2,vst)
		# (n1,n2) = refresh (?Just new) (?Just (fromOk mbw1,fromOk mbw2))
		# (res1,vst) = case n1 of
			(NewTupleSubEditor mbn1)
				= case onReset_a emptyAttr mbn1 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui1,m1,mbw1),vst) = (Ok (ReplaceUI ui1,m1,mbw1),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m1,?None),vst)
			(ExistingTupleSubEditor (?Just n1))
				= onRefresh_a n1 m1 vst
		| res1 =:(Error _) = (liftError res1,vst)
		# (res2,vst) = case n2 of
			(NewTupleSubEditor mbn2)
				= case onReset_b emptyAttr mbn2 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui2,m2,mbw2),vst) = (Ok (ReplaceUI ui2,m2,mbw2),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m2,?None),vst)
			(ExistingTupleSubEditor (?Just n2))
				= onRefresh_b n2 m2 vst
		| res2 =:(Error _) = (liftError res2,vst)
		# (Ok (c1,m1,mbw1)) = res1
		# (Ok (c2,m2,mbw2)) = res2
		# c = case [(i,ChangeChild c) \\ (i,c) <- [(0,c1),(1,c2)] | not c =: NoChange] of
			[] = NoChange
			changes = ChangeUI [] changes
		| isNone mbw1 && isNone mbw2
			= (Ok (c, (), [m1,m2],?None),vst)
		# mbw1 = maybe (writeValue_a m1) Ok mbw1
		| mbw1 =:(Error _)
			= (liftError mbw1,vst)
		# mbw2 = maybe (writeValue_b m2) Ok mbw2
		| mbw2 =:(Error _)
			= (liftError mbw2,vst)
		= (Ok (c, (), [m1,m2], ?Just (fromOk mbw1,fromOk mbw2)),vst)

	writeValue _ [m1, m2] = case (writeValue_a m1, writeValue_b m2) of
		(Ok val1, Ok val2) = Ok (val1, val2)
		(Error e1, _) = Error e1
		(_, Error e2) = Error e2
		_ = Error "corrupt editor state in group2"

group3 :: !UIType !((?rt) (?(wa,wb,wc)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc))
          !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) -> Editor rt (wa,wb,wc)
group3 type refresh
	{Editor |onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
    {Editor |onReset=onReset_b,onEdit=onEdit_b,onRefresh=onRefresh_b,writeValue=writeValue_b}
    {Editor |onReset=onReset_c,onEdit=onEdit_c,onRefresh=onRefresh_c,writeValue=writeValue_c}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst
		# (mbval1,mbval2,mbval3) = case refresh mbval ?None of
			(NewTupleSubEditor mbval1, NewTupleSubEditor mbval2, NewTupleSubEditor mbval3) = (mbval1,mbval2,mbval3)
			_ = (?None,?None,?None)
		= case onReset_a emptyAttr mbval1 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (ui1,m1,mbw1),vst) = case onReset_b emptyAttr mbval2 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (ui2,m2,mbw2),vst) = case onReset_c emptyAttr mbval3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst)
						# ui = UI type attr [ui1,ui2,ui3]
						| isNone mbw1 && isNone mbw2 && isNone mbw3
							= (Ok (ui, (), [m1,m2,m3],?None),vst)
						# mbw1 = maybe (writeValue_a m1) Ok mbw1
						| mbw1 =: Error _ = (liftError mbw1,vst)
						# mbw2 = maybe (writeValue_b m2) Ok mbw2
						| mbw2 =: Error _ = (liftError mbw2,vst)
						# mbw3 = maybe (writeValue_c m3) Ok mbw3
						| mbw3 =: Error _ = (liftError mbw3,vst)
						= (Ok (ui, (), [m1,m2,m3],?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3)),vst)

	onEdit (eventId,e) _ [m1,m2,m3] vst = case onEdit_a (eventId,e) m1 vst of
		(Error e,vst) = (Error e,vst)
		(Ok (?Just (c1,m1,mbw1)),vst)
			# c = case c1 of NoChange = NoChange; _ = ChangeUI [] [(0,ChangeChild c1)]
			| isNone mbw1
				= (Ok (?Just (c, (), [m1,m2,m3], ?None)),vst)
			# mbw2 = writeValue_b m2
			| mbw2 =: Error _ = (liftError mbw2,vst)
			# mbw3 = writeValue_c m3
			| mbw3 =: Error _ = (liftError mbw3,vst)
			= (Ok (?Just (c, (), [m1,m2,m3], ?Just (fromJust mbw1,fromOk mbw2,fromOk mbw3))),vst)
		(Ok ?None,vst) = case onEdit_b (eventId,e) m2 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (?Just (c2,m2,mbw2)),vst)
				# c = case c2 of NoChange = NoChange; _ = ChangeUI [] [(1,ChangeChild c2)]
				| isNone mbw2
					= (Ok (?Just (c, (), [m1,m2,m3], ?None)),vst)
				# mbw1 = writeValue_a m1
				| mbw1 =: Error _ = (liftError mbw1,vst)
				# mbw3 = writeValue_c m3
				| mbw3 =: Error _ = (liftError mbw3,vst)
				= (Ok (?Just (c, (), [m1,m2,m3], ?Just (fromOk mbw1,fromJust mbw2,fromOk mbw3))),vst)
			(Ok ?None,vst) = case onEdit_c (eventId,e) m3 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (?Just (c3,m3,mbw3)),vst)
					# c = case c3 of NoChange = NoChange; _ = ChangeUI [] [(2,ChangeChild c3)]
					| isNone mbw3
						= (Ok (?Just (c, (), [m1,m2,m3], ?None)),vst)
					# mbw1 = writeValue_a m1
					| mbw1 =: Error _ = (liftError mbw1,vst)
					# mbw2 = writeValue_b m2
					| mbw2 =: Error _ = (liftError mbw2,vst)
					= (Ok (?Just (c, (), [m1,m2,m3], ?Just (fromOk mbw1,fromOk mbw2,fromJust mbw3))),vst)
				(Ok ?None,vst) = (Ok ?None,vst)
	onEdit _ _ _ vst = (Error "Corrupt state in group3",vst)

	onRefresh new _ [m1,m2,m3] vst
		# mbw1 = writeValue_a m1
		| mbw1 =: (Error _) = (liftError mbw1,vst)
		# mbw2 = writeValue_b m2
		| mbw2 =: (Error _) = (liftError mbw2,vst)
		# mbw3 = writeValue_c m3
		| mbw3 =: (Error _) = (liftError mbw3,vst)
		# (n1,n2,n3) = refresh (?Just new) (?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3))
		# (res1,vst) = case n1 of
			(NewTupleSubEditor mbn1)
				= case onReset_a emptyAttr mbn1 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui1,m1,mbw1),vst) = (Ok (ReplaceUI ui1,m1,mbw1),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m1,?None),vst)
			(ExistingTupleSubEditor (?Just n1))
				= onRefresh_a n1 m1 vst
		| res1 =:(Error _) = (liftError res1,vst)
		# (res2,vst) = case n2 of
			(NewTupleSubEditor mbn2)
				= case onReset_b emptyAttr mbn2 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui2,m2,mbw2),vst) = (Ok (ReplaceUI ui2,m2,mbw2),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m2,?None),vst)
			(ExistingTupleSubEditor (?Just n2))
				= onRefresh_b n2 m2 vst
		| res2 =:(Error _) = (liftError res2,vst)
		# (res3,vst) = case n3 of
			(NewTupleSubEditor mbn3)
				= case onReset_c emptyAttr mbn3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst) = (Ok (ReplaceUI ui3,m3,mbw3),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m3,?None),vst)
			(ExistingTupleSubEditor (?Just n3))
				= onRefresh_c n3 m3 vst
		| res3 =:(Error _) = (liftError res3,vst)
		# (Ok (c1,m1,mbw1)) = res1
		# (Ok (c2,m2,mbw2)) = res2
		# (Ok (c3,m3,mbw3)) = res3
		# c = case [(i,ChangeChild c) \\ (i,c) <- [(0,c1),(1,c2),(2,c3)] | not c =: NoChange] of
			[] = NoChange
			changes = ChangeUI [] changes
		| isNone mbw1 && isNone mbw2 && isNone mbw3
			= (Ok (c, (), [m1,m2,m3],?None),vst)
		# mbw1 = maybe (writeValue_a m1) Ok mbw1
		| mbw1 =:(Error _)
			= (liftError mbw1,vst)
		# mbw2 = maybe (writeValue_b m2) Ok mbw2
		| mbw2 =:(Error _)
			= (liftError mbw2,vst)
		# mbw3 = maybe (writeValue_c m3) Ok mbw3
		| mbw3 =:(Error _)
			= (liftError mbw3,vst)
		= (Ok (c, (), [m1,m2,m3], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3)),vst)

	writeValue _ [m1, m2, m3] = case (writeValue_a m1, writeValue_b m2, writeValue_c m3) of
		(Ok val1, Ok val2, Ok val3) = Ok (val1, val2, val3)
		(Error e1, _, _) = Error e1
		(_, Error e2, _) = Error e2
		(_, _, Error e3) = Error e3
		_ = Error "corrupt editor state in group3"

group4 :: !UIType !((?rt) (?(wa,wb,wc,wd)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd))
          !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) -> Editor rt (wa,wb,wc,wd)
group4 type refresh
	{Editor |onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
    {Editor |onReset=onReset_b,onEdit=onEdit_b,onRefresh=onRefresh_b,writeValue=writeValue_b}
    {Editor |onReset=onReset_c,onEdit=onEdit_c,onRefresh=onRefresh_c,writeValue=writeValue_c}
    {Editor |onReset=onReset_d,onEdit=onEdit_d,onRefresh=onRefresh_d,writeValue=writeValue_d}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst 
		# (mbval1,mbval2,mbval3,mbval4) = case refresh mbval ?None of
			(NewTupleSubEditor mbval1, NewTupleSubEditor mbval2, NewTupleSubEditor mbval3,NewTupleSubEditor mbval4) = (mbval1,mbval2,mbval3,mbval4)
			_ = (?None,?None,?None,?None)
		= case onReset_a emptyAttr mbval1 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (ui1,m1,mbw1),vst) = case onReset_b emptyAttr mbval2 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (ui2,m2,mbw2),vst) = case onReset_c emptyAttr mbval3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst) = case onReset_d emptyAttr mbval4 vst of
						(Error e,vst) = (Error e,vst)
						(Ok (ui4,m4,mbw4),vst)
							# ui = UI type attr [ui1,ui2,ui3,ui4]
							| isNone mbw1 && isNone mbw2 && isNone mbw3 && isNone mbw4
								= (Ok (ui, (), [m1,m2,m3,m4],?None),vst)
							# mbw1 = maybe (writeValue_a m1) Ok mbw1
							| mbw1 =: Error _ = (liftError mbw1,vst)
							# mbw2 = maybe (writeValue_b m2) Ok mbw2
							| mbw2 =: Error _ = (liftError mbw2,vst)
							# mbw3 = maybe (writeValue_c m3) Ok mbw3
							| mbw3 =: Error _ = (liftError mbw3,vst)
							# mbw4 = maybe (writeValue_d m4) Ok mbw4
							| mbw4 =: Error _ = (liftError mbw4,vst)
							= (Ok (ui, (), [m1,m2,m3,m4],?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4)),vst)

	onEdit (eventId,e) _ [m1,m2,m3,m4] vst = case onEdit_a (eventId,e) m1 vst of
		(Error e,vst) = (Error e,vst)
		(Ok (?Just (c1,m1,mbw1)),vst)
			# c = case c1 of NoChange = NoChange; _ = ChangeUI [] [(0,ChangeChild c1)]
			| isNone mbw1
				= (Ok (?Just (c, (), [m1,m2,m3,m4], ?None)),vst)
			# mbw2 = writeValue_b m2
			| mbw2 =: Error _ = (liftError mbw2,vst)
			# mbw3 = writeValue_c m3
			| mbw3 =: Error _ = (liftError mbw3,vst)
			# mbw4 = writeValue_d m4
			| mbw4 =: Error _ = (liftError mbw4,vst)
			= (Ok (?Just (c, (), [m1,m2,m3,m4], ?Just (fromJust mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4))),vst)
		(Ok ?None,vst) = case onEdit_b (eventId,e) m2 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (?Just (c2,m2,mbw2)),vst)
				# c = case c2 of NoChange = NoChange; _ = ChangeUI [] [(1,ChangeChild c2)]
				| isNone mbw2
					= (Ok (?Just (c, (), [m1,m2,m3,m4], ?None)),vst)
				# mbw1 = writeValue_a m1
				| mbw1 =: Error _ = (liftError mbw1,vst)
				# mbw3 = writeValue_c m3
				| mbw3 =: Error _ = (liftError mbw3,vst)
				# mbw4 = writeValue_d m4
				| mbw4 =: Error _ = (liftError mbw4,vst)
				= (Ok (?Just (c, (), [m1,m2,m3,m4], ?Just (fromOk mbw1,fromJust mbw2,fromOk mbw3,fromOk mbw4))),vst)
			(Ok ?None,vst) = case onEdit_c (eventId,e) m3 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (?Just (c3,m3,mbw3)),vst)
					# c = case c3 of NoChange = NoChange; _ = ChangeUI [] [(2,ChangeChild c3)]
					| isNone mbw3
						= (Ok (?Just (c, (), [m1,m2,m3,m4], ?None)),vst)
					# mbw1 = writeValue_a m1
					| mbw1 =: Error _ = (liftError mbw1,vst)
					# mbw2 = writeValue_b m2
					| mbw2 =: Error _ = (liftError mbw2,vst)
					# mbw4 = writeValue_d m4
					| mbw4 =: Error _ = (liftError mbw4,vst)
					= (Ok (?Just (c, (), [m1,m2,m3,m4], ?Just (fromOk mbw1,fromOk mbw2,fromJust mbw3,fromOk mbw4))),vst)
				(Ok ?None,vst) = case onEdit_d (eventId,e) m4 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (?Just (c4,m4,mbw4)),vst)
						# c = case c4 of NoChange = NoChange; _ = ChangeUI [] [(3,ChangeChild c4)]
						| isNone mbw4
							= (Ok (?Just (c, (), [m1,m2,m3,m4], ?None)),vst)
						# mbw1 = writeValue_a m1
						| mbw1 =: Error _ = (liftError mbw1,vst)
						# mbw2 = writeValue_b m2
						| mbw2 =: Error _ = (liftError mbw2,vst)
						# mbw3 = writeValue_c m3
						| mbw3 =: Error _ = (liftError mbw3,vst)
						= (Ok (?Just (c, (), [m1,m2,m3,m4], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromJust mbw4))),vst)
					(Ok ?None,vst) = (Ok ?None,vst)
	onEdit _ _ _ vst = (Error "Corrupt state in group4",vst)

	onRefresh new _ [m1,m2,m3,m4] vst
		# mbw1 = writeValue_a m1
		| mbw1 =: (Error _) = (liftError mbw1,vst)
		# mbw2 = writeValue_b m2
		| mbw2 =: (Error _) = (liftError mbw2,vst)
		# mbw3 = writeValue_c m3
		| mbw3 =: (Error _) = (liftError mbw3,vst)
		# mbw4 = writeValue_d m4
		| mbw4 =: (Error _) = (liftError mbw4,vst)
		# (n1,n2,n3,n4) = refresh (?Just new) (?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4))
		# (res1,vst) = case n1 of
			(NewTupleSubEditor mbn1)
				= case onReset_a emptyAttr mbn1 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui1,m1,mbw1),vst) = (Ok (ReplaceUI ui1,m1,mbw1),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m1,?None),vst)
			(ExistingTupleSubEditor (?Just n1))
				= onRefresh_a n1 m1 vst
		| res1 =:(Error _) = (liftError res1,vst)
		# (res2,vst) = case n2 of
			(NewTupleSubEditor mbn2)
				= case onReset_b emptyAttr mbn2 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui2,m2,mbw2),vst) = (Ok (ReplaceUI ui2,m2,mbw2),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m2,?None),vst)
			(ExistingTupleSubEditor (?Just n2))
				= onRefresh_b n2 m2 vst
		| res2 =:(Error _) = (liftError res2,vst)
		# (res3,vst) = case n3 of
			(NewTupleSubEditor mbn3)
				= case onReset_c emptyAttr mbn3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst) = (Ok (ReplaceUI ui3,m3,mbw3),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m3,?None),vst)
			(ExistingTupleSubEditor (?Just n3))
				= onRefresh_c n3 m3 vst
		| res3 =:(Error _) = (liftError res3,vst)
		# (res4,vst) = case n4 of
			(NewTupleSubEditor mbn4)
				= case onReset_d emptyAttr mbn4 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui4,m4,mbw4),vst) = (Ok (ReplaceUI ui4,m4,mbw4),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m4,?None),vst)
			(ExistingTupleSubEditor (?Just n4))
				= onRefresh_d n4 m4 vst
		| res4 =:(Error _) = (liftError res4,vst)
		# (Ok (c1,m1,mbw1)) = res1
		# (Ok (c2,m2,mbw2)) = res2
		# (Ok (c3,m3,mbw3)) = res3
		# (Ok (c4,m4,mbw4)) = res4
		# c = case [(i,ChangeChild c) \\ (i,c) <- [(0,c1),(1,c2),(2,c3),(3,c4)] | not c =: NoChange] of
			[] = NoChange
			changes = ChangeUI [] changes
		| isNone mbw1 && isNone mbw2 && isNone mbw3 && isNone mbw4
			= (Ok (c, (), [m1,m2,m3,m4],?None),vst)
		# mbw1 = maybe (writeValue_a m1) Ok mbw1
		| mbw1 =:(Error _)
			= (liftError mbw1,vst)
		# mbw2 = maybe (writeValue_b m2) Ok mbw2
		| mbw2 =:(Error _)
			= (liftError mbw2,vst)
		# mbw3 = maybe (writeValue_c m3) Ok mbw3
		| mbw3 =:(Error _)
			= (liftError mbw3,vst)
		# mbw4 = maybe (writeValue_d m4) Ok mbw4
		| mbw4 =:(Error _)
			= (liftError mbw4,vst)
		= (Ok (c, (), [m1,m2,m3,m4], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4)),vst)

	writeValue _ [m1, m2, m3, m4] = case (writeValue_a m1, writeValue_b m2, writeValue_c m3, writeValue_d m4) of
		(Ok val1, Ok val2, Ok val3, Ok val4) = Ok (val1, val2, val3, val4)
		(Error e1, _, _, _) = Error e1
		(_, Error e2, _, _) = Error e2
		(_, _, Error e3, _) = Error e3
		(_, _, _, Error e4) = Error e4
		_ = Error "corrupt editor state in group4"

group5 :: !UIType !((?rt) (?(wa,wb,wc,wd,we)) -> (TupleSubEditorItem ra,TupleSubEditorItem rb,TupleSubEditorItem rc,TupleSubEditorItem rd,TupleSubEditorItem re))
              !(Editor ra wa) !(Editor rb wb) !(Editor rc wc) !(Editor rd wd) !(Editor re we) -> Editor rt (wa,wb,wc,wd,we)
group5 type refresh
	{Editor |onReset=onReset_a,onEdit=onEdit_a,onRefresh=onRefresh_a,writeValue=writeValue_a}
    {Editor |onReset=onReset_b,onEdit=onEdit_b,onRefresh=onRefresh_b,writeValue=writeValue_b}
    {Editor |onReset=onReset_c,onEdit=onEdit_c,onRefresh=onRefresh_c,writeValue=writeValue_c}
    {Editor |onReset=onReset_d,onEdit=onEdit_d,onRefresh=onRefresh_d,writeValue=writeValue_d}
    {Editor |onReset=onReset_e,onEdit=onEdit_e,onRefresh=onRefresh_e,writeValue=writeValue_e}
	= compoundEditorToEditor
		{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst 
		# (mbval1,mbval2,mbval3,mbval4,mbval5) = case refresh mbval ?None of
			(NewTupleSubEditor mbval1, NewTupleSubEditor mbval2, NewTupleSubEditor mbval3, NewTupleSubEditor mbval4, NewTupleSubEditor mbval5)
				= (mbval1,mbval2,mbval3,mbval4,mbval5)
			_ = (?None,?None,?None,?None,?None)
		= case onReset_a emptyAttr mbval1 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (ui1,m1,mbw1),vst) = case onReset_b emptyAttr mbval2 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (ui2,m2,mbw2),vst) = case onReset_c emptyAttr mbval3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst) = case onReset_d emptyAttr mbval4 vst of
						(Error e,vst) = (Error e,vst)
						(Ok (ui4,m4,mbw4),vst) = case onReset_e emptyAttr mbval5 vst of
							(Error e,vst) = (Error e,vst)
							(Ok (ui5,m5,mbw5),vst)
								# ui = UI type attr [ui1,ui2,ui3,ui4,ui5]
								| isNone mbw1 && isNone mbw2 && isNone mbw3 && isNone mbw4 && isNone mbw5
									= (Ok (ui, (), [m1,m2,m3,m4,m5],?None),vst)
								# mbw1 = maybe (writeValue_a m1) Ok mbw1
								| mbw1 =: Error _ = (liftError mbw1,vst)
								# mbw2 = maybe (writeValue_b m2) Ok mbw2
								| mbw2 =: Error _ = (liftError mbw2,vst)
								# mbw3 = maybe (writeValue_c m3) Ok mbw3
								| mbw3 =: Error _ = (liftError mbw3,vst)
								# mbw4 = maybe (writeValue_d m4) Ok mbw4
								| mbw4 =: Error _ = (liftError mbw4,vst)
								# mbw5 = maybe (writeValue_e m5) Ok mbw5
								| mbw5 =: Error _ = (liftError mbw5,vst)
								= (Ok (ui, (), [m1,m2,m3,m4,m5],?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4,fromOk mbw5)),vst)

	onEdit (eventId,e) _ [m1,m2,m3,m4,m5] vst = case onEdit_a (eventId,e) m1 vst of
		(Error e,vst) = (Error e,vst)
		(Ok (?Just (c1,m1,mbw1)),vst)
			# c = case c1 of NoChange = NoChange; _ = ChangeUI [] [(0,ChangeChild c1)]
			| isNone mbw1
				= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?None)),vst)
			# mbw2 = writeValue_b m2
			| mbw2 =: Error _ = (liftError mbw2,vst)
			# mbw3 = writeValue_c m3
			| mbw3 =: Error _ = (liftError mbw3,vst)
			# mbw4 = writeValue_d m4
			| mbw4 =: Error _ = (liftError mbw4,vst)
			# mbw5 = writeValue_e m5
			| mbw5 =: Error _ = (liftError mbw5,vst)
			= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?Just (fromJust mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4,fromOk mbw5))),vst)
		(Ok ?None,vst) = case onEdit_b (eventId,e) m2 vst of
			(Error e,vst) = (Error e,vst)
			(Ok (?Just (c2,m2,mbw2)),vst)
				# c = case c2 of NoChange = NoChange; _ = ChangeUI [] [(1,ChangeChild c2)]
				| isNone mbw2
					= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?None)),vst)
				# mbw1 = writeValue_a m1
				| mbw1 =: Error _ = (liftError mbw1,vst)
				# mbw3 = writeValue_c m3
				| mbw3 =: Error _ = (liftError mbw3,vst)
				# mbw4 = writeValue_d m4
				| mbw4 =: Error _ = (liftError mbw4,vst)
				# mbw5 = writeValue_e m5
				| mbw5 =: Error _ = (liftError mbw5,vst)
				= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?Just (fromOk mbw1,fromJust mbw2,fromOk mbw3,fromOk mbw4,fromOk mbw5))),vst)
			(Ok ?None,vst) = case onEdit_c (eventId,e) m3 vst of
				(Error e,vst) = (Error e,vst)
				(Ok (?Just (c3,m3,mbw3)),vst)
					# c = case c3 of NoChange = NoChange; _ = ChangeUI [] [(2,ChangeChild c3)]
					| isNone mbw3
						= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?None)),vst)
					# mbw1 = writeValue_a m1
					| mbw1 =: Error _ = (liftError mbw1,vst)
					# mbw2 = writeValue_b m2
					| mbw2 =: Error _ = (liftError mbw2,vst)
					# mbw4 = writeValue_d m4
					| mbw4 =: Error _ = (liftError mbw4,vst)
					# mbw5 = writeValue_e m5
					| mbw5 =: Error _ = (liftError mbw5,vst)
					= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?Just (fromOk mbw1,fromOk mbw2,fromJust mbw3,fromOk mbw4,fromOk mbw5))),vst)
				(Ok ?None,vst) = case onEdit_d (eventId,e) m4 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (?Just (c4,m4,mbw4)),vst)
						# c = case c4 of NoChange = NoChange; _ = ChangeUI [] [(3,ChangeChild c4)]
						| isNone mbw4
							= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?None)),vst)
						# mbw1 = writeValue_a m1
						| mbw1 =: Error _ = (liftError mbw1,vst)
						# mbw2 = writeValue_b m2
						| mbw2 =: Error _ = (liftError mbw2,vst)
						# mbw3 = writeValue_c m3
						| mbw3 =: Error _ = (liftError mbw3,vst)
						# mbw5 = writeValue_e m5
						| mbw5 =: Error _ = (liftError mbw5,vst)
						= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromJust mbw4,fromOk mbw5))),vst)
					(Ok ?None,vst) = case onEdit_e (eventId,e) m5 vst of
						(Error e,vst) = (Error e,vst)
						(Ok (?Just (c5,m5,mbw5)),vst)
							# c = case c5 of NoChange = NoChange; _ = ChangeUI [] [(4,ChangeChild c5)]
							| isNone mbw5
								= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?None)),vst)
							# mbw1 = writeValue_a m1
							| mbw1 =: Error _ = (liftError mbw1,vst)
							# mbw2 = writeValue_b m2
							| mbw2 =: Error _ = (liftError mbw2,vst)
							# mbw3 = writeValue_c m3
							| mbw3 =: Error _ = (liftError mbw3,vst)
							# mbw4 = writeValue_d m4
							| mbw4 =: Error _ = (liftError mbw4,vst)
							= (Ok (?Just (c, (), [m1,m2,m3,m4,m5], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4,fromJust mbw5))),vst)
						(Ok ?None,vst) = (Ok ?None,vst)
	onEdit _ _ _ vst = (Error "Corrupt state in group 5",vst)

	onRefresh new _ [m1,m2,m3,m4,m5] vst
		# mbw1 = writeValue_a m1
		| mbw1 =: (Error _) = (liftError mbw1,vst)
		# mbw2 = writeValue_b m2
		| mbw2 =: (Error _) = (liftError mbw2,vst)
		# mbw3 = writeValue_c m3
		| mbw3 =: (Error _) = (liftError mbw3,vst)
		# mbw4 = writeValue_d m4
		| mbw4 =: (Error _) = (liftError mbw4,vst)
		# mbw5 = writeValue_e m5
		| mbw5 =: (Error _) = (liftError mbw5,vst)
		# (n1,n2,n3,n4,n5) = refresh (?Just new) (?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4,fromOk mbw5))
		# (res1,vst) = case n1 of
			(NewTupleSubEditor mbn1)
				= case onReset_a emptyAttr mbn1 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui1,m1,mbw1),vst) = (Ok (ReplaceUI ui1,m1,mbw1),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m1,?None),vst)
			(ExistingTupleSubEditor (?Just n1))
				= onRefresh_a n1 m1 vst
		| res1 =:(Error _) = (liftError res1,vst)
		# (res2,vst) = case n2 of
			(NewTupleSubEditor mbn2)
				= case onReset_b emptyAttr mbn2 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui2,m2,mbw2),vst) = (Ok (ReplaceUI ui2,m2,mbw2),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m2,?None),vst)
			(ExistingTupleSubEditor (?Just n2))
				= onRefresh_b n2 m2 vst
		| res2 =:(Error _) = (liftError res2,vst)
		# (res3,vst) = case n3 of
			(NewTupleSubEditor mbn3)
				= case onReset_c emptyAttr mbn3 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui3,m3,mbw3),vst) = (Ok (ReplaceUI ui3,m3,mbw3),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m3,?None),vst)
			(ExistingTupleSubEditor (?Just n3))
				= onRefresh_c n3 m3 vst
		| res3 =:(Error _) = (liftError res3,vst)
		# (res4,vst) = case n4 of
			(NewTupleSubEditor mbn4)
				= case onReset_d emptyAttr mbn4 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui4,m4,mbw4),vst) = (Ok (ReplaceUI ui4,m4,mbw4),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m4,?None),vst)
			(ExistingTupleSubEditor (?Just n4))
				= onRefresh_d n4 m4 vst
		| res4 =:(Error _) = (liftError res4,vst)
		# (res5,vst) = case n5 of
			(NewTupleSubEditor mbn5)
				= case onReset_e emptyAttr mbn5 vst of
					(Error e,vst) = (Error e,vst)
					(Ok (ui5,m5,mbw5),vst) = (Ok (ReplaceUI ui5,m5,mbw5),vst)
			(ExistingTupleSubEditor ?None)
				= (Ok (NoChange,m5,?None),vst)
			(ExistingTupleSubEditor (?Just n5))
				= onRefresh_e n5 m5 vst
		| res5 =:(Error _) = (liftError res5,vst)
		# (Ok (c1,m1,mbw1)) = res1
		# (Ok (c2,m2,mbw2)) = res2
		# (Ok (c3,m3,mbw3)) = res3
		# (Ok (c4,m4,mbw4)) = res4
		# (Ok (c5,m5,mbw5)) = res5
		# c = case [(i,ChangeChild c) \\ (i,c) <- [(0,c1),(1,c2),(2,c3),(3,c4),(4,c5)] | not c =: NoChange] of
			[] = NoChange
			changes = ChangeUI [] changes
		| isNone mbw1 && isNone mbw2 && isNone mbw3 && isNone mbw4 && isNone mbw5
			= (Ok (c, (), [m1,m2,m3,m4,m5],?None),vst)
		# mbw1 = maybe (writeValue_a m1) Ok mbw1
		| mbw1 =:(Error _)
			= (liftError mbw1,vst)
		# mbw2 = maybe (writeValue_b m2) Ok mbw2
		| mbw2 =:(Error _)
			= (liftError mbw2,vst)
		# mbw3 = maybe (writeValue_c m3) Ok mbw3
		| mbw3 =:(Error _)
			= (liftError mbw3,vst)
		# mbw4 = maybe (writeValue_d m4) Ok mbw4
		| mbw4 =:(Error _)
			= (liftError mbw4,vst)
		# mbw5 = maybe (writeValue_e m5) Ok mbw5
		| mbw5 =:(Error _)
			= (liftError mbw5,vst)
		= (Ok (c, (), [m1,m2,m3,m4,m5], ?Just (fromOk mbw1,fromOk mbw2,fromOk mbw3,fromOk mbw4,fromOk mbw5)),vst)

	writeValue _ [m1, m2, m3, m4, m5] = case (writeValue_a m1, writeValue_b m2, writeValue_c m3, writeValue_d m4, writeValue_e m5) of
		(Ok val1, Ok val2, Ok val3, Ok val4, Ok val5) = Ok (val1, val2, val3, val4, val5)
		(Error e1, _, _, _, _) = Error e1
		(_, Error e2, _, _, _) = Error e2
		(_, _, Error e3, _, _) = Error e3
		(_, _, _, Error e4, _) = Error e4
		(_, _, _, _, Error e5) = Error e5
		_ = Error "corrupt editor state in group5"

groupc :: !UIType !(Editor Int (?Int)) ![(w -> a, Editor a w)] -> Editor (Int, a) (Either (?Int) w)
groupc type {Editor|onReset=choiceEditorOnReset,onEdit=choiceEditorOnEdit,onRefresh=choiceEditorOnRefresh,writeValue=choiceEditorWriteValue} fieldEditors = compoundEditorToEditor
	{CompoundEditor|onReset=onReset,onEdit=onEdit,onRefresh=onRefresh,writeValue=writeValue}
where
	onReset attr mbval vst = case choiceEditorOnReset emptyAttr (fst <$> mbval) vst of
		(Error e,vst) = (Error e,vst)
		(Ok (uiSelector, stSelector,_),vst) = case choiceEditorWriteValue stSelector of
			//Only generate the field UI if no selection has been made
			Error e = (Error e,vst)
			(Ok ?None) = (Ok (UI type attr [uiSelector], (), [stSelector], ?None),vst)
			(Ok (?Just choice)) = case fieldEditors !? choice of
				//Only generate the field UI if selection is out of bounds
				?None = (Ok (UI type attr [uiSelector], (), [stSelector], ?None),vst)
				?Just (selectFun, editor)
					= case editor.Editor.onReset emptyAttr ?None vst of
						(Error e,vst) = (Error e,vst)
						(Ok (uiField, stField,mbw),vst)
							 = (Ok (UI type attr [uiSelector, uiField], (), [stSelector, stField],fmap Right mbw),vst)

	onEdit (eventId,edit) _ st=:[stateSelector:optStateField] vst
		= case choiceEditorOnEdit (eventId,edit) stateSelector vst of
			(Error e,vst) = (Error e,vst)
			//The event was affecting the choice editor
			(Ok (?Just (choiceUIChange,stateSelector,_)),vst)
				# (?Just currentChoice, val) = valAndChoiceFromState st
				# mbNewChoice = choiceEditorWriteValue stateSelector
				//Based on the effect of the selection change we may need to update the field editor
				= case (optStateField, mbNewChoice) of
					//Previously no choice was made, but now a choice (within the bounds) has been made
					//-> create an initial UI
					([], Ok (?Just newChoice)) | newChoice >= 0 && newChoice < length fieldEditors
						# (selectFun, editor) = fieldEditors !! newChoice
						# val = fmap selectFun val
						= case editor.Editor.onReset emptyAttr val vst of
							(Error e,vst) = (Error e,vst)
							(Ok (uiField, stateField,_),vst)
								# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,InsertChild uiField)]
								= (Ok (?Just (change,(),[stateSelector, stateField], ?Just (Left (?Just newChoice)))), vst)

					//Previously no choice was made and still no choice has been made
					([], _)
						# change = ChangeUI [] [(0,ChangeChild choiceUIChange)]
						= (Ok (?Just (change,(),[stateSelector], ?None)), vst)
					//A new choice (within the bounds) has been made
					(_, Ok (?Just newChoice)) | newChoice >= 0 && newChoice < length fieldEditors
						| newChoice == currentChoice //The selection stayed the same
							# change = ChangeUI [] [(0,ChangeChild choiceUIChange)]
							= (Ok (?Just (change,(),[stateSelector:optStateField], ?None)), vst)
						| otherwise //The selection changed -> replace with an initial UI of the new choice
							# (selectFun,editor) = fieldEditors !! newChoice
							# val = fmap selectFun val
							= case editor.Editor.onReset emptyAttr val vst of
								(Error e,vst) = (Error e,vst)
								(Ok (uiField,stateField,_),vst)
									# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,ChangeChild (ReplaceUI uiField))]
									= (Ok (?Just (change,(),[stateSelector, stateField], ?Just (Left (?Just newChoice)))), vst)
					//The selection has been cleared or an invalid choice is made
					_
						# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,RemoveChild)]
						= (Ok (?Just (change,(),[stateSelector],?Just (Left ?None))), vst)
			//Check if the event was targeted inside the field editor
			(Ok ?None,vst) = case optStateField of
				[stateField:_] = case fst $ valAndChoiceFromState st of
					?Just choice = case snd <$> fieldEditors !? choice of
						(?Just editor) = case editor.Editor.onEdit (eventId,edit) stateField vst of
							(Error e,vst) = (Error e,vst)
							(Ok (?Just (fieldChange, stateField,mbw)),vst)
								# change = ChangeUI [] [(1,ChangeChild fieldChange)]
								= (Ok (?Just (change,(),[stateSelector, stateField], fmap Right mbw)), vst)
							(Ok ?None, vst) = (Ok ?None, vst)
						_ = (Ok ?None, vst)
					_ = (Ok ?None, vst)
				//No editor yet
				_ = (Ok ?None, vst)
	onEdit _ _ _ vst = (Error "Corrupt state in groupc",vst)

	onRefresh (newChoice, newField) _ st=:[stateSelector:optStateField] vst
		//Update the choice selector
		= case choiceEditorOnRefresh newChoice stateSelector vst of
			(Error e,vst) = (Error e,vst)
			(Ok (choiceUIChange,stateSelector,_),vst)
				| optStateField =:[] //Previously no choice was made
					//Still no choice has been made or choice is out of bounds
					# curChoice = choiceEditorWriteValue stateSelector
					| curChoice =: (Error _)
						= (liftError curChoice, vst)
					# curChoice = fromOk curChoice
					| isNone curChoice || newChoice < 0 || newChoice >= length fieldEditors
						# change = ChangeUI [] [(0,ChangeChild choiceUIChange)]
						= (Ok (change,(),[stateSelector],?None), vst)
					| otherwise //A choice has been made -> create an initial UI
						# (selectFun, editor) = fieldEditors !! newChoice
						= case editor.Editor.onReset emptyAttr (?Just newField) vst of
							(Error e,vst) = (Error e,vst)
							(Ok (uiField,stateField,mbw),vst)
								# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,InsertChild uiField)]
								= (Ok (change,(),[stateSelector,stateField],fmap Right mbw), vst)

				| otherwise // Previously an editor was chosen
					# curChoice = choiceEditorWriteValue stateSelector
					| curChoice =: (Error _)
						= (liftError curChoice, vst)
					# curChoice = fromOk curChoice
					//The selection has been cleared or the choice is out of bounds
					| isNone curChoice || newChoice < 0 || newChoice >= length fieldEditors
						# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,RemoveChild)]
						= (Ok (change,(),[stateSelector],?None), vst)
					| ?Just newChoice == curChoice //The selection stayed the same -> update the field
						# (selectFun,editor) = fieldEditors !! newChoice
						= case editor.Editor.onRefresh newField (hd optStateField) vst of
							(Error e,vst) = (Error e,vst)
							(Ok (fieldUIChange,stateField,_),vst)
								# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,ChangeChild fieldUIChange)]
								= (Ok (change,(),[stateSelector,stateField],?None), vst)
					| otherwise //The selection changed -> replace with an initial UI of the new choice
						# (selectFun,editor) = fieldEditors !! newChoice
						= case editor.Editor.onReset emptyAttr (?Just newField) vst of
							(Error e,vst) = (Error e,vst)
							(Ok (uiField,stateField,mbw),vst)
								# change = ChangeUI [] [(0,ChangeChild choiceUIChange),(1,ChangeChild (ReplaceUI uiField))]
								= (Ok (change,(),[stateSelector,stateField],fmap Right mbw), vst)

	valAndChoiceFromState [stateSelector, childSt] = case choiceEditorWriteValue stateSelector of
		Ok (?Just choice) = case fieldEditors !? choice of
			?Just (_, editor) = case editor.Editor.writeValue childSt of
				Ok val = (?Just choice, ?Just val)
				_      = (?Just choice, ?None)
			_ = (?None, ?None)
		_ = (?None, ?None)
	valAndChoiceFromState _ = (?None, ?None)

	writeValue _ [stateSelector,childSt] = case choiceEditorWriteValue stateSelector of
		Ok (?Just choice) = case fieldEditors !? choice of
			?Just (_, editor) = case editor.Editor.writeValue childSt of
				Ok w = Ok (Right w)
				Error e = Error e
			_ = Ok $ Left (?Just choice)
		Ok ?None = Ok $ Left ?None

		Error e = Error e
	writeValue _ _ = Error "corrupt editor state groupc"

