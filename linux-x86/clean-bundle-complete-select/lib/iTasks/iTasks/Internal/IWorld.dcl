definition module iTasks.Internal.IWorld

from symbols_in_program         import :: Symbol
from System.FilePath            import :: FilePath
from Data.Map                   import :: Map
from Data.Error                 import :: MaybeError(..), :: MaybeErrorString(..)
from Data.Set                   import :: Set
from Data.Queue                 import :: Queue
from Data.Either                import :: Either
from StdFile                    import class FileSystem, class FileEnv
from System.Time                import :: Timestamp, :: Timespec
from Text.GenJSON               import :: JSONNode
from iTasks.Engine              import :: EngineOptions
from iTasks.UI.Definition       import :: UI, :: UIType
from iTasks.Internal.TaskState  import :: TaskMeta
from iTasks.Internal.Task       import :: ConnectionTask
from iTasks.Internal.TaskEval   import :: TaskTime
from iTasks.Util.DeferredJSON   import :: DeferredJSON
from iTasks.Internal.TaskServer import :: IOTaskOperations

from iTasks.WF.Definition import :: TaskValue, :: Event, :: TaskId, :: InstanceNo, :: TaskNo, :: TaskException
from iTasks.WF.Combinators.Core import :: ParallelTaskType, :: TaskListItem
from iTasks.Internal.SDS import :: DeferredWrite
from iTasks.SDS.Definition import :: SDSIdentity, :: SDSIdentityHash, :: SDSNotifyRequest,
	:: SDSSource, :: SDSLens, :: SDSParallel, :: SDSBox, :: SDSCacheKey,
	class RWShared, class Registrable, class Modifiable, class Identifiable, class Readable, class Writeable
from iTasks.Extensions.DateTime import :: Time, :: Date, :: DateTime

from System.Signal import :: SigHandler
from TCPIP import :: TCP_Listener, :: TCP_Listener_, :: TCP_RChannel_, :: TCP_SChannel_, :: TCP_DuplexChannel, :: DuplexChannel, :: IPAddress, :: ByteSeq

from ABC.Interpreter import :: PrelinkedInterpretationEnvironment

CLEAN_HOME_VAR	:== "CLEAN_HOME"

:: *IWorld = !
	{ options             :: !EngineOptions                           //* Engine configuration
	, clock               :: !Timespec                                //* Server side clock
	, clockDependencies   :: ![#ClockDependency!]                     //* Dependencies to the clock, sorted from first to last next event
	, current             :: !TaskEvalState                           //* Shared state during task evaluation

	, random              :: [Int]                                    //* Infinite random stream
	, symbols             :: !{#Symbol}                               //* Symbols of the program

	, sdsNotifyRequests   :: !Map SDSIdentityHash (Map SDSNotifyRequest Timespec) //* Notification requests from previously read sds's
	, sdsNotifyReqsByTask :: !Map TaskId (Set SDSIdentityHash)        //* Allows to efficiently find notification by taskID for clearing notifications
	, memoryShares        :: !Map String Dynamic                      //* Run-time memory shares
	, readCache           :: !Map SDSCacheKey Dynamic                 //* Cached share reads
	, writeCache          :: !Map SDSCacheKey (Dynamic,DeferredWrite) //* Cached deferred writes
	, abcInterpreterEnv   :: !PrelinkedInterpretationEnvironment      //* Used to serialize expressions for the client

	, ioTasks             :: !*IOTasks                                //* The low-level input/output tasks
	, ioStates            :: !IOStates                                //* Results of low-level io tasks, indexed by the high-level taskid that it is linked to

	, signalHandlers      :: *[*SigHandler]                           //* Signal handlers
	, world               :: !*World                                  //* The outside world

	, resources           :: *[*Resource]                             //* Experimental database connection cache
	, onClient            :: !Bool                                    //* `False` on the server, `True` on the client
	, shutdown            :: !?Int                                    //* Signals the server function to shut down, the int will be set as exit code
	}

//* A task's dependency to the clock, it's only used internally in the {{IWorld}}
:: ClockDependency =
	{ nextFire :: !Timespec //* The time when the next event is desired
	, taskId   :: !TaskId   //* The task id of the registrant
	}

:: TaskEvalState =
	{ taskTime        :: !TaskTime    //* The 'virtual' time for the task. Increments at every event
	, taskInstance    :: !InstanceNo  //* The current evaluated task instance
	, sessionInstance :: !?InstanceNo //* If we are evaluating a task in response to an event from a session
	, attachmentChain :: ![TaskId]    //* The current way the evaluated task instance is attached to other instances
	, nextTaskNo      :: !TaskNo      //* The next task number to assign
	}

:: *IOTasks =
	{ done :: !*[IOTaskInstance]
	, todo :: !*[IOTaskInstance]
	}

:: *IOTaskInstance
	= ListenerInstance        !ListenerInstanceOpts !*TCP_Listener
	| ConnectionInstance      !ConnectionInstanceOpts !*TCP_DuplexChannel

:: ListenerInstanceOpts =
	{ taskId                :: !TaskId          //Reference to the task that created the listener
	, port                  :: !Int
	, connectionTask        :: !ConnectionTask
	, removeOnClose         :: !Bool            //If this flag is set, states of connections accepted by this listener are removed when the connection is closed
	}
:: ConnectionInstanceOpts =
	{ taskId                :: !TaskId          //Reference to the task that created the connection
	, connectionId          :: !ConnectionId    //Unique connection id (per listener/outgoing connection)
	, remoteHost            :: !IPAddress
	, connectionTask        :: !ConnectionTask  //The io task definition that defines how the connection is handled
	, ioOps                 :: !IOTaskOperations *TCP_DuplexChannel String () //Defines how the basic operations (read/write/close) are handled
	, removeOnClose         :: !Bool            //If this flag is set, the connection state is removed when the connection is closed
	}

:: ConnectionId             :== Int

:: IOStates :== Map TaskId IOState
:: IOState
	= IOActive      !(Map ConnectionId (!Dynamic,!Bool)) // Bool: stability
	| IODestroyed   !(Map ConnectionId (!Dynamic,!Bool)) // Bool: stability
	| IOException   !String

:: *Resource = Resource | .. //Extensible resource type for caching database connections etc...

//Creation and destruction of the iworld
/**
* Creates and initializes the IWorld state
*
* @param The engine options
* @param The world
*
* @result An initialized iworld or world together with an error string on failure
*/
createIWorld :: !EngineOptions !*World -> Either (!String, !*World) *IWorld

/**
* Destroys the iworld state
*/
destroyIWorld :: !*IWorld -> *World

//*Internally used for the clock share, see {{computeNextFire}} for details
:: ClockParameter a =
	{ start    :: !a
	, interval :: !a
	}

/**
 * Determines when the next fire should be according to the parameter and the time of registration
 * This function is only used internally to determine notifications for {{iworldTimestamp}} and {{iworldTimespec}}.
 * It is exported solely for unit tests.
 *
 * If the interval is zero, the next fire is start.
 * If the interval is non-zero, the next fire is the earliest timespec after regTime and start that is equal to start modulo the interval.
 *
 * Or: nextfire = start + n*interval where nextfire > regTime, n >= 0 and minimal.
 *
 * This next timeout may very well be in the past.
 *
 * @param regTime          Time of registration
 * @param {start,interval} Clock Parameter
 *
 * @result the time of the next fire
 */
computeNextFire :: !Timespec !(ClockParameter Timespec) -> Timespec

/**
 * Represents the current timespec (seconds and nanoseconds since the unix epoch).
 * The parameter determines the notification rate (see {{computeNextFire}})
 */
iworldTimespec :: SDSBox (ClockParameter Timespec) Timespec Timespec

/**
 * Represents the current timestamp (seconds since the unix epoch).
 * The parameter determines the notification rate (See {{computeNextFire}})
 */
iworldTimestamp :: SDSLens (ClockParameter Timestamp) Timestamp ()

/**
 * Represents the datetime in the local timezone.
 * The notification rate is once a second.
 */
iworldLocalDateTime :: SDSParallel () DateTime ()

/*
 * Gives you possibly a matching resource while adhering to the uniqueness
 * constraints. Note that this does remove it from the IWorld
 *
 * @param Function that classifies the resource whether it matches
 */
iworldResource :: (*Resource -> (Bool, *Resource)) *IWorld -> (*[*Resource], *IWorld)

instance FileSystem IWorld
instance FileEnv IWorld
