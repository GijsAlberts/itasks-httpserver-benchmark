definition module iTasks.Extensions.JavaScript

import iTasks.WF.Definition
from ABC.Interpreter.JavaScript import :: JSWorld, :: JSVal
from ABC.Interpreter.JavaScript.Monad import :: JS

/**
 * Evaluate a Javascript function on the client that does not have a result.
 */
appJSWorld :: !(*JSWorld -> *JSWorld) -> Task ()

/**
 * Evaluate a Javascript function on the client that has a result.
 *
 * @param The function that must be evaluated
 */
accJSWorld :: !(*JSWorld -> (a, *JSWorld)) -> Task a | iTask a

/**
 * Evaluate a `JS` JavaScript monad on the client and return the result.
 *
 * @param The initial monadic state.
 * @param The monad to evaluate.
 */
runJSMonad :: st !(JS st a) -> Task a | iTask a
