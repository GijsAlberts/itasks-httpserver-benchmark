implementation module iTasks.Extensions.Development.Codebase
import iTasks
import iTasks.UI.Editor.Controls
import StdArray, System.FilePath, System.File, System.Directory, Text, StdFile, Data.Tree, Data.Error
from Data.List import find

derive class iTask SourceTree, SourceTreeSelection, ModuleType, Extension
instance == Extension where (==) x y = x === y

instance toString Extension
where
	toString Dcl = ".dcl"
	toString Icl = ".icl"

moduleList :: SDSSource FilePath [(ModuleName,ModuleType)] ()
moduleList = worldShare read write notify
where
	read path world = case scanPaths [path] world of
		(Ok paths,world) = (Ok (determineModules path paths), world)
		(Error e,world) = (Error (snd e), world)

	write path () world = (Ok (),world)
	notify p1 _ p2 = p1 == p2

	scanPaths [] world = (Ok [],world)
	scanPaths [p:ps] world = case getFileInfo p world of
		(Error e,world) = (Error e,world)
		(Ok info,world)
			| not info.directory
	 			= case scanPaths ps world of
					(Error e,world) = (Error e,world)
					(Ok filesps,world)
						| include p = (Ok ([p:filesps]),world)
                                    = (Ok filesps,world)
				= case readDirectory p world of
					(Error e,world) = (Error e,world)
					(Ok files,world)
						= case scanPaths [p </> name \\ name <- files | not (exclude name)] world of
							(Error e,world) = (Error e,world)
							(Ok filesp,world) = case scanPaths ps world of
								(Error e,world) = (Error e,world)
								(Ok filesps,world) = (Ok (filesp++filesps),world)
					
    //Include
    include p = let ext = takeExtension p in ext == "icl" || ext == "dcl"
       
	//We can skip directories that we know don't contain Clean modules
	exclude p = startsWith "." p || p == "Clean System Files" || p == "WebPublic"

    determineModules root paths = mods (sort pathsWithoutRoot)
	where
		//The module name is determined only from the part of the path without the root directory
		pathsWithoutRoot = [subString (textSize root + 1) (textSize s) s \\ s <- paths]

		mods [] = []
		mods [p1,p2:ps]
			# p1name = dropExtension p1
			# p2name = dropExtension p2
			| p1name == p2name && takeExtension p1 == "dcl" && takeExtension p2 == "icl"
				= [(moduleName p1name,AuxModule):mods ps]
			| takeExtension p1 == "icl"
				= [(moduleName p1name,MainModule):mods [p2:ps]]
			    = mods [p2:ps]
		mods [p1:ps]
			| takeExtension p1 == "icl"
				= [(moduleName (dropExtension p1),MainModule):mods ps]
			    = mods ps

		mods paths = [(p,MainModule) \\ p <- paths]
		moduleName p = replaceSubString {pathSeparator} "." p
	

moduleDefinition :: SDSLens (FilePath,ModuleName) [String] [String]
moduleDefinition =: mapReadWrite mapToLines ?None (sdsTranslate "moduleDefinition" (\(p,m) -> modulePath p m "dcl") (removeMaybe (?Just "") fileShare))

moduleImplementation :: SDSLens (FilePath,ModuleName) [String] [String]
moduleImplementation =: mapReadWrite mapToLines ?None (sdsTranslate "moduleImplementation" (\(p,m) -> modulePath p m "icl") (removeMaybe (?Just "") fileShare))

moduleDocumentation :: SDSLens (FilePath,ModuleName) [String] [String]
moduleDocumentation =: mapReadWrite mapToLines ?None (sdsTranslate "moduleDocumentation" (\(p,m) -> modulePath p m "md") (removeMaybe (?Just "") fileShare))

mapToLines = (split "\n",\w _ -> ?Just (join "\n" w))

modulePath path name ext = path </> addExtension (replaceSubString "." {pathSeparator} name) ext

toModuleSelectTree :: [(ModuleName,ModuleType)] -> [(ChoiceNode)]
toModuleSelectTree modules = foldl addModule [] [(i,name,type) \\(name,type) <- modules & i <- [0..]]
where
	addModule tree (i,name,type) = insert i type (split "." name) tree

	insert i type [s] [t:ts]
		| s == t.ChoiceNode.label= [{ChoiceNode|t & id = i}:ts]
                                 = [t:insert i type [s] ts]
	insert i type [s:ss] [t:ts]
		| s == t.ChoiceNode.label= [{ChoiceNode|t & children = insert i type ss t.ChoiceNode.children}:ts]
                                 = [t:insert i type [s:ss] ts]
	insert i type [s] [] = [{id=i,label=s,icon= ?None,expanded=False,children=[]}]
	insert i type [s:ss] [] = [{ChoiceNode|id= -1,label=s,icon= ?None,expanded=False,children = insert i type ss []}]
	
rescanCodeBase :: CodeBase -> Task CodeBase
rescanCodeBase codebase
    =   allTasks [ accWorld (findModulesForTree tree)
                 @ (\modules -> {SourceTree|tree & modules=modules})
                 \\ tree <- codebase]

navigateCodebase :: CodeBase -> Task SourceTreeSelection
navigateCodebase codebase
    = enterChoice [/* ChooseWith (ChooseFromTree (groupModules (sourceTreeRoots codebase)))*/] (modulesOf codebase)
where
    modulesOf codebase
        = flatten [[SelSourceTree name rootPath:[moduleSelection modName modType modPath \\ (modName,modType,modPath) <- modules]] \\ {SourceTree|name,rootPath,modules} <- codebase]

    moduleSelection modName MainModule modPath = SelMainModule modName modPath
    moduleSelection modName AuxModule modPath = SelAuxModule modName modPath

lookupModule :: ModuleName CodeBase -> ?(ModuleName,ModuleType,FilePath)
lookupModule module [] = ?None
lookupModule module [t=:{SourceTree|modules}:ts]
    = maybe (lookupModule module ts) ?Just (find ((==) module o fst3) modules)

listFilesInCodeBase :: CodeBase -> [CleanFile]
listFilesInCodeBase codeBase
    = flatten [	[(rootPath, modName, Icl) \\ (modName,_,_)         <- modules] ++
    			[(rootPath, modName, Dcl) \\ (modName,AuxModule,_) <- modules]
	    	  \\ {SourceTree|rootPath,modules} <- codeBase]

    //TODO Also add dcl files

cleanFilePath :: CleanFile -> FilePath
cleanFilePath (baseDir,modName,ext) = foldl (</>) baseDir (split "." modName) +++ toString ext

getModuleType :: ModuleName CodeBase -> ?ModuleType
getModuleType modName [] = ?None
getModuleType modName [{SourceTree|modules}:ts] = maybe (getModuleType modName ts) ?Just (search modules)
where
	search [] = ?None
	search [(m,t,p):ms]
		| modName == m = ?Just t
		| otherwise    = search ms

codeBaseToCleanModuleNames :: CodeBase -> [CleanModuleName]
codeBaseToCleanModuleNames codeBase
    = flatten [[(foldl (</>) rootPath (split "." modName), modName) \\ (modName,modType,modPath) <- modules] \\ {SourceTree|rootPath,modules} <- codeBase]

dirsOfTree :: !SourceTree -> [FilePath]
dirsOfTree {SourceTree|rootPath,subPaths=[]} = [rootPath]
dirsOfTree {SourceTree|rootPath,subPaths}    = [rootPath </> subPath \\ subPath <- subPaths]

findModulesForTree :: !SourceTree !*World -> ([(ModuleName,ModuleType,FilePath)],*World)
findModulesForTree tree w
    # (files,w) = foldr addDir ([],w) (dirsOfTree tree)
    = find [] files w
where
    addDir dir (files,w)
        # (filesInDir,w) = getFilesInPath dir w
        = ([dir </> file \\ file <- filesInDir] ++ files,w)

    find modBase [] w = ([],w)
    find modBase [f:fs] w
        # (mbInfo,w)                = getFileInfo f w
        | isError mbInfo            = find modBase fs w
        | (fromOk mbInfo).directory
            # (filesInDir,w)        = getFilesInPath f w
            # (subModules,w)        = find [dropExtension (dropDirectory f):modBase] [f </> file \\ file <- filesInDir] w
            # (fsModules,w)         = find modBase fs w
            = (subModules ++ fsModules,w)
        # (fileName,ext)            = splitExtension (dropDirectory f)
        # (fsModules,w)             = find modBase fs w
        | ext == "icl"
            = (addModule (dropExtension f) (toModuleName fileName modBase) False fsModules, w)
        | ext == "dcl"
            = (addModule (dropExtension f) (toModuleName fileName modBase) True fsModules, w)
        = (fsModules,w)

    addModule path modName isAux []
        = [(modName,if isAux AuxModule MainModule,path)]
    addModule path modName isAux [(m,MainModule,p):ms]
        | modName == m && isAux = [(m,AuxModule,p):ms]
                                = [(m,MainModule,p):addModule path modName isAux ms]
    addModule path modName isAux [(m,type,p):ms]
        | modName == m          = [(m,type,p):ms]
                                = [(m,type,p):addModule path modName isAux ms]


    toModuleName fileName modBase = join "." (reverse [fileName:modBase])

:: FileExtension :== String

getFilesInDir :: [FilePath] [FileExtension] !Bool !*World -> ([(FilePath,RForest FilePath)],*World)
getFilesInDir [] extensions showExtension w = ([],w)
getFilesInDir [path:paths] extensions showExtension w
# (treeFiles,w)	= getTree (takeDirectory path) [dropDirectory path] w
# (ntrees,w)	= getFilesInDir paths extensions showExtension w
= ([(takeDirectory path,treeFiles):ntrees],w)
where
    getTree absolutePath [] w   = ([],w)
    getTree absolutePath [fileName:fileNames] w
    # absoluteFileName          = absolutePath </> fileName
    # (mbInfo,w)                = getFileInfo absoluteFileName w
    | isError mbInfo            = getTree absolutePath fileNames w
    | (fromOk mbInfo).directory // file is directory name
        # (filesInDir,w)        = getFilesInPath absoluteFileName w
        # (dirNodes,w)          = getTree absoluteFileName filesInDir w
        # (filesNodes,w)		= getTree absolutePath fileNames w
        = case dirNodes of
            [] -> (filesNodes,w)
            _  -> ([RNode fileName dirNodes:filesNodes],w)
    | isEmpty extensions || isMember (snd (splitExtension fileName)) extensions
        # (treeNodes,w)         = getTree absolutePath fileNames w
		# name 					= if showExtension fileName (dropExtension fileName)
        = ([RNode name []:treeNodes],w)
    = getTree absolutePath fileNames w

getFilesInPath :: !FilePath !*World -> ([FilePath],!*World)
getFilesInPath path w
# (mbFiles,w)        = readDirectory path w
| isError mbFiles    = ([],w)
= ([name \\ name <- fromOk mbFiles | name <> "." && name <> ".."],w)

readDir :: !FilePath !*World -> ([FilePath],!*World)
readDir path w
# (mbInfo,w)                 = getFileInfo path w
| isError mbInfo             = ([],w)
| (fromOk mbInfo).directory = getFilesInPath path w
= ([],w)

