implementation module iTasks.Extensions.Editors.Select2

import StdEnv

import Data.Func
import Data.Functor

import ABC.Interpreter.JavaScript

import iTasks

JQUERY_JS :== "select2/jquery-3.5.1.slim.min.js"
SELECT2_JS :== "select2/select2.min.js"
SELECT2_CSS :== "select2/select2.min.css"

jQuery :== jsGlobal "jQuery"

withSelect2 :: !Bool !(Editor a w) -> Editor a w
withSelect2 observeMutations editor = withClientSideInit initUI editor
where
	initUI :: !FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
	initUI {FrontendEngineOptions|serverDirectory} me world
		# world = addCSSFromUrl (serverDirectory+++SELECT2_CSS) ?None world
		# (orgInitDOMEl,world) = me .# "initDOMEl" .? world
		# (fun,world) = jsWrapFun (initDOMEl serverDirectory me orgInitDOMEl) me world
		= (me .# "initDOMEl" .= fun) world
	initDOMEl serverDirectory me orgInitDOMEl _ world
		# world = (jsCall (orgInitDOMEl .# "bind") me .$! ()) world
		# (fun,world) = jsWrapFun (initDOMEl` serverDirectory me) me world
		= addJSFromUrl (serverDirectory+++JQUERY_JS) (?Just fun) world
	initDOMEl` serverDirectory me _ world
		# (fun,world) = jsWrapFun (initDOMEl`` me) me world
		= addJSFromUrl (serverDirectory+++SELECT2_JS) (?Just fun) world
	initDOMEl`` me _ world
		# (onSelect,world) = jsWrapFun (onChange True) me world
		# (onUnselect,world) = jsWrapFun (onChange False) me world
		// Install select2 on the currently present select fields
		# world = installSelect2 onSelect onUnselect (me .# "domEl") world
		| not observeMutations
			= world
		// Install a MutationObserver to also install select fields added later
		# (onDOMMutation,world) = jsWrapFun (onDOMMutation onSelect onUnselect ) me world
		# (observer,world) = jsNew "MutationObserver" onDOMMutation world
		# config = jsRecord ["childList" :> True, "subtree" :> True]
		= (observer .# "observe" .$! (me .# "domEl", config)) world

	installSelect2 onSelect onUnselect elem world
		# (tag,world) = elem .# "tagName" .?? ("", world)
		| tag == "SELECT"
			= initSelect2 elem world
		# (elem,world) = (jQuery .$ elem) world
		# (elems,world) = (elem .# "find" .$ "select") world
		# (elems,world) = jsValToList` elems id world
		= seqSt initSelect2 elems world
	where
		initSelect2 elem world
			# (jelem,world) = (jQuery .$ elem) world
			// Don't reinitialize; https://select2.org/programmatic-control/methods#checking-if-the-plugin-is-initialized
			# (initialized,world) = (jelem .# "hasClass" .$? "select2-hidden-accessible") (False, world)
			| initialized
				= world
			// Initialize
			# (width,world) = computeWidth elem world
			# (select2,world) = (jelem .# "select2" .$ jsRecord ["width" :> toString width+++"ex"]) world
			# world = (select2 .# "on" .$! ("select2:select", onSelect)) world
			# world = (select2 .# "on" .$! ("select2:unselect", onUnselect)) world
			// Move original select tag to inside the select2 div, so that node list indices computed by iTasks still work
			= (elem .# "nextElementSibling" .# "appendChild" .$! elem) world

		computeWidth elem world
			# (options,world) = (elem .# "querySelectorAll" .$ "option") world
			# (options,world) = jsValToList` options id world
			# (lengths,world) = mapSt getLength options world
			# (multiple,world) = elem .# "multiple" .?? (False, world)
			= (if multiple 3 1 * maxList [12:lengths], world) // 12ex as a lower limit
		where
			getLength option world
				# (text,world) = option .# "innerText" .? world
				= (maybe 0 size (jsValToString text), world)

	onChange selected {[0]=ev} world
		# target = ev .# "target"
		# (value,world) = ev .# "params.data.id" .?? ("", world)
		# (options,world) = target .# "options" .? world
		# (options,world) = jsValToList` options id world
		# world = selectOption value options world
		# (ev,world) = jsNew "Event" "change" world
		= (target .# "dispatchEvent" .$! ev) world
	where
		selectOption _ [] world
			= world
		selectOption id [opt:opts] world
			# (value,world) = opt .# "value" .?? ("", world)
			| value == id
				= (opt .# "selected" .= selected) world
				= selectOption id opts world

	onDOMMutation onSelect onUnselect {[0]=mutations} world
		# (mutations,world) = jsValToList` mutations id world
		= seqSt handleMutation mutations world
	where
		handleMutation mutation world
			# (addedNodes,world) = jsValToList` (mutation .# "addedNodes") id world
			= seqSt (installSelect2 onSelect onUnselect) addedNodes world

select2Dropdown :: Editor ([ChoiceText], [Int]) [Int]
select2Dropdown = withSelect2 False dropdown

select2DropdownWithGroups :: Editor ([(ChoiceText, ?String)], [Int]) [Int]
select2DropdownWithGroups = withSelect2 False dropdownWithGroups

chooseWithSelect2Dropdown :: ![String] -> Editor Int (?Int)
chooseWithSelect2Dropdown labels
	= mapEditorRead (\i -> [i])
	$ mapEditorWrite (\sel -> case sel of [x] -> ?Just x; _ -> ?None)
	$ withConstantChoices options select2Dropdown <<@ multipleAttr False
where
	options = [{ChoiceText|id=i,text=t} \\ t <- labels & i <- [0..]]
