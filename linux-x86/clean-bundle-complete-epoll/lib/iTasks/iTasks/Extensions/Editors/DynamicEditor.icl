implementation module iTasks.Extensions.Editors.DynamicEditor

import StdEnv
import Data.Func, Data.Functor, Data.Tuple
import qualified Data.Map as Map
import Text, Text.GenPrint, Text.HTML
import iTasks, iTasks.UI.Editor.Common
from Data.List import intersperse, zip4, zipWith

dynamicEditor :: !(DynamicEditor a) -> Editor (DynamicEditorValue a) (?(DynamicEditorValue a)) | TC a
dynamicEditor dynEditor = compoundEditorToEditor $ dynamicCompoundEditor dynEditor

parametrisedDynamicEditor ::
	!(p -> DynamicEditor a) -> Editor (!p, !?(DynamicEditorValue a)) (?(DynamicEditorValue a)) | TC a & gEq{|*|}, TC p
parametrisedDynamicEditor editor =
	compoundEditorToEditor
		{ CompoundEditor
		| onReset = onReset editor, onEdit = onEdit editor, onRefresh = onRefresh editor, writeValue = writeValue editor
		}
where
	onReset ::
		!(p -> DynamicEditor a) !UIAttributes !(?(p, ?(DynamicEditorValue a))) !*VSt
		->
			( !MaybeErrorString (UI, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(?(DynamicEditorValue a))))
			, !*VSt
			)
		| TC a
	onReset editor attr mbval vst
		= case mbval of
			?None
				= abort "Enter mode not supported by parametrisedDynamicEditor.\n"
			?Just (p, val)
				= appFst
					(fmap \(ui,st,cst,mbw) -> (ui,(p,st),cst,mbw))
					((dynamicCompoundEditor $ editor p).CompoundEditor.onReset attr val vst)

	onEdit ::
		!(p -> DynamicEditor a) !(!EditorId, !JSONNode) !(!p, !?(DynamicConsId, ConsType, Bool)) ![EditState] !*VSt
		->
			( !MaybeErrorString
				(?(UIChange, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(?(DynamicEditorValue a)))))
			, *VSt
			)
		| TC a
	onEdit editor event (p, mbSt) childSts vst
		= appFst
			(fmap (fmap \(change,st,cst,mbw) -> (change,(p,st),cst,mbw)))
			((dynamicCompoundEditor $ editor p).CompoundEditor.onEdit event mbSt childSts vst)

	onRefresh ::
		!(p -> DynamicEditor a) !(!p, !?(DynamicEditorValue a)) !(!p, !?(DynamicConsId, ConsType, Bool))
		![EditState] !*VSt
		->
			( !MaybeErrorString
				(UIChange, (p, ?(DynamicConsId, ConsType, Bool)), [EditState], !(?(?(DynamicEditorValue a))))
			, !*VSt
			)
		| TC a
	onRefresh editor (p, new) st=:(p`, mbSt) childSts vst =
		case underlyingEditorOldParam.CompoundEditor.writeValue mbSt childSts of
			// If the editor has no valid value and there is no new value provided,
			// we cannot update it yet without loosing the partial value.
			// TODO: this be solved with a proper `onRefresh` for the ordinary dyn editor, see #338
			Ok ?None | isNone new
				= (Ok (NoChange, st, childSts, ?None), vst)
			Ok (?Just old)
				# value = fromMaybe old new
				# (uiForOldP, vst) = underlyingEditorOldParam.CompoundEditor.onReset 'Map'.newMap (?Just value) vst
				| isError uiForOldP = (liftError uiForOldP, vst)
				# (uiForOldP, _, _, _) = fromOk uiForOldP
				# (uiForNewP, vst) = underlyingEditorNewParam.CompoundEditor.onReset 'Map'.newMap (?Just value) vst
				| isError uiForNewP = (liftError uiForNewP, vst)
				# (uiForNewP, newSt, newChildSts, newMbW) = fromOk uiForNewP
				//Because the generated UI's each have a fresh unique editorId generated during onReset
				//We must ignore all 'editorId' attributes during the comparison
				| isEqualExceptEditorId uiForOldP uiForNewP =
					// If the definition of the editor itself remains unchanged,
					// a refresh on the underlying editor is only required if a new value is provided.
					case new of
						?Just new =
							appFst
								(fmap \(change,st,cst,mbw) -> (change,(p,st),cst,mbw))
								(underlyingEditorNewParam.CompoundEditor.onRefresh new mbSt childSts vst)
						?None =
							(Ok (NoChange, st, childSts, ?None), vst)
				| otherwise =
					(Ok (ReplaceUI uiForNewP, (p, newSt), newChildSts, newMbW), vst)
			Error e
				= (Error e, vst)
	where
		underlyingEditorOldParam = dynamicCompoundEditor $ editor p`
		underlyingEditorNewParam = dynamicCompoundEditor $ editor p

	writeValue ::
		!(p -> DynamicEditor a) !(!p, !?(DynamicConsId, ConsType, Bool)) ![EditState]
		-> MaybeErrorString (?(DynamicEditorValue a)) | TC a
	writeValue editor (p,st) childSts = (dynamicCompoundEditor $ editor p).CompoundEditor.writeValue st childSts

	isEqualExceptEditorId :: !UI !UI -> Bool
	isEqualExceptEditorId (UI t1 a1 i1) (UI t2 a2 i2)
		= t1 === t2
		&& 'Map'.del "editorId" a1 === 'Map'.del "editorId" a2
		&& length i1 == length i2
		&& (and $ zipWith isEqualExceptEditorId i1 i2)

// Bool part of result indicates whether the type is correct, i.e. the child types are matching
dynamicCompoundEditor :: !(DynamicEditor a) -> CompoundEditor
	(?(!DynamicConsId, !ConsType, !Bool)) (DynamicEditorValue a) (?(DynamicEditorValue a)) | TC a
dynamicCompoundEditor dynEditor=:(DynamicEditor elements)
	| not $ isEmpty duplicateIds
		= abort $ concat ["duplicate cons IDs in dynamic editor: ", printToString duplicateIds, "\n"]
	= {CompoundEditor| onReset = onReset, onEdit = onEdit, onRefresh = onRefresh, writeValue = writeValue}
where
	// conses with optional group labels
	conses = consesOf elements

	duplicateIds = duplicateIds` $ (\(b, _) -> b.consId) <$> conses
	where
		duplicateIds` :: ![DynamicConsId] -> [DynamicConsId]
		duplicateIds` [] = []
		duplicateIds` [x: xs]
			| isMember x xs = [x: duplicateIds` xs]
			| otherwise     = duplicateIds` xs

	onReset ::
		!UIAttributes !(?(DynamicEditorValue a)) !*VSt
		-> *(!MaybeErrorString
				(!UI, !?(!DynamicConsId, !ConsType, !Bool)
					, ![EditState], ?(?(DynamicEditorValue a))), !*VSt)
	onReset attr mbval vst=:{VSt|taskId}
		# (editorId,vst) = nextEditorId vst
		= case mbval of
		?None = case matchingConses of
			[(onlyChoice, _)] | hideCons
				# (mbUis, _, type, _, vst) = resetChildEditors onlyChoice.consId ?None vst
				# attrs = 'Map'.union (withContainerClassAttr onlyChoice.uiAttributes) attr
				# mbUis =
					( \(uis, childSts) ->
						( uiContainer attrs uis
						, ?Just (onlyChoice.consId, type, True)
						, [nullState editorId: childSts]
						, ?Just ?None
						)
					) <$> mbUis
				= (mbUis, vst)
			_ = case filter (\(cons, _) -> cons.useAsDefault) matchingConses of
				[(defaultChoice, _): _]
					# (mbUis, idx, type, label, vst) = resetChildEditors defaultChoice.consId ?None vst
					# attrs = 'Map'.union (withContainerClassAttr defaultChoice.uiAttributes) attr
					= case mbUis of
						Ok (uis, childSts)
							| hideCons =
								( Ok (uiContainer attrs uis
									, ?Just (defaultChoice.consId, type, True)
									, [nullState editorId: childSts]
									, ?Just mbval
									)
								, vst
								)
							| otherwise
								# (consChooseUI, chooseSt) = genConsChooseUI taskId editorId (?Just idx)
								= ( Ok ( uiContainer attrs [consChooseUI: uis]
									   , ?Just (defaultChoice.consId, type, True)
									   , [chooseSt: childSts]
									   , ?Just mbval
									   )
								  , vst
								  )
						Error e = (Error e, vst)
				_
					# (consChooseUI, chooseSt) = genConsChooseUI taskId editorId ?None
					= (Ok (uiContainer attr [consChooseUI], ?None, [chooseSt], ?None), vst)
		?Just {constructorId, value}
			# (mbUis, idx, type, label, vst) = resetChildEditors constructorId (?Just value) vst
			# (cons, _)                      = consWithId constructorId matchingConses
			= case mbUis of
				Ok (uis, childSts)
					# attrs    = 'Map'.union (withContainerClassAttr cons.uiAttributes) attr
					# (uis, childSts) =
						if hideCons
							(uis, [nullState editorId: childSts])
							( let (consChooseUI, chooseSt) = genConsChooseUI taskId editorId (?Just idx)
							  in  ([consChooseUI: uis], [chooseSt: childSts])
							)
					# st  = ?Just (constructorId, type, True)
					# val = writeValue st childSts
					| isError val = (liftError val, vst)
					 = (Ok (uiContainer attrs uis, st, childSts, ?Just $ fromOk val), vst)
				Error e = (Error e, vst)

	genConsChooseUI :: !String !EditorId !(?Int) -> (!UI, !EditState)
	genConsChooseUI taskId editorId mbSelectedCons = (consChooseUI, consChooseSt)
	where
		consOptions =
			[ JSONObject $
					[("id", JSONInt i), ("text", JSONString cons.DynamicCons.label)]
				++
					maybe [] (\label -> [("grouplabel", JSONString label)]) mbGroupLabel
			\\ (cons, mbGroupLabel) <- matchingConses & i <- [0..]
			]
		consChooseUI =
			uia
				UIDropdown
				( 'Map'.put
					"width"
					JSONNull
					(choiceAttrs taskId editorId (maybe [] (\x -> [x]) mbSelectedCons) consOptions)
				)
		consChooseSt = LeafState {editorId= ?Just editorId,touched=False,state= ?Just (dynamic mbSelectedCons)}

	resetChildEditors ::
		!DynamicConsId !(?DEVal) !*VSt
		-> *(!MaybeErrorString (![UI], ![EditState]), Int, ConsType, String, !*VSt)
	resetChildEditors cid mbval vst = case cons.builder of
		FunctionCons fbuilder
			# children     = reverse $ zip4 vals (childrenEditors fbuilder) (cons.labels ++ repeat ?None) [0..]
			# (mbUis, vst) = resetChildEditors` children [] [] vst
			= (mbUis, idx, type, cons.DynamicCons.label, vst)
		where
			resetChildEditors` ::
				![(?(String, DEVal), E, ?String, Int)] ![UI] ![EditState] !*VSt
				-> (!MaybeErrorString ([UI], [EditState]), !*VSt)
			resetChildEditors` [] accUi accSt vst = (Ok (accUi, accSt), vst)
			resetChildEditors` [(mbVal, E {Editor|onReset}, mbLabel, i): children] accUi accSt vst
				# mbVal = (\(cid, val) -> {constructorId = cid, value = val}) <$> mbVal
				= case onReset 'Map'.newMap mbVal vst of
					(Ok (ui, st, _), vst) = resetChildEditors` children [withLabel mbLabel ui: accUi] [st: accSt] vst
					(Error e,     vst) = (Error e, vst)
			where
				withLabel :: !(?String) !UI -> UI
				withLabel (?Just label) (UI type attrs item) = UI type ('Map'.union attrs $ labelAttr label) item
				withLabel ?None         ui                   = ui

			vals = case mbval of
				// update mode
				?Just (DEApplication children) = [?Just (cid,val) \\ (cid, val) <- children]
				// enter mode
				_                              = repeat ?None
		ListCons lbuilder
			# listEditorMode = (\(DEApplication listElems) -> listElems) <$> mbval
			# (mbUi, vst) = (listBuilderEditor lbuilder).Editor.onReset 'Map'.newMap listEditorMode vst
			= ((\(ui, st, mbw) -> ([ui], [st])) <$> mbUi, idx, type, cons.DynamicCons.label, vst)
		CustomEditorCons editor
			# editorVal =
				(\(DEJSONValue json) -> fromMaybe (abort "Invalid dynamic editor state\n") $ fromJSON json) <$> mbval
			# (mbUi, vst) = editor.Editor.onReset 'Map'.newMap editorVal vst
			= ((\(ui, st, mbw) -> ([ui], [st])) <$> mbUi, idx, type, cons.DynamicCons.label, vst)
	where
		(cons, idx) = consWithId cid matchingConses
		type = case cons.builder of
			FunctionCons     _ = Function
			ListCons         _ = List
			CustomEditorCons _ = CustomEditor

	onEdit ::
		!(!EditorId, !JSONNode)
		!(?(!DynamicConsId, !ConsType, !Bool))
		![EditState]
		!*VSt
		-> *(!MaybeErrorString (? (!UIChange, !?(!DynamicConsId, !ConsType, !Bool)
				, ![EditState], ?(?(DynamicEditorValue a)))) , !*VSt)
	// new builder is selected: create a UI for the new builder
	onEdit (eventId, JSONArray [JSONInt builderIdx]) st [LeafState {LeafState|editorId= ?Just editorId}: childrenSts] vst | editorId == eventId
		| builderIdx < 0 || builderIdx >= length matchingConses
			= (Error "Dynamic editor selection out of bounds", vst)
		# (cons, _) = matchingConses !! builderIdx
		# (mbRes, _, type, _, vst) = resetChildEditors cons.consId ?None vst
		= case mbRes of
			Ok (uis, childSts)
				// insert new UIs for arguments
				# inserts = [(i, InsertChild ui) \\ ui <- uis & i <- [1..]]
				# removals = removeNChildren $ length childrenSts
				// add "itasks-container" classes as this class always has to be present for containers
				# uiAttrs = withContainerClassAttr cons.uiAttributes
				# attrChange  = if (typeWasInvalid st) removeErrorIconAttrChange []
				# childChange =
						if (typeWasInvalid st) removeErrorIconChange []
					++
						[	( 0
							, ChangeChild $
								ChangeUI (uncurry SetAttribute <$> 'Map'.toList uiAttrs) (removals ++ inserts)
							)
						]
				# builderChooseState = LeafState {editorId = ?Just editorId, touched = True, state = ?Just (dynamic (length uis))}
				# change             = ChangeUI attrChange childChange
				# state              = ?Just (cons.consId, type, True)
				# childStates        = [builderChooseState: childSts]
				# value              = writeValue state childStates
				| value =: (Error _) = (liftError value,vst)
				= (Ok (?Just (change, state, childStates, ?Just (fromOk value))), vst)
			Error e = (Error e, vst)
	// other events targeted directly at this cons
	onEdit (eventId,e) st [LeafState {LeafState|editorId= ?Just editorId}: childSts] vst | eventId == editorId
		| e =: JSONNull || e =: (JSONArray []) // A null or an empty array are accepted as a reset events
			//If necessary remove the fields of the previously selected cons
			# attrChange  = if (typeWasInvalid st) removeErrorIconAttrChange []
			# childChange =
					if (typeWasInvalid st) removeErrorIconChange []
				++
					[(0, ChangeChild $ ChangeUI [] $ removeNChildren $ length childSts)]
			= (Ok (?Just (ChangeUI attrChange childChange, ?None, [nullState editorId], ?Just ?None)), vst)
		| otherwise
			= (Error $ concat ["Unknown dynamic editor select event: '", toString e, "'"], vst)

	// update is potentially targeted somewhere inside this value
	onEdit (eventId, e) (?Just (cid, type, typeWasCorrect)) childSts vst
		# (cons, _) = consWithId cid matchingConses
		# (res, vst) = case cons.builder of
			FunctionCons fbuilder
				# children = childrenEditors fbuilder
				= onEditFunctionCons 0 children (eventId,e) (tl childSts) vst
			ListCons lbuilder
				= case (listBuilderEditor lbuilder).Editor.onEdit (eventId, e) (childSts !! 1) vst of
					(Ok ?None,vst) = (Ok ?None,vst)
					(Ok (?Just (change,state,mbw)),vst) = (Ok (?Just (0,change,state)),vst)
					(Error e,vst) = (Error e,vst)
			CustomEditorCons editor
				= case editor.Editor.onEdit (eventId, e) (childSts !! 1) vst of
					(Ok ?None,vst) = (Ok ?None,vst)
					(Ok (?Just (change,state,mbw)),vst) = (Ok (?Just (0,change,state)),vst)
					(Error e,vst) = (Error e,vst)
		= case res of
			Ok ?None = (Ok ?None,vst)
			Ok (?Just (argIdx, change, childSt))
				# childChange = [(0, ChangeChild $ ChangeUI [] [(argIdx + if hideCons 0 1, ChangeChild change)])]
				# change      = ChangeUI mbErrorIconAttrChange $ childChange ++ mbErrorIconChange
				// replace state for this child
				# state = ?Just (cid, type, isOk typeIsCorrect)
				# value = writeValue state childSts`
				| value =: (Error _) = (liftError value, vst)
				= (Ok (?Just (change, state, childSts`,?Just (fromOk value))), vst)
			where
				childSts`   = updateAt (argIdx + 1) childSt childSts
				typeIsCorrect = childTypesAreMatching cons.builder (drop 1 childSts`)

				(mbErrorIconChange, mbErrorIconAttrChange) = mbErrorIconUpd
				mbErrorIconUpd
					| typeWasCorrect && isError typeIsCorrect
						# classes =
							JSONString <$> ["itasks-container", "itasks-horizontal", "itasks-dynamic-editor-error"]
						=	( [(1, InsertChild errorIcon)]
							, [SetAttribute "class" $ JSONArray classes]
							)
					with
						errorIcon =
							UI
								UIContainer
								('Map'.singleton "class" $ JSONString "itasks-dynamic-editor-icon-error-container")
								[ UI
									UIIcon
									('Map'.union (iconClsAttr "icon-invalid") (tooltipAttr $ fromError typeIsCorrect))
									[]
								]
					| not typeWasCorrect && isOk typeIsCorrect =
						(removeErrorIconChange, removeErrorIconAttrChange)
					| otherwise = ([], [])

			Error e = (Error e, vst)
		where
			onEditFunctionCons i [] _ _ vst
				= (Ok ?None,vst)
			onEditFunctionCons i [c:cs] (eventId,e) [st:states] vst
				# (E editor) = c
				= case editor.Editor.onEdit (eventId,e) st vst of
					(Error e,vst) = (Error e,vst)
					(Ok (?Just (change,st,_)),vst) = (Ok (?Just (i,change,st)),vst)
					(Ok ?None,vst) = onEditFunctionCons (i+1) cs (eventId,e) states vst

	onEdit _ _ _ vst = (Ok ?None, vst)

	typeWasInvalid :: !(?(!DynamicConsId, !ConsType, !Bool)) -> Bool
	typeWasInvalid (?Just (_, _, False)) = True
	typeWasInvalid _                     = False

	removeErrorIconChange     = [(1, RemoveChild)]
	removeErrorIconAttrChange =
		[SetAttribute "class" $ JSONArray [JSONString "itasks-container", JSONString "itasks-horizontal"]]

	// add "itasks-container" classes as this class always has to be present for containers
	withContainerClassAttr :: !(Map String JSONNode) -> Map String JSONNode
	withContainerClassAttr attrs = 'Map'.alter (?Just o addContainerClass) "class" attrs
	where
		addContainerClass :: !(?JSONNode) -> JSONNode
		addContainerClass mbJSONClasses = JSONArray [JSONString "itasks-container": otherClasses]
		where
			otherClasses = maybe [] (\(JSONArray classes) -> classes) mbJSONClasses

	removeNChildren :: !Int -> [(Int, UIChildChange)]
	removeNChildren nrArgs = repeatn nrArgs (1, RemoveChild)

	onRefresh ::
		!(DynamicEditorValue a)
		!(?(!DynamicConsId, !ConsType, !Bool))
		![EditState]
		!*VSt
		-> *(!MaybeErrorString (!UIChange, !(?(!DynamicConsId, !ConsType, !Bool)), ![EditState], ?(?(DynamicEditorValue a))), !*VSt)
	// TODO: how to get UI attributes?
	// TODO: fine-grained replacement
    onRefresh new st childSts vst
		# value = writeValue st childSts
		| value =: (Error _) = (liftError value, vst)
		| isNotChanged (fromOk value) new = (Ok (NoChange, st, childSts, ?None), vst)
		# (res,vst) = onReset 'Map'.newMap (?Just new) vst
		= (fmap (\(ui,st,csts,mbw) -> (ReplaceUI ui, st, csts, mbw)) res, vst)
	where
		isNotChanged (?Just {constructorId, value}) {constructorId = consId`, value = val`} =
			constructorId == consId` && value === val`
		isNotChanged _ _ =
			False

	hideCons = case matchingConses of
		[(onlyChoice, _)] | not onlyChoice.showIfOnlyChoice = True
		_                                                   = False

	matchingConses = catMaybes $
		(\(cons, mbGroupLabel) -> (\cons` -> (cons`, mbGroupLabel)) <$> matchingCons dynEditor cons) <$> conses

	// first arg only used for type
	// packs matching conses, with possibly updated (= more specific) type
	matchingCons :: !(DynamicEditor a) !DynamicCons -> ?DynamicCons | TC a
	matchingCons dynEd cons=:{builder} = (\b -> {cons & builder = b}) <$> mbBuilder`
	where
		mbBuilder` = case builder of
			FunctionCons     fbuilder = matchf fbuilder
			CustomEditorCons editor   = matchc editor
			ListCons         lbuilder = matchl lbuilder

		// works for functions with upto 10 args
		// the type of the dynamic is updated by unifying the function result with the type produced by the editor
		matchf :: !Dynamic -> ?DynamicConsBuilder
		matchf b = case (b, dynamic dynEd) of
			(b :: a b c d e f g h i j -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g h i   -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g h     -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f g       -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e f         -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d e           -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c d             -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b c               -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a b                 -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b :: a                   -> z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			(b ::                        z, _ :: DynamicEditor z) = ?Just $ FunctionCons (dynamic b)
			_                                                     = ?None

		// custom editors do not allow for quantified variables, so no type update is required
		matchc e = case (dynamic e, dynamic dynEd) of
			(_ :: Editor a (?a), _ :: DynamicEditor a) = ?Just $ CustomEditorCons e
			_                                          = ?None

		matchl f = case (f, dynamic dynEd) of
			(f :: (a -> b, [b] -> c), _ :: DynamicEditor c) = ?Just $ ListCons (dynamic f)
			_                                               = ?None

	listBuilderEditor :: !Dynamic -> Editor [(DynamicConsId, DEVal)] [?(DynamicConsId, DEVal)]
	listBuilderEditor ((mapF, _) :: (a -> b, [b] -> c))
		= listEditor False (?Just $ const ?None) True True ?None ?Just childrenEd`
	where
		childrenEd  = childrenEditorList mapF
		childrenEd`
			= mapEditorRead	(\(cid, val) -> {constructorId = cid, value= val})
			$ mapEditorWrite (fmap (\{constructorId, value} -> (constructorId, value)))
			$ childrenEd

		// first argument only used for type
		childrenEditorList :: (a -> b) -> Editor (DynamicEditorValue a) (?(DynamicEditorValue a)) | TC a
		childrenEditorList _ = dynamicEditor (DynamicEditor elements)

	listBuilderEditor _ = abort "dynamic editors: invalid list builder value\n"

	uiContainer :: !UIAttributes ![UI] -> UI
	uiContainer attr uis =
		UI
			UIContainer
			('Map'.singleton "class" $ JSONArray [JSONString "itasks-container", JSONString "itasks-horizontal"])
			[UI UIContainer attr uis]

	writeValue :: !(?(!DynamicConsId, !ConsType, !Bool)) ![EditState] -> *MaybeErrorString (?(DynamicEditorValue a))
	writeValue (?Just (cid, CustomEditor, True)) [_: [editorSt]] = case editor.Editor.writeValue editorSt of
		Ok mbvalue = Ok $ (\val -> {constructorId = cid, value = DEJSONValue $ toJSON` val}) <$> mbvalue
		Error e = Error e
	where
		({builder}, _) = consWithId cid conses

		// toJSON` is used to solve overloading, JSONEncode{|*|} is attached to CustomEditorCons
		(editor, toJSON`) = case builder of
			CustomEditorCons editor = (editor, toJSON)
			_                       = abort "corrupt dynamic editor state\n"

	writeValue (?Just (cid, type, True)) [_: childSts] = case childValuesFor childSts` [] of
		Ok childVals = Ok $ (\vals -> {constructorId = cid, value = DEApplication vals}) <$> childVals
		Error e = Error e
	where
		childSts` = case (type, childSts) of
			(List, [CompoundState _ childSts]) = childSts
			(_,    childSts)                   = childSts

		childValuesFor :: ![EditState] ![(DynamicConsId, DEVal)] -> MaybeErrorString (?[(DynamicConsId, DEVal)])
		childValuesFor [] acc = Ok $ ?Just $ reverse acc
		childValuesFor [childSt: childSts] acc = case (dynamicEditor dynEditor).Editor.writeValue childSt of
			Ok (?Just {constructorId = childCid, value = childVal}) =
				childValuesFor childSts [(childCid, childVal): acc]
			Ok ?None =
				Ok ?None
			Error e =
				Error e

	writeValue _ _ = Ok ?None

	childrenEditors :: !Dynamic -> [E]
	childrenEditors (f :: a -> b) = [E $ dynamicEditorFstArg f : childrenEditors (dynamic (f undef))]
	where
		// first argument only used for type
		dynamicEditorFstArg :: (a -> b) -> Editor (DynamicEditorValue a) (?(DynamicEditorValue a)) | TC a
		dynamicEditorFstArg _ = dynamicEditor $ DynamicEditor elements
	childrenEditors _         = []

	childTypesAreMatching :: !DynamicConsBuilder [EditState] -> MaybeErrorString ()
	childTypesAreMatching (FunctionCons cons) childStates =
		childTypesAreMatching` cons (childValueOf <$> zip2 childStates (childrenEditors cons))
	where
		childTypesAreMatching` :: !Dynamic ![?Dynamic] -> MaybeErrorString ()
		childTypesAreMatching` _ [] = Ok ()
		childTypesAreMatching` cons [?None: otherArgs] =
			case cons of
				(cons` :: a -> z) = childTypesAreMatching` (dynamic cons` undef) otherArgs
		childTypesAreMatching` cons [?Just nextArg: otherArgs] =
			case (cons, nextArg) of
				// `cons` undef` has type z`, which is z updated by unifying the type of the next arg
				(cons` :: a -> z, _ :: a) = childTypesAreMatching` (dynamic cons` undef) otherArgs
				_                         =
					Error $
						concat
							[ "Could not unify\n    ", toString (argOf $ typeCodeOfDynamic cons), "\nwith\n    "
							, toString (typeCodeOfDynamic nextArg)
							]

		childValueOf :: !(!EditState, !E) -> ?Dynamic
		childValueOf (state, E editor) = case editor.Editor.writeValue state of
			Ok mbvalue = valueCorrespondingToDyn (DynamicEditor elements) <$> mbvalue
			Error e = ?None

		argOf :: !TypeCode -> TypeCode
		argOf (TypeApp (TypeApp _ arg) _) = arg
		argOf (TypeScheme _ type)         = argOf type
	// only function conses can have not matching child types
	childTypesAreMatching _ _ = Ok ()

valueCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> a | TC a
valueCorrespondingTo  dynEditor dynEditorValue = case valueCorrespondingToDyn dynEditor dynEditorValue of
	(v :: a^) = v
	_         = abort "corrupt dynamic editor value\n"

stringCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> String
stringCorrespondingTo (DynamicEditor elements) {constructorId, value} =
	concat $ withCapitalisedFirstLetter $
		dropWhile (\s -> textSize (trim s) == 0) $ reverse [".": stringCorrespondingTo` (constructorId, value) []]
where
	withCapitalisedFirstLetter :: ![String] -> [String]
	withCapitalisedFirstLetter [firstString: rest] = [upperCaseFirst firstString: rest]

	stringCorrespondingTo` :: !(!DynamicConsId, !DEVal) ![String] -> [String]
	stringCorrespondingTo` (cid, val) accum = case val of
		DEApplication args = case cons.builder of
			FunctionCons fbuilder =
				foldl (flip stringCorrespondingTo`) [" ", cons.DynamicCons.label : accum] args
			ListCons _
				# listElStrs =
					flatten $
						intersperse
							[" ", cons.DynamicCons.label]
							((\arg -> stringCorrespondingTo` arg []) <$> reverse args)
				= listElStrs ++ [" "] ++ accum
			_ = abort "corrupt dynamic editor value\n"
		DEJSONValue json = case cons.builder of
			CustomEditorCons editor = [ " ", stringCorrespondingToGen editor json
									  , " ", cons.DynamicCons.label
									  : accum
									  ]
			_ = abort "corrupt dynamic editor value\n"
	where
		(cons, _) = consWithId cid $ consesOf elements

stringCorrespondingToGen :: (Editor a (?a)) !JSONNode -> String | gText{|*|}, JSONDecode{|*|}  a
stringCorrespondingToGen editor json = toSingleLineText $ fromJSON` editor json
where
	fromJSON` :: (Editor a (?a)) !JSONNode -> a | JSONDecode{|*|} a
	fromJSON` _ json =
		fromMaybe (abort $ concat ["corrupt dynamic editor value ", toString json, "\n"]) $ fromJSON json

htmlCorrespondingTo :: !(DynamicEditor a) !(DynamicEditorValue a) -> HtmlTag
htmlCorrespondingTo (DynamicEditor elements) {constructorId, value} =
	DivTag [ClassAttr "itasks-dynamic-editor-description"] [toHtml (constructorId, value)]
where
	conses = consesOf elements

	toHtml (cid, val) = case val of
		DEApplication args -> case builder of
			FunctionCons fbuilder ->
				DivTag [classAttr]
					[ SpanTag [] [Text label]
					: [toHtml a \\ a <- args]
					]
			ListCons _ ->
				DivTag [classAttr] $ intersperse (SpanTag [] [Text label]) $ toHtml <$> args
			_ ->
				abort "corrupt dynamic editor value\n"
		DEJSONValue json -> case builder of
			CustomEditorCons editor ->
				DivTag [classAttr]
					[ SpanTag [] [Text $
						if (size label == 0)
							(stringCorrespondingToGen editor json)
							(concat3 label ": " (stringCorrespondingToGen editor json))]
					]
			_ ->
				abort "corrupt dynamic editor value\n"
	where
		({builder,label,uiAttributes}, _) = consWithId cid conses
		cssClasses = case 'Map'.get "class" uiAttributes of
			?Just (JSONArray cs) -> [c \\ JSONString c <- cs]
			_                    -> []
		classAttr = ClassAttr $ join " " ["itasks-container":cssClasses]

:: DynamicCons =
	{ consId           :: !DynamicConsId
	, label            :: !String
	, builder          :: !DynamicConsBuilder
	, showIfOnlyChoice :: !Bool
	, useAsDefault     :: !Bool
	, uiAttributes     :: !UIAttributes
	, labels           :: ![?String]
	}

:: DynamicConsBuilder
	=      FunctionCons     !Dynamic
	| E.a: CustomEditorCons !(Editor a (?a)) & JSONEncode{|*|}, JSONDecode{|*|}, gText{|*|}, TC a
	|      ListCons         !Dynamic    //* must contain a value of type (a -> b, [b] -> c)

functionCons :: !DynamicConsId !String !a -> DynamicCons | TC a
functionCons consId label func = functionConsDyn consId label (dynamic func)

functionConsDyn :: !DynamicConsId !String !Dynamic -> DynamicCons
functionConsDyn consId label func =
	{ consId           = consId
	, label            = label
	, builder          = FunctionCons func
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	}

listCons :: !DynamicConsId !String !([a] -> b) -> DynamicCons | TC a & TC b
listCons consId label func = listConsDyn consId label (dynamic (id, func) :: (a^ -> a^, [a^] -> b^))

listConsDyn :: !DynamicConsId !String !Dynamic -> DynamicCons
listConsDyn consId label func =
	{ consId           = consId
	, label            = label
	, builder          = ListCons func
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	}

customEditorCons ::
	!DynamicConsId !String !(Editor a (?a)) -> DynamicCons | TC, JSONEncode{|*|}, JSONDecode{|*|}, gText{|*|} a
customEditorCons consId label editor =
	{ consId           = consId
	, label            = label
	, builder          = CustomEditorCons editor
	, showIfOnlyChoice = True
	, useAsDefault     = False
	, uiAttributes     = 'Map'.newMap
	, labels           = []
	}

instance tune DynamicConsOption DynamicCons where
	tune :: !DynamicConsOption !DynamicCons -> DynamicCons
	tune HideIfOnlyChoice          cons = {cons & showIfOnlyChoice = False}
	tune UseAsDefault              cons = {cons & useAsDefault = True}
	tune (ApplyCssClasses classes) cons = {cons & uiAttributes = 'Map'.union (classAttr classes) cons.uiAttributes}
	tune (AddLabels labels)        cons = {cons & labels = labels}

valueCorrespondingToDyn :: !(DynamicEditor a) !(DynamicEditorValue a) -> Dynamic | TC a
valueCorrespondingToDyn (DynamicEditor elements) {constructorId, value} = valueCorrespondingTo` (constructorId, value)
where
	valueCorrespondingTo` :: !(!DynamicConsId, !DEVal) -> Dynamic
	valueCorrespondingTo` (cid, val) = case val of
		DEApplication args = case cons.builder of
			FunctionCons fbuilder = valueCorrespondingToFunc fbuilder args
			ListCons     lbuilder = valueCorrespondingToList lbuilder args
			_                     = abort "corrupt dynamic editor value\n"
		DEJSONValue json = case cons.builder of
			CustomEditorCons editor = valueCorrespondingToGen editor json
			_                       = abort "corrupt dynamic editor value\n"
	where
		(cons, _) = consWithId cid $ consesOf elements

	valueCorrespondingToFunc :: !Dynamic ![(DynamicConsId, DEVal)] -> Dynamic
	valueCorrespondingToFunc v [] = v
	valueCorrespondingToFunc f [x=:(consId, _) : xs] = case (f, dynValue) of
		(f :: a -> b, x :: a) = valueCorrespondingToFunc (dynamic (f x)) xs
		_ =
			abort $
				concat
					[ "Cannot unify demanded type with offered type for constructor '", toString consId, "':\n "
					, firstArgString $ typeCodeOfDynamic f, "\n ", toString $ typeCodeOfDynamic dynValue, "\n"
					]
	where
		dynValue = valueCorrespondingTo` x

		firstArgString :: !TypeCode -> String
		firstArgString (TypeScheme _ tc)              = firstArgString tc
		firstArgString (TypeApp (TypeApp _ fstArg) _) = toString fstArg
		firstArgString _                              = "no argument required"

	valueCorrespondingToGen :: (Editor a (?a)) !JSONNode -> Dynamic | JSONDecode{|*|}, TC a
	valueCorrespondingToGen editor json = dynamic (fromJSON` editor json)
	where
		fromJSON` :: (Editor a (?a)) !JSONNode -> a | JSONDecode{|*|} a
		fromJSON` _ json =
			fromMaybe (abort $ corruptValueErrorString ["undecodable JSON ", toString json, "\n"]) $ fromJSON json

	valueCorrespondingToList :: !Dynamic ![(DynamicConsId, DEVal)] -> Dynamic
	valueCorrespondingToList funcs args =
		case [mappedArgument $ valueCorrespondingTo` val \\ val <- args] of
			[]
				-> case funcs of
					((_, g) :: (a -> b, [b] -> c)) = dynamic g []
					_ = abort "corrupt dynamic editor value\n"
			// We use the first element to update the type,
			// an arbitrary element can be used as the `b` and `c` type variable
			// is required to be equal for all list elements.
			args=:[fst: _]
				-> case (funcs, fst) of
					((f, g) :: (x, [b] -> c), _ :: b) -> dynamic (g $ fromJust $ fromDynList args)
					_                                 -> abort "corrupt dynamic editor value\n"
	where
		mappedArgument (val :: a)
			= case funcs of
				((f, _) :: (a^ -> b, x)) -> dynamic f val
				_                        -> abort "corrupt dynamic editor value\n"

		fromDynList :: ![Dynamic] -> ?[b] | TC b
		fromDynList dyns = fromDynList` dyns []
		where
			fromDynList` []           acc = ?Just $ reverse acc
			fromDynList` [(val :: b^): dyns] acc = fromDynList` dyns [val: acc]

:: E = E.a: E (Editor (DynamicEditorValue a) (?(DynamicEditorValue a))) & TC a
:: ConsType = Function | List | CustomEditor

consWithId :: !DynamicConsId ![(DynamicCons, ?String)] -> (!DynamicCons, !Int)
consWithId cid conses = case filter (\(({consId}, _), _) -> consId == cid) $ zip2 conses [0..] of
	[((cons, _), idx)] = (cons, idx)
	[]                 = abort $ concat ["dynamic editor: cons not found: '",   cid, "'\n"]
	_                  = abort $ concat ["dynamic editor: duplicate conses: '", cid, "'\n"]

nullState :: !EditorId -> EditState
nullState editorId = LeafState {editorId = ?Just editorId, touched = True, state = ?None}

consesOf :: ![DynamicEditorElement] -> [(DynamicCons, ?String)]
consesOf elements = flatten $ consesOf <$> elements
where
	consesOf :: !DynamicEditorElement -> [(DynamicCons, ?String)]
	consesOf (DynamicCons cons)              = [(cons, ?None)]
	consesOf (DynamicConsGroup label conses) = (\cons -> (cons, ?Just label)) <$> conses

corruptValueErrorString :: ![String] -> String
corruptValueErrorString errorStrs = concat $ flatten [["Corrupt dynamic editor value: "], errorStrs, [".\n"]]

derive class iTask DynamicEditorValue, DEVal
derive JSONEncode ConsType
derive JSONDecode ConsType
