implementation module iTasks.Extensions.Admin.UserAdmin

import StdEnv
import iTasks, iTasks.Internal.Store
import iTasks.Extensions.CSVFile
import iTasks.UI.Editor
import Text, Data.Tuple, Data.Func, Data.Functor, Crypto.Hash.SHA1
import qualified Data.Map as DM

derive class iTask UserAccount, StoredUserAccount, StoredCredentials

//Initial root user
ROOT_USER :==
	{ StoredUserAccount
	| title       = ?Just "Root user"
	, roles       = ["admin","manager"]
	, token       = ?None
	, credentials =
		{ StoredCredentials
		| username           = Username "root"
		, saltedPasswordHash = "1e27e41d50caf2b0516e77c60fc377e8ae5a32ee" // password is "root"
		, salt               = "25wAdtdQcJZOQLETvki7eJFcI7u5XoSO"
		}
	}

AUTHENTICATION_COOKIE_TTL :== 3600 * 24 * 7 //One week validity

userAccounts :: SDSLens () [StoredUserAccount] [StoredUserAccount]
userAccounts =: sdsFocus "UserAccounts" (storeShare NS_APPLICATION_SHARES False InJSONFile (?Just [ROOT_USER]))

users :: SDSLens () [User] ()
users =: mapReadWrite (\accounts -> [AuthenticatedUser (toString a.StoredUserAccount.credentials.StoredCredentials.username) a.StoredUserAccount.roles a.StoredUserAccount.title
									\\ a <- accounts]
					 , \() _ -> ?None) (?Just \_ _ -> Ok ()) userAccounts

usersWithRole :: !Role -> SDSLens () [User] ()
usersWithRole role = mapRead (filter (hasRole role)) users
where
	hasRole role (AuthenticatedUser _ roles _) = isMember role roles
	hasRole _ _ = False


userAccountsFile :: SDSLens FilePath [StoredUserAccount] [StoredUserAccount]
userAccountsFile = mapReadWrite (parseUsers,printUsers) ?None fileShare
where
	parseUsers mbText
		= [user \\ ?Just user <- map (parseUser o (split ";")) $ maybe [] (split OS_NEWLINE) mbText]
	parseUser [a,b,c,d,e:rest]
		# token  = case rest of
			[f,g,h] = ?Just {username=Username f,saltedPasswordHash=g,salt=h}
			_ = ?None
		= ?Just {credentials={username=Username a,saltedPasswordHash=d,salt=e},title= ?Just b,roles = map trim $ split "," c,token=token}
	parseUser _ = ?None
	printUsers [] _ = ?Just $ ?None
	printUsers users _ = ?Just $ ?Just $ join OS_NEWLINE $ map (join ";" o printUser) users
	printUser {credentials={username=Username username,saltedPasswordHash,salt},title,roles,token}
		# ptoken = maybe [] (\{username=Username username,saltedPasswordHash,salt} -> [username,saltedPasswordHash,salt]) token
		= [username, fromMaybe "-" title, join "," roles,saltedPasswordHash,salt:ptoken]




userAccountIn :: (sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens UserId (?StoredUserAccount) (?StoredUserAccount) | RWShared sds
userAccountIn userAccounts = sdsLens "userAccount" (const ()) (SDSRead read) (SDSWrite write) (SDSNotifyConst notify) ?None userAccounts
where
	read userId accounts = Ok $ listToMaybe [a \\ a <- accounts | identifyUserAccount a == userId]
	write userId accounts ?None = Ok ?None
	write userId accounts (?Just account) = Ok $ ?Just [if (identifyUserAccount a == userId) account a \\ a <- accounts]
	notify p _ t q = p == q

userTokenIn :: (sds () [StoredUserAccount] [StoredUserAccount]) -> SDSLens String (?StoredUserAccount) () | RWShared sds
userTokenIn userAccounts = sdsLens "userToken" (const ()) (SDSRead read) (SDSWriteConst write) (SDSNotifyConst notify) ?None userAccounts
where
	read token accounts = Ok $ listToMaybe [a \\ a <- accounts | maybe False (checkToken (split ":" token)) a.StoredUserAccount.token]

	checkToken [tokenpublic,tokenprivate:_] {StoredCredentials|username=(Username uname),saltedPasswordHash,salt}
		= uname == tokenpublic && saltedPasswordHash == sha1 (tokenprivate +++ salt)
	checkToken _ _ = False

	write token () = Ok ?None
	notify _ _ _ _ = False

identifyUserAccount :: StoredUserAccount -> UserId
identifyUserAccount {StoredUserAccount|credentials={StoredCredentials|username}} = toString username

accountToUser :: !StoredUserAccount -> User
accountToUser {StoredUserAccount|credentials={StoredCredentials|username},title,roles} = AuthenticatedUser (toString username) roles title

accountTitle :: !StoredUserAccount -> String
accountTitle {StoredUserAccount|credentials={StoredCredentials|username},title= ?Just title} = title
accountTitle {StoredUserAccount|credentials={StoredCredentials|username}} = "Untitled (" +++ toString username +++ ")"

isValidPassword :: !Password !StoredCredentials -> Bool
isValidPassword (Password password) credentials =
	sha1 (password +++ credentials.salt) == credentials.saltedPasswordHash

authenticateUser :: !Username !Password	!Bool -> Task (?User)
authenticateUser username password persistent = authenticateUserWith username password persistent (userAccountIn userAccounts)

authenticateUserWith :: !Username !Password !Bool (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task (?User) | RWShared sds
authenticateUserWith (Username username) password persistent accountStore
	| persistent
		= checkUserAccount username password
		>>- maybe (return ?None) (\user -> storeUserToken username >-| return (?Just user))
	| otherwise
		= checkUserAccount username password
where
	checkUserAccount username password
		=	get (sdsFocus username accountStore)
		@	(maybe ?None (\a -> if (isValidPassword password a.StoredUserAccount.credentials) (?Just (accountToUser a)) ?None))

	storeUserToken :: String -> Task ()
	storeUserToken username
		= getRandom -&&- getRandom -&&- getRandom
		>>- \(salt,(tokenpublic,tokenprivate)) ->
			set ("auth-token",tokenpublic +++ ":" +++ tokenprivate, ?Just AUTHENTICATION_COOKIE_TTL) currentTaskInstanceCookies
			-&&-
			upd (fmap $ setToken salt tokenpublic tokenprivate) (sdsFocus username accountStore)
		@! ()
	where
		setToken salt tokenpublic tokenprivate account =
			{account & token = ?Just {username=Username tokenpublic,saltedPasswordHash = sha1 (tokenprivate +++ salt), salt = salt}}
		getRandom = get (sdsFocus 32 randomString)

authenticateUsingToken :: Task (?User)
authenticateUsingToken = authenticateUsingTokenWith (userTokenIn userAccounts)

authenticateUsingTokenWith :: (sds String (?StoredUserAccount) ()) -> Task (?User) | RWShared sds
authenticateUsingTokenWith accountStore
	= watch (mapRead ('DM'.get "auth-token") currentTaskInstanceCookies)
	>>* [OnValue (ifValue isJust (checkToken o fromJust))]
where
	checkToken :: String -> Task (?User)
	checkToken token = get (sdsFocus token accountStore)
		>>- maybe (clearAuthenticationToken @! ?None) (\a -> return (?Just (accountToUser a)))

clearAuthenticationToken :: Task ()
clearAuthenticationToken = set ("auth-token","",?Just 0) currentTaskInstanceCookies @! ()

doAuthenticated :: (Task a) -> Task a | iTask a
doAuthenticated task = doAuthenticatedWith verify task
where
	verify {Credentials|username,password} = authenticateUser username password False
	
doAuthenticatedWith :: !(Credentials -> Task (?User)) (Task a) -> Task a | iTask a
doAuthenticatedWith verifyCredentials task
	=	Title "Log in" @>> Hint "Please enter your credentials" @>> enterInformation []
	>>!	verifyCredentials
	>>- \mbUser -> case mbUser of
		?None      = throw "Authentication failed"
		?Just user = workAs user task
	
createUser :: !UserAccount -> Task StoredUserAccount
createUser account
	=	createStoredAccount >>~ \storedAccount ->
	    get (sdsFocus (identifyUserAccount storedAccount) (userAccountIn userAccounts))
	>>- \mbExisting -> case mbExisting of
		?None
			= upd (\accounts -> accounts ++ [storedAccount]) userAccounts @ const storedAccount
		_	
			= throw ("A user with username '" +++ toString account.UserAccount.credentials.Credentials.username +++ "' already exists.")
where
	createStoredAccount :: Task StoredUserAccount
	createStoredAccount
		= createStoredCredentials account.UserAccount.credentials.Credentials.username
			account.UserAccount.credentials.Credentials.password
		@ \credentials ->
			{ StoredUserAccount | credentials = credentials , title = account.UserAccount.title
			, roles = account.UserAccount.roles, token = ?None }

deleteUser :: !UserId -> Task ()
deleteUser userId = upd (filter (\acc -> identifyUserAccount acc <> userId)) userAccounts @! ()

manageUsers :: Task ()
manageUsers =
	(		Title "Users" @>> Hint "The following users are available" @>> enterChoiceWithSharedAs [ChooseFromGrid id] userAccounts identifyUserAccount
		>>*	[ OnAction		(Action "New")									(always (createUserFlow	@ const False))
			, OnAction 	    (ActionEdit) 						                (hasValue (\u -> updateUserFlow u @ const False))
		    , OnAction      (Action "Change Password")                      (hasValue (\u -> changePasswordFlow u @ const False))
			, OnAction      (ActionDelete) 		            					(hasValue (\u -> deleteUserFlow u @ const False))
			, OnAction      (Action "Import & export/Import CSV file...")	(always (importUserFileFlow @ const False))
			, OnAction      (Action "Import & export/Export CSV file...")	(always (exportUserFileFlow @ const False))
			, OnAction      (Action "Import & export/Import demo users")		(always (importDemoUsersFlow @ const False))
			]
	) <! id @! ()

createUserFlow :: Task ()
createUserFlow =
		Title "Create user" @>> Hint "Enter user information" @>> enterInformation []
	>>*	[ OnAction		ActionCancel	(always (return ()))
		, OnAction	    ActionOk 		(hasValue (\user ->
										    createUser user
										>-| Title "User created" @>> viewInformation [] "Successfully added new user"
										>!| return ()
										))
		]
		
updateUserFlow :: UserId -> Task StoredUserAccount
updateUserFlow userId
	=	get (sdsFocus userId (userAccountIn userAccounts))
	>>- \mbAccount -> case mbAccount of
		?Just account
			=	(Title ("Editing " +++ fromMaybe "Untitled" account.StoredUserAccount.title) @>> Hint "Please make your changes" @>> updateInformation [] account
			>>*	[ OnAction ActionCancel (always (return account))
				, OnAction ActionOk (hasValue (\newAccount ->
					    set (?Just newAccount) (sdsFocus userId (userAccountIn userAccounts))
					>>- \storedAccount -> Title "User updated"
					    @>> viewInformation [ViewAs (\(?Just {StoredUserAccount|title}) -> "Successfully updated " +++ fromMaybe "Untitled" title)] storedAccount
					>!| return newAccount
					))
				])
		?None
			=	(throw "Could not find user details")

changePasswordFlow :: !UserId -> Task StoredUserAccount
changePasswordFlow userId = changePasswordFlowWith userId (userAccountIn userAccounts)

changePasswordFlowWith :: !UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task StoredUserAccount | RWShared sds
changePasswordFlowWith userId accountStore =
	get (sdsFocus userId accountStore) >>~ \mbAccount ->
	case mbAccount of
		?Just account =
			Title ("Change Password for " +++ fromMaybe "Untitled" account.StoredUserAccount.title) @>>
			Hint "Please enter a new password" @>>
			 enterInformation []
			>>* [ OnAction ActionCancel (always   $ return account)
			    , OnAction ActionOk     (hasValue $ updatePassword account)
			    ]
		?None = throw "Could not find user details"
where
	updatePassword account password =
		createStoredCredentials account.StoredUserAccount.credentials.StoredCredentials.username password >>- \creds ->
		let account` = {StoredUserAccount| account & credentials = creds} in
		set (?Just account`) (sdsFocus userId accountStore) >>- \account`` ->
		Hint "Password updated" @>> viewInformation
		                [ ViewAs \(?Just {StoredUserAccount|title}) ->
		                         "Successfully changed password for " +++ fromMaybe "Untitled" title
		                ] account`` >!|
		return account`

deleteUserFlow :: UserId -> Task StoredUserAccount
deleteUserFlow userId
	=	get (sdsFocus userId (userAccountIn userAccounts))
	>>- \mbAccount -> case mbAccount of
		?Just account
			=	Title "Delete user" @>> viewInformation [] ("Are you sure you want to delete " +++ accountTitle account +++ "? This cannot be undone.")
			>>*	[ OnAction ActionNo	(always (return account))
				, OnAction ActionYes (always (deleteUser userId
									>-|	Hint "User deleted" @>> viewInformation [ViewAs (\account -> "Successfully deleted " +++ accountTitle account +++ ".")] account
									>!| return account
									))
				]
				
changeOwnPasswordFlow :: !UserId -> Task ()
changeOwnPasswordFlow userId = changeOwnPasswordFlowWith userId (userAccountIn userAccounts)

changeOwnPasswordFlowWith :: !UserId (sds UserId (?StoredUserAccount) (?StoredUserAccount)) -> Task () | RWShared sds
changeOwnPasswordFlowWith userId accountStore =
		allTasks [Label label @>> enterInformation [] \\ label <- ["Current password","New password","New password (again)"]]
			>>* [ OnAction ActionCancel (always $ return ())
			    , OnAction ActionOk     (ifValue passwordOk $ updatePassword)
			    ]
where
	passwordOk [Password cur,Password new1,Password new2] = new1 == new2
	passwordOk _ = False

	updatePassword [cur,password,_]
		= createStoredCredentials (Username userId) password
		>>- \creds ->
		upd (fmap (\a -> if (isValidPassword cur a.StoredUserAccount.credentials) {StoredUserAccount|a & credentials = creds, token = ?None} a))
			(sdsFocus userId accountStore)
		>>- \mbAccount -> if (maybe False (\a -> isValidPassword password a.StoredUserAccount.credentials) mbAccount)
			(Hint "Password updated" @>> viewInformation [] "Successfully updated password." )
			(Hint "Password not updated" @>> viewInformation [] "The password was not updated." )
		>!| return ()

importUserFileFlow :: Task ()
importUserFileFlow = Hint "Not implemented" @>> viewInformation [] ()

exportUserFileFlow :: Task Document
exportUserFileFlow
	=	get userAccounts -&&- get applicationName
	>>- \(list,app) ->
		createCSVFile (app +++ "-users.csv") (map toRow list)
	>>-	\file -> 
		Title "Export users file" @>>
		Hint "A CSV file containing the users of this application has been created for you to download." @>>
			 viewInformation [] file
where
	toRow {StoredUserAccount| credentials = {StoredCredentials|username = (Username username), saltedPasswordHash, salt}, title, roles}
		= [fromMaybe "" title,username,saltedPasswordHash,salt:roles]
	
importDemoUsersFlow :: Task [UserAccount]
importDemoUsersFlow =
	allTasks [catchAll (createUser u @ const u) (\_ -> return u) \\ u <- demoUser <$> names]
where
	demoUser name
		= {UserAccount
		  | credentials = {Credentials| username = Username (toLowerCase name), password = Password (toLowerCase name)}
		  , title = ?Just name
		  , roles = ["manager"]
		  }
	names = ["Alice","Bob","Carol","Dave","Eve","Fred"]

minimalMultiUserTask :: ((Task (?User)) -> Task (?User))
	(User -> Task ()) (User -> Task ()) (User -> [(String,Task ())])
	(sds UserId (?StoredUserAccount) (?StoredUserAccount))
	(sds String (?StoredUserAccount) ()) -> Task () | RWShared sds
minimalMultiUserTask loginModifier headerTask sessionTask userTasks accountAccess tokenAccess = forever
	(anyTask
		[loginModifier ((enterInformation [] -&&- (Label "Remember login" @>> enterInformation []))
		 >>* [OnAction (Action "Login") (hasValue authenticate)])
		,authenticateUsingTokenWith tokenAccess
		] <<@ ApplyLayout layout
	>>- main headerTask sessionTask
	>-| clearAuthenticationToken
	)
where
	authenticate ({Credentials|username,password},persistent) = authenticateUserWith username password persistent accountAccess

	main h t (?Just user) = workAs user ((manageSession user h userTasks  -|| (t user)) <<@ ArrangeWithHeader 0)
	main h t ?None = (Title "Login failed" @>> viewInformation [] "Your username or password is incorrect" >!| return ()) <<@ ApplyLayout frameCompact
	layout = sequenceLayouts [layoutSubUIs (SelectByType UIAction) (setActionIcon ('DM'.fromList [("Login","login")])), frameCompact]

manageSession user headerTask userTasks
    = ((headerTask user >^* [OnAction (Action a) (always (t <<@ InWindow)) \\ (a,t) <- userTasks user])
		 >>* [OnAction (Action "Sign out") (always (return ()))] )
     <<@ ApplyLayout (sequenceLayouts
        [removeCSSClass "step-actions"
        ,addCSSClass "main-header"
		,moveSubUIs (SelectOR (SelectByType UIAction) (SelectByType UIWindow)) [] 1
		,layoutSubUIs (SelectByPath [0]) unwrapUI
        ,layoutSubUIs (SelectByType UIAction) (setActionIcon ('DM'.fromList [("Sign out","logout")]))
        ] )

createStoredCredentials :: !Username !Password -> Task StoredCredentials
createStoredCredentials username password =
	get (sdsFocus 32 randomString) @ \salt ->
	{ username           = username
	, saltedPasswordHash = sha1 $ toString password +++ salt
	, salt               = salt
	}
