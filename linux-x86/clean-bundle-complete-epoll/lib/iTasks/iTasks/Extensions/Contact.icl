implementation module iTasks.Extensions.Contact
import iTasks
import iTasks.Extensions.EmailAddress
import Text.HTML

derive class iTask PhoneNumber

derive gDefault PhoneNumber, EmailAddress

instance toString PhoneNumber
where
	toString (PhoneNumber num) = num

instance html PhoneNumber
where
	html (PhoneNumber num) = Text num
