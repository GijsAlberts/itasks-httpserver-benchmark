definition module iTasks.WF.Tasks.IO
/**
* This modules provides tasks that support interaction with other systems.
* Either by running external programs, creating network clients and servers, or exchanging files
*/
from TCPIP import :: Timeout
import iTasks.WF.Definition
import iTasks.SDS.Definition
import System.OS
from iTasks.Internal.IWorld import :: ConnectionId
from System.FilePath import :: FilePath
from System.Process import :: ProcessPtyOptions
from Data.Error import :: MaybeError, :: MaybeErrorString
from iTasks.Engine import class Startable

:: ExitCode = ExitCode !Int

:: ConnectionHandlers l r w =
    { onConnect         :: !(ConnectionId String   r -> (MaybeErrorString l, ?w, [String], Bool))
    , onData            :: !(             String l r -> (MaybeErrorString l, ?w, [String], Bool))
    , onShareChange     :: !(                    l r -> (MaybeErrorString l, ?w, [String], Bool))
    , onDisconnect      :: !(                    l r -> (MaybeErrorString l, ?w                ))
    , onDestroy         :: !(                    l   -> (MaybeErrorString l,     [String]      ))
	}

:: ExternalProcessHandlers l r w =
    { onStartup     :: !(         r -> (?w, [String], Bool))
    , onOutData     :: !(String   r -> (?w, [String], Bool))
    , onErrData     :: !(String   r -> (?w, [String], Bool))
    , onShareChange :: !(         r -> (?w, [String], Bool))
    , onExit        :: !(ExitCode r -> (?w                ))
    }

:: ExternalProcessHandlersIWorld l r w =
	{ onStartup     :: !(         r *IWorld -> *(?w, [String], Bool, *IWorld))
	, onOutData     :: !(String   r *IWorld -> *(?w, [String], Bool, *IWorld))
	, onErrData     :: !(String   r *IWorld -> *(?w, [String], Bool, *IWorld))
	, onShareChange :: !(         r *IWorld -> *(?w, [String], Bool, *IWorld))
	, onExit        :: !(ExitCode r *IWorld -> *(?w, *IWorld))

	}

/**
 * On windows, a graceful exit has exitcode 0.
 * On posix, the application can exit gracefully with signal 15 (SIGTERM)
 */
externalProcessGraceful :== IF_POSIX 15 0

/** 
 * Execute an external process. 
 * 
 * @param Path to executable
 * @param Command line arguments
 * @param Used to set relative path.
 * @param Exit code (windows) or signal (posix) to send when destroying
 * @param Pseudotty settings (?None = Use pipes, ?Just = use pty)
 * @param Stdin queue.
 * @param SDS which is read/written to by the ExternalProcessHandlers
 * @param  Callback record which is used to react to events related to the external process occurring.
 * @result Task returning the exit code on termination
 */
externalProcessHandlers :: !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions) 
	!(Shared sdsin [String]) !(sdshandlers () r w) !(ExternalProcessHandlers l r w) -> Task Int
	| RWShared sdsin & RWShared sdshandlers & iTask l & iTask r & iTask w

/**
 * Execute an external process. Data placed in the stdin sds is sent to the process, data received is placed in the (stdout, stderr) sds.
 * New implementation which makes use of an I/O multiplexing mechanism on Windows.
 *
 * @param Poll rate
 * @param Path to executable
 * @param Command line arguments
 * @param Startup directory
 * @param Exit code (windows) or signal (posix) to send when destroying
 * @param Pseudotty settings (?None = Use pipes, ?Just = use pty)
 * @param Stdin queue
 * @param (stdout, stderr) queue
 * @result Task returning the exit code on termination
 */
externalProcess` :: !Timespec !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions) 
	!(Shared sds1 [String]) !(Shared sds2 ([String], [String])) -> Task Int 
	| RWShared sds1 & RWShared sds2

/**
 * Execute an external process. Data placed in the stdin sds is sent to the process, data received is placed in the (stdout, stderr) sds.
 * N.B. The provided shares must be synchronous
 *
 * @param Poll rate
 * @param Path to executable
 * @param Command line arguments
 * @param Startup directory
 * @param Exit code (windows) or signal (posix) to send when destroying
 * @param Pseudotty settings (?None = Use pipes, ?Just = use pty)
 * @param Stdin queue
 * @param (stdout, stderr) queue
 * @result Task returning the exit code on termination
 */
externalProcess :: !Timespec !FilePath ![String] !(?FilePath) !Int !(?ProcessPtyOptions) 
	!(Shared sds1 [String]) !(Shared sds2 ([String], [String])) -> Task Int | RWShared sds1 & RWShared sds2

/**
 * Connect to an external system using TCP. This task's value becomes stable when the connection is closed
 * N.B. The provided share must be synchronous
 *
 * @param Hostname
 * @param Port
 * @param The timeout (in ms) for opening the connection
 * @param A reference to shared data the task has access to
 * @param The event handler functions
 */
tcpconnect :: !String !Int !(?Timeout) !(sds () r w) (ConnectionHandlers l r w) -> Task l | iTask l & iTask r & iTask w & RWShared sds
tcpconnect` :: !String !Int !(sds () r w) (ConnectionHandlers l r w) -> Task l | iTask l & iTask r & iTask w & RWShared sds
/**
 *
 * Listen for connections from external systems using TCP.
 * N.B. The provided share must be synchronous
 *
 * @param Port
 * @param Remove closed connections. If this is true, closed connections are removed from the task value, if not they are kept in the list
 * @param A reference to shared data the task has access to
 * @param Initialization function: function that is called when a new connection is established
 * @param Communication function: function that is called when data arrives, the connection is closed or the observed share changes.
*/
tcplisten :: !Int !Bool !(sds () r w) (ConnectionHandlers l r w) -> Task [l] |iTask l & iTask r & iTask w & RWShared sds
tcplisten` :: !Int !Bool !(sds () r w) (ConnectionHandlers l r w) -> Task [l] | iTask l & iTask r & iTask w & RWShared sds
httpServerTask :: a !Int -> Task () | Startable a
