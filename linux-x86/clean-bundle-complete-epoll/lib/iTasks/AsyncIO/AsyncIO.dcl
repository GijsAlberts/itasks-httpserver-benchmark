definition module AsyncIO

from iTasks.SDS.Definition import :: SDSLens, :: TaskId, :: IWorld, :: Map, class Writeable, class Identifiable
from iTasks.Internal.Task import :: ConnectionTask, :: ConnectionTask`
from iTasks.WF.Tasks.IO import :: ExternalProcessHandlersIWorld
from iTasks.WF.Definition import :: TaskException
from System._Process import :: ProcessHandle(..)
from Data.Error import :: MaybeError (..)
from System.OSError import :: MaybeOSError (..), :: OSError (..), :: OSErrorCode, :: OSErrorMessage
from StdClass import class <

:: ErrorCode     :== Int
/**
 * @representation File descriptor returned through initializing I/O multiplexing mechanism.
 */
:: AsyncIOFD     :== Int
:: TimeoutMS     :== Int
/**
 * @representation File descriptor
 */
:: FD            :== Int
:: Port_         :== Int
:: IPAddr        :== Int

/**
 * Fd used to read from pipe
 */
:: ReadHandleFD  :== Int
/**
 * Fd used to write to pipe.
 */
:: WriteHandleFD :== Int

/**
 * Data type used to represent events returned by the I/O multiplexer through ioGetEventsC (C function).
 */
:: IOEvent = InConnectionEvent | OutConnectionEvent | ReadEventSock | ReadEventPipe | SendEventSock | SendEventPipe
           | SendEventNop | ReadAndSendEventSock | ReadAndSendEventPipe | DisconnectEventSock | DisconnectEventPipe

:: FDType = Socket | Pipe

// Used to seperate stderr and stdout in case of using pipes.
:: PipeContext = StdinPipe FD | StdOutPipe FD | StdErrPipe FD | Pty FD

/**
 * Pair of a file descriptor and the IOEvent that occurred on the descriptor. 
 */
:: FDEvent =
    { fd :: !FD
    , ev :: !IOEvent
    }

instance < ProcessHandle

/*
 * Data record for monitored listener socket.
 * mbLastClient is used on windows to store the client fd returned by the preceding windowsAcceptC/_acceptC call to be able to retrieve the peeraddr once the connection was established.
 */
:: ListenerTask =
    { lTaskId       :: !TaskId
    , mbLastClient  :: !?FD
    , lCt           :: !ConnectionTask`
    , removeOnClose :: !Bool
    }

/*
 * Data record for monitored client socket.
 */
:: ClientTask =
    { cTaskId   :: !TaskId
    , cCt       :: !ConnectionTask`
    , mbIpAddr  :: !?String
    , connected :: !Bool
    , closed    :: !Bool
    , removeOnClose :: !Bool
    }

/** 
 *  Data record for monitored pipe.
 */
:: PipeTask =
    { sdsin         :: !?(SDSLens () [String] [String])
    , sdsout        :: !?(SDSLens () ([String], [String]) ([String], [String]))
    , handlers      :: !?(ExternalProcessHandlersIWorld Dynamic Dynamic Dynamic)
    , sdshandlers   :: !?(SDSLens () Dynamic Dynamic)
    , fdStdIn       :: !FD // Used to send output from callbacks to stdin pipe
    , fdStdErr      :: !FD // Used to check if a pty/pipe is used.
    , fdStdOut      :: !FD 
    , isLegacy      :: !Bool // Used to distinguish externalProcess and externalProcess` from externalProcessHandlers.
    // Used to not evaluate further callbacks after close. This is used to emulate the send behavior of the select implementation in a non-blocking manner.
    , closed        :: !Bool
    // Handle of the process so the process can be terminated after sending data on close.
    , processHandle :: !ProcessHandle
    }

/**
 * Initialize the I/O multiplexing mechanism.
 *
 * @param Strict unique state (e.g World)
 * @result File descriptor used to interact with the I/O multiplexing mechanism, -1 = not ok
 * @result Strict unique state
 */
ioInitC           :: !*env -> (!AsyncIOFD , !*env)

/**
 * Retrieves file descriptor events from the I/O multiplexing mechanism.
 * 
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param How many milliseconds the call will block if no events are returned, may be 0
 * @param Array of values which is altered by ioGetEvents as a side effect. 
 *        After returning, this array contains the fds on which events occurred
 * @param Array of values which is altered by ioGetEvents as a side effect. 
 *        After returning, this array contains the event that occurred on each corresponding fd
 * @param Strict unique state (e.g World)
 * @result Number of events, -1 = error
 * @result Strict unique state
 */
ioGetEventsC      :: !AsyncIOFD !TimeoutMS !{#Int} !{#Int} !*env -> (!Int, !*env)

/**
 * Perform an asynchronous connection accept operation on Windows, do nothing on macOS/Linux.
 * Used because IOCP is a proactive I/O multiplexing mechanism that uses asynchronous operations.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param Listener socket file descriptor
 * @param Strict unique state (e.g World)
 * @result Error code, 1 = ok (Windows), 0 = ok (linux, macOS), -1 = not ok
 * @result Client socket file descriptor
 * @result Strict unique state
 */
windowsAcceptC    :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !FD), !*env)

/**
 * Accept a connection request (linux/macOS) / perform an asynchronous accept operation (Windows).
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param Listener socket file descriptor
 * @param Strict unique state (e.g World)
 * @result Error code, 1 = ok (Windows), 0 = ok (linux, macOS), -1 = not ok, -2 = do nothing (connection request aborted).
 * @result Client socket file descriptor
 * @result Strict unique state
 */
acceptC_          :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !FD), !*env)

/**
 * Create a listener socket and listen for incoming connections.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param Port to listen on
 * @param Strict unique state (e.g World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Listener socket file descriptor
 * @result Strict unique state
 */
tcplistenC        :: !AsyncIOFD !Port_ !*env -> (!(!ErrorCode, !FD),!*env)

/**
 * Create a client socket and connect to a listening socket over TCP.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param IP address to connect to
 * @param Port to connect to
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Client socket file descriptor
 * @result Strict unique state
 */
connectC          :: !AsyncIOFD !IPAddr !Port_ !*env -> (!(!ErrorCode, !FD), !*env)

/**
 * Queue data to write to socket (linux/macos). Send data asynchronously (Windows).
 *
 * @param Socket to queue data for
 * @param Data to be queued
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
queueIOWriteSock  :: !FD !String !*env -> (!ErrorCode, !*env)

/**
 * Queue data to write to pipe. Send data asynchronously (Windows).
 *
 * @param Pipe to queue data for
 * @param Data to be queued
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
queueIOWritePipe  :: !FD !String !*env -> (!ErrorCode, !*env)

/**
 * Signals to monitor socket for writability so data sent through queueIOWritePipe can be written on Linux/macOS
 * Do nothing on Windows as data is written asynchronously by queueIOWrite.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param Socket to monitor for writability
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
signalSendSockC   :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)

/**
 * Signals to monitor pipe for writability so data sent through queueIOWritePipe can be written on Linux/macOS.
 * Do nothing on Windows as data is written asynchronously by queueIOWrite.
 *
 * @param Pipe/pty to monitor for writability
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
signalSendPipeC   :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)

/**
 * Returns ip address of connected peer.
 * Returns an int representing the IP Address which can be converted to a String representation by toString o toDottedDecimal.
 *
 * @param Client file descriptor
 * @param Listener file descriptor
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Ip address of connected peer
 * @result Strict unique state
 */
getpeernameC      :: !FD !FD !*env -> (!(!ErrorCode, !IPAddr), !*env)

/* Retrieves data from file descriptor.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param File descriptor to read data from (can be a pipe or socket)
 * @param Strict unique state (e.g !*World)
 * @result Error code: 1 = success, 0 = disconnect detected, -1 = not ok
 * @result Received data
 * @result Strict unique state
 */
retrieveDataC :: !AsyncIOFD !FD !*env -> (!(!ErrorCode, !String), !*env)

/**
 * Read data from socket asynchronously on Windows. This starts a "readloop" that keeps reading data from the socket while it remains connected.
 * Do nothing on Linux/macOS.
 *
 * @param Client file descriptor.
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
windowsReadSockC :: !FD !*env -> (!ErrorCode, !*env)

/**
 * Read data from pipe asynchronously on Windows. This starts a "readloop" that keeps reading data from the pipe while it remains connected.
 * Do nothing on Linux/macOS.
 *
 * @param Pipe file descriptor.
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
windowsReadPipeC  :: !FD !*env -> (!ErrorCode, !*env)

/**
 * Closes and frees allocated memory for a file descriptor.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param Strict unique state (e.g !*World)
 * @param Bool indicating if the fd is a socket (if False it is a pipe/pty)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
cleanupFdC :: !AsyncIOFD !FD !Bool !*env -> (!ErrorCode, !*env)

/**
 * Associates pipe/pty with the I/O multiplexing mechanism.
 *
 * @param AsyncIOFd (obtained through ioInitC/IWorld)
 * @param pipe/pty to monitor
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
ioMonitorPipeC    :: !AsyncIOFD !FD !*env -> (!ErrorCode, !*env)

/**
 * Increments amount of packets left to send for windows.
 * This is used to make sure all the packets are sent before the connection is closed when a callback returns close = true.
 *
 * @param File descriptor to increment packets left to send for
 * @param Amount of packets to increment with
 * @param Strict unique state
 * @result ErrorCode: 0 = ok, -1 = not ok
 * @result Strict unique state
 */
windowsSetPacketsToSendC :: !FD !Int !*env -> (!ErrorCode, !*env)

/**
 * Creates a windows named pipe (anonymous pipes may not be monitored with IOCP).
 *
 * @param Strict unique state (e.g !*World)
 * @result Error code: 0 = ok, -1 = not ok
 * @result Read handle of pipe
 * @result Write handle of pipe
 * @result Strict unique state
 */
windowsCreatePipe :: !*env -> (!MaybeOSError (!ReadHandleFD, !WriteHandleFD), !*env)

/**
 * Closes and frees allocated memory for socket, removes socket from the IWorld state.
 *
 * @param Socket to clean up
 * @param IWorld
 * @result IWorld
 */
cleanupSocket :: !FD *IWorld -> *IWorld

/**
 * Processes things that should happen every event loop iteration for clients and pipes.
 *
 * @param IWorld
 * @result Iworld
 */
processIdle :: !Bool *IWorld -> *IWorld

/**
 * Processes FDEvents for listeners, clients and pipes, these are events that are related to the I/O multiplexing mechanism.
 *
 * @param List of events that occured on file descriptors
 * @param IWorld
 * @result IWorld
 */
processIO :: ![FDEvent] *IWorld -> *IWorld

/**
 * Sends data to write to file descriptor.
 *
 * @param File descriptor to write to
 * @param Whether the file descriptor is a socket or pipe
 * @param Data to write
 * @param IWorld
 * @result IWorld
 */
sendData :: !FD !FDType ![String] *IWorld -> *IWorld

/**
 * Converts events returned by C interface (integers) to IOEvent constructors.
 *
 * @param Events returned through ioGetEventsC in integer representation
 * @result Corresponding list of IOEvent constructors
 */
toEvents  :: ![Int] -> [IOEvent]

/**
 * Check processes for termination so that a stable value may be returned once an external process terminates.
 *
 * @param IWorld
 * @result IWorld
 */
checkProcesses :: *IWorld -> *IWorld 

/**
 * Used to write a Just value to an SDS or do nothing when the value is None.
 *
 * @param The sds to write to
 * @param Maybe the value to write
 * @param IWorld
 * @result Possibly a Taskexception and IWorld
 */
writeShareIfNeeded :: !(sds () r w) !(?w) !*IWorld -> (!MaybeError TaskException (), !*IWorld) | TC r & TC w & Writeable sds

/**
 * Used for handling a callback returning close = True for a pipe/pty.
 *
 * @param The pipe/pty which is to be closed
 * @param Data to send before closing (this is done non-blocking)
 * @param IWorld
 * @result IWorld
 */
onClosePipe :: !PipeTask ![String] *IWorld -> *IWorld

/**
 * Used for handling a callback returning close = True for a socket.
 *
 * @param The socket which is to be closed
 * @param Data to send before closing (this is done non-blocking)
 * @param IWorld
 * @result IWorld
 */
onCloseSocket :: !FD ![String] *IWorld -> *IWorld

/**
 * Used to check if file descriptor has been closed this event loop iteration.
 *
 * @param File descriptor to check for being closed
 * @param sendMap
 *
 * @result True if closed, false if not closed
 */
isFDBeingClosed :: !FD !(Map FD ([String], Bool)) -> Bool

/**
 * Add Data to sendmap for a client or pipe/pty.
 *
 * @param Data to add
 * @param Value of the sendMap for the involved FD
 *
 * @result New value for the sendMap
 */
addToSendMap :: ![String] !(?([String], !Bool)) -> (?([String], Bool))
