definition module Clean.Types.Unify

/**
 * Functions to unify Clean types.
 *
 * @property-bootstrap
 *   import StdEnv
 *   import Clean.Types.Parse
 *   import Data.Either
 *   import Data.Func
 *   import Data.Functor
 *   import qualified Data.Map
 *   import Data.Maybe
 *   import Text
 *
 *   derive genShow ?, UnifyingAssignment, Type, TypeContext, TypeRestriction
 *   derive gPrint ?, UnifyingAssignment, Type, TypeContext, TypeRestriction
 *
 *   instance == UnifyingAssignment
 *   where
 *   	(==) (LeftToRight a) (LeftToRight b) = a == b
 *   	(==) (RightToLeft a) (RightToLeft b) = a == b
 *   	(==) _               _               = False
 *
 *   parse :: !String -> Type
 *   parse s = fromJust (parseType [c \\ c <-: s])
 *
 *   expect :: !String !String !(?[Either (String,String) (String,String)]) -> Property
 *   expect t1 t2 res = name
 *       (concat3 t1 " ≃ " t2)
 *       (unifier =.= (map toUnifyingAssignment <$> res))
 *   where
 *       (_,t1`) = prepare_unification True  (const False) 'Data.Map'.newMap (parse t1)
 *       (_,t2`) = prepare_unification False (const False) 'Data.Map'.newMap (parse t2)
 *       unifier = assignments <$> finish_unification [] <$> unify t1` t2`
 *
 *       assignments u = u.assignments
 *
 *       toUnifyingAssignment (Left (v,t))  = LeftToRight (v, parse t)
 *       toUnifyingAssignment (Right (v,t)) = RightToLeft (v, parse t)
 */

import Clean.Types
from Data.Map import :: Map

/**
 * Check whether a unification result indicates that the left type generalised
 * the right type.
 */
isGeneralisingUnifier :: ![TVAssignment] -> Bool

/**
 * Check whether a unification result indicates that the unified types are
 * isomorphic.
 */
isIsomorphicUnifier :: ![TVAssignment] -> Bool

/**
 * `True` iff the first type is more general or equal to the second type.
 */
(generalises) infix 4 :: !Type !Type -> Bool

/**
 * `True` iff the first type is more specific or equal to the second type.
 */
(specialises) infix 4 :: !Type !Type -> Bool

/**
 * `True` if two types are isomorphic to each other.
 */
(isomorphic_to) infix 4 :: !Type !Type -> Bool

/**
 * Prepare a type for unification. Unification always happens between a 'left'
 * and a 'right' type. Unification of two left or two right types may yield
 * unexpected results.
 *
 * @param True if this is the left type
 * @param A predicate indicating if a type is always unique, like, e.g., World
 * @param Known type definitions to use for resolving synonyms
 * @param The type to prepare
 * @result The type synonyms used and the type after preparation
 */
prepare_unification :: !Bool (String -> Bool) (Map String [TypeDef]) !Type -> ([TypeDef], Type)

/**
 * Finish unification, yielding a unifier.
 *
 * @param The used type synonyms
 * @param The variable assignments
 * @result The unifier
 */
finish_unification :: ![TypeDef] ![TVAssignment] -> Unifier

/**
 * Core of the unification. An implementation of Martelli and Montanari, 'An
 * Efficient Unification Algorithm'. ACM Transactions on Programming Languages
 * and Systems, Vol. 4, No. 2, April 1982, pp. 258-282.
 * It has been modified slightly to deal with constructor variables, universal
 * quantifiers and uniqueness.
 *
 * @param The left type
 * @param The right type
 * @result A list of type variable assignments, or `?None` if unification failed
 *
 * @property arrows are type constructors:
 *   expect "m T" "((->) T)"
 *     (?Just [Left ("m", "(->)")]) /\
 *   expect "(f (t -> u)) (f t) -> f u" "(a b -> c) (a -> b) a -> c"
 *     (?Just [Left ("f", "(->) a"), Left ("u", "c"), Left ("t", "b")]) /\
 *   expect "(a b -> c) (a -> b) a -> c" "(f (t -> u)) (f t) -> f u"
 *     (?Just [Right ("f", "(->) a"), Right ("u", "c"), Left ("b", "t")])
 * @property uniqueness propagation does not hinder unification:
 *   expect "(a, *b)" "(a, b)"
 *     (?Just [Right ("b", "*b"), Left ("a", "a")])
 */
unify :: !Type !Type -> ?[TVAssignment]
