#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <winsock2.h>

#pragma comment(lib,"ws2_32.lib")

int main(int argc, char *argv[])
{
	WSADATA wsa;

	int err = WSAStartup(MAKEWORD(2,2), &wsa);
	if (err)
	{
		return -1;
	}

	int client_count = atoi(argv[1]);
	
	for (int i = 0; i < client_count; i++) {	
		struct sockaddr_in server;
		SOCKET s = socket(AF_INET,SOCK_STREAM,0);
		if(s == INVALID_SOCKET)
		{
			return -1;
		}

		server.sin_addr.s_addr = inet_addr("127.0.0.1");
		server.sin_family = AF_INET;
		server.sin_port = htons(80);

		int err = connect(s, (struct sockaddr*)&server, sizeof(server));
		if (err)
		{
			return -1;
		}
		
		printf("Connected socket number: %d\n", i);
		fflush(stdout);
	}
	int i;
	scanf("%d", &i);
	return 0;
}

