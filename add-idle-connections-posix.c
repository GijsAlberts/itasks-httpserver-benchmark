#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
	int client_count = atoi(argv[1]);
	for (int i = 0; i < client_count; i++) {
		struct sockaddr_in srv_adr,client_adr;
		int client_fd = socket (PF_INET, SOCK_STREAM, 0);

		if (client_fd==-1) {
			return -1;
		}

		client_adr.sin_family = AF_INET;         
		client_adr.sin_addr.s_addr = INADDR_ANY; 
		client_adr.sin_port = 0;                 

		int err = bind (client_fd, (struct sockaddr*) &client_adr, sizeof(client_adr));

		if (err){
			close (client_fd);
			return -1;
		}

		srv_adr.sin_family = AF_INET;            
		char *host_addr = "127.0.0.1";
		srv_adr.sin_addr.s_addr = inet_addr(host_addr);
		srv_adr.sin_port = htons ((short int)8080);  
		err = connect (client_fd, (struct sockaddr*) &srv_adr, sizeof(srv_adr));
		printf("%d\n", i);
		fflush(stdout);
	}
	int i;
	scanf("%d", &i);
	return 0;
}
