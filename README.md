# iTasks-httpserver-benchmark
The linux directory contains the iTasks distributions which were used to benchmark the Linux platform. 
clean-bundle-complete-epoll is a version of the replacement implementation on Linux. 
clean-bundle-complete-select is a version of the existing implementation on Linux. 

Similarly, the macOS directory contains the iTasks distributions which were used to benchmark the macOS platform. 
clean-bundle-complete-kqueue is a version of the replacement implementation on macOS. 
clean-bundle-complete-select is a version of the existing implementation on macOS. 

The windows directory contains the iTasks distributions which were used to benchmark the Windows platform. 
clean-bundle-complete-iocp is a version of the replacement implementation on Windows. 
clean-bundle-complete-select is a version of the existing implementation on Windows. 

The add-idle-connections-posix.c program may be used to establish idle connections to the iTasks HTTP server on Linux and macOS. 
The add-idle-connections-windows.c program may be used to establish idle connections to the iTasks HTTP server on Windows. 

In case of benchmarking the WebSocket connection, the itasks-core.js file should be copied to examples/itasks/BasicAPIExamples-www/js after building the BasicAPIExamples project.
This should be done before starting the HTTP server. 

